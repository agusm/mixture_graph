#include"lux_footprint_assembly.h"
#include<omp.h>
#include<intrin.h>
#include"flags.h"

#ifdef BMI1
#define TZCNT(x) _tzcnt_u32(x)
#define LZCNT(x) __lzcnt(x)
#else
inline uint32_t TZCNT(uint32_t x) {
	__popcnt((n&(-n)) ^ (n ^ (n - 1)));
}
#endif

lux::fetch_function_t lux::default_fetch_function = [](lux::coordu32_t& p, void* pParam) {
	reinterpret_cast<std::vector<lux::coordu32_t>*>(pParam)->push_back(p);
};

size_t lux::footprint1d(const rangeu32_t& R, const lux::fetch_function_t& F, void* pParam) {
	if (R.vmin == R.vmax) {
		coordu32_t t = coordu32_t(R.vmin, 0, 0, 0);
		F(t, pParam);
		return 1;
	}
	uint32_t s = R.vmin;
	size_t N = 0;
	while (s <= R.vmax) {
		uint32_t log_k = TZCNT(s);
		uint32_t k = (s == 0 ? std::numeric_limits<uint32_t>::max() : uint32_t(1) << log_k);
		if (s + k <= R.vmax) {
			coordu32_t t = coordu32_t(s, 0, 0, log_k);
			F(t, pParam);
			N++;
			s += k;
		}
		else {
			log_k = 31-LZCNT(R.vmax+1 - s);
			k = uint32_t(1) << log_k;
			coordu32_t t = coordu32_t(s, 0, 0, log_k);
			F(t, pParam);
			N++;
			s += k;
		}
	}
	return N;
}

size_t lux::footprint2d(const rangeu32_t& X, const rangeu32_t& Y, const fetch_function_t& F, void* pParam) {
	std::vector<coordu32_t> fpx = footprint1d(X); 
	std::vector<coordu32_t> fpy = footprint1d(Y); 

	// unshift 1d footprints
	for (auto& elem : fpx) elem.i <<= elem.l;
	for (auto& elem : fpy) elem.i <<= elem.l;

	size_t N = 0;
	for (size_t y = 0; y < fpy.size(); y++) {
		for (size_t x = 0; x < fpx.size(); x++) {
			uint32_t log_k = std::min(fpy[y].l, fpx[x].l);
			uint32_t k = uint32_t(1) << log_k;
			uint32_t lim_x = fpx[x].i + (uint32_t(1) << fpx[x].l);
			uint32_t lim_y = fpy[y].i + (uint32_t(1) << fpy[y].l);
			coordu32_t c(fpx[x].i, fpy[y].i, 0, log_k);
			while (c.j < lim_y) {
				while (c.i < lim_x) {
					F(c, pParam);
					N++;
					c.i += k;
				}
				c.i = fpx[x].i;
				c.j += k;
			}
		}
	}
	return N;
}

size_t lux::footprint3d(const rangeu32_t& X, const rangeu32_t& Y, const rangeu32_t& Z, const fetch_function_t& F, void* pParam) {
	std::vector<coordu32_t> fpx = footprint1d(X);
	std::vector<coordu32_t> fpy = footprint1d(Y);
	std::vector<coordu32_t> fpz = footprint1d(Z);

	size_t N = 0;
	for (size_t z = 0; z < fpz.size(); z++) {
		for (size_t y = 0; y < fpy.size(); y++) {
			uint32_t minyz = std::min(fpy[y].l, fpz[z].l);
			for (size_t x = 0; x < fpx.size(); x++) {
				uint32_t log_k = std::min(minyz, fpx[x].l);
				uint32_t k = uint32_t(1) << log_k;
				uint32_t lim_x = fpx[x].i + (uint32_t(1) << fpx[x].l);
				uint32_t lim_y = fpy[y].i + (uint32_t(1) << fpy[y].l);
				uint32_t lim_z = fpz[z].i + (uint32_t(1) << fpz[z].l);
				coordu32_t c(fpx[x].i, fpy[y].i, fpz[z].i, log_k);
				for (c.k = fpz[z].i; c.k < lim_z; c.k+=k) {
					for (c.j = fpy[y].i; c.j < lim_y; c.j+=k) {
						for (c.i = fpx[x].i; c.i < lim_x; c.i+=k) {
							F(c, pParam);
						}
					}
				}
				N += ((lim_z - fpz[z].i) >> log_k)*((lim_y - fpy[y].i) >> log_k)*((lim_x - fpx[x].i) >> log_k);
			} // endfor x
		} // endfor y
	} // endfor z
	return N;
}

size_t lux::par_footprint3d(const rangeu32_t& X, const rangeu32_t& Y, const rangeu32_t& Z, const fetch_function_t& F, void* pParam) {
	std::vector<coordu32_t> fpx = footprint1d(X);
	std::vector<coordu32_t> fpy = footprint1d(Y);
	std::vector<coordu32_t> fpz = footprint1d(Z);

	std::vector<coordu32_t> to_fetch;
	constexpr static const size_t workload = 32;
	to_fetch.reserve(2*workload* omp_get_max_threads());
	size_t N = 0;
	for (size_t z = 0; z < fpz.size(); z++) {
		for (size_t y = 0; y < fpy.size(); y++) {
			uint32_t minyz = std::min(fpy[y].l, fpz[z].l);
			for (size_t x = 0; x < fpx.size(); x++) {
				uint32_t log_k = std::min(minyz, fpx[x].l);
				uint32_t k = uint32_t(1) << log_k;
				uint32_t lim_x = fpx[x].i + (uint32_t(1) << fpx[x].l);
				uint32_t lim_y = fpy[y].i + (uint32_t(1) << fpy[y].l);
				uint32_t lim_z = fpz[z].i + (uint32_t(1) << fpz[z].l);
				coordu32_t c(fpx[x].i, fpy[y].i, fpz[z].i, log_k);
				for (c.k = fpz[z].i; c.k<lim_z; c.k+=k) {
					for (c.j = fpy[y].i; c.j < lim_y; c.j += k) {
						for (c.i = fpx[x].i; c.i < lim_x; c.i += k) {
							to_fetch.push_back(c);
						}
					}
				}
				if (to_fetch.size() > workload * omp_get_max_threads()) {
#pragma omp parallel for schedule(dynamic,workload>>1)
					for (int n = 0; n<int(to_fetch.size()); n++) {
						F(to_fetch[n], pParam);
					}
					N += to_fetch.size();
					to_fetch.clear();
				}
			} // endfor x
		} // endfor y
	} // endfor z
	if (!to_fetch.empty()) {
#pragma omp parallel for schedule(dynamic,2)
		for (int n = 0; n<int(to_fetch.size()); n++) {
			F(to_fetch[n], pParam);
		}
		N += to_fetch.size();
		to_fetch.clear();
	}
	return N;
}

std::vector<lux::coordu32_t> lux::footprint1d(const rangeu32_t& R) {
	std::vector<coordu32_t> result;
	footprint1d(R, default_fetch_function, &result);
	return result;
}

std::vector<lux::coordu32_t> lux::footprint2d(const rangeu32_t& X, const rangeu32_t& Y) {
	std::vector<coordu32_t> result;
	footprint2d(X, Y, default_fetch_function, &result);
	return result;
}

std::vector<lux::coordu32_t> lux::footprint3d(const rangeu32_t& X, const rangeu32_t& Y, const rangeu32_t& Z) {
	std::vector<coordu32_t> result;
	footprint3d(X, Y, Z, default_fetch_function, &result);
	return result;
}