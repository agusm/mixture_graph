#ifndef __LUX_FOOTPRINT_ASSEMBLY_H__
#define __LUX_FOOTPRINT_ASSEMBLY_H__

// (c) 2017 by Jens Schneider
// Visual Computing Center (VCC)
// King Abdullah University of Science and Technology (KAUST)
// 23955 Thuwal, Kingdom of Saudi Arabia
//
// Performs footprint gathering in 1d, 2d, 3d
//
// Given a set of ranges X, (Y, Z), and mipmapped data, footprint*d generates fetches (i,j,k,level) to cover the specified domain [X.vmin,X.vmax] x ( [Y.vmin,Y.vmax] x [Z.vmin,Z.vmax] )
// The return value yields the number of fetches made. For each fetch, a callback of type fetch_function_t is called, with optional parameters in pParam.
// Alternatively, fetch coordinates can be accumulated in a std::vector<coordu32_t>.

#define NOMINMAX
#include<vector>
#include<inttypes.h>
#include<functional>
#include<algorithm>
#include<immintrin.h>

namespace lux {
	template<class T> struct range_t;
	template<class T> struct coord_t;
	typedef range_t<uint32_t> rangeu32_t;
	typedef range_t<double> ranged_t;
	typedef range_t<float> rangef_t;

	typedef coord_t<uint32_t> coordu32_t;

	extern std::vector<coordu32_t> footprint1d(const rangeu32_t& R);
	extern std::vector<coordu32_t> footprint2d(const rangeu32_t& X, const rangeu32_t& Y);
	extern std::vector<coordu32_t> footprint3d(const rangeu32_t& X, const rangeu32_t& Y, const rangeu32_t& Z);

	typedef std::function<void(coordu32_t& p, void* pParam)> fetch_function_t;
	extern fetch_function_t default_fetch_function;

	extern size_t footprint1d(const rangeu32_t& R, const fetch_function_t& F, void* pParam = nullptr);
	extern size_t footprint2d(const rangeu32_t& X, const rangeu32_t& Y, const fetch_function_t& F, void* pParam = nullptr);
	extern size_t footprint3d(const rangeu32_t& X, const rangeu32_t& Y, const rangeu32_t& Z, const fetch_function_t& F, void* pParam = nullptr);

	extern size_t par_footprint3d(const rangeu32_t& X, const rangeu32_t& Y, const rangeu32_t& Z, const fetch_function_t& F, void* pParam = nullptr);
};

template<class T>
struct lux::range_t {
	range_t(const T& _vmin=T(0), const T& _vmax=T(0)) : vmin(_vmin), vmax(_vmax) {}
	T vmin, vmax;
	range_t(const range_t& other) : vmin(other.vmin), vmax(other.vmax) {}
	inline double map(size_t val, size_t limit) const {
		double v = double(val) / double(limit - 1);
		return double(vmin) + v*(double(vmax) - double(vmin));
	}
	inline bool empty(void) const { return vmin == vmax; }
};

template<class T>
struct lux::coord_t {
	T i, j, k, l;
	coord_t(const T& _i, const T& _j, const T& _k, const T& _l) : i(_i), j(_j), k(_k), l(_l) {}
	coord_t(void) : i(0), j(0), k(0), l(0) {}
};

#endif

