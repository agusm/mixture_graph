#ifndef __LUX_DISTANCE_VOLUME_H__
#define __LUX_DISTANCE_VOLUME_H__

#include"lux_math.h"
#include<limits>
#include<vector>
#include<cassert>

namespace lux {
	// computes a distance volume for line segments p,q. Each point stores x,y,z and one attribute in the 4th component
	template<class T>
	class distance_volume {
	public:
		typedef T base_type;
		distance_volume(void);
		distance_volume(const distance_volume& other);
		distance_volume(size_t dimx, size_t dimy, size_t dimz);
		bool empty(void);
		void clear(void);
		size_t dim(size_t n) const;
		distance_volume& operator=(const distance_volume& other);
		size_t size(void);
		void resize(size_t dimx, size_t dimy, size_t dimz);
		void init(void);
		void add_segment(const vec4<T>& p, const vec4<T>& q);
		void finish(void);

		inline T distance2(size_t x, size_t y, size_t z) const;
		inline T distance2(size_t at) const;
		inline T distance(size_t x, size_t y, size_t z) const;
		inline T distance(size_t at) const;
		
		T attribute(size_t x, size_t y, size_t z) const;
		T attribute(size_t at) const;
		void swap(distance_volume& other);
		inline size_t linear_address(size_t i, size_t j, size_t k) const;
	protected:
		std::vector<std::pair<vec4<T>, vec4<T> > >	m_segments;
		std::vector<std::pair<size_t, T> >			m_closest;
		static constexpr const size_t INVALID = std::numeric_limits<size_t>::max();
		size_t m_dims[3];
		inline T point_segment_dist2(const vec4<T>& p, size_t segment) const;
		inline T point_segment_param(const vec4<T>& p, size_t segment) const;
		inline void sweep_px(void);
		inline void sweep_nx(void);
		inline void sweep_py(void);
		inline void sweep_ny(void);
		inline void sweep_pz(void);
		inline void sweep_nz(void);
		inline void rasterize(void);
		inline void update(size_t candidates[9], size_t i, size_t j, size_t k);
	};

	typedef distance_volume<float> distance_volumef;
	typedef distance_volume<double> distance_volumed;
};

namespace std {
	template<class T>
	void swap(lux::distance_volume<T>& lhs, lux::distance_volume<T>& rhs) {
		lhs.swap(rhs);
	}
};

template<class T>
lux::distance_volume<T>::distance_volume(void) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
}

template<class T>
lux::distance_volume<T>::distance_volume(const distance_volume<T>& other) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	*this = other;
}

template<class T>
lux::distance_volume<T>::distance_volume(size_t dimx, size_t dimy, size_t dimz) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	resize(dimx, dimy, dimz);
}

template<class T>
bool lux::distance_volume<T>::empty(void) {
	return m_closest.empty();
}

template<class T>
void lux::distance_volume<T>::clear(void) {
	m_segments.clear();
	m_closest.clear();
	m_dims[0] = m_dims[1] = m_dims[2];
}

template<class T>
size_t lux::distance_volume<T>::dim(size_t n) const {
	assert("lux::distance_volume::dim -- invalid parameter" && n < 3);
	return m_dims[n];
}

template<class T>
lux::distance_volume<T>& lux::distance_volume<T>::operator=(const distance_volume& other) {
	for (size_t n = 0; n < 3; n++) m_dims[n] = other.m_dims[n];
	m_segments = other.m_segments;
	m_closest = other.m_closest;
}

template<class T>
size_t lux::distance_volume<T>::size(void) {
	return m_closest.size();
}

template<class T>
void lux::distance_volume<T>::resize(size_t dimx, size_t dimy, size_t dimz) {
	size_t nsize = dimx*dimy*dimz;
	if (nsize == 0) return clear();
	if (m_closest.size() != nsize) m_closest.resize(nsize);
	m_dims[0] = dimx;
	m_dims[1] = dimy;
	m_dims[2] = dimz;
}

template<class T>
void lux::distance_volume<T>::init(void) {
	printf("lux::distance_volume::init\n");
	for (auto& elem : m_closest) elem = std::make_pair(INVALID, std::numeric_limits<T>::infinity());
	m_segments.clear();
}

template<class T>
void lux::distance_volume<T>::add_segment(const vec4<T>& p, const vec4<T>& q) {
	m_segments.push_back(std::make_pair(p, q));
	rasterize();
}

template<class T>
void lux::distance_volume<T>::finish(void) {
	size_t k = 0;
	T avg_dist = T(0);
	for (const auto& elem : m_closest) if (elem.first != INVALID) {
		k++; avg_dist += elem.second;
	};
	avg_dist /= T(k);
	printf("%zi seeds, avg distance %f\n", k,avg_dist);
	printf("px\n");
	sweep_px();
	printf("nx\n");
	sweep_nx();
	printf("py\n");
	sweep_py();
	printf("ny\n");
	sweep_ny();
	printf("pz\n");
	sweep_pz();
	printf("nz\n");
	sweep_nz();
}


template<class T>
inline T lux::distance_volume<T>::distance2(size_t x, size_t y, size_t z) const {
	return m_closest[linear_address(x, y, z)].second;
}

template<class T>
inline T lux::distance_volume<T>::distance2(size_t at) const {
	assert("lux::distance_volume::distance2 -- invalid parameter" && at < m_distance2.size());
	return m_closest[at].second;
}

template<class T>
inline T lux::distance_volume<T>::distance(size_t x, size_t y, size_t z) const {
	return sqrt(distance2(x,y,z));
}

template<class T>
inline T lux::distance_volume<T>::distance(size_t at) const {
	return sqrt(distance2(n));
}

template<class T>
T lux::distance_volume<T>::attribute(size_t x, size_t y, size_t z) const {
	size_t seg = m_closest[linear_address(x, y, z)].first;
	T s = point_segment_param(vec4<T>(T(x), T(y), T(z), T(0)), seg);
	return m_segments[seg].first(3) + s*(m_segments[seg].second(3) - m_segments[seg].first(3));
}

template<class T>
inline T lux::distance_volume<T>::point_segment_dist2(const vec4<T>& p, size_t segment) const {
	// debugged, assumed correct and working
	if (segment == INVALID) return std::numeric_limits<T>::infinity();
	assert("lux::distance_volume::point_segment_dist2 -- invalid parameter" && segment < m_segments.size());
	const auto& S(m_segments[segment]);
	vec4<T> b = S.first - S.second; b(3) = T(0);
	T b2 = b.dot(b);
	if (b2 == T(0)) return vec4<T>(p - S.first).length2(3); // vanishing segment
	vec4<T> a = p - S.first; a(3) = T(0);
	T s = -a.dot(b) / b2;
	s = std::min(T(1), std::max(T(0), s));
	return (a + s*b).dot(a + s*b);
}

template<class T>
inline T lux::distance_volume<T>::point_segment_param(const vec4<T>& p, size_t segment) const {
	// debugged, assumed correct and working
	assert("lux::distance_volume::point_segment_param -- invalid parameter" && segment < m_segments.size());
	const auto& S(m_segments[segment]);
	vec4<T> b = S.first - S.second; b(3) = T(0);
	T b2 = b.dot(b);
	if (b2 == T(0)) return vec4<T>(p - S.first).length2(3); // vanishing segment
	vec4<T> a = p - S.first; a(3) = T(0);
	T s = -a.dot(b) / b2;
	return std::min(T(1), std::max(T(0), s));
}

template<class T>
inline void lux::distance_volume<T>::update(size_t candidates[9], size_t i, size_t j, size_t k) {
	vec4<T> P(T(i), T(j), T(k), T(0));
	auto& cp = m_closest[linear_address(i, j, k)];
	for (size_t n = 0; n < 9; n++) {
		auto& C = m_closest[candidates[n]].first; // [segment,distance2] of candidate
		if (C == INVALID) continue;
		T d2 = point_segment_dist2(P, C);
		if (d2 < cp.second) cp = std::make_pair(C, d2);
	}
}

template<class T>
inline void lux::distance_volume<T>::sweep_px(void) {
	for (int64_t i = 1; i < int64_t(m_dims[0]); i++) {
#pragma omp parallel for schedule(dynamic,4)
		for (int64_t k = 0; k < int64_t(m_dims[2]); k++) {
			int64_t kl = std::max(int64_t(0), k-1);
			int64_t kr = std::min(k+1, int64_t(m_dims[2]) - 1);
			for (int64_t j = 0; j < int64_t(m_dims[1]); j++) {
				int64_t jl = std::max(int64_t(0), j-1);
				int64_t jr = std::min(j+1, int64_t(m_dims[1]) - 1);
				size_t candidates[9] = {
					linear_address(i - 1,jl,kl),linear_address(i - 1,j,kl),linear_address(i - 1,jr,kl),
					linear_address(i - 1,jl,k),linear_address(i - 1,j,k),linear_address(i - 1,jr,k),
					linear_address(i - 1,jl,kr),linear_address(i - 1,j,kr),linear_address(i - 1,jr,kr),
				};
				update(candidates, i, j, k);
			}
		}
	}
}

template<class T>
inline void lux::distance_volume<T>::sweep_nx(void) {
	for (int64_t i = m_dims[0] - 2; i >= 0; i--) {
#pragma omp parallel for schedule(dynamic,4)
		for (int64_t k = 0; k < int64_t(m_dims[2]); k++) {
			int64_t kl = std::max(int64_t(0), k-1);
			int64_t kr = std::min(k+1, int64_t(m_dims[2]) - 1);
			for (int64_t j = 0; j < int64_t(m_dims[1]); j++) {
				int64_t jl = std::max(int64_t(0), j-1);
				int64_t jr = std::min(j+1, int64_t(m_dims[1]) - 1);
				size_t candidates[9] = {
					linear_address(i + 1,jl,kl),linear_address(i + 1,j,kl),linear_address(i + 1,jr,kl),
					linear_address(i + 1,jl,k),linear_address(i + 1,j,k),linear_address(i + 1,jr,k),
					linear_address(i +  1,jl,kr),linear_address(i + 1,j,kr),linear_address(i + 1,jr,kr),
				};
				update(candidates, i, j, k);
			}
		}
	}
}

template<class T>
inline void lux::distance_volume<T>::sweep_py(void) {
	for (int64_t j = 1; j < int64_t(m_dims[1]); j++) {
#pragma omp parallel for schedule(dynamic,4)
		for (int64_t k = 0; k < int64_t(m_dims[2]); k++) {
			int64_t kl = std::max(int64_t(0), k-1);
			int64_t kr = std::min(k+1, int64_t(m_dims[2]) - 1);
			for (int64_t i = 0; i < int64_t(m_dims[0]); i++) {
				int64_t il = std::max(int64_t(0), i-1);
				int64_t ir = std::min(i+1, int64_t(m_dims[0]) - 1);
				size_t candidates[9] = {
					linear_address(il, j - 1, kl), linear_address(i, j - 1, kl), linear_address(ir, j - 1, kl),
					linear_address(il, j - 1, k), linear_address(i, j - 1, k), linear_address(ir, j - 1, k),
					linear_address(il, j - 1, kr), linear_address(i, j - 1, kr), linear_address(ir, j - 1, kr),
				};
				update(candidates, i, j, k);
			}
		}
	}
}

template<class T>
inline void lux::distance_volume<T>::sweep_ny(void) {
	for (int64_t j = int64_t(m_dims[1])-2; j>=0; j--) {
#pragma omp parallel for schedule(dynamic,4)
		for (int64_t k = 0; k < int64_t(m_dims[2]); k++) {
			int64_t kl = std::max(int64_t(0), k-1);
			int64_t kr = std::min(k+1, int64_t(m_dims[2]) - 1);
			for (int64_t i = 0; i < int64_t(m_dims[0]); i++) {
				int64_t il = std::max(int64_t(0), i-1);
				int64_t ir = std::min(i+1, int64_t(m_dims[0]) - 1);
				size_t candidates[9] = {
					linear_address(il, j + 1, kl), linear_address(i, j + 1, kl), linear_address(ir, j +1, kl),
					linear_address(il, j + 1, k), linear_address(i, j + 1, k), linear_address(ir, j + 1, k),
					linear_address(il, j + 1, kr), linear_address(i, j + 1, kr), linear_address(ir, j + 1, kr),
				};
				update(candidates, i, j, k);
			}
		}
	}
}

template<class T>
inline void lux::distance_volume<T>::sweep_pz(void) {
	for (int64_t k = 1; k < int64_t(m_dims[2]); k++) {
#pragma omp parallel for schedule(dynamic,4)
		for (int64_t j = 0; j < int64_t(m_dims[1]); j++) {
			int64_t jl = std::max(j-1, int64_t(0));
			int64_t jr = std::min(j+1, int64_t(m_dims[1]) - 1);
			for (int64_t i = 0; i < int64_t(m_dims[1]); i++) {
				int64_t il = std::max(i-1, int64_t(0));
				int64_t ir = std::min(i+1, int64_t(m_dims[0]) - 1);
				size_t candidates[9] = {
					linear_address(il, jl, k - 1), linear_address(i, jl, k - 1), linear_address(ir, jl, k - 1),
					linear_address(il, j, k - 1), linear_address(i, j, k - 1), linear_address(ir, j, k - 1),
					linear_address(il, jr, k - 1), linear_address(i, jr, k - 1), linear_address(ir, jr, k - 1),
				};
				update(candidates, i, j, k);
			}
		}
	}
}

template<class T>
inline void lux::distance_volume<T>::sweep_nz(void) {
	for (int64_t k = int64_t(m_dims[2])-2; k >=0; k--) {
#pragma omp parallel for schedule(dynamic,4)
		for (int64_t j = 0; j < int64_t(m_dims[1]); j++) {
			int64_t jl = std::max(j-1, int64_t(0));
			int64_t jr = std::min(j+1, int64_t(m_dims[1]) - 1);
			for (int64_t i = 0; i < int64_t(m_dims[1]); i++) {
				int64_t il = std::max(i-1, int64_t(0));
				int64_t ir = std::min(i+1, int64_t(m_dims[0]) - 1);
				size_t candidates[9] = {
					linear_address(il, jl, k + 1), linear_address(i, jl, k + 1), linear_address(ir, jl, k + 1),
					linear_address(il, j, k + 1), linear_address(i, j, k + 1), linear_address(ir, j, k + 1),
					linear_address(il, jr, k + 1), linear_address(i, jr, k + 1), linear_address(ir, jr, k + 1),
				};
				update(candidates, i, j, k);
			}
		}
	}
}

template<class T>
inline size_t lux::distance_volume<T>::linear_address(size_t i, size_t j, size_t k) const {
	assert("lux::distance_volume::linear_address -- invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	return i + m_dims[0] * (j + m_dims[1] * k);
}

template<class T>
void lux::distance_volume<T>::swap(distance_volume& other) {
	m_closest.swap(other.m_closest);
	m_segments.swap(other.m_segments);
	for (size_t n = 0; n < 3; n++) std::swap(m_dims[n], other.m_dims[n]);
}

template<class T>
inline void lux::distance_volume<T>::rasterize(void) {
	const auto& S(m_segments.back());
	vec4<T> dir = S.second - S.first;
	T len = std::max(std::abs(dir(0)), std::max(std::abs(dir(1)), std::abs(dir(2))));
	dir /= len;
	auto in_volume = [&](const vec4<T>& p) {
		for (size_t n = 0; n < 3; n++) {
			if (p(n) < T(0) || p(n) >= T(m_dims[n])) return false;
		}
		return true;
	};
	for (size_t n = 0; n < size_t(len + 1.0); n++) {
		T s = T(n) / T(len);
		vec4<T> p = S.first + s*vec4<T>(S.second - S.first);
		if (in_volume(p)) {
			size_t ip[3] = { size_t(p(0)),size_t(p(1)),size_t(p(2)) };
			for (size_t k = ip[2]; k <= std::min(ip[2] + 1,m_dims[2]-1); k++) {
				for (size_t j = ip[1]; j <= std::min(ip[1] + 1,m_dims[1]-1); j++) {
					for (size_t i = ip[0]; i <= std::min(ip[0] + 1,m_dims[0]-1); i++) {
						auto& S(m_closest[linear_address(i, j, k)]);
						T cur_d2 = point_segment_dist2(vec4<T>(T(i), T(j), T(k), T(0)), m_segments.size()-1);
						if (S.first == INVALID || cur_d2<S.second) S = std::make_pair(m_segments.size() - 1, cur_d2);
					}
				}
			}
		}
	}
}

#endif
