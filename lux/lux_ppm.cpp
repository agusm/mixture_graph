#include"lux_ppm.h"
#include<vector>
#include<string>
#include<iterator>
#include<sstream>

bool lux::ppm::write(const std::string& name, const void* rgb_buffer, size_t width, size_t height, const lux::ppm::TYPE& t, bool flip) {
	if (name.empty()) return false;
	if (rgb_buffer == nullptr) return false;
	if (width*height == 0) return false;
	FILE* stream = nullptr;
	const char* ch = name.c_str();
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream, rgb_buffer, width, height, t, flip);
	fclose(stream);
	return result;
}

bool lux::ppm::write(FILE* stream, const void* rgb_buffer, size_t width, size_t height, const lux::ppm::TYPE& t, bool flip) {
	switch (t) {
	case UINT8:
		fprintf(stream, "P6%c%zi %zi%c255%c", 0xA, width, height, 0xA, 0xA);
		if (flip) {
			const uint8_t* ptr = reinterpret_cast<const uint8_t*>(rgb_buffer);
			for (size_t j = 0; j < height; j++) {
				if (fwrite(&ptr[3 * width*(height - 1 - j)], 3 * sizeof(uint8_t), width, stream) != width) return false;
			}
		}
		else {
			if (fwrite(rgb_buffer, 3 * sizeof(uint8_t), width*height, stream) != width*height) return false;
		}
		return true;
	case UINT16:
		fprintf(stream, "P6%c%zi %zi%c65535%c", 0xA, width, height, 0xA, 0xA);
		if (flip) {
			const uint16_t* ptr = reinterpret_cast<const uint16_t*>(rgb_buffer);
			for (size_t j = 0; j < height; j++) {
				if (fwrite(&ptr[3 * width*(height - 1 - j)], 3 * sizeof(uint16_t), width, stream) != width) return false;
			}
		}
		else {
			if (fwrite(rgb_buffer, 3 * sizeof(uint16_t), width*height, stream) != width*height) return false;
		}
		return true;
	default:
		return false;
	}
}

void* lux::ppm::read(const std::string& name, size_t& width, size_t& height, lux::ppm::TYPE& t, bool flip) {
	width = height = 0;
	t = INVALID;
	if (name.empty()) return nullptr;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	void* result = read(stream, width, height, t, flip);
	fclose(stream);
	return result;
}

void* lux::ppm::read(FILE* stream, size_t& width, size_t& height, lux::ppm::TYPE& t, bool flip) {
	width = height = 0;
	t = INVALID;
	if (stream == nullptr) return false;
	// header line	
	auto read_header_line = [](FILE* stream) {
		std::string header;
		do {
			char ch = 0x00;
			header.clear();
			while (ch != 0xA && !feof(stream)) {
				if (fread(&ch, 1, 1, stream) != 1) return std::string();
				if (ch != 0xA) header.push_back(ch);
			}
		} while (header[0] == '#' && !feof(stream));
		return header;
	};
	if (read_header_line(stream) != std::string("P6")) return nullptr; // wrong format
	{
		std::istringstream str(read_header_line(stream));
		std::vector<std::string> tokens{ std::istream_iterator<std::string>(str), std::istream_iterator<std::string>() };
		if (tokens.size() != 2) return nullptr;
		width = size_t(_atoi64(tokens[0].c_str()));
		height = size_t(_atoi64(tokens[1].c_str()));
	}
	size_t range = 0;
	{
		std::istringstream str(read_header_line(stream));
		std::vector<std::string> tokens{ std::istream_iterator<std::string>(str), std::istream_iterator<std::string>() };
		if (tokens.size() != 1) return nullptr;
		range = size_t(_atoi64(tokens[0].c_str()));
	}
	if (width*height == 0) return nullptr;
	if (range > 255) {
		t = UINT16;
		uint16_t* result = new uint16_t[width*height * 3];
		if (fread(result, 3 * sizeof(uint16_t), width*height, stream) != width*height) {
			delete[] result;
			return nullptr;
		}
		if (flip) {
			size_t len = 3 * width;
			uint16_t* line = new uint16_t[len];
			for (size_t j = 0; j < height / 2; j++) {
				memcpy(line, &result[j*len], sizeof(uint16_t) * len);
				memcpy(&result[j*len], &result[(height - 1 - j)*len], sizeof(uint16_t)*len);
				memcpy(&result[(height - 1 - j)*len], line, sizeof(uint16_t)*len);
			}
			delete[] line;
		}
		return result;
	}
	else {
		t = UINT8;
		uint8_t* result = new uint8_t[width*height * 3];
		if (fread(result, 3 * sizeof(uint8_t), width*height, stream) != width*height) {
			delete[] result;
			return nullptr;
		}
		if (flip) {
			size_t len = 3 * width;
			uint8_t* line = new uint8_t[len];
			for (size_t j = 0; j < height / 2; j++) {
				memcpy(line, &result[j*len], sizeof(uint8_t) * len);
				memcpy(&result[j*len], &result[(height - 1 - j)*len], sizeof(uint8_t)*len);
				memcpy(&result[(height - 1 - j)*len], line, sizeof(uint8_t)*len);
			}
			delete[] line;
		}
		return result;
	}
}
