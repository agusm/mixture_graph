#ifndef __LUX_IMAGE_H__
#define __LUX_IMAGE_H__

#include<vector>
#include<string>
#include<functional>
#include<cassert>
#include"lux_ppm.h"
#include"lux_footprint_assembly.h" // range_t

namespace lux {

	template<class T> 
	class image {
	public:
		constexpr static const size_t max_components = 4;
		typedef T base_type;
		image(void);
		image(size_t dimx, size_t dimy, size_t components);
		image(const image& other);
		~image(void);
		void clear(void);
		size_t size(void) const;
		bool empty(void) const;
		void resize(size_t dimx, size_t dimy, size_t components);
		size_t dim(size_t n) const;
		size_t components(void) const;
		std::string& name(void);
		const std::string& name(void) const;
		inline size_t linear_address(size_t i, size_t j, size_t c) const { 
			assert("lux::image::linear_address -- invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && c < m_dims[2]);
			return m_dims[2] * (i + j*m_dims[0]) + c; 
		}
		typename std::vector<T>::reference operator()(size_t i, size_t j, size_t c);
		typename std::vector<T>::reference operator[](size_t n);
		typename std::vector<T>::const_reference operator()(size_t i, size_t j, size_t c) const;
		typename std::vector<T>::const_reference operator[](size_t n) const;
		typedef std::function<void(double*, const ranged_t& X, const ranged_t& Y, size_t)> fill_function;
		void fill(const ranged_t& X, const ranged_t& Y, const fill_function& F);
		void swap(image& other);
		image& operator=(const image& other);
		template<class S> image<S> as(void) const;
		constexpr static const size_t NONE = std::numeric_limits<size_t>::max();
		bool write_ppm(const std::string& name, size_t red_channel, size_t green_channel, size_t blue_channel, const lux::ppm::TYPE& t = lux::ppm::UINT8, bool flip = false) const;
		bool write_ppm(FILE* stream, size_t red_channel, size_t green_channel, size_t blue_channel, const lux::ppm::TYPE& t = lux::ppm::UINT8, bool flip = false) const;
		bool read_ppm(const std::string& name, bool flip = false);
		bool read_ppm(FILE* stream, bool flip = false);
		const T* data(void) const;
	protected:
		std::vector<T> m_data;
		std::string m_name;
		size_t m_dims[3];
	};
};

namespace std {
	template<class T>
	void swap(lux::image<T>& lhs, lux::image<T>& rhs) {
		lhs.swap(rhs);
	}
};

template<class T>
lux::image<T>::image(void) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
}

template<class T>
lux::image<T>::image(size_t dimx, size_t dimy, size_t components) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	resize(dimx, dimy, components);
}

template<class T>
lux::image<T>::image(const image& other) {
	*this = other;
}

template<class T>
lux::image<T>::~image(void) {
	clear();
}

template<class T>
void lux::image<T>::clear() {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	m_data.clear();
	m_name.clear();
}

template<class T>
size_t lux::image<T>::size(void) const {
	return m_data.size();
}

template<class T>
bool lux::image<T>::empty(void) const {
	return m_data.empty();
}

template<class T>
void lux::image<T>::resize(size_t dimx, size_t dimy, size_t components) {
	size_t nsize = dimx*dimy*components;
	if (nsize == 0) {
		m_data.clear();
		m_dims[0] = m_dims[1] = m_dims[2] = 0;
		return;
	}
	if (nsize != m_data.size()) m_data.resize(nsize);
	m_dims[0] = dimx;
	m_dims[1] = dimy;
	m_dims[2] = components;
}

template<class T>
size_t lux::image<T>::dim(size_t n) const {
	assert("lux::image::dim -- invalid parameter" && n < 2);
	return m_dims[n];
}

template<class T>
size_t lux::image<T>::components(void) const {
	return m_dims[2];
}

template<class T>
std::string& lux::image<T>::name(void) {
	return m_name;
}

template<class T>
const std::string& lux::image<T>::name(void) const {
	return m_name;
}

template<class T>
typename std::vector<T>::reference lux::image<T>::operator()(size_t i, size_t j, size_t c) {
	return m_data[linear_address(i, j, c)];
}
template<class T>
typename std::vector<T>::reference lux::image<T>::operator[](size_t n) {
	assert("lux::image[] -- invalid parameter" && n < m_data.size());
	return m_data[n];
}

template<class T>
typename std::vector<T>::const_reference lux::image<T>::operator()(size_t i, size_t j, size_t c) const {
	return m_data[linear_address(i, j, c)];
}

template<class T>
typename std::vector<T>::const_reference lux::image<T>::operator[](size_t n) const {
	assert("lux::image[] -- invalid parameter" && n < m_data.size());
	return m_data[n];
}

template<class T>
void lux::image<T>::fill(const ranged_t& X, const ranged_t& Y, const fill_function& F) {
	static double buf[max_components];
	T* ptr = m_data.data();
	for (size_t j = 0; j < m_dims[1]; j++) {
		double y = Y.map(j, m_dims[1]);
		for (size_t i=0; i<m_dims[0]; i++) {
			double x = X.map(i, m_dims[0]);
			for (size_t c = 0; c < m_dims[2]; c++) buf[c] = double(ptr[c]);
			F(buf, x, y, m_dims[2]);
			for (size_t c = 0; c < m_dims[2]; c++) ptr[c] = T(buf[c]);
			ptr += m_dims[2];
		}
	}
}

template<class T>
void lux::image<T>::swap(image& other) {
	m_data.swap(other.m_data);
	m_name.swap(other.m_data);
	for (int n = 0; n < 3; n++) std::swap(m_dims[n], other.m_dims[n]);
}

template<class T>
lux::image<T>& lux::image<T>::operator=(const image& other) {
	m_data = other.m_data;
	m_name = other.m_name;
	for (int n = 0; n < 3; n++) m_dims[n] = other.m_dims[n];
	return *this;
}

template<class T>
template<class S> lux::image<S> lux::image<T>::as(void) const {
	image<S> result;
	result.name = m_name;
	if (empty()) return result;
	result.resize(m_dims[0], m_dims[1], m_dims[2]);
	for (size_t n = 0; n < m_data.size(); n++) result[n] = S(m_data[n]);
	return result;
}

template<class T>
bool lux::image<T>::write_ppm(const std::string& name, size_t red_channel, size_t green_channel, size_t blue_channel, const lux::ppm::TYPE& t, bool flip) const {
	if (empty()) return false;
	if (name.empty()) return false;
	if (red_channel > m_dims[2] && red_channel != NONE) return false;
	if (green_channel > m_dims[2] && green_channel != NONE) return false;
	if (blue_channel > m_dims[2] && blue_channel != NONE) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write_ppm(stream, red_channel, green_channel, blue_channel, t, flip);
	fclose(stream);
	return result;
}

template<class T>
bool lux::image<T>::write_ppm(FILE* stream, size_t red_channel, size_t green_channel, size_t blue_channel, const lux::ppm::TYPE& t, bool flip) const {
	if (empty()) return false;
	if (stream==nullptr) return false;
	if (red_channel > m_dims[2] && red_channel != NONE) return false;
	if (green_channel > m_dims[2] && green_channel != NONE) return false;
	if (blue_channel > m_dims[2] && blue_channel != NONE) return false;
	size_t channel[3] = { red_channel, green_channel, blue_channel };
	switch (t) {
	case lux::ppm::UINT8:
	{
		typedef uint8_t data_type;
		data_type* buf = new data_type[3 * m_dims[0] * m_dims[1]];
		for (size_t n = 0; n < m_dims[0] * m_dims[1]; n++) {
			for (size_t c = 0; c < 3; c++) buf[3 * n + c] = data_type(channel[c] == NONE ? T(0) : std::min(T(std::numeric_limits<data_type>::max()), std::max(T(std::numeric_limits<data_type>::min()), m_data[m_dims[2] * n + channel[c]])));
		}
		bool result = lux::ppm::write(stream, buf, m_dims[0], m_dims[1], t, flip);
		delete[] buf;
		return result;
	}
	case lux::ppm::UINT16:
	{
		typedef uint16_t data_type;
		data_type* buf = new data_type[3 * m_dims[0] * m_dims[1]];
		for (size_t n = 0; n < m_dims[0] * m_dims[1]; n++) {
			for (size_t c = 0; c < 3; c++) buf[3 * n + c] = data_type(channel[c] == NONE ? T(0) : std::min(T(std::numeric_limits<data_type>::max()), std::max(T(std::numeric_limits<data_type>::min()), m_data[m_dims[2] * n + channel[c]])));
		}
		bool result = lux::ppm::write(stream, buf, m_dims[0], m_dims[1], t, flip);
		delete[] buf;
		return result;
	}
	default:
		return false;
	}
	return true;
}

template<class T>
bool lux::image<T>::read_ppm(const std::string& name, bool flip) {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read_ppm(stream, flip);
	fclose(stream);
	return result;
}

template<class T>
bool lux::image<T>::read_ppm(FILE* stream, bool flip) {
	if (stream == nullptr) return false;
	size_t width, height;
	lux::ppm::TYPE t;
	void* buf = lux::ppm::read(stream, width, height, t, flip);
	if (buf == nullptr) return false;
	resize(width, height, 3);
	switch (t) {
	case lux::ppm::UINT8:
	{
		typedef uint8_t data_type;
		const data_type* ptr = reinterpret_cast<const data_type*>(buf);
		for (size_t n = 0; n < m_data.size(); n++) m_data[n] = T(buf[n]);
		delete[] buf;
		return true;
	}
	case lux::ppm::UINT16:
	{
		typedef uint16_t data_type;
		const data_type* ptr = reinterpret_cast<const data_type*>(buf);
		for (size_t n = 0; n < m_data.size(); n++) m_data[n] = T(buf[n]);
		delete[] buf;
		return true;
	}
	default:
		delete[] buf;
		return false;
	}
}

template<class T>
const T* lux::image<T>::data(void) const {
	return m_data.data();
}

#endif
