#include"lux_glerror.h"

std::string lux::errstr(GLenum val) {
	switch (val) {
	case GL_FRAMEBUFFER_COMPLETE:
	case GL_NO_ERROR:									return std::string("No Error");
	case GL_INVALID_ENUM:								return std::string("Invalid Enumerator");
	case GL_INVALID_VALUE:								return std::string("Invalid Value");
	case GL_INVALID_OPERATION:							return std::string("Invalid Operation");
	case GL_INVALID_FRAMEBUFFER_OPERATION:				return std::string("Invalid Framebuffer Operation");
	case GL_OUT_OF_MEMORY:								return std::string("Out of Memory");
	case GL_STACK_UNDERFLOW:							return std::string("Stack Underflow");
	case GL_STACK_OVERFLOW:								return std::string("Stack Overflow");
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:			return std::string("Incomplete Framebuffer Attachment");
	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:		return std::string("Incomplete Framebuffer Dimensions");
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:	return std::string("Missing Framebuffer Attachment");
	case GL_FRAMEBUFFER_UNSUPPORTED:					return std::string("Framebuffer Unsupported");
	default:											return std::string("Unknown Error");
	}
}