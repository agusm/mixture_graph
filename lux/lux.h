#ifndef __LUX_H__
#define __LUX_H__

#define NOMINMAX
#include"GL/glew.h"
#include"GL/wglew.h"
#include"GL/freeglut.h"
#include"lux_math.h"
#include"lux_arcball.h"
#include"lux_camera.h"
#include"lux_shader.h"
#include"lux_glgeometry.h"
#include"lux_volume.h"
#include"lux_image.h"
#include"lux_sampler.h"
#include"lux_glerror.h"
#include"lux_auxiliary.h"
#include"lux_color.h"
#include"lux_vectorfield3.h"
#include"lux_trajectory.h"
#include"lux_catmull_rom.h"
#include"lux_interpolation.h"		// deprecated
#include"lux_distance_volume.h"		// deprecated
#include"timing.h"
#include"lux_coordinateframe.h"		// deprecated
#include"lux_integrators.h"
#include"lux_HermiteCurveS3.h"
#include"lux_weno.h"
#include"lux_footprint_assembly.h"

#pragma comment(lib,"freeglut.lib")
#pragma comment(lib,"glew32.lib")

#endif
