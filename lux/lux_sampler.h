#ifndef __LUX_SAMPLER_H__
#define __LUX_SAMPLER_H__

#include"lux_volume.h"
#include"lux_vectorfield3.h"
#include"lux_math.h"
#include<functional>
#include<cfenv>
#include<inttypes.h>
#include"lux_color.h"

// ALL SAMPLERS USE VOXEL COORDINATES

namespace lux {
	enum INTERPOLATOR {
		SAMPLER_NEAREST,
		SAMPLER_LINEAR,
		SAMPLER_CUBIC
	};
	enum  BOUNDARY {
		SAMPLER_ZERO,	// PAD WITH ZEROS
		SAMPLER_CLAMP,
		SAMPLER_REPEAT // TODO: SAMPLER_MIRROR
	};

	template<class T> inline T clamp(const T& v, const T& vmin, const T& vmax);
	template<class T> inline T cubic(const T& A, const T& B, const T& C, const T& D, const T& w);
	template<class T> inline lux::vec4<T> cubic(const lux::vec4<T>& A, const lux::vec4<T>& B, const lux::vec4<T>& C, const lux::vec4<T>& D, const T& w, size_t dim = 4);
	inline lux::color cubic(const lux::color& A, const lux::color& B, const lux::color& C, const lux::color& D, const float& w);

	// coordinate mappers
	inline int64_t map_coordinate(const int64_t& i, const int64_t& dim, const lux::BOUNDARY& b);

	// fetchers for volumes
	template<class T> inline const T* ptrfetch(const lux::volume<T>& vol, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b);
	template<class T> inline lux::vec4<T> vfetch(const lux::volume<T>& vol, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b);
	template<class T> inline T fetch(const lux::volume<T>& vol, int64_t i, int64_t j, int64_t k, size_t c, const lux::BOUNDARY& b);

	// fetchers for images
	template<class T> inline const T* ptrfetch(const lux::image<T>& vol, int64_t i, int64_t j, const lux::BOUNDARY& b);
	template<class T> inline lux::vec4<T> vfetch(const lux::image<T>& vol, int64_t i, int64_t j, const lux::BOUNDARY& b);
	template<class T> inline T fetch(const lux::image<T>& vol, int64_t i, int64_t j, size_t c, const lux::BOUNDARY& b);

	// fetchers for colormaps
	inline const float* ptrfetch(const lux::colormap& M, int64_t i, const lux::BOUNDARY& b);
	inline const lux::color& vfetch(const lux::colormap& M, int64_t i, const lux::BOUNDARY& b);
	inline float fetch(const lux::colormap& M, int64_t i, size_t c, const lux::BOUNDARY& b);

	// fetchers for vectorfields
	template<class T> inline const T* ptrfetch(const lux::vectorfield3<T>& V, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b);
	template<class T> inline lux::vec4<T> vfetch(const lux::vectorfield3<T>& V, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b);
	template<class T> inline T fetch(const lux::vectorfield3<T>& vol, int64_t i, int64_t j, int64_t k, size_t c, const lux::BOUNDARY& b);

	// sampler for volumes
	template<class T> inline T sample(const lux::volume<T>& vol, double x, double y, double z, size_t channel = 0, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);
	template<class T> inline lux::vec4<T> vsample(const lux::volume<T>& vol, double x, double y, double z, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);

	// sampler for images
	template<class T> inline T sample(const lux::image<T>& vol, double x, double y, size_t channel = 0, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);
	template<class T> inline lux::vec4<T> vsample(const lux::image<T>& vol, double x, double y, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);

	// sampler for colormaps
	inline float sample(const lux::colormap& M, double x, size_t channel = 0, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);
	inline lux::color vsample(const lux::colormap& M, double x, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);
	
	// sampler for vectorfields
	template<class T> inline T sample(const vectorfield3<T>& V, double x, double y, double z, size_t channel = 0, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);
	template<class T> inline vec4<T> vsample(const vectorfield3<T>& V, double x, double y, double z, const lux::INTERPOLATOR& s = lux::SAMPLER_LINEAR, const lux::BOUNDARY& b = lux::SAMPLER_CLAMP);
	
	template<class T> constexpr const T __zero[4] = { T(0),T(0),T(0),T(0) };
};

template<class T>
inline T lux::clamp(const T& v, const T& vmin, const T& vmax) {
	return std::min(vmax, std::max(vmin, v));
}

template<class T>
inline T lux::cubic(const T& A, const T& B, const T& C, const T& D, const T& w) {
	double c[4] = {
		0.5*(D - A) + 1.5*(B - C),
		A - 2.5*B + 2.0*C - 0.5*D,
		0.5*(C - A),
		B
	};
	return T(c[3] + w*(c[2] + w*(c[1] + w*c[0])));
}

template<class T>
inline lux::vec4<T> lux::cubic(const lux::vec4<T>& A, const lux::vec4<T>& B, const lux::vec4<T>& C, const lux::vec4<T>& D, const T& w, size_t dim) {
	dim = std::min(dim, size_t(4));
	lux::vec4<T> result;
	for (size_t n = 0; n < dim; n++) {
		double c[4] = {
			0.5*(D(n) - A(n)) + 1.5*(B(n) - C(n)),
			A(n) - 2.5*B(n) + 2.0*C(n) - 0.5*D(n),
			0.5*(C(n) - A(n)),
			B(n)
		};
		result(n) = T(c[3] + w*(c[2] + w*(c[1] + w*c[0])));
	}
	return result;
}

inline lux::color lux::cubic(const lux::color& A, const lux::color& B, const lux::color& C, const lux::color& D, const float& w) {
	lux::color result;
	for (size_t n = 0; n < 4; n++) {
		double c[4] = {
			0.5*(D(n) - A(n)) + 1.5*(B(n) - C(n)),
			A(n) - 2.5*B(n) + 2.0*C(n) - 0.5*D(n),
			0.5*(C(n) - A(n)),
			B(n)
		};
		result(n) = std::min(1.0f,std::max(0.0f,float(c[3] + w*(c[2] + w*(c[1] + w*c[0])))));
	}
	return result;
}

inline int64_t lux::map_coordinate(const int64_t& i, const int64_t& dim, const lux::BOUNDARY& b) {
	switch (b) {
	case SAMPLER_ZERO:		if (i < 0 || i >= dim) return -1; else return i;
	case SAMPLER_CLAMP:		return lux::clamp(i, int64_t(0), dim - 1);
	case SAMPLER_REPEAT:	return (i%dim + dim)%dim;
	default:				return -1;
	}
}

// fetcher for volumes
template<class T>
inline const T* lux::ptrfetch(const lux::volume<T>& vol, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b) {
	i = map_coordinate(i, int64_t(vol.dim(0)), b);	if (i < 0) return __zero;
	j = map_coordinate(j, int64_t(vol.dim(1)), b);	if (j < 0) return __zero;
	k = map_coordinate(k, int64_t(vol.dim(2)), b);	if (k < 0) return __zero;
	return &vol(i, j, k, 0);
}

template<class T>
inline lux::vec4<T> lux::vfetch(const lux::volume<T>& vol, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b) {
	lux::vec4<T> result;
	const T* ptr = ptrfetch(vol, i, j, k, b);
	for (size_t n = 0; n < vol.components(); n++) result(n) = ptr[n];
	return result;
}

template<class T>
inline T lux::fetch(const lux::volume<T>& vol, int64_t i, int64_t j, int64_t k, size_t c, const lux::BOUNDARY& b) {
	return ptrfetch(vol, i, j, k, b)[c];
}

template<class T> 
inline const T* lux::ptrfetch(const lux::vectorfield3<T>& V, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b) {
	i = map_coordinate(i, int64_t(V.dim(0)), b);	if (i < 0) return __zero;
	j = map_coordinate(j, int64_t(V.dim(1)), b);	if (j < 0) return __zero;
	k = map_coordinate(k, int64_t(V.dim(2)), b);	if (k < 0) return __zero;
	return &V(i, j, k, 0);
}

template<class T> 
inline lux::vec4<T> lux::vfetch(const lux::vectorfield3<T>& V, int64_t i, int64_t j, int64_t k, const lux::BOUNDARY& b) {
	lux::vec4<T> result;
	const T* ptr = ptrfetch(V, i, j, k, b);
	for (size_t n = 0; n < V.components(); n++) result(n) = ptr[n];
	return result;
}

template<class T> 
inline T lux::fetch(const lux::vectorfield3<T>& V, int64_t i, int64_t j, int64_t k, size_t c, const lux::BOUNDARY& b) {
	return ptrfetch(V, i, j, k, b)[c];
}

// fetcher for images
template<class T>
inline const T* lux::ptrfetch(const lux::image<T>& img, int64_t i, int64_t j, const lux::BOUNDARY& b) {
	i = map_coordinate(i, int64_t(V.dim(0)), b);	if (i < 0) return __zero;
	j = map_coordinate(j, int64_t(V.dim(1)), b);	if (j < 0) return __zero;
	return &img(i, j, 0);
}

template<class T>
inline lux::vec4<T> lux::vfetch(const lux::image<T>& img, int64_t i, int64_t j, const lux::BOUNDARY& b) {
	lux::vec4<T> result;
	const T* ptr = ptrfetch(img, i, j, b);
	for (size_t n = 0; n < img.components(); n++)  result(n) = ptr[n];
	return result;
}

template<class T>
inline T lux::fetch(const lux::image<T>& vol, int64_t i, int64_t j, size_t c, const lux::BOUNDARY& b) {
	return ptrfetch(i, j, c, b)[n];
}

// Fetcher for colormaps
inline const lux::color& lux::vfetch(const lux::colormap& M, int64_t i, const lux::BOUNDARY& b) {
	i = map_coordinate(i, int64_t(M.size()), b);	if (i < 0) return lux::color::Zero;
	return M[size_t(i)];
}

inline const float* lux::ptrfetch(const lux::colormap& M, int64_t i, const lux::BOUNDARY& b) {
	return (const float*)&vfetch(M, i, b);
}

inline float lux::fetch(const lux::colormap& M, int64_t i, size_t c, const lux::BOUNDARY& b) {
	return vfetch(M, i, b)[c];
}

// Sampler for volumes
template<class T>
inline T lux::sample(const lux::volume<T>& vol, double x, double y, double z, size_t channel, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return fetch(vol, int64_t(std::nearbyint(x)), int64_t(std::nearbyint(y)), int64_t(std::nearbyint(z)), channel, b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p[3] = { int64_t(floor(x)),int64_t(floor(y)),int64_t(floor(z)) };
		int64_t q[3] = { p[0] + 1, p[1] + 1, p[2] + 1 };
		T w[3] = { T(x - p[0]), T(y - p[1]), T(z - p[2]) };
		T t[8];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = fetch(vol, i, j, k, channel, b);
				}
			}
		}
		size_t off = 4;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] += w[d] * (t[o + off] - t[o]);
			}
			off >>= 1;
		}
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p[3] = { int64_t(floor(x)) - 1,int64_t(floor(y)) - 1,int64_t(floor(z)) - 1 };
		int64_t q[3] = { p[0] + 3, p[1] + 3, p[2] + 3 };
		T t[64];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = fetch(vol, i, j, k, channel, b);
				}
			}
		}
		T w[3] = { T(x - p[0] - 1), T(y - p[1] - 1), T(z - p[2] - 1) };
		size_t off = 16;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] = lux::cubic(t[o], t[o + off], t[o + 2 * off], t[o + 3 * off],w[d]);
			}
			off >>= 2;
		}
		return t[0];
	}
	default:
		return T(0);
	}
}

template<class T>
inline lux::vec4<T> lux::vsample(const lux::volume<T>& vol, double x, double y, double z, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	lux::vec4<T> result;
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return vfetch(vol, int64_t(std::nearbyint(x)), int64_t(std::nearbyint(y)), int64_t(std::nearbyint(z)), b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p[3] = { int64_t(floor(x)),int64_t(floor(y)),int64_t(floor(z)) };
		int64_t q[3] = { p[0] + 1, p[1] + 1, p[2] + 1 };
		T w[3] = { T(x - p[0]), T(y - 0[1]), T(z - p[2]) };
		lux::vec4<T> t[8];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = vfetch(vol, i, j, k, b);
				}
			}
		}
		size_t off = 4;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] += w[d] * (t[o + off] - t[o]);
			}
			off >>= 1;
		}
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p[3] = { int64_t(floor(x)) - 1,int64_t(floor(y)) - 1,int64_t(floor(z)) - 1 };
		int64_t q[3] = { p[0] + 3, p[1] + 3, p[2] + 3 };
		lux::vec4<T> t[64];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = vfetch(vol, i, j, k, b);
				}
			}
		}
		T w[3] = { T(x - p[0] - 1), T(y - p[1] - 1), T(z - p[2] - 1) };
		size_t off = 16;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] = lux::cubic(t[o], t[o + offs], t[o + 2 * offs], t[o + 3 * offs],w[d]);
			}
			off >= 2;
		}
		return t[0];
	}
	default:
		return lux::vec4(T(0), T(0), T(0), T(0));
	}
}

template<class T> 
inline T lux::sample(const vectorfield3<T>& V, double x, double y, double z, size_t channel, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return fetch(V, int64_t(std::nearbyint(x)), int64_t(std::nearbyint(y)), int64_t(std::nearbyint(z)), channel, b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p[3] = { int64_t(floor(x)),int64_t(floor(y)),int64_t(floor(z)) };
		int64_t q[3] = { p[0] + 1, p[1] + 1, p[2] + 1 };
		T w[3] = { T(x - p[0]), T(y - p[1]), T(z - p[2]) };
		T t[8];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = fetch(V, i, j, k, channel, b);
				}
			}
		}
		size_t off = 4;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] += w[d] * (t[o + off] - t[o]);
			}
			off >>= 1;
		}
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p[3] = { int64_t(floor(x)) - 1,int64_t(floor(y)) - 1,int64_t(floor(z)) - 1 };
		int64_t q[3] = { p[0] + 3, p[1] + 3, p[2] + 3 };
		T t[64];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = fetch(V, i, j, k, channel, b);
				}
			}
		}
		T w[3] = { T(x - p[0] - 1), T(y - p[1] - 1), T(z - p[2] - 1) };
		size_t off = 16;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] = lux::cubic(t[o], t[o + off], t[o + 2 * off], t[o + 3 * off], w[d]);
			}
			off >>= 2;
		}
		return t[0];
	}
	default:
		return T(0);
	}
}

template<class T> 
inline lux::vec4<T> lux::vsample(const vectorfield3<T>& V, double x, double y, double z, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	lux::vec4<T> result;
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return vfetch(V, int64_t(std::nearbyint(x)), int64_t(std::nearbyint(y)), int64_t(std::nearbyint(z)), b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p[3] = { int64_t(floor(x)),int64_t(floor(y)),int64_t(floor(z)) };
		int64_t q[3] = { p[0] + 1, p[1] + 1, p[2] + 1 };
		T w[3] = { T(x - p[0]), T(y - p[1]), T(z - p[2]) };
		lux::vec4<T> t[8];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = vfetch(V, i, j, k, b);
				}
			}
		}
		size_t off = 4;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] += w[d] * (t[o + off] - t[o]);
			}
			off >>= 1;
		}
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p[3] = { int64_t(floor(x)) - 1,int64_t(floor(y)) - 1,int64_t(floor(z)) - 1 };
		int64_t q[3] = { p[0] + 3, p[1] + 3, p[2] + 3 };
		lux::vec4<T> t[64];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				for (int64_t k = p[2]; k <= q[2]; k++) {
					t[addr++] = vfetch(V, i, j, k, b);
				}
			}
		}
		T w[3] = { T(x - p[0] - 1), T(y - p[1] - 1), T(z - p[2] - 1) };
		size_t offs = 16;
		for (size_t d = 0; d < 3; d++) {
			for (size_t o = 0; o < offs; o++) {
				t[o] = lux::cubic(t[o], t[o + offs], t[o + 2 * offs], t[o + 3 * offs], w[d]);
			}
			offs >>= 2;
		}
		return t[0];
	}
	default:
		return lux::vec4<T>(T(0), T(0), T(0), T(0));
	}
}

// Sampler for images
template<class T>
inline T lux::sample(const lux::image<T>& img, double x, double y, size_t channel, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return fetch(img, int64_t(std::nearbyint(x)), int64_t(std::nearbyint(y)), channel, b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p[2] = { int64_t(floor(x)),int64_t(floor(y)) };
		int64_t q[2] = { p[0] + 1,p[1] + 1 };
		T w[2] = { T(x - p[0]), T(y - p[1]) };
		T t[4];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				t[addr++] = fetch(img, i, j, channel, b);
			}
		}
		size_t off = 2;
		for (size_t d = 0; d < 2; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] += w[d] * (t[o + off] - t[o]);
			}
			off >>= 1;
		}
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p[2] = { int64_t(floor(x)) - 1,int64_t(floor(y)) - 1 };
		int64_t q[2] = { p[0] + 3, p[1] + 3 };
		T t[16];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				t[addr++] = fetch(img, i, j, channel, b);
			}
		}
		T w[2] = { T(x - p[0] - 1), T(y - p[1] - 1) };
		size_t off = 4;
		for (size_t d = 0; d < 2; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] = lux::cubic(t[o], t[o + offs], t[o + 2 * offs], t[o + 3 * offs]);
			}
			off >= 2;
		}
		return t[0];
	}
	default:
		return T(0);
	}
}

template<class T>
inline lux::vec4<T> lux::vsample(const lux::image<T>& img, double x, double y, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	lux::vec4<T> result;
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return vfetch(img, int64_t(std::nearbyint(x)), int64_t(std::nearbyint(y)), b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p[2] = { int64_t(floor(x)),int64_t(floor(y)) };
		int64_t q[2] = { p[0] + 1,p[1] + 1 };
		T w[2] = { T(x - p[0]), T(y - p[1]) };
		lux::vec4<T> t[4];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				t[addr++] = vfetch(img, i, j, b);
			}
		}
		size_t off = 2;
		for (size_t d = 0; d < 2; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] += w[d] * (t[o + off] - t[o]);
			}
			off >>= 1;
		}
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p[2] = { int64_t(floor(x)) - 1,int64_t(floor(y)) - 1 };
		int64_t q[2] = { p[0] + 3, p[1] + 3 };
		lux::vec4<T> t[16];
		size_t addr = 0;
		for (int64_t i = p[0]; i <= q[0]; i++) {
			for (int64_t j = p[1]; j <= q[1]; j++) {
				t[addr++] = vfetch(img, i, j, b);
			}
		}
		T w[2] = { T(x - p[0] - 1), T(y - p[1] - 1) };
		size_t off = 4;
		for (size_t d = 0; d < 2; d++) {
			for (size_t o = 0; o < off; o++) {
				t[o] = lux::cubic(t[o], t[o + off], t[o + 2 * off], t[o + 3 * off],w[d]);
			}
			off >>= 2;
		}
		return t[0];
	}
	default:
		return lux::vec4<T>(T(0), T(0), T(0), T(0));
	}
}

inline float lux::sample(const lux::colormap& M, double x, size_t channel, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return fetch(M, int64_t(std::nearbyint(x)), channel, b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p = int64_t(floor(x));
		int64_t q = p + 1;
		float w = float(x - p);
		float t[2];
		size_t addr = 0;
		for (int64_t i = p; i <= q; i++) t[addr++] = fetch(M, i, channel, b);
		return t[0] + w *(t[1] - t[0]);
	}
	case SAMPLER_CUBIC:
	{
		int64_t p = int64_t(floor(x)) - 1;
		int64_t q = p + 3;
		float t[4];
		size_t addr = 0;
		for (int64_t i = p; i <= q; i++) t[addr++] = fetch(M, i, channel, b);
		float w = float(x - p - 1);
		return lux::cubic(t[0], t[1], t[2], t[3], w);
	}
	default:
		return 0.0f;
	}
}
inline lux::color lux::vsample(const lux::colormap& M, double x, const lux::INTERPOLATOR& s, const lux::BOUNDARY& b) {
	switch (s) {
	case SAMPLER_NEAREST:
	{
		std::fesetround(FE_TONEAREST);
		return vfetch(M, int64_t(std::nearbyint(x)), b);
	}
	case SAMPLER_LINEAR:
	{
		int64_t p = int64_t(floor(x));
		int64_t q = p + 1;
		float w = float(x - p);
		lux::color t[2];
		size_t addr = 0;
		for (int64_t i = p; i <= q; i++) t[addr++] = vfetch(M, i, b);
		for (size_t n = 0; n < 4; n++) t[0](n) += w*(t[1](n) - t[0](n));
		return t[0];
	}
	case SAMPLER_CUBIC:
	{
		int64_t p = int64_t(floor(x)) - 1;
		int64_t q = p + 3;
		lux::color t[4];
		size_t addr = 0;
		for (int64_t i = p; i <= q; i++) t[addr++] = vfetch(M, i, b);
		float w = float(x - p - 1);
		//printf("x=%f, w=%f, p=%i, q=%i\n", x, w, p, q);
		//lux::color clr = lux::cubic(t[0], t[1], t[2], t[3], w);
		//for (int i=0; i<4; i++) printf("t%i: %f %f %f %f\n", i, t[i](0), t[i](1), t[i](2), t[i](3));
		//printf("clr %f %f %f %f\n", clr(0), clr(1), clr(2), clr(3));
		//exit(0);

		return lux::cubic(t[0], t[1], t[2], t[3], w);
	}
	default:
		return lux::color(0.0f, 0.0f, 0.0f, 0.0f);
	}
}


#endif

