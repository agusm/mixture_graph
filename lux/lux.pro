include(../mixture_graph.pri)


QMAKE_CXXFLAGS -= -Zc:rvalueCast -Zc:inline -Zc:strictStrings -Zc:throwingNew -Zc:referenceBinding -Zc:__cplusplus -w34100 -w34189 -w44996 -w44456 -w44457 -w44458 

QMAKE_CFLAGS -= -Zc:rvalueCast -Zc:inline -Zc:strictStrings -Zc:throwingNew -Zc:referenceBinding -Zc:__cplusplus -w34100 -w34189 -w44996 -w44456 -w44457 -w44458 

  
CONFIG+=staticlib  
TEMPLATE= lib

INCLUDEPATH += ../ext

HEADERS= \
flags.h \
lux.h \
lux_HermiteCurveS3.h \
lux_arcball.h \
lux_auxiliary.h \
lux_camera.h \
lux_catmull_rom.h \
lux_color.h \
lux_coordinateframe.h \
lux_distance_volume.h \
lux_footprint_assembly.h \
lux_glerror.h \
lux_glgeometry.h \
lux_image.h \ 
lux_integrators.h \
lux_interpolation.h \
lux_math.h \
lux_ppm.h \
lux_sampler.h \
lux_shader.h \ 
lux_trajectory.h \
lux_vectorfield3.h \
lux_volume.h \
lux_weno.h \
timing.h 


SOURCES= \
lux_auxiliary.cpp \ 
lux_shader.cpp \
lux_footprint_assembly.cpp \
lux_ppm.cpp  \
lux_color.cpp \ 
lux_glerror.cpp \
lux_glgeometry.cpp 


