#include"lux_glgeometry.h"
#include<inttypes.h>

GLuint lux::static_cube::m_hVAO = 0;
GLuint lux::static_cube::m_hVBO = 0;
GLuint lux::static_cube::m_hIBO = 0;
GLuint lux::static_quad::m_hVAO = 0;
GLuint lux::static_quad::m_hVBO = 0;
GLuint lux::static_quad::m_hIBO = 0;


void lux::static_cube::draw(void) {
	if (!m_hVAO) init();
	glBindVertexArray(m_hVAO);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, nullptr);
}

void lux::static_cube::draw(GLuint shader, GLenum mode, int elems, const void* indices) {
	if (!m_hVAO) init();
	glUseProgram(shader);
	glBindVertexArray(m_hVAO);
	glDrawElements(mode, elems, GL_UNSIGNED_SHORT, indices);
}

void lux::static_cube::clear(void) {
	if (m_hVAO != 0) glDeleteVertexArrays(1, &m_hVAO);
	if (m_hVBO != 0) glDeleteBuffers(1, &m_hVBO);
	if (m_hIBO != 0) glDeleteBuffers(1, &m_hIBO);
	m_hVAO = m_hVBO = m_hIBO = 0;
}

void lux::static_cube::init(void) {
	constexpr static const float vtx[] = {
		-1.0f,-1.0f,-1.0f,
		 1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f,
		 1.0f, 1.0f,-1.0f,
		-1.0f,-1.0f, 1.0f,
		 1.0f,-1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		 1.0f, 1.0f, 1.0f
	};
	constexpr static uint16_t idx[] = {
		0,1,3,0,3,2,	// front
		1,5,7,1,7,3,	// right
		5,4,6,5,6,7,	// back
		4,0,2,4,2,6,	// left
		2,3,7,2,7,6,	// top
		4,5,1,4,1,0		// bottom
	};
	clear();
	glGenVertexArrays(1, &m_hVAO);
	glBindVertexArray(m_hVAO);
	glGenBuffers(1, &m_hVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_hVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vtx), vtx, GL_STATIC_DRAW);
	glBindVertexBuffer(0, m_hVBO, 0, 0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(0);
	glGenBuffers(1, &m_hIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_hIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idx), idx, GL_STATIC_DRAW);
	glBindVertexArray(0);
}

void lux::static_quad::draw(void) {
	if (!m_hVAO) init();
	glBindVertexArray(m_hVAO);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, nullptr);
}

void lux::static_quad::draw(GLuint shader) {
	if (!m_hVAO) init();
	glUseProgram(shader);
	glBindVertexArray(m_hVAO);
	glDrawElements(GL_QUADS, 4, GL_UNSIGNED_SHORT, nullptr);
}

void lux::static_quad::clear(void) {
	if (m_hVAO != 0) glDeleteVertexArrays(1, &m_hVAO);
	if (m_hVBO != 0) glDeleteBuffers(1, &m_hVBO);
	if (m_hIBO != 0) glDeleteBuffers(1, &m_hIBO);
	m_hVAO = m_hVBO = m_hIBO = 0;
}

void lux::static_quad::init(void) {
	constexpr static const float vtx[] = {
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f,
		1.0f, 1.0f,0.0f,
		1.0f, -1.0f,0.0f,
	};
	constexpr static uint16_t idx[] = {
		0,1,2,3
	};
	clear();
	glGenVertexArrays(1, &m_hVAO);
	glBindVertexArray(m_hVAO);
	glGenBuffers(1, &m_hVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_hVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vtx), vtx, GL_STATIC_DRAW);
	glBindVertexBuffer(0, m_hVBO, 0, 0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(0);
	glGenBuffers(1, &m_hIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_hIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idx), idx, GL_STATIC_DRAW);
	glBindVertexArray(0);
}
