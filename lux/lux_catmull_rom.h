#ifndef __LUX_CATMULL_ROM_H__
#define __LUX_CATMULL_ROM_H__

#include"lux_math.h"

namespace lux {
	// Catmull-Rom spline interpolating cp[1], cp[2] and using cp[0],cp[3] for tangent information
	// input vectors contain 3D positions plus the parameter (knot) value in the 4th component
	// output vector contains a 3D position plus the parameter 'at' in the 4th component
	template<class T>
	inline vec4<T> catmull_rom(const lux::vec4<T> cp[4], const T& at);
	// Catmull-Rom spline padding at the front
	// Given three control points 'cp', compute control point 'cp[-1]' on the curve
	// such that an evaluation of catmull_rom at t0 yields 'cp[-1]'
	template<class T>
	inline vec4<T> catmull_rom_pad_front(const lux::vec4<T> cp[3], const T& t0);
	// Catmull-Rom spline padding at the back
	// Given three control points 'cp', compute control point 'cp[3]' on the curve
	// such that an evaluation of catmull_rom at t3 yields 'cp[3]'
	template<class T>
	inline vec4<T> catmull_rom_pad_back(const lux::vec4<T> cp[3], const T& t3);

	template<class T>
	inline vec4<T> catmull_rom_diff1(const lux::vec4<T> cp[4], const T& at, lux::vec4<T>* eval = nullptr);

};

template<class T>
inline lux::vec4<T> lux::catmull_rom(const vec4<T> cp[4], const T& at) {
	lux::vec4<T> tmp[3];
	for (size_t n = 0; n < 3; n++) tmp[n] = ((cp[n + 1](3) - at)*cp[n] + (at - cp[n](3))*cp[n + 1]) / (cp[n + 1](3) - cp[n](3));
	for (size_t n = 0; n < 2; n++) tmp[n] = ((cp[n + 2](3) - at)*tmp[n] + (at - cp[n](3))*tmp[n + 1]) / (cp[n + 2](3) - cp[n](3));
	tmp[0] = ((cp[2](3) - at)*tmp[0] + (at - cp[1](3))*tmp[1]) / (cp[2](3) - cp[1](3));
	tmp[0](3) = at;
	return tmp[0];
}

template<class T>
lux::vec4<T> lux::catmull_rom_pad_front(const lux::vec4<T> cp[3], const T& t0) {
	const T t[4] = { t0, cp[0](3),cp[1](3),cp[2](3) };
	lux::vec4<T> A2 = ((t[2] - t[0])*cp[0] + (t[0] - t[1])*cp[1]) / (t[2] - t[1]);
	lux::vec4<T> A3 = ((t[3] - t[0])*cp[1] + (t[0] - t[2])*cp[2]) / (t[3] - t[2]);
	lux::vec4<T> B2 = ((t[3] - t[0])*A2 + (t[0] - t[1])*A3) / (t[3] - t[1]);
	B2(3) = t0;
	return B2;
}

template<class T>
lux::vec4<T> lux::catmull_rom_pad_back(const lux::vec4<T> cp[3], const T& t3) {
	const T t[4] = { cp[0](3),cp[1](3),cp[2](3), t3 };
	lux::vec4<T> A1 = ((t[1] - t[3])*cp[0] + (t[3] - t[0])*cp[1]) / (t[1] - t[0]);
	lux::vec4<T> A2 = ((t[2] - t[3])*cp[1] + (t[3] - t[1])*cp[2]) / (t[2] - t[1]);
	lux::vec4<T> B1 = ((t[2] - t[3])*A1 + (t[3] - t[0]) * A2) / (t[2] - t[0]);
	B1(3) = t3;
	return B1;
}

template<class T>
lux::vec4<T> lux::catmull_rom_diff1(const lux::vec4<T> cp[4], const T& at, lux::vec4<T>* eval) {
	const T t[4] = { cp[0](3), cp[1](3), cp[2](3), cp[3](3) };
	lux::vec4<T> A1  = ((t[1] - at)*cp[0] + (at - t[0])*cp[1]) / (t[1] - t[0]);			// correct
	lux::vec4<T> A1t = (cp[1] - cp[0]) / (t[1] - t[0]);									// correct
	lux::vec4<T> A2 = ((t[2] - at)*cp[1] + (at - t[1])*cp[2]) / (t[2] - t[1]);			// correct
	lux::vec4<T> A2t = (cp[2] - cp[1]) / (t[2] - t[1]);									// correct
	lux::vec4<T> A3 = ((t[3] - at)*cp[2] + (at - t[2])*cp[3]) / (t[3] - t[2]);			// correct
	lux::vec4<T> A3t = (cp[3] - cp[2]) / (t[3] - t[2]);									// correct
	lux::vec4<T> B1 = ((t[2] - at)*A1 + (at - t[0]) * A2) / (t[2] - t[0]);				// correct
	lux::vec4<T> B1t = (A2 - A1 + (t[2] - at)*A1t + (at - t[0])*A2t) / (t[2] - t[0]);	// correct
	lux::vec4<T> B2 = ((t[3] - at)*A2 + (at - t[1]) * A3) / (t[3] - t[1]);				// correct
	lux::vec4<T> B2t = (A3 - A2 + (t[3] - at)*A2t + (at - t[1])*A3t) / (t[3] - t[1]);	// correct
	if (eval != nullptr) *eval = ((t[2] - at)*B1 + (at - t[1])*B2) / (t[2] - t[1]);
	lux::vec4<T> Ct = (B2 - B1 + (t[2] - at)*B1t + (at - t[1])*B2t) / (t[2] - t[1]);
	Ct(3) = at;
	return Ct;
}

#endif
