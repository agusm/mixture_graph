#ifndef __LUX_COORDINATEFRAME_H__
#define __LUX_COORDINATEFRAME_H__

#include"lux_math.h"
#include<cassert>

namespace lux {
	template<class T>
	coordinateframe<T> interpolate(const coordinateframe<T>& a, const coordinateframe<T>& b, const T& t);

	template<class T>
	class coordinateframe {
	public:
		coordinateframe(void);
		coordinateframe(const vec4<T>& origin, const quat4<T>& orientation);
		coordinateframe(const vec4<T>& origin, const vec4<T>& X, const vec4<T>& Y, const vec4<T>& Z);
		coordinateframe(const vec4<T>& origin, const mat4<T>& R);
		coordinateframe(const mat4<T>& RT);
		coordinateframe(const coordinateframe& other);
		~coordinateframe(void);
		inline const vec4<T>& get_origin(void) const;
		inline void set_origin(const vec4<T>& origin);
		inline quat4<T> get_orientation(int dir=1) const;
		inline void set_orientation(const quat4<T>& orientation, int dir = 1);
		inline const T& curvature(void) const { return m_kappa; }
		inline const T& torsion(void) const { return m_tau; }
		inline T& curvature(void) { return m_kappa; }
		inline T& torsion(void) { return m_tau; }
		inline vec4<T> rotate(const vec4<T>& v, int dir = 1) const;
		coordinateframe& operator=(const coordinateframe& other);
		friend coordinateframe<T> lux::interpolate(const coordinateframe<T>& a, const coordinateframe<T>& b, const T& t);
		T diff(const coordinateframe<T>& other) const;
	protected:
		vec4<T>  m_origin;
		quat4<T> m_orientation;
		T m_kappa, m_tau;
	};

	typedef coordinateframe<float> coordinateframef;
	typedef coordinateframe<double> coordinateframed;

	inline float copysign(const float& dst, const float& src) {
		if (src == 0.0f) return 0.0f;
		return std::copysign(dst, src);
	}
	inline double copysign(const double& dst, const double& src) {
		if (src == 0.0) return 0.0;
		return std::copysign(dst, src);
	}
};

template<class T>
lux::coordinateframe<T>::coordinateframe(void) : m_origin(vec4<T>(T(0), T(0), T(0), T(1))), m_orientation(quat4<T>(T(1), T(0), T(0), T(0))), m_kappa(T(0)), m_tau(T(0)) {}

template<class T>
lux::coordinateframe<T>::coordinateframe(const vec4<T>& origin, const quat4<T>& orientation) : m_origin(origin), m_orientation(orientation), m_kappa(T(0)), m_tau(T(0)) {}

template<class T>
lux::coordinateframe<T>::coordinateframe(const vec4<T>& origin, const vec4<T>& X, const vec4<T>& Y, const vec4<T>& Z) : m_origin(origin) {
	m_orientation.real() = T(0.5*sqrt(std::max(0.0, 1.0 + X(0) + Y(1) + Z(2))));
	m_orientation.imag(0) = T(0.5*sqrt(std::max(0.0, 1.0 + X(0) - Y(1) - Z(2))));
	m_orientation.imag(1) = T(0.5*sqrt(std::max(0.0, 1.0 - X(0) + Y(1) - Z(2))));
	m_orientation.imag(2) = T(0.5*sqrt(std::max(0.0, 1.0 - X(0) - Y(1) + Z(2))));
	m_orientation.imag(0) = lux::copysign(m_orientation.imag(0), Y(2) - Z(1));
	m_orientation.imag(1) = lux::copysign(m_orientation.imag(1), Z(0) - X(2));
	m_orientation.imag(2) = lux::copysign(m_orientation.imag(2), X(1) - Y(0));
	m_orientation.real() = sqrt(std::max(T(0), T(1) - m_orientation.imag(0)*m_orientation.imag(0) - m_orientation.imag(1)*m_orientation.imag(1) - m_orientation.imag(2)*m_orientation.imag(2)));
	m_kappa = m_tau = T(0);
}

template<class T>
lux::coordinateframe<T>::coordinateframe(const vec4<T>& origin, const mat4<T>& R) : m_origin(origin) {
	m_orientation.real() = T(0.5*sqrt(std::max(0.0, 1.0 + R(0,0) + R(1,1) + R(2,2))));
	m_orientation.imag(0) = T(0.5*sqrt(std::max(0.0, 1.0 + R(0,0) - R(1,1) - R(2,2))));
	m_orientation.imag(1) = T(0.5*sqrt(std::max(0.0, 1.0 - R(0,0) + R(1,1) - R(2,2))));
	m_orientation.imag(2) = T(0.5*sqrt(std::max(0.0, 1.0 - R(0,0) - R(1,1) + R(2,2))));
	m_orientation.imag(0) = lux::copysign(m_orientation.imag(0), R(2,1) - R(1,2));
	m_orientation.imag(1) = lux::copysign(m_orientation.imag(1), R(0,2) - R(2,0));
	m_orientation.imag(2) = lux::copysign(m_orientation.imag(2), R(1,0) - R(0,1));
	m_orientation.real() = sqrt(std::max(T(0), T(1) - m_orientation.imag(0)*m_orientation.imag(0) - m_orientation.imag(1)*m_orientation.imag(1) - m_orientation.imag(2)*m_orientation.imag(2)));
	m_kappa = m_tau = T(0);
}

template<class T>
lux::coordinateframe<T>::coordinateframe(const mat4<T>& RT) {
	for (int i = 0; i < 3; i++) m_origin(i) = RT(i, 3);
	m_orientation(3) = T(1);
	m_orientation.real() = T(0.5*sqrt(std::max(0.0, 1.0 + RT(0, 0) + RT(1, 1) + RT(2, 2))));
	m_orientation.imag(0) = T(0.5*sqrt(std::max(0.0, 1.0 + RT(0, 0) - RT(1, 1) - RT(2, 2))));
	m_orientation.imag(1) = T(0.5*sqrt(std::max(0.0, 1.0 - RT(0, 0) + RT(1, 1) - RT(2, 2))));
	m_orientation.imag(2) = T(0.5*sqrt(std::max(0.0, 1.0 - RT(0, 0) - RT(1, 1) + RT(2, 2))));
	m_orientation.imag(0) = lux::copysign(m_orientation.imag(0), RT(2, 1) - RT(1, 2));
	m_orientation.imag(1) = lux::copysign(m_orientation.imag(1), RT(0, 2) - RT(2, 0));
	m_orientation.imag(2) = lux::copysign(m_orientation.imag(2), RT(1, 0) - RT(0, 1));
	m_orientation.real() = sqrt(std::max(T(0), T(1) - m_orientation.imag(0)*m_orientation.imag(0) - m_orientation.imag(1)*m_orientation.imag(1) - m_orientation.imag(2)*m_orientation.imag(2)));
	m_kappa = m_tau = T(0);
}

template<class T>
lux::coordinateframe<T>::coordinateframe(const coordinateframe& other) : m_origin(other.m_origin), m_orientation(other.m_orientation) {}

template<class T>
lux::coordinateframe<T>::~coordinateframe(void) {}

template<class T>
inline const lux::vec4<T>& lux::coordinateframe<T>::get_origin(void) const {
	return m_origin;
}

template<class T>
inline void lux::coordinateframe<T>::set_origin(const vec4<T>& origin) {
	m_origin = origin;
}

template<class T>
inline lux::quat4<T> lux::coordinateframe<T>::get_orientation(int dir) const {
	assert("lux::coordinateframe::get_orientation -- invalid parameter" && (dir == 1 || dir == -1));
	if (dir == 1) return m_orientation;
	if (dir == -1) return m_orientation.conjugate();
	return quat4<T>();
}

template<class T>
inline void lux::coordinateframe<T>::set_orientation(const quat4<T>& orientation, int dir) {
	assert("lux::coordinateframe::set_orientation -- invalid parameter" && (dir == 1 || dir == -1));
	if (dir == 1) {
		m_orientation = orientation;
		return;
	}
	if (dir == -1) {
		m_orientation = orientation.conjugate();
		return;
	}
}


template<class T>
inline lux::vec4<T> lux::coordinateframe<T>::rotate(const vec4<T>& v, int fwd) const {
	assert("lux::coordinateframe::rotate -- invalid parameter" && (fwd == 1 || fwd == -1));
	lux::quat4 versor(T(0), v(0)-m_origin(0), v(1)-m_origin(1), v(2)-m_origin(2));
	if (dir == 1) {
		quat4<T> q = m_origin*versor*m_origin.conjugate();
		return vec4<T>(q.imag(0)+m_origin(0), q.imag(1)+m_origin(1), q.imag(2)+m_origin(2), T(1));
	}
	if (dir == -1) {
		quat4<T> q = m_origin.conjugate()*versor*m_origin;
		return vec4<T>(q.imag(0)+m_origin(0), q.imag(1)+m_origin(1), q.imag(2)+m_origin(2), T(1));
	}
	return v;
}

template<class T>
lux::coordinateframe<T>& lux::coordinateframe<T>::operator=(const coordinateframe<T>& other) {
	m_origin = other.m_origin;
	m_orientation = other.m_orientation;
	m_kappa = other.m_kappa;
	m_tau = other.m_tau;
	return *this;
}

template<class T>
lux::coordinateframe<T> lux::interpolate(const lux::coordinateframe<T>& a, const lux::coordinateframe<T>& b, const T& t) {
	if (t <= T(0)) return a;
	if (t >= T(1)) return b;

	const quat4<T>& Q0 = a.get_orientation(1);
	quat4<T> Q1 = b.get_orientation(1);
	coordinateframe<T> result(a);
	result.m_origin += t*vec4<T>(b.m_origin - a.m_origin);
	result.m_origin(3) = T(1);

	T dot = Q0.real()*Q1.real() + Q0.imag(0)*Q1.imag(0) + Q0.imag(1)*Q1.imag(1) + Q0.imag(2)*Q1.imag(2);
	if (dot < T(0)) {
		dot = -dot;
		Q1 = -Q1;
	}
	if (dot > T(0.9995)) {
		result.m_orientation = Q0 + t*quat4<T>(Q1 - Q0);
		result.m_orientation.normalize();
		return result;
	}
	dot = std::min(T(1), std::max(T(0), dot));
	T theta_0 = acos(dot);
	T theta = theta_0*t;
	T s0 = cos(theta) - dot*sin(theta) / sin(theta_0);
	T s1 = sin(theta) / sin(theta_0);

	result.m_orientation = s0*Q0 + s1*Q1;
	return result;
}

template<class T>
T lux::coordinateframe<T>::diff(const coordinateframe<T>& other) const {
	quat4<T> Q = (m_orientation*other.m_orientation.conjugate() - quat4<T>(T(1),T(0),T(0),T(0)));
	T result = Q(0)*Q(0) + Q(1)*Q(1) + Q(2)*Q(2) + Q(3)*Q(3);
	if (result > T(2)) result = T(4) - result;
	return result;
}

#endif
