#ifndef __LUX_ARCBALL_H__
#define __LUX_ARCBALL_H__

#include"lux_math.h"

namespace lux {
	template<class T>
	class arcball {
	public:
		constexpr arcball(void) : m_width(0), m_height(0), m_click(1.0f, 0.0f, 0.0f, 0.0f), m_orientation(1.0f, 0.0f, 0.0f, 0.0f), m_old_orientation(1.0f, 0.0f,0.0f,0.0f), m_dragging(false) {}

		arcball(const arcball& other) { *this = other; }

		~arcball(void) {}

		void reshape(size_t width, size_t height) {
			m_width = std::max(size_t(1), width);
			m_height = std::max(size_t(1), height);
		}

		void click(size_t x, size_t y) {
			if (!is_over(x,y)) return;
			m_dragging = true;
			m_old_orientation = xy2quat(x, y) * m_orientation;
		}

		void drag(size_t x, size_t y) {
			if (!m_dragging) return;
			quat4f t = -xy2quat(x, y);
			m_orientation = t * m_old_orientation; 
		}

		void release(size_t x, size_t y) {
			m_dragging = false;
			m_orientation.normalize();
		}

		bool dragging(void) const { return m_dragging; }

		arcball& operator=(const arcball& other) {
			m_width = other.m_width;
			m_height = other.m_height;
			m_click = other.m_click;
			m_current = other.m_current;
			m_dragging = other.m_dragging;
			return *this;
		}	

		lux::quat4<T> getOrientation(void) const {
			return m_orientation;
		}
		
		bool setOrientation(const lux::quat4f& q) {
			if (m_dragging) return false;
			m_orientation = q;
			m_orientation.normalize();
			return true;
		}

	protected:
		size_t m_width, m_height;
		quat4<T> m_click, m_orientation, m_old_orientation;
		bool m_dragging;
		quat4<T> xy2quat(size_t x, size_t y) const {
			quat4<T> Q(
				T(0),
				T(2)*T(x) / T(m_width - 1) - T(1),
				T(2)*T(y) / T(m_height-1)-T(1),
				T(0)
			);
			if (Q(1)*Q(1)+Q(2)*Q(2)<T(1)) Q.imag(2) = sqrt(T(1) - Q.imag(0)*Q.imag(0) - Q.imag(1)*Q.imag(1));
			else Q.normalize();
			return Q;
		}
		bool is_over(size_t x, size_t y) {
			return x < m_width && y < m_height;
		}
	};

	typedef arcball<float> arcballf;
	typedef arcball<double> arcballd;
};

#endif
