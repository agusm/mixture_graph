#ifndef __LUX_GEOMETRY_H__
#define __LUX_GEOMETRY_H__

#include"GL/glew.h"

namespace lux {
	class static_cube {
	public:
		static void draw(void);
		static void draw(GLuint shader, GLenum mode = GL_TRIANGLES, int elems = 36, const void* indices = nullptr);
		static void clear(void);
	protected:
		static GLuint m_hVAO;
		static GLuint m_hVBO;
		static GLuint m_hIBO;
		static void init(void);
	private:
		static_cube(void);
		static_cube(const static_cube&);
	};

	class static_quad {
	public:
		static void draw(void);
		static void draw(GLuint shader);
		static void clear(void);
	protected:
		static GLuint m_hVAO;
		static GLuint m_hVBO;
		static GLuint m_hIBO;
		static void init(void);
	private:
		static_quad(void);
		static_quad(const static_quad&);
	};

};

#endif

