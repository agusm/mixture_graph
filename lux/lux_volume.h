#ifndef __LUX_VOLUME_H__
#define __LUX_VOLUME_H__

#include"lux_image.h" // for range_t
#include<vector>
#include<string>
#include<cassert>
#include<type_traits>

namespace lux {
	template<class T>
	class volume {
	public:
		constexpr static const size_t max_components = 4;
		typedef T base_type;
		volume(void);
		volume(size_t dimx, size_t dimy, size_t dimz, size_t components=1);
		volume(const volume& other);
		~volume(void);
		void clear(void);
		size_t size(void) const;
		bool empty(void) const;
		void resize(size_t dimx, size_t dimy, size_t dimz, size_t components = 1);
		size_t dim(size_t n) const;
		size_t components(void) const;
		std::string& name(void);
		const std::string& name(void) const;
		inline size_t linear_address(size_t i, size_t j, size_t k, size_t c) const {
			assert("lux::volume::linear_address -- invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2] && c<m_dims[3]);
			return m_dims[3] * (i + (j+k*m_dims[1])*m_dims[0]) + c;
		}
		typename std::vector<T>::reference operator()(size_t i, size_t j, size_t k, size_t c);
		typename std::vector<T>::reference operator[](size_t n);
		typename std::vector<T>::const_reference operator()(size_t i, size_t j, size_t k, size_t c) const;
		typename std::vector<T>::const_reference operator[](size_t n) const;
		typedef std::function<void(double*, const double&, const double&, const double&, size_t)> fill_function;
		void fill(const ranged_t& X, const ranged_t& Y, const ranged_t& Z, const fill_function& F);
		void swap(volume& other);
		volume& operator=(const volume& other);
		template<class S> volume<S> as(void) const;
		const T* data(void) const;
		image<T> slice(size_t index, char dir) const;
		void set_voxel_extents(const float& sx, const float& sy, const float& sz);
		float& voxel_extent(size_t n);
		const float& voxel_extent(size_t n) const;
	protected:
		std::vector<T>	m_data;
		std::string		m_name;
		size_t			m_dims[4];
		float			m_voxel_extent[3];
	};

	template<class T, class S>
	volume<T> gradient_volume(const volume<S>& other, size_t channel=0);

	template<class T>
	typename std::enable_if<std::is_integral<T>::value, T>::type encode(const double& v) {
		return T(std::numeric_limits<T>::min() + (std::min(1.0, std::max(-1.0, v))*0.5 + 0.5)*(std::numeric_limits<T>::max() - std::numeric_limits<T>::min()));
	}

	template<class T>
	typename std::enable_if<!std::is_integral<T>::value, T>::type encode(const double& v) {
		return T(v);
	}

	template<class T>
	typename std::enable_if<std::is_integral<T>::value, double>::type decode(const T& val) {
		return T(2.0*(double(val) - double(std::numeric_limits<T>::min())) / double(std::numeric_limits<T>::max() - std::numeric_limits<T>::min()) - 1.0);
	}

	template<class T>
	typename std::enable_if<!std::is_integral<T>::value, double>::type decode(const T& val) {
		return double(v);
	}

};

namespace std {
	template<class T>
	void swap(lux::volume<T>& lhs, lux::volume<T>& rhs) {
		lhs.swap(rhs);
	}
};

template<class T>
lux::volume<T>::volume(void) {
	for (size_t n = 0; n < 3; n++) {
		m_dims[n] = 0;
		m_voxel_extent[n] = T(1);
	}
	m_dims[3] = 0;
}

template<class T>
lux::volume<T>::volume(size_t dimx, size_t dimy, size_t dimz, size_t components) {
	for (size_t n = 0; n < 3; n++) {
		m_dims[n] = 0;
		m_voxel_extent[n] = T(1);
	}
	m_dims[3] = 0;
	resize(dimx, dimy, dimz, components);
}

template<class T>
lux::volume<T>::volume(const volume& other) {
	*this = other;
}

template<class T>
lux::volume<T>::~volume(void) {
	clear();
}

template<class T>
void lux::volume<T>::clear(void) {
	m_data.clear();
	m_name.clear();
	m_dims[0] = m_dims[1] = m_dims[2] = m_dims[3] = 0;
}

template<class T>
size_t lux::volume<T>::size(void) const {
	return m_data.size();
}

template<class T>
bool lux::volume<T>::empty(void) const {
	return m_data.empty();
}

template<class T>
void lux::volume<T>::resize(size_t dimx, size_t dimy, size_t dimz, size_t components) {
	size_t nsize = dimx*dimy*dimz*components;
	if (nsize == 0) {
		m_data.clear();
		m_dims[0] = m_dims[1] = m_dims[2] = m_dims[3] = 0;
		return;
	}
	if (nsize != m_data.size()) m_data.resize(nsize);
	m_dims[0] = dimx;
	m_dims[1] = dimy;
	m_dims[2] = dimz;
	m_dims[3] = components;
}

template<class T>
size_t lux::volume<T>::dim(size_t n) const {
	assert("lux::volume::dim -- invalid parameter" && n < 3);
	return m_dims[n];
}

template<class T>
size_t lux::volume<T>::components(void) const {
	return m_dims[3];
}

template<class T>
std::string& lux::volume<T>::name(void) {
	return m_name;
}

template<class T>
const std::string& lux::volume<T>::name(void) const {
	return m_name;
}

template<class T>
typename std::vector<T>::reference lux::volume<T>::operator()(size_t i, size_t j, size_t k, size_t c) {
	return m_data[linear_address(i, j, k, c)];
}

template<class T>
typename std::vector<T>::reference lux::volume<T>::operator[](size_t n) {
	assert("lux::volume[] -- invalid parameter" && n < m_data.size());
	return m_data[n];
}

template<class T>
typename std::vector<T>::const_reference lux::volume<T>::operator()(size_t i, size_t j, size_t k, size_t c) const {
	return m_data[linear_address(i, j, k, c)];
}

template<class T>
typename std::vector<T>::const_reference lux::volume<T>::operator[](size_t n) const {
	assert("lux::volume[] -- invalid parameter" && n < m_data.size());
	return m_data[n];
}

template<class T>
void lux::volume<T>::fill(const ranged_t& X, const ranged_t& Y, const ranged_t& Z, const fill_function& F) {
	static double buf[max_components];
	size_t addr = 0;
	for (size_t k = 0; k < m_dims[2]; k++) {
		double z = Z.map(k, m_dims[2]);
		for (size_t j = 0; j < m_dims[1]; j++) {
			double y = Y.map(j, m_dims[1]);
			for (size_t i = 0; i < m_dims[0]; i++) {
				double x = X.map(i, m_dims[0]);
				for (size_t c = 0; c < m_dims[3]; c++) buf[c] = double(m_data[addr + c]);
				F(buf, x, y, z, m_dims[3]);
				for (size_t c = 0; c < m_dims[3]; c++) m_data[addr + c] = T(buf[c]);
				addr += m_dims[3];
			}
		}
	}
}

template<class T>
void lux::volume<T>::swap(volume& other) {
	m_data.swap(other.m_data);
	m_name.swap(other.m_name);
	for (size_t n = 0; n < 3; n++) {
		std::swap(m_dims[n], other.m_dims[n]);
		std::swap(m_voxel_extent[n], other.m_voxel_extent[n]);
	}
	std::swap(m_dims[3], other.m_dims[3]);
}

template<class T>
lux::volume<T>& lux::volume<T>::operator=(const volume& other) {
	m_data = other.m_data;
	m_name = other.m_name;
	for (size_t n = 0; n < 3; n++) {
		m_dims[n] = other.m_dims[n];
		m_voxel_extent[n] = other.m_voxel_extent[n];
	}
	m_dims[3] = other.m_dims[3];
	return *this;
}

template<class T>
template<class S> lux::volume<S> lux::volume<T>::as(void) const {
	volume<S> result;
	result.name() = name();
	if (empty()) return result;
	result.resize(m_dims[0], m_dims[1], m_dims[2], m_dims[3]);
	result.set_voxel_extents(m_voxel_extent[0], m_voxel_extent[1], m_voxel_extent[2]);
	for (size_t n = 0; n < m_data.size(); n++) result[n] = S(m_data[n]);
	return result;
}
template<class T>
const T* lux::volume<T>::data(void) const {
	return m_data.data();
}

template<class T>
lux::image<T> lux::volume<T>::slice(size_t index, char dir) const {
	lux::image<T> result;
	if (empty()) return result;
	switch (dir) {
	case 'x': case 'X': // yz slice
		assert("lux::volume::slice -- invalid parameter" && index < m_dims[0]);
		result.resize(m_dims[1], m_dims[2], m_dims[3]);
		for (size_t j = 0; j < m_dims[2]; j++) {
			for (size_t i = 0; i < m_dims[1]; i++) {
				for (size_t c = 0; c < m_dims[3]; c++) result(i, j, c) = m_data[linear_address(index, i, j, c)];
			}
		}
		return result;
	case 'y': case 'Y': // zx slice
		assert("lux::volume::slice -- invalid parameter" && index < m_dims[1]);
		result.resize(m_dims[2], m_dims[0], m_dims[3]);
		for (size_t j = 0; j < m_dims[0]; j++) {
			for (size_t i = 0; i < m_dims[2]; i++) {
				for (size_t c = 0; c < m_dims[3]; c++) result(i, j, c) = m_data[linear_address(i, index, j, c)];
			}
		}
		return result;
	case 'z': case 'Z': // xy slice
		assert("lux::volume::slice -- invalid parameter" && index < m_dims[2]);
		result.resize(m_dims[0], m_dims[1], m_dims[3]);
		for (size_t j = 0; j < m_dims[1]; j++) {
			for (size_t i = 0; i < m_dims[0]; i++) {
				for (size_t c = 0; c < m_dims[3]; c++) result(i, j, c) = m_data[linear_address(i, j, index, c)];
			}
		}
		return result;
	default:
		assert("lux::volume::slice -- invalid parameter" && false);
		return result;
	}
}

template<class T>
void lux::volume<T>::set_voxel_extents(const float& sx, const float& sy, const float& sz) {
	m_voxel_extent[0] = sx;
	m_voxel_extent[1] = sy;
	m_voxel_extent[2] = sz;
}

template<class T>
float& lux::volume<T>::voxel_extent(size_t n) {
	assert("lux::volume::voxel_extent -- invalid parameter" && n < 3);
	return m_voxel_extent[n];
}

template<class T>
const float& lux::volume<T>::voxel_extent(size_t n) const {
	assert("lux::volume::voxel_extent -- invalid parameter" && n < 3);
	return m_voxel_extent[n];
}

template<class T, class S>
lux::volume<T> lux::gradient_volume(const volume<S>& other, size_t channel) {
	if (channel >= other.components() || other.empty()) return lux::volume<T>();
	lux::volume<T> result;
	result.resize(other.dim(0), other.dim(1), other.dim(2), 3);
	result.name() = std::string("grad ") + other.name();
	result.set_voxel_extents(other.voxel_extent(0), other.voxel_extent(1), other.voxel_extent(2));
	for (size_t k = 0; k < other.dim(2); k++) {
		size_t kl = k > 0 ? k - 1 : k;
		size_t kr = k + 1 < other.dim(2) ? k + 1 : k;
		for (size_t j = 0; j < other.dim(1); j++) {
			size_t jl = j > 0 ? j - 1 : j;
			size_t jr = j + 1 < other.dim(1) ? j + 1 : j;
			for (size_t i = 0; i < other.dim(0); i++) {
				size_t il = i > 0 ? i - 1 : i;
				size_t ir = i + 1 < other.dim(0) ? i + 1 : i;
				double g[3] = {
					(double(other(ir,j,k,channel)) - double(other(il,j,k,channel))) / double(ir - il),
					(double(other(i,jr,k,channel)) - double(other(i,jl,k,channel))) / double(jr - jl),
					(double(other(i,j,kr,channel)) - double(other(i,j,kl,channel))) / double(kr - kl)
				};
				double gn = g[0] * g[0] + g[1] * g[1] + g[2] * g[2];
				gn = (gn > 0.0 ? 1.0 / sqrt(gn) : 0.0);
				result(i, j, k, 0) = encode<T>(g[0]);
				result(i, j, k, 1) = encode<T>(g[1]);
				result(i, j, k, 2) = encode<T>(g[2]);
			}
		}
	}
	return result;
}

#endif

