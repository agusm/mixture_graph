#ifndef __LUX_CAMERA_H__
#define __LUX_CAMERA_H__

namespace lux {
	template<class T>
	class camera {
	public:
		typedef T base_type;
		camera(void);
		camera(const camera& other);
		~camera(void);
		camera& operator=(const camera& other);
		void pan(const T& dx, const T& dy);
		void zoom(const T& s);
		void reshape(size_t width, size_t height);
		void setFOV(const T& fov);
		void setNear(const T& n);
		void setFar(const T& f);
		void setUp(const lux::vec4<T>& u);
		void setEye(const lux::vec4<T>& e);
		void setCenter(const lux::vec4<T>& c);

		const T& getFOV(void) const;
		const T& getNear(void) const;
		const T& getFar(void) const;
		const lux::vec4<T>& getUp(void) const;
		const lux::vec4<T>& getEye(void) const;
		const lux::vec4<T>& getCenter(void)const;

		const lux::mat4<T>& getView(void);
		const lux::mat4<T>& getProjection(void);
	protected:
		T m_fov;
		T m_near;
		T m_far;
		size_t m_width, m_height;
		bool m_view_valid;
		bool m_projection_valid;
		lux::vec4<T> m_Eye, m_Center, m_Up;
		lux::mat4<T> m_View;
		lux::mat4<T> m_Projection;
	};

	typedef camera<float> cameraf;
	typedef camera<double> camerad;
};

template<class T>
lux::camera<T>::camera(void) :
	m_fov(45.0f), m_near(0.1f), m_far(100.0f),
	m_width(1), m_height(1),
	m_view_valid(false), m_projection_valid(false),
	m_Eye(lux::vec4f(T(0), T(0), T(-3), T(1))),
	m_Center(lux::vec4f(T(0), T(0), T(0), T(1))),
	m_Up(lux::vec4f(T(0), T(1),T(0),T(0))) 
{}

template<class T>
lux::camera<T>::camera(const camera& other) {
	*this = other;
}

template<class T>
lux::camera<T>::~camera(void) {
}

template<class T>
lux::camera<T>& lux::camera<T>::operator=(const camera& other) {
	m_fov = other.m_fov;
	m_near = other.m_near;
	m_far = other.m_far;
	m_width = other.m_width;
	m_height = other.m_height;
	m_view_valid = other.m_view_valid;
	m_projection_valid = other.m_projection_valid;
	m_Up = other.m_Up;
	m_Center = other.m_Center;
	m_Eye = other.m_Eye;
	m_Projection = other.m_Projection;
	m_View = other.m_View;
	return *this;
}

template<class T>
void lux::camera<T>::pan(const T& dx, const T& dy) {
	lux::vec4<T> e = m_Eye - m_Center;
	lux::vec4<T> right = e^m_Up;
	right.normalize(3);
	m_Eye += dx*right + dy*m_Up;
	m_Center += dx*right + dy*m_Up;
	m_view_valid = false;
}

template<class T>
void lux::camera<T>::zoom(const T& s) {
	m_Eye = (m_Eye - m_Center) / s;
	m_view_valid = false;
}

template<class T>
void lux::camera<T>::reshape(size_t width, size_t height) {
	m_width = width;
	m_height = height;
	m_projection_valid = false;
}

template<class T>
void lux::camera<T>::setFOV(const T& fov) {
	m_projection_valid = false;
	m_fov = fov;
}

template<class T>
void lux::camera<T>::setNear(const T& n) {
	m_projection_valid = false;
	m_near = n;
}

template<class T>
void lux::camera<T>::setFar(const T& f) {
	m_projection_valid = false;
	m_far = f;
}

template<class T>
void lux::camera<T>::setUp(const lux::vec4<T>& u) {
	m_view_valid = false;
	m_Up = u;
	m_Up.normalize(3);
}

template<class T>
void lux::camera<T>::setEye(const lux::vec4<T>& e) {
	m_view_valid = false;
	m_Eye = e;
}

template<class T>
void lux::camera<T>::setCenter(const lux::vec4<T>& c) {
	m_view_valid = false;
	m_Center = c;
}

template<class T>
const T& lux::camera<T>::getFOV(void) const {
	return m_fov;
}

template<class T>
const T& lux::camera<T>::getNear(void) const {
	return m_near;
}

template<class T>
const T& lux::camera<T>::getFar(void) const {
	return m_far;
}

template<class T>
const lux::vec4<T>& lux::camera<T>::getUp(void) const {
	return m_Up;
}

template<class T>
const lux::vec4<T>& lux::camera<T>::getEye(void) const {
	return m_Eye;
}

template<class T>
const lux::vec4<T>& lux::camera<T>::getCenter(void) const {
	return m_Center;
}

template<class T>
const lux::mat4<T>& lux::camera<T>::getView(void) {
	if (!m_view_valid) {
		m_View = lux::mat4<T>::Lookat(m_Eye, m_Center, m_Up);
		m_view_valid = true;
	}
	return m_View;
}

template<class T>
const lux::mat4<T>& lux::camera<T>::getProjection(void) {
	if (!m_projection_valid) {
		m_Projection = lux::mat4<T>::Perspective(m_fov, T(m_width) / T(m_height), m_near, m_far);
		m_projection_valid = true;
	}
	return m_Projection;
}

#endif
