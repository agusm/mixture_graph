#ifndef __LUX_PPM_H__
#define __LUX_PPM_H__

#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<inttypes.h>

namespace lux {
	namespace ppm {
		enum TYPE {
			INVALID,
			UINT8,
			UINT16
		};

		extern bool write(const std::string& name, const void* rgb_buffer, size_t width, size_t height, const ppm::TYPE& t, bool flip = true);
		extern bool write(FILE* stream, const void* rgb_buffer, size_t width, size_t height, const ppm::TYPE& t, bool flip = true);
		extern void* read(const std::string& name, size_t& width, size_t& height, ppm::TYPE& t, bool flip = true);
		extern void* read(FILE* stream, size_t& width, size_t& height, ppm::TYPE& t, bool flip = true);
	};
};

#endif
