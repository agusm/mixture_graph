#ifndef __LUX_INTERPOLATION_H__
#define __LUX_INTERPOLATION_H__

#include"lux_math.h"
#include<vector>

// TODO: cubic Spline with generic dimensions

namespace lux {
	template<class T> using control_type = typename std::vector<lux::vec4<T> >;
	
	// computes second-order derivatives for cubic spline interpolation. Each control point stores x,y,z,t
	// uses natural boundary conditions
	template<class T> control_type<T> cubic_spline(const control_type<T>& cp); 

	// cubic interpolation given control points and second derivatives
	template<class T> lux::vec4<T> cubic_int(const control_type<T>& cp, const control_type<T>& dt2, const T& at);

	// first oder derivative of cubic spline
	template<class T> lux::vec4<T> cubic_int_diff1(const control_type<T>& cp, const control_type<T>& dt2, const T& at);

	// second order derivative of cubic spline
	template<class T> lux::vec4<T> cubic_int_diff2(const control_type<T>& dt2, const T& at);

	// Frenet frame of cubic spline
	template<class T> lux::mat4<T> cubic_frenet_frame(const control_type<T>& cp, const control_type<T>& dt2, const T& at,
		lux::vec4<T>* position = nullptr, lux::vec4<T>* tangent = nullptr, lux::vec4<T>* normal = nullptr, lux::vec4<T>* binormal = nullptr);

	// Generic polynomial interpolator
	template<class T, size_t N> inline vec4<T> poly_interpolate(const vec4<T> cp[N], const T& at);
};

template<class T>
lux::control_type<T> lux::cubic_spline(const control_type<T>& cp) {
	size_t n = cp.size();
	control_type<T> result(cp.size());
	control_type<double> u(cp.size() - 1);
	result[0] = vec4<T>(T(0), T(0), T(0), T(0)); // natural boundaries
	u[0] = vec4d(0.0, 0.0, 0.0, 0.0);
	for (size_t i = 1; i < u.size(); i++) {
		double sig = (cp[i](3) - cp[i - 1](3)) / (cp[i + 1](3) - cp[i - 1](3));
		for (size_t c = 0; c < 3; c++) {
			double p = sig*result[i - 1](c) + 2.0;
			result[i](c) = T((sig - 1.0) / p);
			u[i](c) = (cp[i + 1](c) - cp[i](c)) / (cp[i + 1](3) - cp[i](3)) - (cp[i](c) - cp[i - 1](c)) / (cp[i](3) - cp[i - 1](3));
			u[i](c) = (6.0*u[i](c) / (cp[i + 1](3) - cp[i - 1](3)) - sig*u[i - 1](c)) / p;
		}
	}
	result.back() = vec4<T>(T(0), T(0), T(0), T(0));
	for (int64_t k = cp.size() - 2; k >= 0; k--) {
		for (size_t c = 0; c < 3; c++) {
			result[k](c) = result[k](c)*result[k + 1](c) + T(u[k](c));
		}
		result[k](3) = cp[k](3);
	}
	return result;
}

template<class T>
lux::vec4<T> lux::cubic_int(const control_type<T>& cp, const control_type<T>& dt2, const T& at) {
	size_t klo = 0;
	size_t khi = cp.size() - 1;
	while (khi - klo > 1) {
		size_t k = (khi + klo) >> 1;
		if (cp[k](3) > at) khi = k;
		else klo = k;
	}
	double h = cp[khi](3) - cp[klo](3);
	assert("lux::cubic_int -- invalid parameter" && h != T(0));
	double a = (cp[khi](3) - at) / h;
	double b = (at - cp[klo](3)) / h;
	lux::vec4<T> result = a*cp[klo] + b*cp[khi] + ((a*a*a - a)*dt2[klo] + (b*b*b - b)*dt2[khi])*(h*h) / 6.0;
	result(3) = at;
	return result;
}
template<class T>
lux::vec4<T> lux::cubic_int_diff1(const control_type<T>& cp, const control_type<T>& dt2, const T& at) {
	size_t klo = 0;
	size_t khi = cp.size() - 1;
	while (khi - klo > 1) {
		size_t k = (khi + klo) >> 1;
		if (cp[k](3) > at) khi = k;
		else klo = k;
	}
	double h = cp[khi](3) - cp[klo](3);
	assert("lux::cubic_int_diff1 -- invalid parameter" && h != T(0));
	double a = (cp[khi](3) - at) / h;
	double b = (at - cp[klo](3)) / h;
	lux::vec4<T> result = (cp[khi] - cp[klo]) / h + ((1.0 - 3.0*a*a)*dt2[klo] + (3.0*b*b - 1.0)*dt2[khi])*h / 6.0;
	result(3) = at;
	return result;
}

template<class T>
lux::vec4<T> lux::cubic_int_diff2(const control_type<T>& dt2, const T& at) {
	size_t klo = 0;
	size_t khi = dt2.size() - 1;
	while (khi - klo > 1) {
		size_t k = (khi + klo) >> 1;
		if (dt2[k](3) > at) khi = k;
		else klo = k;
	}
	double h = dt2[khi](3) - dt2[klo](3);
	assert("lux::cubic_int_diff2 -- invalid parameter" && h != T(0));
	double a = (at - dt2[klo](3)) / h;
	lux::vec4<T> result = dt2[klo] + a*(dt2[khi] - dt2[klo]);
	result(3) = at;
	return result;
}

template<class T>
lux::mat4<T> lux::cubic_frenet_frame(const control_type<T>& cp, const control_type<T>& dt2, const T& at,
	lux::vec4<T>* position, lux::vec4<T>* tangent, lux::vec4<T>* normal, lux::vec4<T>* binormal) {
	size_t klo = 0;
	size_t khi = cp.size() - 1;
	while (khi - klo > 1) {
		size_t k = (khi + klo) >> 1;
		if (cp[k](3) > at) khi = k;
		else klo = k;
	}
	double h = cp[khi](3) - cp[klo](3);
	assert("lux::cubic_frenet_frame -- invalid parameter" && h != T(0));
	// Position
	double a = (cp[khi](3) - at) / h;
	double b = (at - cp[klo](3)) / h;
	lux::vec4<T> P = a*cp[klo] + b*cp[khi] + ((a*a*a - a)*dt2[klo] + (b*b*b - b)*dt2[khi])*(h*h) / 6.0;
	P(3) = at;
	if (position != nullptr) *position = P;
	// Tangent
	lux::vec4<T> tng = (cp[khi] - cp[klo]) / h + ((1.0 - 3.0*a*a)*dt2[klo] + (3.0*b*b - 1.0)*dt2[khi])*h / 6.0;
	T len = tng.length(3);
	tng.normalize(3);
	tng(3) = len;
	// Normal
	lux::vec4<T> nrm = a*dt2[klo] + b*dt2[khi];
	len = nrm.length(3);
	if (len == T(0)) nrm = lux::vec4<T>(T(0), T(1), T(0),T(0)); // unfolded trajectory
	// Binormal, right-handed coordinate system
	lux::vec4<T> bnm = tng^nrm;
	bnm.normalize(3);
	nrm = bnm^tng;
	//nrm.normalize(3);
	nrm(3) = len;
	if (tangent != nullptr) *tangent = tng;
	if (normal != nullptr) *normal = nrm;
	if (binormal != nullptr) *binormal = bnm;
	
	// Build Frenet frame matrix
	mat4<T> R(
		tng(0), tng(1), tng(2), T(0),
		nrm(0), nrm(1), nrm(2), T(0),
		bnm(0), bnm(1), bnm(2), T(0),
		T(0), T(0), T(0), T(1)
	);
	return R;
}

template<class T, size_t N>
inline lux::vec4<T> lux::poly_interpolate(const vec4<T> cp[N], const T& at) {
	typedef typename Eigen::Matrix<T, N, N, Eigen::ColMajor, N, N> matrix_type;
	typedef typename Eigen::Matrix<T, N, 3, Eigen::ColMajor, N, 3> control_type;
	typedef typename Eigen::Matrix<T, 1, N, Eigen::RowMajor, 1, N> basis_type;
	typedef typename Eigen::Matrix<T, 3, 1, Eigen::ColMajor, 3, 1> result_type;
	// Build 3xN power basis
	basis_type p;
	p(0, 0) = T(1);
	for (size_t j = 1; j < N; j++) p(0, j) = p(0, j - 1)*at;

	matrix_type M;
	for (size_t i = 0; i < N; i++) {
		M(i, 0) = T(1);
		for (size_t j = 1; j < N; j++) M(i, j) = cp[i][3] * M(i, j - 1);
	}

	// LU decomposition
	Eigen::FullPivLU<matrix_type> LU(M);

	// right hand
	control_type b;
	for (size_t i = 0; i < N; i++) {
		for (size_t j = 0; j < 3; j++) b(i, j) = cp[i][j];
	}

	result_type r = p*LU.solve(b);
	return lux::vec4<T>(r(0), r(1), r(2), param);
}

#endif

