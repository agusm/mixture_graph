#ifndef __LUX_SHADER_H__
#define __LUX_SHADER_H__

#include"GL/glew.h"
#include<stdio.h>
#include<string>
#include<unordered_set>
#include"lux_math.h"
#include"lux_color.h"

#define AUTO_CONVERT_UNIFORMS

namespace lux {
	enum shader_source {
		SOURCE_TEXT,
		SOURCE_FILE
	};

	enum VENDOR {
		VENDOR_INTEL,
		VENDOR_NVIDIA,
		VENDOR_UNKNOWN
	};

	inline VENDOR get_vendor(void) {
		std::string v((const char*)glGetString(GL_VENDOR));
		if (v.find("NVIDIA") != std::string::npos) return VENDOR_NVIDIA;
		if (v.find("Intel") != std::string::npos) return VENDOR_INTEL;
		return VENDOR_UNKNOWN;
	}

	class shader {
	public:
		shader(void);
		shader(GLenum type, const char* source, const shader_source& s);
		~shader(void);
		bool compile(GLenum type, const char* source, const shader_source& s);
		const char* load_shader(const char* filename);
		void set_errstream(FILE* stream);
		void parse_log(FILE* errstream, const char* log, const char* source);
		inline operator const GLuint(void) const { return m_handle; }
		inline operator GLuint(void) { return m_handle; }
	protected:
		GLuint m_handle;
		FILE* m_error;
		GLenum m_type;
	private:
		shader(const shader& other);
	};

	class program {
	public:
		program(const std::string& name = std::string(""));
		~program(void);
		void detach_all(void);
		bool attach(const shader& handle);
		bool attach(const GLuint& handle);
		bool detach(const shader& handle);
		bool detach(const GLuint& handle);
		void set_errstream(FILE* stream);
		bool link(void);
		bool enable(void);
		bool disable(void);
		bool set_uniform(const std::string& name, float v0);
		bool set_uniform(const std::string& name, int v0);
		bool set_uniform(const std::string& name, uint32_t v0);
		bool set_uniform(const std::string& name, bool v0);

		bool set_uniform(const std::string& name, float v0, float v1);
		bool set_uniform(const std::string& name, int v0, int v1);
		bool set_uniform(const std::string& name, uint32_t v0, uint32_t v1);
		bool set_uniform(const std::string& name, bool v0, bool v1);

		bool set_uniform(const std::string& name, float v0, float v1, float v2);
		bool set_uniform(const std::string& name, int v0, int v1, int v2);
		bool set_uniform(const std::string& name, uint32_t v0, uint32_t v1, uint32_t v2);
		bool set_uniform(const std::string& name, bool v0, bool v1, bool v2);

		bool set_uniform(const std::string& name, float v0, float v1, float v2, float v3);
		bool set_uniform(const std::string& name, int v0, int v1, int v2, int v3);
		bool set_uniform(const std::string& name, uint32_t v0, uint32_t v1, uint32_t v2, uint32_t v3);
		bool set_uniform(const std::string& name, bool v0, bool v1, bool v2, bool v3);
		bool set_uniform(const std::string& name, const lux::vec4f& v);
		bool set_uniform(const std::string& name, const lux::vec4<int>& v);
		bool set_uniform(const std::string& name, const lux::vec4<uint32_t>& v);
		bool set_uniform(const std::string& name, const lux::color& clr);
		bool set_uniform(const std::string& name, const lux::mat4f& m, bool transpose = false);
		inline operator const GLuint(void) { return m_handle; }
		inline bool is_valid(void) { return m_valid; }
		inline std::string& name(void) { return m_name; }
		inline const std::string& name(void) const { return m_name; }
	protected:
		bool get_uniform(const std::string& name, GLint& loc, GLenum& type);

		std::unordered_set<GLuint> m_attached;
		FILE* m_error;
		bool m_valid;
		bool m_enabled;
		GLuint m_handle;
		std::string m_name;
	private:
		program(const program& other);
	};
};

#endif
