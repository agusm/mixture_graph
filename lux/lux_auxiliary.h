#ifndef __LUX_AUXILIARY_H__
#define __LUX_AUXILIARY_H__

#include<string>

namespace lux {
	extern bool file_exists(const std::string& name);
	extern size_t find_file_slot(const std::string& mask);
};

#endif
