#ifndef __LUX_MATH_H__
#define __LUX_MATH_H__

#include"Eigen\Core"
#include"Eigen\LU"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

namespace lux {
	template<class T> class vec4;
	template<class T> class vec3;
	template<class T> class quat4;

	template<class T>
	inline T deg2rad(const T& val) { return T(val*M_PI / 180.0); }

	template<class T>
	inline quat4<T> ternary_product(const quat4<T>& p, const quat4<T>& q, const quat4<T>& r, bool normalize=false) {
		vec3<T> x(p(0), q(0), r(0));
		vec3<T> y(p(1), q(1), r(1));
		vec3<T> z(p(2), q(2), r(2));
		vec3<T> w(p(3), q(3), r(3));
		
		vec3<T> zw = z^w;
		vec3<T> xy = x^y;
		quat4<T> result(y.dot(zw), -x.dot(zw), w.dot(xy), -z.dot(xy));
		if (normalize) result.normalize();
		return result;
	}

	template<class T>
	class mat4 : public Eigen::Matrix<T,4,4,Eigen::ColMajor,4,4> {
	public:
		typedef typename Eigen::Matrix<T, 4, 4, Eigen::ColMajor, 4, 4> base_type;
		
		// OpenGL export
		inline operator const T*(void) const { return base_type::data(); }

		// EigenStuff
		template<typename OtherDerived> mat4(const Eigen::MatrixBase<OtherDerived>& other) : base_type(other) {}
		template<typename OtherDerived> mat4& operator=(const Eigen::MatrixBase<OtherDerived>& other) { base_type::operator=(other); return *this; }

		inline quat4<T> asQuaternion(void) const; // rotation matrix as quaternion

		constexpr explicit mat4(
			const T& m11, const T& m12, const T& m13, const T& m14,
			const T& m21, const T& m22, const T& m23, const T& m24,
			const T& m31, const T& m32, const T& m33, const T& m34,
			const T& m41, const T& m42, const T& m43, const T& m44) {
			mat4<T>& M(*this);
			M(0, 0) = m11; M(0, 1) = m12; M(0, 2) = m13; M(0, 3) = m14;
			M(1, 0) = m21; M(1, 1) = m22; M(1, 2) = m23; M(1, 3) = m24;
			M(2, 0) = m31; M(2, 1) = m32; M(2, 2) = m33; M(2, 3) = m34;
			M(3, 0) = m41; M(3, 1) = m42; M(3, 2) = m43; M(3, 3) = m44;
		}

		constexpr mat4(void) : base_type() {}

		constexpr static mat4<T> Zero(void);
		constexpr static mat4<T> Identity(void);
		static mat4<T> Rotate(const T& angle, const lux::vec4<T>& axis);
		static mat4<T> Translate(const lux::vec4<T>& t);
		static mat4<T> Scale(const lux::vec4<T>& s);
		static mat4<T> Frustum(const T& l, const T& r, const T& b, const T& t, const T& n, const T& f);
		static mat4<T> Ortho(const T& l, const T& r, const T& b, const T& t, const T& n, const T& f);
		static mat4<T> Perspective(const T& fov, const T& aspect, const T& n, const T& f);
		static mat4<T> Lookat(const lux::vec4<T>& eye, const lux::vec4<T>& center, const vec4<T>& up);

		mat4<T> inverse(T* det = nullptr) const;
		mat4<T> inverse_transpose(T* det = nullptr) const;
	};

	
	typedef mat4<float> mat4f;
	typedef mat4<double> mat4d;

	template<class T>
	class vec4 : public Eigen::Matrix<T, 4, 1, Eigen::ColMajor, 4, 1> {
	public:
		typedef typename Eigen::Matrix<T, 4, 1, Eigen::ColMajor, 4, 1> base_type;

		// OpenGL export
		inline operator const T*(void) const { return base_type::data(); }

		// EigenStuff
		template<typename OtherDerived> vec4(const Eigen::MatrixBase<OtherDerived>& other) : base_type(other) {}
		template<typename OtherDerived> vec4& operator=(const Eigen::MatrixBase<OtherDerived>& other) { base_type::operator=(other); return *this; }

		constexpr vec4(void) : base_type() {}
		constexpr vec4(const T& x, const T& y, const T& z, const T& w) : base_type(x, y, z, w) {}

		static const vec4<T> Zero;

		vec4 operator^(const vec4& other) const;
		void normalize(size_t dim=4);
		T length2(size_t dim = 4) const;
		inline T length(size_t dim = 4) const;
		template<class S>
		inline vec4<S> as(void) const {
			const vec4<T>& v(*this);
			return vec4<S>(S(v(0)), S(v(1)), S(v(2)), S(v(3)));
		}
	};
	typedef vec4<float> vec4f;
	typedef vec4<double> vec4d;

	template<class T>
	class vec3 : public Eigen::Matrix<T, 3, 1, Eigen::ColMajor, 3, 1> {
	public:
		typedef typename Eigen::Matrix<T, 3, 1, Eigen::ColMajor, 3, 1> base_type;

		// OpenGL export
		inline operator const T*(void) const { return base_type::data(); }

		// EigenStuff
		template<typename OtherDerived> vec3(const Eigen::MatrixBase<OtherDerived>& other) : base_type(other) {}
		template<typename OtherDerived> vec3& operator=(const Eigen::MatrixBase<OtherDerived>& other) { base_type::operator=(other); return *this; }

		constexpr vec3(void) : base_type() {}
		constexpr vec3(const T& x, const T& y, const T& z) : base_type(x, y, z) {}

		static const vec3<T> Zero;

		inline vec3 operator^(const vec3& other) const {
			const vec3<T>& v(*this);
			return vec3<T>(
				v(1)*other(2) - v(2)*other(1),
				v(2)*other(0) - v(0)*other(2),
				v(0)*other(1) - v(1)*other(0)
				);
		}
		inline void normalize(size_t dim = 3) {
			double len = length2(dim);
			if (len == 0.0) return;
			T nrm = T(1.0 / ::sqrt(len));
			for (size_t d = 0; d < dim; d++) (*this)(d) *= nrm;
		}
		inline T length2(size_t dim = 3) const {
			T len = T(0);
			dim = std::min(dim, size_t(3));
			for (size_t d = 0; d < dim; d++) len += (*this)(d)*(*this)(d);
			return len;

		}
		inline T length(size_t dim = 3) const {
			return ::sqrt(length2(dim));
		}
	};
	typedef vec3<float> vec3f;
	typedef vec3<double> vec3d;

	template<class T>
	class quat4 : public Eigen::Matrix<T, 4, 1, Eigen::ColMajor, 4, 1> {
	public:
		typedef typename Eigen::Matrix<T, 4, 1, Eigen::ColMajor, 4, 1> base_type;

		// OpenGL export
		inline operator const T*(void) const { return base_type::data(); }

		// EigenStuff
		template<typename OtherDerived> quat4(const Eigen::MatrixBase<OtherDerived>& other) : base_type(other) {}
		template<typename OtherDerived> quat4& operator=(const Eigen::MatrixBase<OtherDerived>& other) { base_type::operator=(other); return *this; }

		constexpr quat4(void) : base_type() {}
		constexpr explicit quat4(const T& r, const T& i1, const T& i2, const T& i3) : base_type(r, i1, i2, i3) {}

		quat4<T> operator*(const quat4<T>& other) const;
		quat4<T>& operator*=(const quat4<T>& other);
		quat4<T> conjugate(void) const;

		inline T& real(void) { return (*this)(0); }
		inline const T& real(void) const { return (*this)(0); }
		inline T& imag(size_t n) {
			assert("lux::quat4::imag -- invalid parameter" && n < 3);
			return (*this)(n + 1);
		}
		inline const T& imag(size_t n) const {
			assert("lux::quat4::imag -- invalid parameter" && n < 3);
			return (*this)(n + 1);
		}
		inline T length2(void) const {
			const quat4<T>& Q(*this);
			return Q(0)*Q(0) + Q(1)*Q(1) + Q(2)*Q(2) + Q(3)*Q(3);
		}
		inline T length(void) const {
			return ::sqrt(length2());
		}
		inline void normalize(void) {
			double len = length2();
			if (len == 0.0) return;
			T nrm = T(1.0 / ::sqrt(len));
			quat4<T>& Q(*this);
			for (int i = 0; i < 4; i++) Q(i) *= nrm;
		}
		inline T dot(const quat4<T>& other) const {
			const quat4<T>& Q(*this);
			return Q(0)*other(0) + Q(1)*other(1) + Q(2)*other(2) + Q(3)*other(3);
		}
		mat4<T> asMatrix(void) const;
	};

	typedef quat4<float> quat4f;
	typedef quat4<double> quat4d;
};

template<class T>
inline lux::quat4<T> lux::mat4<T>::asQuaternion(void) const {
	quat4<T> result;
	const mat4<T>& M(*this);
	result.real() = T(0.5*::sqrt(std::max(0.0, 1.0 + M(0,0) + M(1,1) + M(2,2))));
	result.imag(0) = T(0.5*::sqrt(std::max(0.0, 1.0 + M(0,0) - M(1,1) - M(2,2))));
	result.imag(1) = T(0.5*::sqrt(std::max(0.0, 1.0 - M(0,0) + M(1,1) - M(2,2))));
	result.imag(2) = T(0.5*::sqrt(std::max(0.0, 1.0 - M(0,0) - M(1,1) + M(2,2))));
	result.imag(0) = lux::copysign(result.imag(0), M(2,1) - M(1,2));
	result.imag(1) = lux::copysign(result.imag(1), M(0,2) - M(2,0));
	result.imag(2) = lux::copysign(result.imag(2), M(1,0) - M(0,1));
	result.real() = ::sqrt(std::max(T(0), T(1) - result.imag(0)*result.imag(0) - result.imag(1)*result.imag(1) - result.imag(2)*result.imag(2)));
	return result;
}

template<class T>
const lux::vec4<T> lux::vec4<T>::Zero(T(0), T(0), T(0), T(0));

template<class T>
T lux::vec4<T>::length2(size_t dim) const {
	T result = 0.0;
	dim = std::min(dim, size_t(4));
	for (size_t n = 0; n < dim; n++) result += (*this)(n)*(*this)(n);
	return result;
}

template<class T>
inline T lux::vec4<T>::length(size_t dim) const {
	return ::sqrt(length2(dim));
}

template<class T>
lux::quat4<T> lux::quat4<T>::operator*(const quat4<T>& q) const {
	const quat4<T>& p(*this);
	return quat4<T>(
		p(0)*q(0) - p(1)*q(1) - p(2)*q(2) - p(3)*q(3),
		p(0)*q(1) + p(1)*q(0) + p(2)*q(3) - p(3)*q(2),
		p(0)*q(2) - p(1)*q(3) + p(2)*q(0) + p(3)*q(1),
		p(0)*q(3) + p(1)*q(2) - p(2)*q(1) + p(3)*q(0)
	);
}

template<class T>
lux::mat4<T> lux::quat4<T>::asMatrix(void) const {
	const quat4<T>& q(*this);
	return lux::mat4<T>(
		T(1) - T(2) * (q(2)*q(2) + q(3)*q(3)),	T(2) * (q(1) * q(2) - q(3) * q(0)),			T(2) * (q(1) * q(3) + q(2) * q(0)),			T(0),
		T(2) * (q(1) * q(2) + q(3) * q(0)),		T(1) - T(2) * (q(1) * q(1) + q(3) * q(3)),	T(2) * (q(2) * q(3) - q(1) * q(0)),			T(0),
		T(2) * (q(1) * q(3) - q(2) * q(0)),		T(2) * (q(2) * q(3) + q(1) * q(0)),			T(1) - T(2) * (q(1) * q(1) + q(2) * q(2)),	T(0),
		T(0), T(0), T(0), T(1)
	);
}

template<class T>
lux::quat4<T>& lux::quat4<T>::operator*=(const quat4<T>& q) {
	*this = *this * q;
	return *this;
}

template<class T>
lux::quat4<T> lux::quat4<T>::conjugate(void) const {
	return lux::quat4<T>((*this)(0), -(*this)(1), -(*this)(2), -(*this)(3));
}

template<class T>
lux::vec4<T> lux::vec4<T>::operator^(const vec4& other) const {
	const vec4<T>& u(*this);
	return lux::vec4<T>(
		u(1)*other(2) - u(2)*other(1),
		u(2)*other(0) - u(0)*other(2),
		u(0)*other(1) - u(1)*other(0),
		T(0)
		);
}

template<class T>
void lux::vec4<T>::normalize(size_t dim) {
	dim = std::min(dim, size_t(4));
	double nrm = 0.0;
	for (size_t n = 0; n < dim; n++) nrm += double((*this)(n)*(*this)(n));
	T sc = T(nrm > 0.0 ? 1.0 / ::sqrt(nrm) : 0.0);
	for (size_t n = 0; n < dim; n++) (*this)(n) *= sc;
}

template<class T>
constexpr lux::mat4<T> lux::mat4<T>::Zero(void) {
	return lux::mat4<T>(
		T(0), T(0), T(0), T(0), 
		T(0), T(0), T(0), T(0), 
		T(0), T(0), T(0), T(0), 
		T(0), T(0), T(0), T(0));
}

template<class T>
constexpr lux::mat4<T> lux::mat4<T>::Identity(void) {
	return lux::mat4<T>(
		T(1), T(0), T(0), T(0),
		T(0), T(1), T(0), T(0),
		T(0), T(0), T(1), T(0),
		T(0), T(0), T(0), T(1));
}

template<class T>
lux::mat4<T> lux::mat4<T>::Rotate(const T& angle, const lux::vec4<T>& axis) {
	T cs = ::cos(deg2rad(angle));
	T sn = ::sqrt(T(1) - cs*cs);
	T c = T(1) - cs;
	lux::vec4<T> u(axis);
	u.normalize(3);
	return lux::mat4<T>(
		c*u(0)*u(0)+cs, c*u(0)*u(1) - sn*u(2), c*u(0)*u(2)+sn*u(1), T(0),
		c*u(0)*u(1)+sn*u(2), c*u(1)*u(1)+cs, c*u(1)*u(2)-sn*u(0), T(0),
		c*u(0)*u(2)-sn*u(1), c*u(1)*u(2)+sn*u(0), c*u(2)*u(2)+cs, T(0),
		T(0), T(0), T(0), T(1)
		);
}
template<class T>
lux::mat4<T> lux::mat4<T>::Translate(const lux::vec4<T>& t) {
	return lux::mat4<T>(
		T(1), T(0), T(0), t(0),
		T(0), T(1), T(0), t(1),
		T(0), T(0), T(1), t(2),
		T(0), T(0), T(0), T(1)
		);
}
template<class T>
lux::mat4<T> lux::mat4<T>::Scale(const lux::vec4<T>& s) {
	return lux::mat4<T>(
		s(0), T(0), T(0), T(0),
		T(0), s(1), T(0), T(0),
		T(0), T(0), s(2), T(0),
		T(0), T(0), T(0), T(1)
		);
}
template<class T>
lux::mat4<T> lux::mat4<T>::Frustum(const T& l, const T& r, const T& b, const T& t, const T& n, const T& f) {
	T rl = T(1.0 / (r - l));
	T tb = T(1.0 / (t - b));
	T nf = T(1.0 / (n - f));
	return lux::mat4<T>(
		T(2)*n*rl, T(0), (r + l)*rl, T(0),
		T(0), T(2)*n*tb, (t + b)*tb, T(0),
		T(0), T(0), (f + n)*nf, T(2)*f*n*nf,
		T(0), T(0), T(-1), T(0)
		);
}
template<class T>
lux::mat4<T> lux::mat4<T>::Ortho(const T& l, const T& r, const T& b, const T& t, const T& n, const T& f) {
	T rl = T(1.0 / (r - l));
	T tb = T(1.0 / (t - b));
	T nf = T(1.0 / (n - f));
	return lux::mat4<T>(
		T(2)*rl, T(0), T(0), -(r + l)*rl,
		T(0), T(2)*tb, T(0), -(t + b)*tb,
		T(0), T(0), T(2)*nf, (f + n)*nf,
		T(0), T(0), T(0), T(1)
		);
}

template<class T>
lux::mat4<T> lux::mat4<T>::Perspective(const T& fov, const T& aspect, const T& n, const T& f) {
	T y = n*tan(deg2rad(fov*T(0.5)));
	T x = y*aspect;
	return Frustum(-x, x, -y, y, n, f);
}

template<class T>
lux::mat4<T> lux::mat4<T>::Lookat(const lux::vec4<T>& eye, const lux::vec4<T>& center, const vec4<T>& up) {
	lux::vec4<T> fwd = center - eye;
	fwd.normalize(3);
	lux::vec4<T> right = fwd^up;
	right.normalize(3);
	lux::vec4<T> nup = right^fwd;
	return mat4<T>(
		right(0), nup(0), -fwd(0), T(0),
		right(1), nup(1), -fwd(1), T(0),
		right(2), nup(2), -fwd(2), T(0),
		T(0), T(0), T(0), T(1)
		)*Translate(-eye);
}


template<class T>
lux::mat4<T> lux::mat4<T>::inverse(T* det) const {
	base_type inv;
	T D;
	bool invertible;
	this->computeInverseAndDetWithCheck(inv,D,invertible);
	if (det) *det = D;
	return lux::mat4<T>(inv);
}


template<class T>
lux::mat4<T> lux::mat4<T>::inverse_transpose(T* det) const {
	lux::mat4<T> M = inverse(det);
	M.transposeInPlace();
	return M;
}
#endif
