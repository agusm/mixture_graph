#include"lux_color.h"
#include<algorithm>

const lux::color lux::color::Zero(0.0f, 0.0f, 0.0f, 0.0f);

//std::unordered_map<std::string, std::vector<lux::color> > lux::colormap::m_database;

lux::colormap::colormap(void) {
	if (m_database.empty()) init();
	m_data = m_database.cend();
}

lux::colormap::colormap(const std::string& name) {
	if (m_database.empty()) init();
	std::string table = name;
	std::transform(table.begin(), table.end(), table.begin(), ::toupper);
	m_data = m_database.find(table);

}

lux::colormap::~colormap(void) {
	m_data = m_database.end();
}

lux::colormap::colormap(const colormap& other) {
	*this = other;
}

// STATIC ACCESS
bool lux::colormap::has_map(const std::string& name) {
	if (m_database.empty()) init();
	std::string table = name;
	std::transform(table.begin(), table.end(), table.begin(), ::toupper);
	return m_database.find(table) != m_database.cend();
}

size_t lux::colormap::map_size(const std::string& name) {
	if (m_database.empty()) init();
	std::string table = name;
	std::transform(table.begin(), table.end(), table.begin(), ::toupper);
	const auto it = m_database.find(table);
	return (it == m_database.cend() ? 0 : it->second.size());
}

const lux::color& lux::colormap::get_color(const std::string& name, size_t n) {
	if (m_database.empty()) init();
	std::string table = name;
	std::transform(table.begin(), table.end(), table.begin(), ::toupper);
	const auto it = m_database.find(table);
	assert("colormap::get_color -- invalid colormap" && it != m_database.cend());
	assert("colormap::get_color -- invalid index" && n < it->second.size());
	return it->second[n];
}

// initializer
void lux::colormap::init(void) {
	m_database.clear();
	// 01. Accents
	static const color accent3[] = {
		color(uint8_t(127u), 201, 127, 255),
		color(uint8_t(190), 174, 212, 255),
		color(uint8_t(253), 192, 134, 255)
	};
	static const color accent4[] = {
		color(uint8_t(127), 201, 127, 255),
		color(uint8_t(190), 174, 212, 255),
		color(uint8_t(253), 192, 134, 255),
		color(uint8_t(255), 255, 153, 255)
	};
	static const color accent5[] = {
		color(uint8_t(127), 201, 127, 255),
		color(uint8_t(190), 174, 212, 255),
		color(uint8_t(253), 192, 134, 255),
		color(uint8_t(255), 255, 153, 255),
		color(uint8_t(56), 108, 176, 255)
	};
	static const color accent6[] = {
		color(uint8_t(127), 201, 127, 255),
		color(uint8_t(190), 174, 212, 255),
		color(uint8_t(253), 192, 134, 255),
		color(uint8_t(255), 255, 153, 255),
		color(uint8_t(56), 108, 176, 255),
		color(uint8_t(240), 2, 127, 255)
	};
	static const color accent7[] = {
		color(uint8_t(127), 201, 127, 255),
		color(uint8_t(190), 174, 212, 255),
		color(uint8_t(253), 192, 134, 255),
		color(uint8_t(255), 255, 153, 255),
		color(uint8_t(56), 108, 176, 255),
		color(uint8_t(240), 2, 127, 255),
		color(uint8_t(191), 91, 23, 255)
	};
	static const color accent8[] = {
		color(uint8_t(127), 201, 127, 255),
		color(uint8_t(190), 174, 212, 255),
		color(uint8_t(253), 192, 134, 255),
		color(uint8_t(255), 255, 153, 255),
		color(uint8_t(56), 108, 176, 255),
		color(uint8_t(240), 2, 127, 255),
		color(uint8_t(191), 91, 23, 255),
		color(uint8_t(102), 102, 102, 255)
	};
	m_database["ACCENT3"] = std::vector<color>(accent3, accent3 + sizeof(accent3) / sizeof(color));
	m_database["ACCENT4"] = std::vector<color>(accent4, accent4 + sizeof(accent4) / sizeof(color));
	m_database["ACCENT5"] = std::vector<color>(accent5, accent5 + sizeof(accent5) / sizeof(color));
	m_database["ACCENT6"] = std::vector<color>(accent6, accent6 + sizeof(accent6) / sizeof(color));
	m_database["ACCENT7"] = std::vector<color>(accent7, accent7 + sizeof(accent7) / sizeof(color));
	m_database["ACCENT8"] = std::vector<color>(accent8, accent8 + sizeof(accent8) / sizeof(color));

	// 02. Blues
	static const color blues3[] = {
		color(uint8_t(222), 235, 247, 255),
		color(uint8_t(158), 202, 225, 255),
		color(uint8_t(49), 130, 189, 255)
	};
	static const color blues4[] = {
		color(uint8_t(239), 243, 255, 255),
		color(uint8_t(189), 215, 231, 255),
		color(uint8_t(107), 174, 214, 255),
		color(uint8_t(33), 113, 181, 255)
	};
	static const color blues5[] = {
		color(uint8_t(239), 243, 255, 255),
		color(uint8_t(189), 215, 231, 255),
		color(uint8_t(107), 174, 214, 255),
		color(uint8_t(49), 130, 189, 255),
		color(uint8_t(8), 81, 156, 255)
	};
	static const color blues6[] = {
		color(uint8_t(239), 243, 255, 255),
		color(uint8_t(198), 219, 239, 255),
		color(uint8_t(158), 202, 225, 255),
		color(uint8_t(107), 174, 214, 255),
		color(uint8_t(49), 130, 189, 255),
		color(uint8_t(8), 81, 156, 255)
	};
	static const color blues7[] = {
		color(uint8_t(239), 243, 255, 255),
		color(uint8_t(198), 219, 239, 255),
		color(uint8_t(158), 202, 225, 255),
		color(uint8_t(107), 174, 214, 255),
		color(uint8_t(66), 146, 198, 255),
		color(uint8_t(33), 113, 181, 255),
		color(uint8_t(8), 69, 148, 255)
	};
	static const color blues8[] = {
		color(uint8_t(247), 251, 255, 255),
		color(uint8_t(222), 235, 247, 255),
		color(uint8_t(198), 219, 239, 255),
		color(uint8_t(158), 202, 225, 255),
		color(uint8_t(107), 174, 214, 255),
		color(uint8_t(66), 146, 198, 255),
		color(uint8_t(33), 113, 181, 255),
		color(uint8_t(8), 69, 148, 255)
	};
	static const color blues9[] = {
		color(uint8_t(247), 251, 255, 255),
		color(uint8_t(222), 235, 247, 255),
		color(uint8_t(198), 219, 239, 255),
		color(uint8_t(158), 202, 225, 255),
		color(uint8_t(107), 174, 214, 255),
		color(uint8_t(66), 146, 198, 255),
		color(uint8_t(33), 113, 181, 255),
		color(uint8_t(8), 81, 156, 255),
		color(uint8_t(8), 48, 107, 255)
	};
	m_database["BLUES3"] = std::vector<color>(blues3, blues3 + sizeof(blues3) / sizeof(color));
	m_database["BLUES4"] = std::vector<color>(blues4, blues4 + sizeof(blues4) / sizeof(color));
	m_database["BLUES5"] = std::vector<color>(blues5, blues5 + sizeof(blues5) / sizeof(color));
	m_database["BLUES6"] = std::vector<color>(blues6, blues6 + sizeof(blues6) / sizeof(color));
	m_database["BLUES7"] = std::vector<color>(blues7, blues7 + sizeof(blues7) / sizeof(color));
	m_database["BLUES8"] = std::vector<color>(blues8, blues8 + sizeof(blues8) / sizeof(color));
	m_database["BLUES9"] = std::vector<color>(blues9, blues9 + sizeof(blues9) / sizeof(color));

	// 03. BrBG
	static const color brbg3[] = {
		color(uint8_t(216), 179, 101, 255),
		color(uint8_t(245), 245, 245, 255),
		color(uint8_t(90), 180, 172, 255)
	};
	static const color brbg4[] = {
		color(uint8_t(166), 97, 26, 255),
		color(uint8_t(223), 194, 125, 255),
		color(uint8_t(128), 205, 193, 255),
		color(uint8_t(1), 133, 113, 255)
	};
	static const color brbg5[] = {
		color(uint8_t(166), 97, 26, 255),
		color(uint8_t(223), 194, 125, 255),
		color(uint8_t(245), 245, 245, 255),
		color(uint8_t(128), 205, 193, 255),
		color(uint8_t(1), 133, 113, 255)
	};
	static const color brbg6[] = {
		color(uint8_t(140), 81, 10, 255),
		color(uint8_t(216), 179, 101, 255),
		color(uint8_t(246), 232, 195, 255),
		color(uint8_t(199), 234, 229, 255),
		color(uint8_t(90), 180, 172, 255),
		color(uint8_t(1), 102, 94, 255)
	};
	static const color brbg7[] = {
		color(uint8_t(140), 81, 10, 255),
		color(uint8_t(216), 179, 101, 255),
		color(uint8_t(246), 232, 195, 255),
		color(uint8_t(245), 245, 245, 255),
		color(uint8_t(199), 234, 229, 255),
		color(uint8_t(90), 180, 172, 255),
		color(uint8_t(1), 102, 94, 255)
	};
	static const color brbg8[] = {
		color(uint8_t(140), 81, 10, 255),
		color(uint8_t(191), 129, 45, 255),
		color(uint8_t(223), 194, 125, 255),
		color(uint8_t(246), 232, 195, 255),
		color(uint8_t(199), 234, 229, 255),
		color(uint8_t(128), 205, 193, 255),
		color(uint8_t(53), 151, 143, 255),
		color(uint8_t(1), 102, 94, 255)
	};
	static const color brbg9[] = {
		color(uint8_t(140), 81, 10, 255),
		color(uint8_t(191), 129, 45, 255),
		color(uint8_t(223), 194, 125, 255),
		color(uint8_t(246), 232, 195, 255),
		color(uint8_t(245), 245, 245, 255),
		color(uint8_t(199), 234, 229, 255),
		color(uint8_t(128), 205, 193, 255),
		color(uint8_t(53), 151, 143, 255),
		color(uint8_t(1), 102, 94, 255)
	};
	static const color brbg10[] = {
		color(uint8_t(84), 48, 5, 255),
		color(uint8_t(140), 81, 10, 255),
		color(uint8_t(191), 129, 45, 255),
		color(uint8_t(223), 194, 125, 255),
		color(uint8_t(246), 232, 195, 255),
		color(uint8_t(199), 234, 229, 255),
		color(uint8_t(128), 205, 193, 255),
		color(uint8_t(53), 151, 143, 255),
		color(uint8_t(1), 102, 94, 255),
		color(uint8_t(0), 60, 48, 255)
	};
	static const color brbg11[] = {
		color(uint8_t(84), 48, 5, 255),
		color(uint8_t(140), 81, 10, 255),
		color(uint8_t(191), 129, 45, 255),
		color(uint8_t(223), 194, 125, 255),
		color(uint8_t(246), 232, 195, 255),
		color(uint8_t(245), 245, 245, 255),
		color(uint8_t(199), 234, 229, 255),
		color(uint8_t(128), 205, 193, 255),
		color(uint8_t(53), 151, 143, 255),
		color(uint8_t(1), 102, 94, 255),
		color(uint8_t(0), 60, 48, 255)
	};
	m_database["BRBG3"] = std::vector<color>(brbg3, brbg3 + sizeof(brbg3) / sizeof(color));
	m_database["BRBG4"] = std::vector<color>(brbg4, brbg4 + sizeof(brbg4) / sizeof(color));
	m_database["BRBG5"] = std::vector<color>(brbg5, brbg5 + sizeof(brbg5) / sizeof(color));
	m_database["BRBG6"] = std::vector<color>(brbg6, brbg6 + sizeof(brbg6) / sizeof(color));
	m_database["BRBG7"] = std::vector<color>(brbg7, brbg7 + sizeof(brbg7) / sizeof(color));
	m_database["BRBG8"] = std::vector<color>(brbg8, brbg8 + sizeof(brbg8) / sizeof(color));
	m_database["BRBG9"] = std::vector<color>(brbg9, brbg9 + sizeof(brbg9) / sizeof(color));
	m_database["BRBG10"] = std::vector<color>(brbg10, brbg10 + sizeof(brbg10) / sizeof(color));
	m_database["BRBG11"] = std::vector<color>(brbg11, brbg11 + sizeof(brbg11) / sizeof(color));

	// 04. BuGn
	static const color bugn3[] = {
		color(uint8_t(229), 245, 249, 255),
		color(uint8_t(153), 216, 201, 255),
		color(uint8_t(44), 162, 95, 255)
	};
	static const color bugn4[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(178), 226, 226, 255),
		color(uint8_t(102), 194, 164, 255),
		color(uint8_t(35), 139, 69, 255)
	};
	static const color bugn5[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(178), 226, 226, 255),
		color(uint8_t(102), 194, 164, 255),
		color(uint8_t(44), 162, 95, 255),
		color(uint8_t(0), 109, 44, 255)
	};
	static const color bugn6[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(204), 236, 230, 255),
		color(uint8_t(153), 216, 201, 255),
		color(uint8_t(102), 194, 164, 255),
		color(uint8_t(44), 162, 95, 255),
		color(uint8_t(0), 109, 44, 255)
	};
	static const color bugn7[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(204), 236, 230, 255),
		color(uint8_t(153), 216, 201, 255),
		color(uint8_t(102), 194, 164, 255),
		color(uint8_t(65), 174, 118, 255),
		color(uint8_t(35), 139, 69, 255),
		color(uint8_t(0), 88, 36, 255)
	};
	static const color bugn8[] = {
		color(uint8_t(247), 252, 253, 255),
		color(uint8_t(229), 245, 249, 255),
		color(uint8_t(204), 236, 230, 255),
		color(uint8_t(153), 216, 201, 255),
		color(uint8_t(102), 194, 164, 255),
		color(uint8_t(65), 174, 118, 255),
		color(uint8_t(35), 139, 69, 255),
		color(uint8_t(0), 88, 36, 255)
	};
	static const color bugn9[] = {
		color(uint8_t(247), 252, 253, 255),
		color(uint8_t(229), 245, 249, 255),
		color(uint8_t(204), 236, 230, 255),
		color(uint8_t(153), 216, 201, 255),
		color(uint8_t(102), 194, 164, 255),
		color(uint8_t(65), 174, 118, 255),
		color(uint8_t(35), 139, 69, 255),
		color(uint8_t(0), 109, 44, 255),
		color(uint8_t(0), 68, 27, 255)
	};
	m_database["BUGN3"] = std::vector<color>(bugn3, bugn3 + sizeof(bugn3) / sizeof(color));
	m_database["BUGN4"] = std::vector<color>(bugn4, bugn4 + sizeof(bugn4) / sizeof(color));
	m_database["BUGN5"] = std::vector<color>(bugn5, bugn5 + sizeof(bugn5) / sizeof(color));
	m_database["BUGN6"] = std::vector<color>(bugn6, bugn6 + sizeof(bugn6) / sizeof(color));
	m_database["BUGN7"] = std::vector<color>(bugn7, bugn7 + sizeof(bugn7) / sizeof(color));
	m_database["BUGN8"] = std::vector<color>(bugn8, bugn8 + sizeof(bugn8) / sizeof(color));
	m_database["BUGN9"] = std::vector<color>(bugn9, bugn9 + sizeof(bugn9) / sizeof(color));

	// 05. BuPu
	static const color bupu3[] = {
		color(uint8_t(224), 236, 244, 255),
		color(uint8_t(158), 188, 218, 255),
		color(uint8_t(136), 86, 167, 255)
	};
	static const color bupu4[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(140), 150, 198, 255),
		color(uint8_t(136), 65, 157, 255)
	};
	static const color bupu5[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(140), 150, 198, 255),
		color(uint8_t(136), 86, 167, 255),
		color(uint8_t(129), 15, 124, 255)
	};
	static const color bupu6[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(191), 211, 230, 255),
		color(uint8_t(158), 188, 218, 255),
		color(uint8_t(140), 150, 198, 255),
		color(uint8_t(136), 86, 167, 255),
		color(uint8_t(129), 15, 124, 255)
	};
	static const color bupu7[] = {
		color(uint8_t(237), 248, 251, 255),
		color(uint8_t(191), 211, 230, 255),
		color(uint8_t(158), 188, 218, 255),
		color(uint8_t(140), 150, 198, 255),
		color(uint8_t(140), 107, 177, 255),
		color(uint8_t(136), 65, 157, 255),
		color(uint8_t(110), 1, 107, 255)
	};
	static const color bupu8[] = {
		color(uint8_t(247), 252, 253, 255),
		color(uint8_t(224), 236, 244, 255),
		color(uint8_t(191), 211, 230, 255),
		color(uint8_t(158), 188, 218, 255),
		color(uint8_t(140), 150, 198, 255),
		color(uint8_t(140), 107, 177, 255),
		color(uint8_t(136), 65, 157, 255),
		color(uint8_t(110), 1, 107, 255)
	};
	static const color bupu9[] = {
		color(uint8_t(247), 252, 253, 255),
		color(uint8_t(224), 236, 244, 255),
		color(uint8_t(191), 211, 230, 255),
		color(uint8_t(158), 188, 218, 255),
		color(uint8_t(140), 150, 198, 255),
		color(uint8_t(140), 107, 177, 255),
		color(uint8_t(136), 65, 157, 255),
		color(uint8_t(129), 15, 124, 255),
		color(uint8_t(77), 0, 75, 255)
	};
	m_database["BUPU3"] = std::vector<color>(bupu3, bupu3 + sizeof(bupu3) / sizeof(color));
	m_database["BUPU4"] = std::vector<color>(bupu4, bupu4 + sizeof(bupu4) / sizeof(color));
	m_database["BUPU5"] = std::vector<color>(bupu5, bupu5 + sizeof(bupu5) / sizeof(color));
	m_database["BUPU6"] = std::vector<color>(bupu6, bupu6 + sizeof(bupu6) / sizeof(color));
	m_database["BUPU7"] = std::vector<color>(bupu7, bupu7 + sizeof(bupu7) / sizeof(color));
	m_database["BUPU8"] = std::vector<color>(bupu8, bupu8 + sizeof(bupu8) / sizeof(color));
	m_database["BUPU9"] = std::vector<color>(bupu9, bupu9 + sizeof(bupu9) / sizeof(color));

	// 06. Dark2
	static const color dark2_3[] = {
		color(uint8_t(27), 158, 119, 255),
		color(uint8_t(217), 95, 2, 255),
		color(uint8_t(117), 112, 179, 255)
	};
	static const color dark2_4[] = {
		color(uint8_t(27), 158, 119, 255),
		color(uint8_t(217), 95, 2, 255),
		color(uint8_t(117), 112, 179, 255),
		color(uint8_t(231), 41, 138, 255)
	};
	static const color dark2_5[] = {
		color(uint8_t(27), 158, 119, 255),
		color(uint8_t(217), 95, 2, 255),
		color(uint8_t(117), 112, 179, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(102), 166, 30, 255)
	};
	static const color dark2_6[] = {
		color(uint8_t(27), 158, 119, 255),
		color(uint8_t(217), 95, 2, 255),
		color(uint8_t(117), 112, 179, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(102), 166, 30, 255),
		color(uint8_t(230), 171, 2, 255)
	};
	static const color dark2_7[] = {
		color(uint8_t(27), 158, 119, 255),
		color(uint8_t(217), 95, 2, 255),
		color(uint8_t(117), 112, 179, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(102), 166, 30, 255),
		color(uint8_t(230), 171, 2, 255),
		color(uint8_t(166), 118, 29, 255)
	};
	static const color dark2_8[] = {
		color(uint8_t(27), 158, 119, 255),
		color(uint8_t(217), 95, 2, 255),
		color(uint8_t(117), 112, 179, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(102), 166, 30, 255),
		color(uint8_t(230), 171, 2, 255),
		color(uint8_t(166), 118, 29, 255),
		color(uint8_t(102), 102, 102, 255)
	};
	m_database["DARK2_3"] = std::vector<color>(dark2_3, dark2_3 + sizeof(dark2_3) / sizeof(color));
	m_database["DARK2_4"] = std::vector<color>(dark2_4, dark2_4 + sizeof(dark2_4) / sizeof(color));
	m_database["DARK2_5"] = std::vector<color>(dark2_5, dark2_5 + sizeof(dark2_5) / sizeof(color));
	m_database["DARK2_6"] = std::vector<color>(dark2_6, dark2_6 + sizeof(dark2_6) / sizeof(color));
	m_database["DARK2_7"] = std::vector<color>(dark2_7, dark2_7 + sizeof(dark2_7) / sizeof(color));
	m_database["DARK2_8"] = std::vector<color>(dark2_8, dark2_8 + sizeof(dark2_8) / sizeof(color));

	// 07. GnBu
	static const color gnbu3[] = {
		color(uint8_t(224), 243, 219, 255),
		color(uint8_t(168), 221, 181, 255),
		color(uint8_t(67), 162, 202, 255)
	};
	static const color gnbu4[] = {
		color(uint8_t(240), 249, 232, 255),
		color(uint8_t(186), 228, 188, 255),
		color(uint8_t(123), 204, 196, 255),
		color(uint8_t(43), 140, 190, 255)
	};
	static const color gnbu5[] = {
		color(uint8_t(240), 249, 232, 255),
		color(uint8_t(186), 228, 188, 255),
		color(uint8_t(123), 204, 196, 255),
		color(uint8_t(67), 162, 202, 255),
		color(uint8_t(8), 104, 172, 255)
	};
	static const color gnbu6[] = {
		color(uint8_t(240), 249, 232, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(168), 221, 181, 255),
		color(uint8_t(123), 204, 196, 255),
		color(uint8_t(67), 162, 202, 255),
		color(uint8_t(8), 104, 172, 255)
	};
	static const color gnbu7[] = {
		color(uint8_t(240), 249, 232, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(168), 221, 181, 255),
		color(uint8_t(123), 204, 196, 255),
		color(uint8_t(78), 179, 211, 255),
		color(uint8_t(43), 140, 190, 255),
		color(uint8_t(8), 88, 158, 255)
	};
	static const color gnbu8[] = {
		color(uint8_t(247), 252, 240, 255),
		color(uint8_t(224), 243, 219, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(168), 221, 181, 255),
		color(uint8_t(123), 204, 196, 255),
		color(uint8_t(78), 179, 211, 255),
		color(uint8_t(43), 140, 190, 255),
		color(uint8_t(8), 88, 158, 255)
	};
	static const color gnbu9[] = {
		color(uint8_t(247), 252, 240, 255),
		color(uint8_t(224), 243, 219, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(168), 221, 181, 255),
		color(uint8_t(123), 204, 196, 255),
		color(uint8_t(78), 179, 211, 255),
		color(uint8_t(43), 140, 190, 255),
		color(uint8_t(8), 104, 172, 255),
		color(uint8_t(8), 64, 129, 255)
	};
	m_database["GNBU3"] = std::vector<color>(gnbu3, gnbu3 + sizeof(gnbu3) / sizeof(color));
	m_database["GNBU4"] = std::vector<color>(gnbu4, gnbu4 + sizeof(gnbu4) / sizeof(color));
	m_database["GNBU5"] = std::vector<color>(gnbu5, gnbu5 + sizeof(gnbu5) / sizeof(color));
	m_database["GNBU6"] = std::vector<color>(gnbu6, gnbu6 + sizeof(gnbu6) / sizeof(color));
	m_database["GNBU7"] = std::vector<color>(gnbu7, gnbu7 + sizeof(gnbu7) / sizeof(color));
	m_database["GNBU8"] = std::vector<color>(gnbu8, gnbu8 + sizeof(gnbu8) / sizeof(color));
	m_database["GNBU9"] = std::vector<color>(gnbu9, gnbu9 + sizeof(gnbu9) / sizeof(color));

	// 08. Greens
	static const color greens3[] = {
		color(uint8_t(229), 245, 224, 255),
		color(uint8_t(161), 217, 155, 255),
		color(uint8_t(49), 163, 84, 255)
	};
	static const color greens4[] = {
		color(uint8_t(237), 248, 233, 255),
		color(uint8_t(186), 228, 179, 255),
		color(uint8_t(116), 196, 118, 255),
		color(uint8_t(35), 139, 69, 255)
	};
	static const color greens5[] = {
		color(uint8_t(237), 248, 233, 255),
		color(uint8_t(186), 228, 179, 255),
		color(uint8_t(116), 196, 118, 255),
		color(uint8_t(49), 163, 84, 255),
		color(uint8_t(0), 109, 44, 255)
	};
	static const color greens6[] = {
		color(uint8_t(237), 248, 233, 255),
		color(uint8_t(199), 233, 192, 255),
		color(uint8_t(161), 217, 155, 255),
		color(uint8_t(116), 196, 118, 255),
		color(uint8_t(49), 163, 84, 255),
		color(uint8_t(0), 109, 44, 255)
	};
	static const color greens7[] = {
		color(uint8_t(237), 248, 233, 255),
		color(uint8_t(199), 233, 192, 255),
		color(uint8_t(161), 217, 155, 255),
		color(uint8_t(116), 196, 118, 255),
		color(uint8_t(65), 171, 93, 255),
		color(uint8_t(35), 139, 69, 255),
		color(uint8_t(0), 90, 50, 255)
	};
	static const color greens8[] = {
		color(uint8_t(247), 252, 245, 255),
		color(uint8_t(229), 245, 224, 255),
		color(uint8_t(199), 233, 192, 255),
		color(uint8_t(161), 217, 155, 255),
		color(uint8_t(116), 196, 118, 255),
		color(uint8_t(65), 171, 93, 255),
		color(uint8_t(35), 139, 69, 255),
		color(uint8_t(0), 90, 50, 255)
	};
	static const color greens9[] = {
		color(uint8_t(247), 252, 245, 255),
		color(uint8_t(229), 245, 224, 255),
		color(uint8_t(199), 233, 192, 255),
		color(uint8_t(161), 217, 155, 255),
		color(uint8_t(116), 196, 118, 255),
		color(uint8_t(65), 171, 93, 255),
		color(uint8_t(35), 139, 69, 255),
		color(uint8_t(0), 109, 44, 255),
		color(uint8_t(0), 68, 27, 255)
	};
	m_database["GREENS3"] = std::vector<color>(greens3, greens3 + sizeof(greens3) / sizeof(color));
	m_database["GREENS4"] = std::vector<color>(greens4, greens4 + sizeof(greens4) / sizeof(color));
	m_database["GREENS5"] = std::vector<color>(greens5, greens5 + sizeof(greens5) / sizeof(color));
	m_database["GREENS6"] = std::vector<color>(greens6, greens6 + sizeof(greens6) / sizeof(color));
	m_database["GREENS7"] = std::vector<color>(greens7, greens7 + sizeof(greens7) / sizeof(color));
	m_database["GREENS8"] = std::vector<color>(greens8, greens8 + sizeof(greens8) / sizeof(color));
	m_database["GREENS9"] = std::vector<color>(greens9, greens9 + sizeof(greens9) / sizeof(color));

	// 09. Greys
	static const color greys3[] = {
		color(uint8_t(240), 240, 240, 255),
		color(uint8_t(189), 189, 189, 255),
		color(uint8_t(99), 99, 99, 255)
	};
	static const color greys4[] = {
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(204), 204, 204, 255),
		color(uint8_t(150), 150, 150, 255),
		color(uint8_t(82), 82, 82, 255)
	};
	static const color greys5[] = {
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(204), 204, 204, 255),
		color(uint8_t(150), 150, 150, 255),
		color(uint8_t(99), 99, 99, 255),
		color(uint8_t(37), 37, 37, 255)
	};
	static const color greys6[] = {
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(189), 189, 189, 255),
		color(uint8_t(150), 150, 150, 255),
		color(uint8_t(99), 99, 99, 255),
		color(uint8_t(37), 37, 37, 255)
	};
	static const color greys7[] = {
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(189), 189, 189, 255),
		color(uint8_t(150), 150, 150, 255),
		color(uint8_t(115), 115, 115, 255),
		color(uint8_t(82), 82, 82, 255),
		color(uint8_t(37), 37, 37, 255)
	};
	static const color greys8[] = {
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(240), 240, 240, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(189), 189, 189, 255),
		color(uint8_t(150), 150, 150, 255),
		color(uint8_t(115), 115, 115, 255),
		color(uint8_t(82), 82, 82, 255),
		color(uint8_t(37), 37, 37, 255)
	};
	static const color greys9[] = {
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(240), 240, 240, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(189), 189, 189, 255),
		color(uint8_t(150), 150, 150, 255),
		color(uint8_t(115), 115, 115, 255),
		color(uint8_t(82), 82, 82, 255),
		color(uint8_t(37), 37, 37, 255),
		color(uint8_t(0), 0, 0, 255)
	};
	m_database["GREYS3"] = std::vector<color>(greys3, greys3 + sizeof(greys3) / sizeof(color));
	m_database["GREYS4"] = std::vector<color>(greys4, greys4 + sizeof(greys4) / sizeof(color));
	m_database["GREYS5"] = std::vector<color>(greys5, greys5 + sizeof(greys5) / sizeof(color));
	m_database["GREYS6"] = std::vector<color>(greys6, greys6 + sizeof(greys6) / sizeof(color));
	m_database["GREYS7"] = std::vector<color>(greys7, greys7 + sizeof(greys7) / sizeof(color));
	m_database["GREYS8"] = std::vector<color>(greys8, greys8 + sizeof(greys8) / sizeof(color));
	m_database["GREYS9"] = std::vector<color>(greys9, greys9 + sizeof(greys9) / sizeof(color));

	// 10. Oranges
	static const color oranges3[] = {
		color(uint8_t(254), 230, 206, 255),
		color(uint8_t(253), 174, 107, 255),
		color(uint8_t(230), 85, 13, 255)
	};
	static const color oranges4[] = {
		color(uint8_t(254), 237, 222, 255),
		color(uint8_t(253), 190, 133, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(217), 71, 1, 255)
	};
	static const color oranges5[] = {
		color(uint8_t(254), 237, 222, 255),
		color(uint8_t(253), 190, 133, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(230), 85, 13, 255),
		color(uint8_t(166), 54, 3, 255)
	};
	static const color oranges6[] = {
		color(uint8_t(254), 237, 222, 255),
		color(uint8_t(253), 208, 162, 255),
		color(uint8_t(253), 174, 107, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(230), 85, 13, 255),
		color(uint8_t(166), 54, 3, 255)
	};
	static const color oranges7[] = {
		color(uint8_t(254), 237, 222, 255),
		color(uint8_t(253), 208, 162, 255),
		color(uint8_t(253), 174, 107, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(241), 105, 19, 255),
		color(uint8_t(217), 72, 1, 255),
		color(uint8_t(140), 45, 4, 255)
	};
	static const color oranges8[] = {
		color(uint8_t(255), 245, 235, 255),
		color(uint8_t(254), 230, 206, 255),
		color(uint8_t(253), 208, 162, 255),
		color(uint8_t(253), 174, 107, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(241), 105, 19, 255),
		color(uint8_t(217), 72, 1, 255),
		color(uint8_t(140), 45, 4, 255)
	};
	static const color oranges9[] = {
		color(uint8_t(255), 245, 235, 255),
		color(uint8_t(254), 230, 206, 255),
		color(uint8_t(253), 208, 162, 255),
		color(uint8_t(253), 174, 107, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(241), 105, 19, 255),
		color(uint8_t(217), 72, 1, 255),
		color(uint8_t(166), 54, 3, 255),
		color(uint8_t(127), 39, 4, 255)
	};
	m_database["ORANGES3"] = std::vector<color>(oranges3, oranges3 + sizeof(oranges3) / sizeof(color));
	m_database["ORANGES4"] = std::vector<color>(oranges4, oranges4 + sizeof(oranges4) / sizeof(color));
	m_database["ORANGES5"] = std::vector<color>(oranges5, oranges5 + sizeof(oranges5) / sizeof(color));
	m_database["ORANGES6"] = std::vector<color>(oranges6, oranges6 + sizeof(oranges6) / sizeof(color));
	m_database["ORANGES7"] = std::vector<color>(oranges7, oranges7 + sizeof(oranges7) / sizeof(color));
	m_database["ORANGES8"] = std::vector<color>(oranges8, oranges8 + sizeof(oranges8) / sizeof(color));
	m_database["ORANGES9"] = std::vector<color>(oranges9, oranges9 + sizeof(oranges9) / sizeof(color));

	// 11. OrRd
	static const color orrd3[] = {
		color(uint8_t(254), 232, 200, 255),
		color(uint8_t(253), 187, 132, 255),
		color(uint8_t(227), 74, 51, 255)
	};
	static const color orrd4[] = {
		color(uint8_t(254), 240, 217, 255),
		color(uint8_t(253), 204, 138, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(215), 48, 31, 255)
	};
	static const color orrd5[] = {
		color(uint8_t(254), 240, 217, 255),
		color(uint8_t(253), 204, 138, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(227), 74, 51, 255),
		color(uint8_t(179), 0, 0, 255)
	};
	static const color orrd6[] = {
		color(uint8_t(254), 240, 217, 255),
		color(uint8_t(253), 212, 158, 255),
		color(uint8_t(253), 187, 132, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(227), 74, 51, 255),
		color(uint8_t(179), 0, 0, 255)
	};
	static const color orrd7[] = {
		color(uint8_t(254), 240, 217, 255),
		color(uint8_t(253), 212, 158, 255),
		color(uint8_t(253), 187, 132, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(239), 101, 72, 255),
		color(uint8_t(215), 48, 31, 255),
		color(uint8_t(153), 0, 0, 255)
	};
	static const color orrd8[] = {
		color(uint8_t(255), 247, 236, 255),
		color(uint8_t(254), 232, 200, 255),
		color(uint8_t(253), 212, 158, 255),
		color(uint8_t(253), 187, 132, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(239), 101, 72, 255),
		color(uint8_t(215), 48, 31, 255),
		color(uint8_t(153), 0, 0, 255)
	};
	static const color orrd9[] = {
		color(uint8_t(255), 247, 236, 255),
		color(uint8_t(254), 232, 200, 255),
		color(uint8_t(253), 212, 158, 255),
		color(uint8_t(253), 187, 132, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(239), 101, 72, 255),
		color(uint8_t(215), 48, 31, 255),
		color(uint8_t(179), 0, 0, 255),
		color(uint8_t(127), 0, 0, 255)
	};
	m_database["ORRD3"] = std::vector<color>(orrd3, orrd3 + sizeof(orrd3) / sizeof(color));
	m_database["ORRD4"] = std::vector<color>(orrd4, orrd4 + sizeof(orrd4) / sizeof(color));
	m_database["ORRD5"] = std::vector<color>(orrd5, orrd5 + sizeof(orrd5) / sizeof(color));
	m_database["ORRD6"] = std::vector<color>(orrd6, orrd6 + sizeof(orrd6) / sizeof(color));
	m_database["ORRD7"] = std::vector<color>(orrd7, orrd7 + sizeof(orrd7) / sizeof(color));
	m_database["ORRD8"] = std::vector<color>(orrd8, orrd8 + sizeof(orrd8) / sizeof(color));
	m_database["ORRD9"] = std::vector<color>(orrd9, orrd9 + sizeof(orrd9) / sizeof(color));

	// 12. Paired
	static const color paired3[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255)
	};
	static const color paired4[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255)
	};
	static const color paired5[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255)
	};
	static const color paired6[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255)
	};
	static const color paired7[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(253), 191, 111, 255)
	};
	static const color paired8[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(253), 191, 111, 255),
		color(uint8_t(255), 127, 0, 255)
	};
	static const color paired9[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(253), 191, 111, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(202), 178, 214, 255)
	};
	static const color paired10[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(253), 191, 111, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(202), 178, 214, 255),
		color(uint8_t(106), 61, 154, 255)
	};
	static const color paired11[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(253), 191, 111, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(202), 178, 214, 255),
		color(uint8_t(106), 61, 154, 255),
		color(uint8_t(255), 255, 153, 255)
	};
	static const color paired12[] = {
		color(uint8_t(166), 206, 227, 255),
		color(uint8_t(31), 120, 180, 255),
		color(uint8_t(178), 223, 138, 255),
		color(uint8_t(51), 160, 44, 255),
		color(uint8_t(251), 154, 153, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(253), 191, 111, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(202), 178, 214, 255),
		color(uint8_t(106), 61, 154, 255),
		color(uint8_t(255), 255, 153, 255),
		color(uint8_t(177), 89, 40, 255)
	};
	m_database["PAIRED3"] = std::vector<color>(paired3, paired3 + sizeof(paired3) / sizeof(color));
	m_database["PAIRED4"] = std::vector<color>(paired4, paired4 + sizeof(paired4) / sizeof(color));
	m_database["PAIRED5"] = std::vector<color>(paired5, paired5 + sizeof(paired5) / sizeof(color));
	m_database["PAIRED6"] = std::vector<color>(paired6, paired6 + sizeof(paired6) / sizeof(color));
	m_database["PAIRED7"] = std::vector<color>(paired7, paired7 + sizeof(paired7) / sizeof(color));
	m_database["PAIRED8"] = std::vector<color>(paired8, paired8 + sizeof(paired8) / sizeof(color));
	m_database["PAIRED9"] = std::vector<color>(paired9, paired9 + sizeof(paired9) / sizeof(color));
	m_database["PAIRED10"] = std::vector<color>(paired10, paired10 + sizeof(paired10) / sizeof(color));
	m_database["PAIRED11"] = std::vector<color>(paired11, paired11 + sizeof(paired11) / sizeof(color));
	m_database["PAIRED12"] = std::vector<color>(paired12, paired12 + sizeof(paired12) / sizeof(color));

	// 13. Pastell1
	static const color pastel1_3[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255)
	};
	static const color pastel1_4[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(222), 203, 228, 255)
	};
	static const color pastel1_5[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(222), 203, 228, 255),
		color(uint8_t(254), 217, 166, 255)
	};
	static const color pastel1_6[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(222), 203, 228, 255),
		color(uint8_t(254), 217, 166, 255),
		color(uint8_t(255), 255, 204, 255)
	};
	static const color pastel1_7[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(222), 203, 228, 255),
		color(uint8_t(254), 217, 166, 255),
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(229), 216, 189, 255)
	};
	static const color pastel1_8[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(222), 203, 228, 255),
		color(uint8_t(254), 217, 166, 255),
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(229), 216, 189, 255),
		color(uint8_t(253), 218, 236, 255)
	};
	static const color pastel1_9[] = {
		color(uint8_t(251), 180, 174, 255),
		color(uint8_t(179), 205, 227, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(222), 203, 228, 255),
		color(uint8_t(254), 217, 166, 255),
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(229), 216, 189, 255),
		color(uint8_t(253), 218, 236, 255),
		color(uint8_t(242), 242, 242, 255)
	};
	m_database["PASTEL1_3"] = std::vector<color>(pastel1_3, pastel1_3 + sizeof(pastel1_3) / sizeof(color));
	m_database["PASTEL1_4"] = std::vector<color>(pastel1_4, pastel1_4 + sizeof(pastel1_4) / sizeof(color));
	m_database["PASTEL1_5"] = std::vector<color>(pastel1_5, pastel1_5 + sizeof(pastel1_5) / sizeof(color));
	m_database["PASTEL1_6"] = std::vector<color>(pastel1_6, pastel1_6 + sizeof(pastel1_6) / sizeof(color));
	m_database["PASTEL1_7"] = std::vector<color>(pastel1_7, pastel1_7 + sizeof(pastel1_7) / sizeof(color));
	m_database["PASTEL1_8"] = std::vector<color>(pastel1_8, pastel1_8 + sizeof(pastel1_8) / sizeof(color));
	m_database["PASTEL1_9"] = std::vector<color>(pastel1_9, pastel1_9 + sizeof(pastel1_9) / sizeof(color));

	// 14. Pastell2
	static const color pastel2_3[] = {
		color(uint8_t(179), 226, 205, 255),
		color(uint8_t(253), 205, 172, 255),
		color(uint8_t(203), 213, 232, 255)
	};
	static const color pastel2_4[] = {
		color(uint8_t(179), 226, 205, 255),
		color(uint8_t(253), 205, 172, 255),
		color(uint8_t(203), 213, 232, 255),
		color(uint8_t(244), 202, 228, 255)
	};
	static const color pastel2_5[] = {
		color(uint8_t(179), 226, 205, 255),
		color(uint8_t(253), 205, 172, 255),
		color(uint8_t(203), 213, 232, 255),
		color(uint8_t(244), 202, 228, 255),
		color(uint8_t(230), 245, 201, 255)
	};
	static const color pastel2_6[] = {
		color(uint8_t(179), 226, 205, 255),
		color(uint8_t(253), 205, 172, 255),
		color(uint8_t(203), 213, 232, 255),
		color(uint8_t(244), 202, 228, 255),
		color(uint8_t(230), 245, 201, 255),
		color(uint8_t(255), 242, 174, 255)
	};
	static const color pastel2_7[] = {
		color(uint8_t(179), 226, 205, 255),
		color(uint8_t(253), 205, 172, 255),
		color(uint8_t(203), 213, 232, 255),
		color(uint8_t(244), 202, 228, 255),
		color(uint8_t(230), 245, 201, 255),
		color(uint8_t(255), 242, 174, 255),
		color(uint8_t(241), 226, 204, 255)
	};
	static const color pastel2_8[] = {
		color(uint8_t(179), 226, 205, 255),
		color(uint8_t(253), 205, 172, 255),
		color(uint8_t(203), 213, 232, 255),
		color(uint8_t(244), 202, 228, 255),
		color(uint8_t(230), 245, 201, 255),
		color(uint8_t(255), 242, 174, 255),
		color(uint8_t(241), 226, 204, 255),
		color(uint8_t(204), 204, 204, 255)
	};
	m_database["PASTEL2_3"] = std::vector<color>(pastel2_3, pastel2_3 + sizeof(pastel2_3) / sizeof(color));
	m_database["PASTEL2_4"] = std::vector<color>(pastel2_4, pastel2_4 + sizeof(pastel2_4) / sizeof(color));
	m_database["PASTEL2_5"] = std::vector<color>(pastel2_5, pastel2_5 + sizeof(pastel2_5) / sizeof(color));
	m_database["PASTEL2_6"] = std::vector<color>(pastel2_6, pastel2_6 + sizeof(pastel2_6) / sizeof(color));
	m_database["PASTEL2_7"] = std::vector<color>(pastel2_7, pastel2_7 + sizeof(pastel2_7) / sizeof(color));
	m_database["PASTEL2_8"] = std::vector<color>(pastel2_8, pastel2_8 + sizeof(pastel2_8) / sizeof(color));

	// 15. PiYG
	static const color piyg3[] = {
		color(uint8_t(233), 163, 201, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(161), 215, 106, 255)
	};
	static const color piyg4[] = {
		color(uint8_t(208), 28, 139, 255),
		color(uint8_t(241), 182, 218, 255),
		color(uint8_t(184), 225, 134, 255),
		color(uint8_t(77), 172, 38, 255)
	};
	static const color piyg5[] = {
		color(uint8_t(208), 28, 139, 255),
		color(uint8_t(241), 182, 218, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(184), 225, 134, 255),
		color(uint8_t(77), 172, 38, 255)
	};
	static const color piyg6[] = {
		color(uint8_t(197), 27, 125, 255),
		color(uint8_t(233), 163, 201, 255),
		color(uint8_t(253), 224, 239, 255),
		color(uint8_t(230), 245, 208, 255),
		color(uint8_t(161), 215, 106, 255),
		color(uint8_t(77), 146, 33, 255)
	};
	static const color piyg7[] = {
		color(uint8_t(197), 27, 125, 255),
		color(uint8_t(233), 163, 201, 255),
		color(uint8_t(253), 224, 239, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(230), 245, 208, 255),
		color(uint8_t(161), 215, 106, 255),
		color(uint8_t(77), 146, 33, 255)
	};
	static const color piyg8[] = {
		color(uint8_t(197), 27, 125, 255),
		color(uint8_t(222), 119, 174, 255),
		color(uint8_t(241), 182, 218, 255),
		color(uint8_t(253), 224, 239, 255),
		color(uint8_t(230), 245, 208, 255),
		color(uint8_t(184), 225, 134, 255),
		color(uint8_t(127), 188, 65, 255),
		color(uint8_t(77), 146, 33, 255)
	};
	static const color piyg9[] = {
		color(uint8_t(197), 27, 125, 255),
		color(uint8_t(222), 119, 174, 255),
		color(uint8_t(241), 182, 218, 255),
		color(uint8_t(253), 224, 239, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(230), 245, 208, 255),
		color(uint8_t(184), 225, 134, 255),
		color(uint8_t(127), 188, 65, 255),
		color(uint8_t(77), 146, 33, 255)
	};
	static const color piyg10[] = {
		color(uint8_t(142), 1, 82, 255),
		color(uint8_t(197), 27, 125, 255),
		color(uint8_t(222), 119, 174, 255),
		color(uint8_t(241), 182, 218, 255),
		color(uint8_t(253), 224, 239, 255),
		color(uint8_t(230), 245, 208, 255),
		color(uint8_t(184), 225, 134, 255),
		color(uint8_t(127), 188, 65, 255),
		color(uint8_t(77), 146, 33, 255),
		color(uint8_t(39), 100, 25, 255)
	};
	static const color piyg11[] = {
		color(uint8_t(142), 1, 82, 255),
		color(uint8_t(197), 27, 125, 255),
		color(uint8_t(222), 119, 174, 255),
		color(uint8_t(241), 182, 218, 255),
		color(uint8_t(253), 224, 239, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(230), 245, 208, 255),
		color(uint8_t(184), 225, 134, 255),
		color(uint8_t(127), 188, 65, 255),
		color(uint8_t(77), 146, 33, 255),
		color(uint8_t(39), 100, 25, 255)
	};
	m_database["PIYG3"] = std::vector<color>(piyg3, piyg3 + sizeof(piyg3) / sizeof(color));
	m_database["PIYG4"] = std::vector<color>(piyg4, piyg4 + sizeof(piyg4) / sizeof(color));
	m_database["PIYG5"] = std::vector<color>(piyg5, piyg5 + sizeof(piyg5) / sizeof(color));
	m_database["PIYG6"] = std::vector<color>(piyg6, piyg6 + sizeof(piyg6) / sizeof(color));
	m_database["PIYG7"] = std::vector<color>(piyg7, piyg7 + sizeof(piyg7) / sizeof(color));
	m_database["PIYG8"] = std::vector<color>(piyg8, piyg8 + sizeof(piyg8) / sizeof(color));
	m_database["PIYG9"] = std::vector<color>(piyg9, piyg9 + sizeof(piyg9) / sizeof(color));
	m_database["PIYG10"] = std::vector<color>(piyg10, piyg10 + sizeof(piyg10) / sizeof(color));
	m_database["PIYG11"] = std::vector<color>(piyg11, piyg11 + sizeof(piyg11) / sizeof(color));

	// 17. PRGn
	static const color prgn3[] = {
		color(uint8_t(175), 141, 195, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(127), 191, 123, 255)
	};
	static const color prgn4[] = {
		color(uint8_t(123), 50, 148, 255),
		color(uint8_t(194), 165, 207, 255),
		color(uint8_t(166), 219, 160, 255),
		color(uint8_t(0), 136, 55, 255)
	};
	static const color prgn5[] = {
		color(uint8_t(123), 50, 148, 255),
		color(uint8_t(194), 165, 207, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(166), 219, 160, 255),
		color(uint8_t(0), 136, 55, 255)
	};
	static const color prgn6[] = {
		color(uint8_t(118), 42, 131, 255),
		color(uint8_t(175), 141, 195, 255),
		color(uint8_t(231), 212, 232, 255),
		color(uint8_t(217), 240, 211, 255),
		color(uint8_t(127), 191, 123, 255),
		color(uint8_t(27), 120, 55, 255)
	};
	static const color prgn7[] = {
		color(uint8_t(118), 42, 131, 255),
		color(uint8_t(175), 141, 195, 255),
		color(uint8_t(231), 212, 232, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(217), 240, 211, 255),
		color(uint8_t(127), 191, 123, 255),
		color(uint8_t(27), 120, 55, 255)
	};
	static const color prgn8[] = {
		color(uint8_t(118), 42, 131, 255),
		color(uint8_t(153), 112, 171, 255),
		color(uint8_t(194), 165, 207, 255),
		color(uint8_t(231), 212, 232, 255),
		color(uint8_t(217), 240, 211, 255),
		color(uint8_t(166), 219, 160, 255),
		color(uint8_t(90), 174, 97, 255),
		color(uint8_t(27), 120, 55, 255)
	};
	static const color prgn9[] = {
		color(uint8_t(118), 42, 131, 255),
		color(uint8_t(153), 112, 171, 255),
		color(uint8_t(194), 165, 207, 255),
		color(uint8_t(231), 212, 232, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(217), 240, 211, 255),
		color(uint8_t(166), 219, 160, 255),
		color(uint8_t(90), 174, 97, 255),
		color(uint8_t(27), 120, 55, 255)
	};
	static const color prgn10[] = {
		color(uint8_t(64), 0, 75, 255),
		color(uint8_t(118), 42, 131, 255),
		color(uint8_t(153), 112, 171, 255),
		color(uint8_t(194), 165, 207, 255),
		color(uint8_t(231), 212, 232, 255),
		color(uint8_t(217), 240, 211, 255),
		color(uint8_t(166), 219, 160, 255),
		color(uint8_t(90), 174, 97, 255),
		color(uint8_t(27), 120, 55, 255),
		color(uint8_t(0), 68, 27, 255)
	};
	static const color prgn11[] = {
		color(uint8_t(64), 0, 75, 255),
		color(uint8_t(118), 42, 131, 255),
		color(uint8_t(153), 112, 171, 255),
		color(uint8_t(194), 165, 207, 255),
		color(uint8_t(231), 212, 232, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(217), 240, 211, 255),
		color(uint8_t(166), 219, 160, 255),
		color(uint8_t(90), 174, 97, 255),
		color(uint8_t(27), 120, 55, 255),
		color(uint8_t(0), 68, 27, 255)
	};
	m_database["PRGN3"] = std::vector<color>(prgn3, prgn3 + sizeof(prgn3) / sizeof(color));
	m_database["PRGN4"] = std::vector<color>(prgn4, prgn4 + sizeof(prgn4) / sizeof(color));
	m_database["PRGN5"] = std::vector<color>(prgn5, prgn5 + sizeof(prgn5) / sizeof(color));
	m_database["PRGN6"] = std::vector<color>(prgn6, prgn6 + sizeof(prgn6) / sizeof(color));
	m_database["PRGN7"] = std::vector<color>(prgn7, prgn7 + sizeof(prgn7) / sizeof(color));
	m_database["PRGN8"] = std::vector<color>(prgn8, prgn8 + sizeof(prgn8) / sizeof(color));
	m_database["PRGN9"] = std::vector<color>(prgn9, prgn9 + sizeof(prgn9) / sizeof(color));
	m_database["PRGN10"] = std::vector<color>(prgn10, prgn10 + sizeof(prgn10) / sizeof(color));
	m_database["PRGN11"] = std::vector<color>(prgn11, prgn11 + sizeof(prgn11) / sizeof(color));

	// 18. PuBu
	static const color pubu3[] = {
		color(uint8_t(236), 231, 242, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(43), 140, 190, 255)
	};
	static const color pubu4[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(189), 201, 225, 255),
		color(uint8_t(116), 169, 207, 255),
		color(uint8_t(5), 112, 176, 255)
	};
	static const color pubu5[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(189), 201, 225, 255),
		color(uint8_t(116), 169, 207, 255),
		color(uint8_t(43), 140, 190, 255),
		color(uint8_t(4), 90, 141, 255)
	};
	static const color pubu6[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(116), 169, 207, 255),
		color(uint8_t(43), 140, 190, 255),
		color(uint8_t(4), 90, 141, 255)
	};
	static const color pubu7[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(116), 169, 207, 255),
		color(uint8_t(54), 144, 192, 255),
		color(uint8_t(5), 112, 176, 255),
		color(uint8_t(3), 78, 123, 255)
	};
	static const color pubu8[] = {
		color(uint8_t(255), 247, 251, 255),
		color(uint8_t(236), 231, 242, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(116), 169, 207, 255),
		color(uint8_t(54), 144, 192, 255),
		color(uint8_t(5), 112, 176, 255),
		color(uint8_t(3), 78, 123, 255)
	};
	static const color pubu9[] = {
		color(uint8_t(255), 247, 251, 255),
		color(uint8_t(236), 231, 242, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(116), 169, 207, 255),
		color(uint8_t(54), 144, 192, 255),
		color(uint8_t(5), 112, 176, 255),
		color(uint8_t(4), 90, 141, 255),
		color(uint8_t(2), 56, 88, 255)
	};
	m_database["PUBU3"] = std::vector<color>(pubu3, pubu3 + sizeof(pubu3) / sizeof(color));
	m_database["PUBU4"] = std::vector<color>(pubu4, pubu4 + sizeof(pubu4) / sizeof(color));
	m_database["PUBU5"] = std::vector<color>(pubu5, pubu5 + sizeof(pubu5) / sizeof(color));
	m_database["PUBU6"] = std::vector<color>(pubu6, pubu6 + sizeof(pubu6) / sizeof(color));
	m_database["PUBU7"] = std::vector<color>(pubu7, pubu7 + sizeof(pubu7) / sizeof(color));
	m_database["PUBU8"] = std::vector<color>(pubu8, pubu8 + sizeof(pubu8) / sizeof(color));
	m_database["PUBU9"] = std::vector<color>(pubu9, pubu9 + sizeof(pubu9) / sizeof(color));

	// 19. PuBuGn
	static const color pubugn3[] = {
		color(uint8_t(236), 226, 240, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(28), 144, 153, 255)
	};
	static const color pubugn4[] = {
		color(uint8_t(246), 239, 247, 255),
		color(uint8_t(189), 201, 225, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(2), 129, 138, 255)
	};
	static const color pubugn5[] = {
		color(uint8_t(246), 239, 247, 255),
		color(uint8_t(189), 201, 225, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(28), 144, 153, 255),
		color(uint8_t(1), 108, 89, 255)
	};
	static const color pubugn6[] = {
		color(uint8_t(246), 239, 247, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(28), 144, 153, 255),
		color(uint8_t(1), 108, 89, 255)
	};
	static const color pubugn7[] = {
		color(uint8_t(246), 239, 247, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(54), 144, 192, 255),
		color(uint8_t(2), 129, 138, 255),
		color(uint8_t(1), 100, 80, 255)
	};
	static const color pubugn8[] = {
		color(uint8_t(255), 247, 251, 255),
		color(uint8_t(236), 226, 240, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(54), 144, 192, 255),
		color(uint8_t(2), 129, 138, 255),
		color(uint8_t(1), 100, 80, 255)
	};
	static const color pubugn9[] = {
		color(uint8_t(255), 247, 251, 255),
		color(uint8_t(236), 226, 240, 255),
		color(uint8_t(208), 209, 230, 255),
		color(uint8_t(166), 189, 219, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(54), 144, 192, 255),
		color(uint8_t(2), 129, 138, 255),
		color(uint8_t(1), 108, 89, 255),
		color(uint8_t(1), 70, 54, 255)
	};
	m_database["PUBUGN3"] = std::vector<color>(pubugn3, pubugn3 + sizeof(pubugn3) / sizeof(color));
	m_database["PUBUGN4"] = std::vector<color>(pubugn4, pubugn4 + sizeof(pubugn4) / sizeof(color));
	m_database["PUBUGN5"] = std::vector<color>(pubugn5, pubugn5 + sizeof(pubugn5) / sizeof(color));
	m_database["PUBUGN6"] = std::vector<color>(pubugn6, pubugn6 + sizeof(pubugn6) / sizeof(color));
	m_database["PUBUGN7"] = std::vector<color>(pubugn7, pubugn7 + sizeof(pubugn7) / sizeof(color));
	m_database["PUBUGN8"] = std::vector<color>(pubugn8, pubugn8 + sizeof(pubugn8) / sizeof(color));
	m_database["PUBUGN9"] = std::vector<color>(pubugn9, pubugn9 + sizeof(pubugn9) / sizeof(color));

	// 20. PuOr
	static const color puor3[] = {
		color(uint8_t(241), 163, 64, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(153), 142, 195, 255)
	};
	static const color puor4[] = {
		color(uint8_t(230), 97, 1, 255),
		color(uint8_t(253), 184, 99, 255),
		color(uint8_t(178), 171, 210, 255),
		color(uint8_t(94), 60, 153, 255)
	};
	static const color puor5[] = {
		color(uint8_t(230), 97, 1, 255),
		color(uint8_t(253), 184, 99, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(178), 171, 210, 255),
		color(uint8_t(94), 60, 153, 255)
	};
	static const color puor6[] = {
		color(uint8_t(179), 88, 6, 255),
		color(uint8_t(241), 163, 64, 255),
		color(uint8_t(254), 224, 182, 255),
		color(uint8_t(216), 218, 235, 255),
		color(uint8_t(153), 142, 195, 255),
		color(uint8_t(84), 39, 136, 255)
	};
	static const color puor7[] = {
		color(uint8_t(179), 88, 6, 255),
		color(uint8_t(241), 163, 64, 255),
		color(uint8_t(254), 224, 182, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(216), 218, 235, 255),
		color(uint8_t(153), 142, 195, 255),
		color(uint8_t(84), 39, 136, 255)
	};
	static const color puor8[] = {
		color(uint8_t(179), 88, 6, 255),
		color(uint8_t(224), 130, 20, 255),
		color(uint8_t(253), 184, 99, 255),
		color(uint8_t(254), 224, 182, 255),
		color(uint8_t(216), 218, 235, 255),
		color(uint8_t(178), 171, 210, 255),
		color(uint8_t(128), 115, 172, 255),
		color(uint8_t(84), 39, 136, 255)
	};
	static const color puor9[] = {
		color(uint8_t(179), 88, 6, 255),
		color(uint8_t(224), 130, 20, 255),
		color(uint8_t(253), 184, 99, 255),
		color(uint8_t(254), 224, 182, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(216), 218, 235, 255),
		color(uint8_t(178), 171, 210, 255),
		color(uint8_t(128), 115, 172, 255),
		color(uint8_t(84), 39, 136, 255)
	};
	static const color puor10[] = {
		color(uint8_t(127), 59, 8, 255),
		color(uint8_t(179), 88, 6, 255),
		color(uint8_t(224), 130, 20, 255),
		color(uint8_t(253), 184, 99, 255),
		color(uint8_t(254), 224, 182, 255),
		color(uint8_t(216), 218, 235, 255),
		color(uint8_t(178), 171, 210, 255),
		color(uint8_t(128), 115, 172, 255),
		color(uint8_t(84), 39, 136, 255),
		color(uint8_t(45), 0, 75, 255)
	};
	static const color puor11[] = {
		color(uint8_t(127), 59, 8, 255),
		color(uint8_t(179), 88, 6, 255),
		color(uint8_t(224), 130, 20, 255),
		color(uint8_t(253), 184, 99, 255),
		color(uint8_t(254), 224, 182, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(216), 218, 235, 255),
		color(uint8_t(178), 171, 210, 255),
		color(uint8_t(128), 115, 172, 255),
		color(uint8_t(84), 39, 136, 255),
		color(uint8_t(45), 0, 75, 255)
	};
	m_database["PUOR3"] = std::vector<color>(puor3, puor3 + sizeof(puor3) / sizeof(color));
	m_database["PUOR4"] = std::vector<color>(puor4, puor4 + sizeof(puor4) / sizeof(color));
	m_database["PUOR5"] = std::vector<color>(puor5, puor5 + sizeof(puor5) / sizeof(color));
	m_database["PUOR6"] = std::vector<color>(puor6, puor6 + sizeof(puor6) / sizeof(color));
	m_database["PUOR7"] = std::vector<color>(puor7, puor7 + sizeof(puor7) / sizeof(color));
	m_database["PUOR8"] = std::vector<color>(puor8, puor8 + sizeof(puor8) / sizeof(color));
	m_database["PUOR9"] = std::vector<color>(puor9, puor9 + sizeof(puor9) / sizeof(color));
	m_database["PUOR10"] = std::vector<color>(puor10, puor10 + sizeof(puor10) / sizeof(color));
	m_database["PUOR11"] = std::vector<color>(puor11, puor11 + sizeof(puor11) / sizeof(color));

	// 21. PuRd
	static const color purd3[] = {
		color(uint8_t(231), 225, 239, 255),
		color(uint8_t(201), 148, 199, 255),
		color(uint8_t(221), 28, 119, 255)
	};
	static const color purd4[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(215), 181, 216, 255),
		color(uint8_t(223), 101, 176, 255),
		color(uint8_t(206), 18, 86, 255)
	};
	static const color purd5[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(215), 181, 216, 255),
		color(uint8_t(223), 101, 176, 255),
		color(uint8_t(221), 28, 119, 255),
		color(uint8_t(152), 0, 67, 255)
	};
	static const color purd6[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(212), 185, 218, 255),
		color(uint8_t(201), 148, 199, 255),
		color(uint8_t(223), 101, 176, 255),
		color(uint8_t(221), 28, 119, 255),
		color(uint8_t(152), 0, 67, 255)
	};
	static const color purd7[] = {
		color(uint8_t(241), 238, 246, 255),
		color(uint8_t(212), 185, 218, 255),
		color(uint8_t(201), 148, 199, 255),
		color(uint8_t(223), 101, 176, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(206), 18, 86, 255),
		color(uint8_t(145), 0, 63, 255)
	};
	static const color purd8[] = {
		color(uint8_t(247), 244, 249, 255),
		color(uint8_t(231), 225, 239, 255),
		color(uint8_t(212), 185, 218, 255),
		color(uint8_t(201), 148, 199, 255),
		color(uint8_t(223), 101, 176, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(206), 18, 86, 255),
		color(uint8_t(145), 0, 63, 255)
	};
	static const color purd9[] = {
		color(uint8_t(247), 244, 249, 255),
		color(uint8_t(231), 225, 239, 255),
		color(uint8_t(212), 185, 218, 255),
		color(uint8_t(201), 148, 199, 255),
		color(uint8_t(223), 101, 176, 255),
		color(uint8_t(231), 41, 138, 255),
		color(uint8_t(206), 18, 86, 255),
		color(uint8_t(152), 0, 67, 255),
		color(uint8_t(103), 0, 31, 255)
	};
	m_database["PURD3"] = std::vector<color>(purd3, purd3 + sizeof(purd3) / sizeof(color));
	m_database["PURD4"] = std::vector<color>(purd4, purd4 + sizeof(purd4) / sizeof(color));
	m_database["PURD5"] = std::vector<color>(purd5, purd5 + sizeof(purd5) / sizeof(color));
	m_database["PURD6"] = std::vector<color>(purd6, purd6 + sizeof(purd6) / sizeof(color));
	m_database["PURD7"] = std::vector<color>(purd7, purd7 + sizeof(purd7) / sizeof(color));
	m_database["PURD8"] = std::vector<color>(purd8, purd8 + sizeof(purd8) / sizeof(color));
	m_database["PURD9"] = std::vector<color>(purd9, purd9 + sizeof(purd9) / sizeof(color));

	// 22. Purples
	static const color purples3[] = {
		color(uint8_t(239), 237, 245, 255),
		color(uint8_t(188), 189, 220, 255),
		color(uint8_t(117), 107, 177, 255)
	};
	static const color purples4[] = {
		color(uint8_t(242), 240, 247, 255),
		color(uint8_t(203), 201, 226, 255),
		color(uint8_t(158), 154, 200, 255),
		color(uint8_t(106), 81, 163, 255)
	};
	static const color purples5[] = {
		color(uint8_t(242), 240, 247, 255),
		color(uint8_t(203), 201, 226, 255),
		color(uint8_t(158), 154, 200, 255),
		color(uint8_t(117), 107, 177, 255),
		color(uint8_t(84), 39, 143, 255)
	};
	static const color purples6[] = {
		color(uint8_t(242), 240, 247, 255),
		color(uint8_t(218), 218, 235, 255),
		color(uint8_t(188), 189, 220, 255),
		color(uint8_t(158), 154, 200, 255),
		color(uint8_t(117), 107, 177, 255),
		color(uint8_t(84), 39, 143, 255)
	};
	static const color purples7[] = {
		color(uint8_t(242), 240, 247, 255),
		color(uint8_t(218), 218, 235, 255),
		color(uint8_t(188), 189, 220, 255),
		color(uint8_t(158), 154, 200, 255),
		color(uint8_t(128), 125, 186, 255),
		color(uint8_t(106), 81, 163, 255),
		color(uint8_t(74), 20, 134, 255)
	};
	static const color purples8[] = {
		color(uint8_t(252), 251, 253, 255),
		color(uint8_t(239), 237, 245, 255),
		color(uint8_t(218), 218, 235, 255),
		color(uint8_t(188), 189, 220, 255),
		color(uint8_t(158), 154, 200, 255),
		color(uint8_t(128), 125, 186, 255),
		color(uint8_t(106), 81, 163, 255),
		color(uint8_t(74), 20, 134, 255)
	};
	static const color purples9[] = {
		color(uint8_t(252), 251, 253, 255),
		color(uint8_t(239), 237, 245, 255),
		color(uint8_t(218), 218, 235, 255),
		color(uint8_t(188), 189, 220, 255),
		color(uint8_t(158), 154, 200, 255),
		color(uint8_t(128), 125, 186, 255),
		color(uint8_t(106), 81, 163, 255),
		color(uint8_t(84), 39, 143, 255),
		color(uint8_t(63), 0, 125, 255)
	};
	m_database["PURPLES3"] = std::vector<color>(purples3, purples3 + sizeof(purples3) / sizeof(color));
	m_database["PURPLES4"] = std::vector<color>(purples4, purples4 + sizeof(purples4) / sizeof(color));
	m_database["PURPLES5"] = std::vector<color>(purples5, purples5 + sizeof(purples5) / sizeof(color));
	m_database["PURPLES6"] = std::vector<color>(purples6, purples6 + sizeof(purples6) / sizeof(color));
	m_database["PURPLES7"] = std::vector<color>(purples7, purples7 + sizeof(purples7) / sizeof(color));
	m_database["PURPLES8"] = std::vector<color>(purples8, purples8 + sizeof(purples8) / sizeof(color));
	m_database["PURPLES9"] = std::vector<color>(purples9, purples9 + sizeof(purples9) / sizeof(color));

	// 23. RdBu
	static const color rdbu3[] = {
		color(uint8_t(239), 138, 98, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(103), 169, 207, 255)
	};
	static const color rdbu4[] = {
		color(uint8_t(202), 0, 32, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(146), 197, 222, 255),
		color(uint8_t(5), 113, 176, 255)
	};
	static const color rdbu5[] = {
		color(uint8_t(202), 0, 32, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(146), 197, 222, 255),
		color(uint8_t(5), 113, 176, 255)
	};
	static const color rdbu6[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(239), 138, 98, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(209), 229, 240, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(33), 102, 172, 255)
	};
	static const color rdbu7[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(239), 138, 98, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(209), 229, 240, 255),
		color(uint8_t(103), 169, 207, 255),
		color(uint8_t(33), 102, 172, 255)
	};
	static const color rdbu8[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(209), 229, 240, 255),
		color(uint8_t(146), 197, 222, 255),
		color(uint8_t(67), 147, 195, 255),
		color(uint8_t(33), 102, 172, 255)
	};
	static const color rdbu9[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(209), 229, 240, 255),
		color(uint8_t(146), 197, 222, 255),
		color(uint8_t(67), 147, 195, 255),
		color(uint8_t(33), 102, 172, 255)
	};
	static const color rdbu10[] = {
		color(uint8_t(103), 0, 31, 255),
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(209), 229, 240, 255),
		color(uint8_t(146), 197, 222, 255),
		color(uint8_t(67), 147, 195, 255),
		color(uint8_t(33), 102, 172, 255),
		color(uint8_t(5), 48, 97, 255)
	};
	static const color rdbu11[] = {
		color(uint8_t(103), 0, 31, 255),
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(247), 247, 247, 255),
		color(uint8_t(209), 229, 240, 255),
		color(uint8_t(146), 197, 222, 255),
		color(uint8_t(67), 147, 195, 255),
		color(uint8_t(33), 102, 172, 255),
		color(uint8_t(5), 48, 97, 255)
	};
	m_database["RDBU3"] = std::vector<color>(rdbu3, rdbu3 + sizeof(rdbu3) / sizeof(color));
	m_database["RDBU4"] = std::vector<color>(rdbu4, rdbu4 + sizeof(rdbu4) / sizeof(color));
	m_database["RDBU5"] = std::vector<color>(rdbu5, rdbu5 + sizeof(rdbu5) / sizeof(color));
	m_database["RDBU6"] = std::vector<color>(rdbu6, rdbu6 + sizeof(rdbu6) / sizeof(color));
	m_database["RDBU7"] = std::vector<color>(rdbu7, rdbu7 + sizeof(rdbu7) / sizeof(color));
	m_database["RDBU8"] = std::vector<color>(rdbu8, rdbu8 + sizeof(rdbu8) / sizeof(color));
	m_database["RDBU9"] = std::vector<color>(rdbu9, rdbu9 + sizeof(rdbu9) / sizeof(color));
	m_database["RDBU10"] = std::vector<color>(rdbu10, rdbu10 + sizeof(rdbu10) / sizeof(color));
	m_database["RDBU11"] = std::vector<color>(rdbu11, rdbu11 + sizeof(rdbu11) / sizeof(color));

	// 24. RdGy
	static const color rdgy3[] = {
		color(uint8_t(239), 138, 98, 255),
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(153), 153, 153, 255)
	};
	static const color rdgy4[] = {
		color(uint8_t(202), 0, 32, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(186), 186, 186, 255),
		color(uint8_t(64), 64, 64, 255)
	};
	static const color rdgy5[] = {
		color(uint8_t(202), 0, 32, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(186), 186, 186, 255),
		color(uint8_t(64), 64, 64, 255)
	};
	static const color rdgy6[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(239), 138, 98, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(224), 224, 224, 255),
		color(uint8_t(153), 153, 153, 255),
		color(uint8_t(77), 77, 77, 255)
	};
	static const color rdgy7[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(239), 138, 98, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(224), 224, 224, 255),
		color(uint8_t(153), 153, 153, 255),
		color(uint8_t(77), 77, 77, 255)
	};
	static const color rdgy8[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(224), 224, 224, 255),
		color(uint8_t(186), 186, 186, 255),
		color(uint8_t(135), 135, 135, 255),
		color(uint8_t(77), 77, 77, 255)
	};
	static const color rdgy9[] = {
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(224), 224, 224, 255),
		color(uint8_t(186), 186, 186, 255),
		color(uint8_t(135), 135, 135, 255),
		color(uint8_t(77), 77, 77, 255)
	};
	static const color rdgy10[] = {
		color(uint8_t(103), 0, 31, 255),
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(224), 224, 224, 255),
		color(uint8_t(186), 186, 186, 255),
		color(uint8_t(135), 135, 135, 255),
		color(uint8_t(77), 77, 77, 255),
		color(uint8_t(26), 26, 26, 255)
	};
	static const color rdgy11[] = {
		color(uint8_t(103), 0, 31, 255),
		color(uint8_t(178), 24, 43, 255),
		color(uint8_t(214), 96, 77, 255),
		color(uint8_t(244), 165, 130, 255),
		color(uint8_t(253), 219, 199, 255),
		color(uint8_t(255), 255, 255, 255),
		color(uint8_t(224), 224, 224, 255),
		color(uint8_t(186), 186, 186, 255),
		color(uint8_t(135), 135, 135, 255),
		color(uint8_t(77), 77, 77, 255),
		color(uint8_t(26), 26, 26, 255)
	};
	m_database["RDGY3"] = std::vector<color>(rdgy3, rdgy3 + sizeof(rdgy3) / sizeof(color));
	m_database["RDGY4"] = std::vector<color>(rdgy4, rdgy4 + sizeof(rdgy4) / sizeof(color));
	m_database["RDGY5"] = std::vector<color>(rdgy5, rdgy5 + sizeof(rdgy5) / sizeof(color));
	m_database["RDGY6"] = std::vector<color>(rdgy6, rdgy6 + sizeof(rdgy6) / sizeof(color));
	m_database["RDGY7"] = std::vector<color>(rdgy7, rdgy7 + sizeof(rdgy7) / sizeof(color));
	m_database["RDGY8"] = std::vector<color>(rdgy8, rdgy8 + sizeof(rdgy8) / sizeof(color));
	m_database["RDGY9"] = std::vector<color>(rdgy9, rdgy9 + sizeof(rdgy9) / sizeof(color));
	m_database["RDGY10"] = std::vector<color>(rdgy10, rdgy10 + sizeof(rdgy10) / sizeof(color));
	m_database["RDGY11"] = std::vector<color>(rdgy11, rdgy11 + sizeof(rdgy11) / sizeof(color));

	// 25.RdPu
	static const color rdpu3[] = {
		color(uint8_t(253), 224, 221, 255),
		color(uint8_t(250), 159, 181, 255),
		color(uint8_t(197), 27, 138, 255)
	};
	static const color rdpu4[] = {
		color(uint8_t(254), 235, 226, 255),
		color(uint8_t(251), 180, 185, 255),
		color(uint8_t(247), 104, 161, 255),
		color(uint8_t(174), 1, 126, 255)
	};
	static const color rdpu5[] = {
		color(uint8_t(254), 235, 226, 255),
		color(uint8_t(251), 180, 185, 255),
		color(uint8_t(247), 104, 161, 255),
		color(uint8_t(197), 27, 138, 255),
		color(uint8_t(122), 1, 119, 255)
	};
	static const color rdpu6[] = {
		color(uint8_t(254), 235, 226, 255),
		color(uint8_t(252), 197, 192, 255),
		color(uint8_t(250), 159, 181, 255),
		color(uint8_t(247), 104, 161, 255),
		color(uint8_t(197), 27, 138, 255),
		color(uint8_t(122), 1, 119, 255)
	};
	static const color rdpu7[] = {
		color(uint8_t(254), 235, 226, 255),
		color(uint8_t(252), 197, 192, 255),
		color(uint8_t(250), 159, 181, 255),
		color(uint8_t(247), 104, 161, 255),
		color(uint8_t(221), 52, 151, 255),
		color(uint8_t(174), 1, 126, 255),
		color(uint8_t(122), 1, 119, 255)
	};
	static const color rdpu8[] = {
		color(uint8_t(255), 247, 243, 255),
		color(uint8_t(253), 224, 221, 255),
		color(uint8_t(252), 197, 192, 255),
		color(uint8_t(250), 159, 181, 255),
		color(uint8_t(247), 104, 161, 255),
		color(uint8_t(221), 52, 151, 255),
		color(uint8_t(174), 1, 126, 255),
		color(uint8_t(122), 1, 119, 255)
	};
	static const color rdpu9[] = {
		color(uint8_t(255), 247, 243, 255),
		color(uint8_t(253), 224, 221, 255),
		color(uint8_t(252), 197, 192, 255),
		color(uint8_t(250), 159, 181, 255),
		color(uint8_t(247), 104, 161, 255),
		color(uint8_t(221), 52, 151, 255),
		color(uint8_t(174), 1, 126, 255),
		color(uint8_t(122), 1, 119, 255),
		color(uint8_t(73), 0, 106, 255)
	};
	m_database["RDPU3"] = std::vector<color>(rdpu3, rdpu3 + sizeof(rdpu3) / sizeof(color));
	m_database["RDPU4"] = std::vector<color>(rdpu4, rdpu4 + sizeof(rdpu4) / sizeof(color));
	m_database["RDPU5"] = std::vector<color>(rdpu5, rdpu5 + sizeof(rdpu5) / sizeof(color));
	m_database["RDPU6"] = std::vector<color>(rdpu6, rdpu6 + sizeof(rdpu6) / sizeof(color));
	m_database["RDPU7"] = std::vector<color>(rdpu7, rdpu7 + sizeof(rdpu7) / sizeof(color));
	m_database["RDPU8"] = std::vector<color>(rdpu8, rdpu8 + sizeof(rdpu8) / sizeof(color));
	m_database["RDPU9"] = std::vector<color>(rdpu9, rdpu9 + sizeof(rdpu9) / sizeof(color));


	// 26. Reds
	static const color reds3[] = {
		color(uint8_t(254), 224, 210, 255),
		color(uint8_t(252), 146, 114, 255),
		color(uint8_t(222), 45, 38, 255)
	};
	static const color reds4[] = {
		color(uint8_t(254), 229, 217, 255),
		color(uint8_t(252), 174, 145, 255),
		color(uint8_t(251), 106, 74, 255),
		color(uint8_t(203), 24, 29, 255)
	};
	static const color reds5[] = {
		color(uint8_t(254), 229, 217, 255),
		color(uint8_t(252), 174, 145, 255),
		color(uint8_t(251), 106, 74, 255),
		color(uint8_t(222), 45, 38, 255),
		color(uint8_t(165), 15, 21, 255)
	};
	static const color reds6[] = {
		color(uint8_t(254), 229, 217, 255),
		color(uint8_t(252), 187, 161, 255),
		color(uint8_t(252), 146, 114, 255),
		color(uint8_t(251), 106, 74, 255),
		color(uint8_t(222), 45, 38, 255),
		color(uint8_t(165), 15, 21, 255)
	};
	static const color reds7[] = {
		color(uint8_t(254), 229, 217, 255),
		color(uint8_t(252), 187, 161, 255),
		color(uint8_t(252), 146, 114, 255),
		color(uint8_t(251), 106, 74, 255),
		color(uint8_t(239), 59, 44, 255),
		color(uint8_t(203), 24, 29, 255),
		color(uint8_t(153), 0, 13, 255)
	};
	static const color reds8[] = {
		color(uint8_t(255), 245, 240, 255),
		color(uint8_t(254), 224, 210, 255),
		color(uint8_t(252), 187, 161, 255),
		color(uint8_t(252), 146, 114, 255),
		color(uint8_t(251), 106, 74, 255),
		color(uint8_t(239), 59, 44, 255),
		color(uint8_t(203), 24, 29, 255),
		color(uint8_t(153), 0, 13, 255)
	};
	static const color reds9[] = {
		color(uint8_t(255), 245, 240, 255),
		color(uint8_t(254), 224, 210, 255),
		color(uint8_t(252), 187, 161, 255),
		color(uint8_t(252), 146, 114, 255),
		color(uint8_t(251), 106, 74, 255),
		color(uint8_t(239), 59, 44, 255),
		color(uint8_t(203), 24, 29, 255),
		color(uint8_t(165), 15, 21, 255),
		color(uint8_t(103), 0, 13, 255)
	};
	m_database["REDS3"] = std::vector<color>(reds3, reds3 + sizeof(reds3) / sizeof(color));
	m_database["REDS4"] = std::vector<color>(reds4, reds4 + sizeof(reds4) / sizeof(color));
	m_database["REDS5"] = std::vector<color>(reds5, reds5 + sizeof(reds5) / sizeof(color));
	m_database["REDS6"] = std::vector<color>(reds6, reds6 + sizeof(reds6) / sizeof(color));
	m_database["REDS7"] = std::vector<color>(reds7, reds7 + sizeof(reds7) / sizeof(color));
	m_database["REDS8"] = std::vector<color>(reds8, reds8 + sizeof(reds8) / sizeof(color));
	m_database["REDS9"] = std::vector<color>(reds9, reds9 + sizeof(reds9) / sizeof(color));

	// 27. RdYlBu
	static const color rdylbu3[] = {
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(145), 191, 219, 255)
	};
	static const color rdylbu4[] = {
		color(uint8_t(215), 25, 28, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(171), 217, 233, 255),
		color(uint8_t(44), 123, 182, 255)
	};
	static const color rdylbu5[] = {
		color(uint8_t(215), 25, 28, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(171), 217, 233, 255),
		color(uint8_t(44), 123, 182, 255)
	};
	static const color rdylbu6[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(254), 224, 144, 255),
		color(uint8_t(224), 243, 248, 255),
		color(uint8_t(145), 191, 219, 255),
		color(uint8_t(69), 117, 180, 255)
	};
	static const color rdylbu7[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(254), 224, 144, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(224), 243, 248, 255),
		color(uint8_t(145), 191, 219, 255),
		color(uint8_t(69), 117, 180, 255)
	};
	static const color rdylbu8[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 144, 255),
		color(uint8_t(224), 243, 248, 255),
		color(uint8_t(171), 217, 233, 255),
		color(uint8_t(116), 173, 209, 255),
		color(uint8_t(69), 117, 180, 255)
	};
	static const color rdylbu9[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 144, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(224), 243, 248, 255),
		color(uint8_t(171), 217, 233, 255),
		color(uint8_t(116), 173, 209, 255),
		color(uint8_t(69), 117, 180, 255)
	};
	static const color rdylbu10[] = {
		color(uint8_t(165), 0, 38, 255),
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 144, 255),
		color(uint8_t(224), 243, 248, 255),
		color(uint8_t(171), 217, 233, 255),
		color(uint8_t(116), 173, 209, 255),
		color(uint8_t(69), 117, 180, 255),
		color(uint8_t(49), 54, 149, 255)
	};
	static const color rdylbu11[] = {
		color(uint8_t(165), 0, 38, 255),
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 144, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(224), 243, 248, 255),
		color(uint8_t(171), 217, 233, 255),
		color(uint8_t(116), 173, 209, 255),
		color(uint8_t(69), 117, 180, 255),
		color(uint8_t(49), 54, 149, 255)
	};
	m_database["RDYLBU3"] = std::vector<color>(rdylbu3, rdylbu3 + sizeof(rdylbu3) / sizeof(color));
	m_database["RDYLBU4"] = std::vector<color>(rdylbu4, rdylbu4 + sizeof(rdylbu4) / sizeof(color));
	m_database["RDYLBU5"] = std::vector<color>(rdylbu5, rdylbu5 + sizeof(rdylbu5) / sizeof(color));
	m_database["RDYLBU6"] = std::vector<color>(rdylbu6, rdylbu6 + sizeof(rdylbu6) / sizeof(color));
	m_database["RDYLBU7"] = std::vector<color>(rdylbu7, rdylbu7 + sizeof(rdylbu7) / sizeof(color));
	m_database["RDYLBU8"] = std::vector<color>(rdylbu8, rdylbu8 + sizeof(rdylbu8) / sizeof(color));
	m_database["RDYLBU9"] = std::vector<color>(rdylbu9, rdylbu9 + sizeof(rdylbu9) / sizeof(color));
	m_database["RDYLBU10"] = std::vector<color>(rdylbu10, rdylbu10 + sizeof(rdylbu10) / sizeof(color));
	m_database["RDYLBU11"] = std::vector<color>(rdylbu11, rdylbu11 + sizeof(rdylbu11) / sizeof(color));

	// 28. RdYlGn
	static const color rdylgn3[] = {
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(145), 207, 96, 255)
	};
	static const color rdylgn4[] = {
		color(uint8_t(215), 25, 28, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(166), 217, 106, 255),
		color(uint8_t(26), 150, 65, 255)
	};
	static const color rdylgn5[] = {
		color(uint8_t(215), 25, 28, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(166), 217, 106, 255),
		color(uint8_t(26), 150, 65, 255)
	};
	static const color rdylgn6[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(217), 239, 139, 255),
		color(uint8_t(145), 207, 96, 255),
		color(uint8_t(26), 152, 80, 255)
	};
	static const color rdylgn7[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(217), 239, 139, 255),
		color(uint8_t(145), 207, 96, 255),
		color(uint8_t(26), 152, 80, 255)
	};
	static const color rdylgn8[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(217), 239, 139, 255),
		color(uint8_t(166), 217, 106, 255),
		color(uint8_t(102), 189, 99, 255),
		color(uint8_t(26), 152, 80, 255)
	};
	static const color rdylgn9[] = {
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(217), 239, 139, 255),
		color(uint8_t(166), 217, 106, 255),
		color(uint8_t(102), 189, 99, 255),
		color(uint8_t(26), 152, 80, 255)
	};
	static const color rdylgn10[] = {
		color(uint8_t(165), 0, 38, 255),
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(217), 239, 139, 255),
		color(uint8_t(166), 217, 106, 255),
		color(uint8_t(102), 189, 99, 255),
		color(uint8_t(26), 152, 80, 255),
		color(uint8_t(0), 104, 55, 255)
	};
	static const color rdylgn11[] = {
		color(uint8_t(165), 0, 38, 255),
		color(uint8_t(215), 48, 39, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(217), 239, 139, 255),
		color(uint8_t(166), 217, 106, 255),
		color(uint8_t(102), 189, 99, 255),
		color(uint8_t(26), 152, 80, 255),
		color(uint8_t(0), 104, 55, 255)
	};
	m_database["RDYLGN3"] = std::vector<color>(rdylgn3, rdylgn3 + sizeof(rdylgn3) / sizeof(color));
	m_database["RDYLGN4"] = std::vector<color>(rdylgn4, rdylgn4 + sizeof(rdylgn4) / sizeof(color));
	m_database["RDYLGN5"] = std::vector<color>(rdylgn5, rdylgn5 + sizeof(rdylgn5) / sizeof(color));
	m_database["RDYLGN6"] = std::vector<color>(rdylgn6, rdylgn6 + sizeof(rdylgn6) / sizeof(color));
	m_database["RDYLGN7"] = std::vector<color>(rdylgn7, rdylgn7 + sizeof(rdylgn7) / sizeof(color));
	m_database["RDYLGN8"] = std::vector<color>(rdylgn8, rdylgn8 + sizeof(rdylgn8) / sizeof(color));
	m_database["RDYLGN9"] = std::vector<color>(rdylgn9, rdylgn9 + sizeof(rdylgn9) / sizeof(color));
	m_database["RDYLGN10"] = std::vector<color>(rdylgn10, rdylgn10 + sizeof(rdylgn10) / sizeof(color));
	m_database["RDYLGN11"] = std::vector<color>(rdylgn11, rdylgn11 + sizeof(rdylgn11) / sizeof(color));

	// 29. Set1
	static const color set1_3[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255)
	};
	static const color set1_4[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255)
	};
	static const color set1_5[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255),
		color(uint8_t(255), 127, 0, 255)
	};
	static const color set1_6[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(255), 255, 51, 255)
	};
	static const color set1_7[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(255), 255, 51, 255),
		color(uint8_t(166), 86, 40, 255)
	};
	static const color set1_8[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(255), 255, 51, 255),
		color(uint8_t(166), 86, 40, 255),
		color(uint8_t(247), 129, 191, 255)
	};
	static const color set1_9[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(255), 255, 51, 255),
		color(uint8_t(166), 86, 40, 255),
		color(uint8_t(247), 129, 191, 255),
		color(uint8_t(153), 153, 153, 255)
	};
	m_database["SET1_3"] = std::vector<color>(set1_3, set1_3 + sizeof(set1_3) / sizeof(color));
	m_database["SET1_4"] = std::vector<color>(set1_4, set1_4 + sizeof(set1_4) / sizeof(color));
	m_database["SET1_5"] = std::vector<color>(set1_5, set1_5 + sizeof(set1_5) / sizeof(color));
	m_database["SET1_6"] = std::vector<color>(set1_6, set1_6 + sizeof(set1_6) / sizeof(color));
	m_database["SET1_7"] = std::vector<color>(set1_7, set1_7 + sizeof(set1_7) / sizeof(color));
	m_database["SET1_8"] = std::vector<color>(set1_8, set1_8 + sizeof(set1_8) / sizeof(color));
	m_database["SET1_9"] = std::vector<color>(set1_9, set1_9 + sizeof(set1_9) / sizeof(color));

	// 30. Set2
	static const color set2_3[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255)
	};
	static const color set2_4[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255),
		color(uint8_t(231), 138, 195, 255)
	};
	static const color set2_5[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255),
		color(uint8_t(231), 138, 195, 255),
		color(uint8_t(166), 216, 84, 255)
	};
	static const color set2_6[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255),
		color(uint8_t(231), 138, 195, 255),
		color(uint8_t(166), 216, 84, 255),
		color(uint8_t(255), 217, 47, 255)
	};
	static const color set2_7[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255),
		color(uint8_t(231), 138, 195, 255),
		color(uint8_t(166), 216, 84, 255),
		color(uint8_t(255), 217, 47, 255),
		color(uint8_t(229), 196, 148, 255)
	};
	static const color set2_8[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255),
		color(uint8_t(231), 138, 195, 255),
		color(uint8_t(166), 216, 84, 255),
		color(uint8_t(255), 217, 47, 255),
		color(uint8_t(229), 196, 148, 255),
		color(uint8_t(179), 179, 179, 255)
	};
	m_database["SET2_3"] = std::vector<color>(set2_3, set2_3 + sizeof(set2_3) / sizeof(color));
	m_database["SET2_4"] = std::vector<color>(set2_4, set2_4 + sizeof(set2_4) / sizeof(color));
	m_database["SET2_5"] = std::vector<color>(set2_5, set2_5 + sizeof(set2_5) / sizeof(color));
	m_database["SET2_6"] = std::vector<color>(set2_6, set2_6 + sizeof(set2_6) / sizeof(color));
	m_database["SET2_7"] = std::vector<color>(set2_7, set2_7 + sizeof(set2_7) / sizeof(color));
	m_database["SET2_8"] = std::vector<color>(set2_8, set2_8 + sizeof(set2_8) / sizeof(color));

	// 31. Set3
	static const color set3_3[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255)
	};
	static const color set3_4[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255)
	};
	static const color set3_5[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255)
	};
	static const color set3_6[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255)
	};
	static const color set3_7[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255)
	};
	static const color set3_8[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255),
		color(uint8_t(252), 205, 229, 255)
	};
	static const color set3_9[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255),
		color(uint8_t(252), 205, 229, 255),
		color(uint8_t(217), 217, 217, 255)
	};
	static const color set3_10[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255),
		color(uint8_t(252), 205, 229, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(188), 128, 189, 255)
	};
	static const color set3_11[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255),
		color(uint8_t(252), 205, 229, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(188), 128, 189, 255),
		color(uint8_t(204), 235, 197, 255)
	};
	static const color set3_12[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 128, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255),
		color(uint8_t(252), 205, 229, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(188), 128, 189, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(255), 237, 111, 255)
	};
	m_database["SET3_3"] = std::vector<color>(set3_3, set3_3 + sizeof(set3_3) / sizeof(color));
	m_database["SET3_4"] = std::vector<color>(set3_4, set3_4 + sizeof(set3_4) / sizeof(color));
	m_database["SET3_5"] = std::vector<color>(set3_5, set3_5 + sizeof(set3_5) / sizeof(color));
	m_database["SET3_6"] = std::vector<color>(set3_6, set3_6 + sizeof(set3_6) / sizeof(color));
	m_database["SET3_7"] = std::vector<color>(set3_7, set3_7 + sizeof(set3_7) / sizeof(color));
	m_database["SET3_8"] = std::vector<color>(set3_8, set3_8 + sizeof(set3_8) / sizeof(color));
	m_database["SET3_9"] = std::vector<color>(set3_9, set3_9 + sizeof(set3_9) / sizeof(color));
	m_database["SET3_10"] = std::vector<color>(set3_10, set3_10 + sizeof(set3_10) / sizeof(color));
	m_database["SET3_11"] = std::vector<color>(set3_11, set3_11 + sizeof(set3_11) / sizeof(color));
	m_database["SET3_12"] = std::vector<color>(set3_12, set3_12 + sizeof(set3_12) / sizeof(color));

	// 32. Spectral
	static const color spectral3[] = {
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(153), 213, 148, 255)
	};

	static const color spectral4[] = {
		color(uint8_t(215), 25, 28, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(171), 221, 164, 255),
		color(uint8_t(43), 131, 186, 255)
	};

	static const color spectral5[] = {
		color(uint8_t(215), 25, 28, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(171), 221, 164, 255),
		color(uint8_t(43), 131, 186, 255)
	};

	static const color spectral6[] = {
		color(uint8_t(213), 62, 79, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(230), 245, 152, 255),
		color(uint8_t(153), 213, 148, 255),
		color(uint8_t(50), 136, 189, 255)
	};

	static const color spectral7[] = {
		color(uint8_t(213), 62, 79, 255),
		color(uint8_t(252), 141, 89, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(230), 245, 152, 255),
		color(uint8_t(153), 213, 148, 255),
		color(uint8_t(50), 136, 189, 255)
	};

	static const color spectral8[] = {
		color(uint8_t(213), 62, 79, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(230), 245, 152, 255),
		color(uint8_t(171), 221, 164, 255),
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(50), 136, 189, 255)
	};

	static const color spectral9[] = {
		color(uint8_t(213), 62, 79, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(230), 245, 152, 255),
		color(uint8_t(171), 221, 164, 255),
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(50), 136, 189, 255)
	};

	static const color spectral10[] = {
		color(uint8_t(158), 1, 66, 255),
		color(uint8_t(213), 62, 79, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(230), 245, 152, 255),
		color(uint8_t(171), 221, 164, 255),
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(50), 136, 189, 255),
		color(uint8_t(94), 79, 162, 255)
	};

	static const color spectral11[] = {
		color(uint8_t(158), 1, 66, 255),
		color(uint8_t(213), 62, 79, 255),
		color(uint8_t(244), 109, 67, 255),
		color(uint8_t(253), 174, 97, 255),
		color(uint8_t(254), 224, 139, 255),
		color(uint8_t(255), 255, 191, 255),
		color(uint8_t(230), 245, 152, 255),
		color(uint8_t(171), 221, 164, 255),
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(50), 136, 189, 255),
		color(uint8_t(94), 79, 162, 255)
	};
	m_database["SPECTRAL3"] = std::vector<color>(spectral3, spectral3 + sizeof(spectral3) / sizeof(color));
	m_database["SPECTRAL4"] = std::vector<color>(spectral4, spectral4 + sizeof(spectral4) / sizeof(color));
	m_database["SPECTRAL5"] = std::vector<color>(spectral5, spectral5 + sizeof(spectral5) / sizeof(color));
	m_database["SPECTRAL6"] = std::vector<color>(spectral6, spectral6 + sizeof(spectral6) / sizeof(color));
	m_database["SPECTRAL7"] = std::vector<color>(spectral7, spectral7 + sizeof(spectral7) / sizeof(color));
	m_database["SPECTRAL8"] = std::vector<color>(spectral8, spectral8 + sizeof(spectral8) / sizeof(color));
	m_database["SPECTRAL9"] = std::vector<color>(spectral9, spectral9 + sizeof(spectral9) / sizeof(color));
	m_database["SPECTRAL10"] = std::vector<color>(spectral10, spectral10 + sizeof(spectral10) / sizeof(color));
	m_database["SPECTRAL11"] = std::vector<color>(spectral11, spectral11 + sizeof(spectral11) / sizeof(color));



	// 33. YlGn
	static const color ylgn3[] = {
		color(uint8_t(247), 252, 185, 255),
		color(uint8_t(173), 221, 142, 255),
		color(uint8_t(49), 163, 84, 255)
	};

	static const color ylgn4[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(194), 230, 153, 255),
		color(uint8_t(120), 198, 121, 255),
		color(uint8_t(35), 132, 67, 255)
	};

	static const color ylgn5[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(194), 230, 153, 255),
		color(uint8_t(120), 198, 121, 255),
		color(uint8_t(49), 163, 84, 255),
		color(uint8_t(0), 104, 55, 255)
	};

	static const color ylgn6[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(217), 240, 163, 255),
		color(uint8_t(173), 221, 142, 255),
		color(uint8_t(120), 198, 121, 255),
		color(uint8_t(49), 163, 84, 255),
		color(uint8_t(0), 104, 55, 255)
	};

	static const color ylgn7[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(217), 240, 163, 255),
		color(uint8_t(173), 221, 142, 255),
		color(uint8_t(120), 198, 121, 255),
		color(uint8_t(65), 171, 93, 255),
		color(uint8_t(35), 132, 67, 255),
		color(uint8_t(0), 90, 50, 255)
	};

	static const color ylgn8[] = {
		color(uint8_t(255), 255, 229, 255),
		color(uint8_t(247), 252, 185, 255),
		color(uint8_t(217), 240, 163, 255),
		color(uint8_t(173), 221, 142, 255),
		color(uint8_t(120), 198, 121, 255),
		color(uint8_t(65), 171, 93, 255),
		color(uint8_t(35), 132, 67, 255),
		color(uint8_t(0), 90, 50, 255)
	};

	static const color ylgn9[] = {
		color(uint8_t(255), 255, 229, 255),
		color(uint8_t(247), 252, 185, 255),
		color(uint8_t(217), 240, 163, 255),
		color(uint8_t(173), 221, 142, 255),
		color(uint8_t(120), 198, 121, 255),
		color(uint8_t(65), 171, 93, 255),
		color(uint8_t(35), 132, 67, 255),
		color(uint8_t(0), 104, 55, 255),
		color(uint8_t(0), 69, 41, 255)
	};
	m_database["YLGN3"] = std::vector<color>(ylgn3, ylgn3 + sizeof(ylgn3) / sizeof(color));
	m_database["YLGN4"] = std::vector<color>(ylgn4, ylgn4 + sizeof(ylgn4) / sizeof(color));
	m_database["YLGN5"] = std::vector<color>(ylgn5, ylgn5 + sizeof(ylgn5) / sizeof(color));
	m_database["YLGN6"] = std::vector<color>(ylgn6, ylgn6 + sizeof(ylgn6) / sizeof(color));
	m_database["YLGN7"] = std::vector<color>(ylgn7, ylgn7 + sizeof(ylgn7) / sizeof(color));
	m_database["YLGN8"] = std::vector<color>(ylgn8, ylgn8 + sizeof(ylgn8) / sizeof(color));
	m_database["YLGN9"] = std::vector<color>(ylgn9, ylgn9 + sizeof(ylgn9) / sizeof(color));

	// 34. YlGnBu
	static const color ylgnbu3[] = {
		color(uint8_t(237), 248, 177, 255),
		color(uint8_t(127), 205, 187, 255),
		color(uint8_t(44), 127, 184, 255)
	};

	static const color ylgnbu4[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(161), 218, 180, 255),
		color(uint8_t(65), 182, 196, 255),
		color(uint8_t(34), 94, 168, 255)
	};

	static const color ylgnbu5[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(161), 218, 180, 255),
		color(uint8_t(65), 182, 196, 255),
		color(uint8_t(44), 127, 184, 255),
		color(uint8_t(37), 52, 148, 255)
	};

	static const color ylgnbu6[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(199), 233, 180, 255),
		color(uint8_t(127), 205, 187, 255),
		color(uint8_t(65), 182, 196, 255),
		color(uint8_t(44), 127, 184, 255),
		color(uint8_t(37), 52, 148, 255)
	};

	static const color ylgnbu7[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(199), 233, 180, 255),
		color(uint8_t(127), 205, 187, 255),
		color(uint8_t(65), 182, 196, 255),
		color(uint8_t(29), 145, 192, 255),
		color(uint8_t(34), 94, 168, 255),
		color(uint8_t(12), 44, 132, 255)
	};

	static const color ylgnbu8[] = {
		color(uint8_t(255), 255, 217, 255),
		color(uint8_t(237), 248, 177, 255),
		color(uint8_t(199), 233, 180, 255),
		color(uint8_t(127), 205, 187, 255),
		color(uint8_t(65), 182, 196, 255),
		color(uint8_t(29), 145, 192, 255),
		color(uint8_t(34), 94, 168, 255),
		color(uint8_t(12), 44, 132, 255)
	};

	static const color ylgnbu9[] = {
		color(uint8_t(255), 255, 217, 255),
		color(uint8_t(237), 248, 177, 255),
		color(uint8_t(199), 233, 180, 255),
		color(uint8_t(127), 205, 187, 255),
		color(uint8_t(65), 182, 196, 255),
		color(uint8_t(29), 145, 192, 255),
		color(uint8_t(34), 94, 168, 255),
		color(uint8_t(37), 52, 148, 255),
		color(uint8_t(8), 29, 88, 255)
	};
	m_database["YLGNBU3"] = std::vector<color>(ylgnbu3, ylgnbu3 + sizeof(ylgnbu3) / sizeof(color));
	m_database["YLGNBU4"] = std::vector<color>(ylgnbu4, ylgnbu4 + sizeof(ylgnbu4) / sizeof(color));
	m_database["YLGNBU5"] = std::vector<color>(ylgnbu5, ylgnbu5 + sizeof(ylgnbu5) / sizeof(color));
	m_database["YLGNBU6"] = std::vector<color>(ylgnbu6, ylgnbu6 + sizeof(ylgnbu6) / sizeof(color));
	m_database["YLGNBU7"] = std::vector<color>(ylgnbu7, ylgnbu7 + sizeof(ylgnbu7) / sizeof(color));
	m_database["YLGNBU8"] = std::vector<color>(ylgnbu8, ylgnbu8 + sizeof(ylgnbu8) / sizeof(color));
	m_database["YLGNBU9"] = std::vector<color>(ylgnbu9, ylgnbu9 + sizeof(ylgnbu9) / sizeof(color));

	// 35. YlOrBr
	static const color ylorbr3[] = {
		color(uint8_t(255), 247, 188, 255),
		color(uint8_t(254), 196, 79, 255),
		color(uint8_t(217), 95, 14, 255)
	};

	static const color ylorbr4[] = {
		color(uint8_t(255), 255, 212, 255),
		color(uint8_t(254), 217, 142, 255),
		color(uint8_t(254), 153, 41, 255),
		color(uint8_t(204), 76, 2, 255)
	};

	static const color ylorbr5[] = {
		color(uint8_t(255), 255, 212, 255),
		color(uint8_t(254), 217, 142, 255),
		color(uint8_t(254), 153, 41, 255),
		color(uint8_t(217), 95, 14, 255),
		color(uint8_t(153), 52, 4, 255)
	};

	static const color ylorbr6[] = {
		color(uint8_t(255), 255, 212, 255),
		color(uint8_t(254), 227, 145, 255),
		color(uint8_t(254), 196, 79, 255),
		color(uint8_t(254), 153, 41, 255),
		color(uint8_t(217), 95, 14, 255),
		color(uint8_t(153), 52, 4, 255)
	};

	static const color ylorbr7[] = {
		color(uint8_t(255), 255, 212, 255),
		color(uint8_t(254), 227, 145, 255),
		color(uint8_t(254), 196, 79, 255),
		color(uint8_t(254), 153, 41, 255),
		color(uint8_t(236), 112, 20, 255),
		color(uint8_t(204), 76, 2, 255),
		color(uint8_t(140), 45, 4, 255)
	};

	static const color ylorbr8[] = {
		color(uint8_t(255), 255, 229, 255),
		color(uint8_t(255), 247, 188, 255),
		color(uint8_t(254), 227, 145, 255),
		color(uint8_t(254), 196, 79, 255),
		color(uint8_t(254), 153, 41, 255),
		color(uint8_t(236), 112, 20, 255),
		color(uint8_t(204), 76, 2, 255),
		color(uint8_t(140), 45, 4, 255)
	};

	static const color ylorbr9[] = {
		color(uint8_t(255), 255, 229, 255),
		color(uint8_t(255), 247, 188, 255),
		color(uint8_t(254), 227, 145, 255),
		color(uint8_t(254), 196, 79, 255),
		color(uint8_t(254), 153, 41, 255),
		color(uint8_t(236), 112, 20, 255),
		color(uint8_t(204), 76, 2, 255),
		color(uint8_t(153), 52, 4, 255),
		color(uint8_t(102), 37, 6, 255)
	};
	m_database["YLORBR3"] = std::vector<color>(ylorbr3, ylorbr3 + sizeof(ylorbr3) / sizeof(color));
	m_database["YLORBR4"] = std::vector<color>(ylorbr4, ylorbr4 + sizeof(ylorbr4) / sizeof(color));
	m_database["YLORBR5"] = std::vector<color>(ylorbr5, ylorbr5 + sizeof(ylorbr5) / sizeof(color));
	m_database["YLORBR6"] = std::vector<color>(ylorbr6, ylorbr6 + sizeof(ylorbr6) / sizeof(color));
	m_database["YLORBR7"] = std::vector<color>(ylorbr7, ylorbr7 + sizeof(ylorbr7) / sizeof(color));
	m_database["YLORBR8"] = std::vector<color>(ylorbr8, ylorbr8 + sizeof(ylorbr8) / sizeof(color));
	m_database["YLORBR9"] = std::vector<color>(ylorbr9, ylorbr9 + sizeof(ylorbr9) / sizeof(color));

	// 36. YlOrRd
	static const color ylorrd3[] = {
		color(uint8_t(255), 237, 160, 255),
		color(uint8_t(254), 178, 76, 255),
		color(uint8_t(240), 59, 32, 255)
	};

	static const color ylorrd4[] = {
		color(uint8_t(255), 255, 178, 255),
		color(uint8_t(254), 204, 92, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(227), 26, 28, 255)
	};

	static const color ylorrd5[] = {
		color(uint8_t(255), 255, 178, 255),
		color(uint8_t(254), 204, 92, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(240), 59, 32, 255),
		color(uint8_t(189), 0, 38, 255)
	};

	static const color ylorrd6[] = {
		color(uint8_t(255), 255, 178, 255),
		color(uint8_t(254), 217, 118, 255),
		color(uint8_t(254), 178, 76, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(240), 59, 32, 255),
		color(uint8_t(189), 0, 38, 255)
	};

	static const color ylorrd7[] = {
		color(uint8_t(255), 255, 178, 255),
		color(uint8_t(254), 217, 118, 255),
		color(uint8_t(254), 178, 76, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(252), 78, 42, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(177), 0, 38, 255)
	};

	static const color ylorrd8[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(255), 237, 160, 255),
		color(uint8_t(254), 217, 118, 255),
		color(uint8_t(254), 178, 76, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(252), 78, 42, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(177), 0, 38, 255)
	};

	static const color ylorrd9[] = {
		color(uint8_t(255), 255, 204, 255),
		color(uint8_t(255), 237, 160, 255),
		color(uint8_t(254), 217, 118, 255),
		color(uint8_t(254), 178, 76, 255),
		color(uint8_t(253), 141, 60, 255),
		color(uint8_t(252), 78, 42, 255),
		color(uint8_t(227), 26, 28, 255),
		color(uint8_t(189), 0, 38, 255),
		color(uint8_t(128), 0, 38, 255)
	};
	m_database["YLORRD3"] = std::vector<color>(ylorrd3, ylorrd3 + sizeof(ylorrd3) / sizeof(color));
	m_database["YLORRD4"] = std::vector<color>(ylorrd4, ylorrd4 + sizeof(ylorrd4) / sizeof(color));
	m_database["YLORRD5"] = std::vector<color>(ylorrd5, ylorrd5 + sizeof(ylorrd5) / sizeof(color));
	m_database["YLORRD6"] = std::vector<color>(ylorrd6, ylorrd6 + sizeof(ylorrd6) / sizeof(color));
	m_database["YLORRD7"] = std::vector<color>(ylorrd7, ylorrd7 + sizeof(ylorrd7) / sizeof(color));
	m_database["YLORRD8"] = std::vector<color>(ylorrd8, ylorrd8 + sizeof(ylorrd8) / sizeof(color));
	m_database["YLORRD9"] = std::vector<color>(ylorrd9, ylorrd9 + sizeof(ylorrd9) / sizeof(color));

	static const color graph1_9[] = {
		color(uint8_t(228), 26, 28, 255),
		color(uint8_t(55), 126, 184, 255),
		color(uint8_t(77), 175, 74, 255),
		color(uint8_t(152), 78, 163, 255),
		color(uint8_t(255), 127, 0, 255),
		color(uint8_t(255), 255, 51, 255),
		color(uint8_t(166), 86, 40, 255),
		color(uint8_t(247), 129, 191, 255),
		color(uint8_t(153), 153, 153, 255)
	};
	m_database["GRAPH1_9"] = std::vector<color>(graph1_9, graph1_9 + sizeof(graph1_9) / sizeof(color));

	static const color graph2_8[] = {
		color(uint8_t(102), 194, 165, 255),
		color(uint8_t(252), 141, 98, 255),
		color(uint8_t(141), 160, 203, 255),
		color(uint8_t(231), 138, 195, 255),
		color(uint8_t(166), 216, 84, 255),
		color(uint8_t(255), 217, 47, 255),
		color(uint8_t(229), 196, 148, 255),
		color(uint8_t(179), 179, 179, 255)
	};
	m_database["GRAPH2_8"] = std::vector<color>(graph2_8, graph2_8 + sizeof(graph2_8) / sizeof(color));

	static const color graph3_12[] = {
		color(uint8_t(141), 211, 199, 255),
		color(uint8_t(190), 186, 218, 255),
		color(uint8_t(251), 126, 114, 255),
		color(uint8_t(128), 177, 211, 255),
		color(uint8_t(253), 180, 98, 255),
		color(uint8_t(179), 222, 105, 255),
		color(uint8_t(252), 205, 229, 255),
		color(uint8_t(217), 217, 217, 255),
		color(uint8_t(188), 128, 189, 255),
		color(uint8_t(204), 235, 197, 255),
		color(uint8_t(255), 255, 179, 255),
		color(uint8_t(255), 237, 111, 255)
	};
	m_database["GRAPH3_12"] = std::vector<color>(graph3_12, graph3_12 + sizeof(graph3_12) / sizeof(color));

	static const color node_error6[] = {
		color(uint8_t(33), 102, 172),
		color(uint8_t(90), 180, 172),
		color(uint8_t(153), 213, 148),
		color(uint8_t(240), 185, 120),
		color(uint8_t(239), 138, 98),
		color(uint8_t(213), 62, 79)
	};
	m_database["NODE_ERROR_6"] = std::vector<color>(node_error6, node_error6 + sizeof(node_error6) / sizeof(color));
}