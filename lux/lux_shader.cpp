#include"lux_shader.h"
#include<stdio.h>
#include<stdlib.h>
#include<inttypes.h>
#include<string>
#include<vector>
#include<functional>
#include<algorithm>

#ifdef _DEBUG
#define DEFAULT_ERR_STREAM stderr
#else
#define DEFAULT_ERR_STREAM stderr //nullptr
#endif
	

lux::shader::shader(void) : m_handle(0), m_error(DEFAULT_ERR_STREAM), m_type(0) {}

lux::shader::shader(GLenum type, const char* source, const shader_source& s) : m_handle(0), m_error(DEFAULT_ERR_STREAM), m_type(0) {
	compile(type, source, s);
}

lux::shader::~shader(void) {
	if (m_handle != 0) {
		glDeleteShader(m_handle);
		m_handle = 0;
	}
}

bool lux::shader::compile(GLenum type, const char* source, const shader_source& s) {
	if (source == nullptr) return false;
	const char* src;
	bool delete_src = false;
	switch (s) {
	case SOURCE_FILE:
		src = load_shader(source);
		if (src == nullptr) return false;
		delete_src = true;
		break;
	case SOURCE_TEXT:
		src = source;
		break;
	}
	if (m_handle != 0) glDeleteShader(m_handle);
	m_handle = glCreateShader(type);

	
	glShaderSource(m_handle, 1, &src, nullptr);
	switch (s) {
	case SOURCE_FILE: delete[] src; break;
	}
	glCompileShader(m_handle);
	
	GLint success = 0;
	glGetShaderiv(m_handle, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE) {
		// Error handling
		if (m_error == nullptr) { // silent fail
			delete[] src;
			glDeleteShader(m_handle);
			m_handle = 0;
			return false;
		}
		GLint logsize = 0;
		glGetShaderiv(m_handle, GL_INFO_LOG_LENGTH, &logsize);
		char* log = new char[logsize];
		GLsizei len;
		glGetShaderInfoLog(m_handle, logsize, &len, log);

		parse_log(m_error, log, src);
		if (delete_src) delete[] src;
		delete[] log;

		glDeleteShader(m_handle);
		m_handle = 0;
		return false;
	}
	m_type = type;
	return true;
}

void lux::shader::set_errstream(FILE* stream) {
	m_error = stream;
}

const char* lux::shader::load_shader(const char* filename) {
	if (filename == nullptr) return nullptr;
	FILE* stream = nullptr;
	if (fopen_s(&stream, filename, "rt")) return nullptr;
	fseek(stream, 0, SEEK_END);
	int64_t len = _ftelli64(stream);
	fseek(stream, 0, SEEK_SET);
	char* buf = new char[len + 1];
	size_t nChars = fread(buf, sizeof(char), len, stream);
	buf[nChars] = 0;
	fclose(stream);
	return buf;
}

void lux::shader::parse_log(FILE* errstream, const char* log, const char* source) {
	// Parse info log
	std::vector<std::pair<size_t, std::string> > parsed_log;
	std::string slog(log);
	auto next_token = [&]() {
		size_t pos = slog.find_first_of('\n');
		if (pos == std::string::npos) {
			std::string result;
			result.swap(slog);
			return result;
		}
		std::string result = slog.substr(0, pos);
		slog = slog.substr(pos);
		pos = slog.find_first_not_of('\n');
		if (pos == std::string::npos) slog.clear();
		else slog = slog.substr(pos);
		return result;
	};


	while (!slog.empty()) {
		parsed_log.push_back(std::make_pair(0, next_token()));
	}

	// Parse line numbers
	std::function<size_t(const std::string&)> line;
	switch (get_vendor()) {
	case VENDOR_NVIDIA:
		line = std::function<size_t(const std::string&)>([](const std::string& str) {
			size_t pos1 = str.find_first_of('(');
			size_t pos2 = str.find_first_of(')', pos1);
			if (pos1 == std::string::npos || pos2 == std::string::npos) return std::string::npos;
			std::string num(str.cbegin() + pos1 + 1, str.cbegin() + pos2);
			return size_t(atoi(num.c_str()));
		});
		break;
	case VENDOR_INTEL:
		line = std::function<size_t(const std::string&)>([](const std::string& str) {
			size_t pos1 = str.find_first_of(':');
			if (pos1 == std::string::npos) return pos1;
			size_t pos2 = str.find_first_of(':', pos1 + 1);
			if (pos2 == std::string::npos) return pos2;
			size_t pos3 = str.find_first_of(':', pos2 + 1);
			if (pos3 == std::string::npos) return pos3;
			std::string num(str.cbegin() + pos2 + 1, str.cbegin() + pos3);
			return size_t(atoi(num.c_str()));
		});
		break;
	default:
		line = std::function<size_t(const std::string&)>([](const std::string& str) { return std::string::npos; });
		break;
	}

	for (size_t n = 0; n < parsed_log.size(); n++) parsed_log[n].first = line(parsed_log[n].second);
	std::stable_sort(parsed_log.begin(), parsed_log.end());
	size_t nLine = 1;
	size_t pos = 0;
	std::string sSrc(source);
	
	size_t errpos = 0;
	while (!sSrc.empty()) {
		size_t pos = sSrc.find_first_of('\n');
		if (pos == std::string::npos) break;
		fprintf(errstream, "[%.3zi] %s\n", nLine, sSrc.substr(0, pos).c_str());
		sSrc = sSrc.substr(pos + 1);
		while (errpos < parsed_log.size()) {
			if (parsed_log[errpos].first != nLine) break;
			fprintf(errstream, "----> %s\n", parsed_log[errpos++].second.c_str());
		}
		nLine++;
	}
	fprintf(errstream, "\n");
}

lux::program::program(const std::string& name) : m_handle(0), m_error(DEFAULT_ERR_STREAM), m_enabled(false), m_valid(false), m_name(name) {
	m_handle = glCreateProgram();
}

lux::program::~program(void) {
	if (m_enabled) disable();
	detach_all();
	glDeleteProgram(m_handle);
	m_handle = 0;
	m_valid = false;
}

void lux::program::detach_all(void) {
	for (auto& it : m_attached) glDetachShader(m_handle, it);
	m_attached.clear();
}

bool lux::program::attach(const shader& handle) {
	if ((GLuint)handle == 0) return false;
	if (m_enabled) disable();
	glAttachShader(m_handle, (GLuint)handle);
	m_valid = false;
	m_attached.insert((GLuint)handle);
	return true;
}

bool lux::program::detach(const shader& handle) {
	if (m_attached.find((GLuint)handle) == m_attached.end()) return false;
	m_attached.erase((GLuint)handle);
	return true;
}

bool lux::program::attach(const GLuint& handle) {
	if (handle == 0) return false;
	if (m_enabled) disable();
	glAttachShader(m_handle, handle);
	m_valid = false;
	return true;
}

bool lux::program::detach(const GLuint& handle) {
	if (m_attached.find((GLuint)handle) == m_attached.end()) return false;
	m_attached.erase((GLuint)handle);
	return true;
}

void lux::program::set_errstream(FILE* stream) {
	m_error = stream;
}

bool lux::program::link(void) {
	glLinkProgram(m_handle);
	GLint isLinked = 0;
	glGetProgramiv(m_handle, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE) {
		if (m_error == nullptr) return false;
		
		GLint len = 0;
		glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &len);
		char* log = new char[len];
		glGetProgramInfoLog(m_handle, len, &len, log);

		fprintf(m_error, "%s\n", log);
		delete[] log;
		return false;
	}
	m_valid = true;
	return true;
}

bool lux::program::enable(void) {
	if (!m_valid) return false;
	if (m_enabled) return false;
	glUseProgram(m_handle);
	return true;
}

bool lux::program::disable(void) {
	if (!m_valid) return false;
	if (!m_enabled) return false;
	glUseProgram(0);
	return true;
}

bool lux::program::get_uniform(const std::string& name, GLint& loc, GLenum& type) {
	if (m_handle == 0) return false;
	loc = glGetUniformLocation(m_handle, name.c_str());
	if (loc == -1) {
		if (m_error != nullptr) {
			fprintf(m_error, "Error retrieving location of uniform '%s'\n", name.c_str());
		}
		return false;
	}
	// Type-check
	GLsizei len = 0;
	GLint size = 0;
	glGetActiveUniform(m_handle, loc, 0, &len, &size, &type, nullptr);
	return true;
}

bool lux::program::set_uniform(const std::string& name, float v0) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_FLOAT:
		glProgramUniform1f(m_handle, loc, v0); break;
	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v0)); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v0 != 0.0f ? 1 : 0); break;
	case GL_INT:
		glProgramUniform1i(m_handle, loc, int(v0)); break;
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v0)); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to float\n", name.c_str());
		}
		return false;
	}
	return true;
}

bool lux::program::set_uniform(const std::string& name, int v0) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_INT:
		glProgramUniform1i(m_handle, loc, v0); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v0 != 0 ? 1 : 0); break;
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v0)); break;
	case GL_FLOAT:
		glProgramUniform1f(m_handle, loc, float(v0)); break;
	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v0)); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to int\n", name.c_str());
		}
		return false;
	}
	return true;
}

bool lux::program::set_uniform(const std::string& name, uint32_t v0) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v0)); break;
#ifdef AUTO_CONVERT_UNIFORMS
	case GL_INT:
		glProgramUniform1i(m_handle, loc, v0); break;
	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v0 != 0 ? 1 : 0); break;
	case GL_FLOAT:
		glProgramUniform1f(m_handle, loc, float(v0)); break;
	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v0)); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to uint\n", name.c_str());
		}
		return false;
	}
	return true;
}

bool lux::program::set_uniform(const std::string& name, bool v0) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, int(v0)); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v0)); break;
	case GL_INT:
		glProgramUniform1i(m_handle, loc, v0); break;
	case GL_FLOAT:
		glProgramUniform1f(m_handle, loc, float(v0)); break;
	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v0)); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to bool\n", name.c_str());
		}
		return false;
	}
	return true;
}

bool lux::program::set_uniform(const std::string& name, float v0, float v1) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, int v0, int v1) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, uint32_t v0, uint32_t v1) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, bool v0, bool v1) {
	assert(!"NOT IMPLEMENTED");
	return false;
}


bool lux::program::set_uniform(const std::string& name, float v0, float v1, float v2) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, int v0, int v1, int v2) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, uint32_t v0, uint32_t v1, uint32_t v2) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, bool v0, bool v1, bool v2) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, float v0, float v1, float v2, float v4) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, int v0, int v1, int v2, int v4) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, uint32_t v0, uint32_t v1, uint32_t v2, uint32_t v4) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, bool v0, bool v1, bool v2, bool v4) {
	assert(!"NOT IMPLEMENTED");
	return false;
}

bool lux::program::set_uniform(const std::string& name, const lux::vec4f& v) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_FLOAT:
		glProgramUniform1fv(m_handle, loc, 1, v); break;
	case GL_FLOAT_VEC2:
		glProgramUniform2fv(m_handle, loc, 1, v); break;
	case GL_FLOAT_VEC3:
		glProgramUniform3fv(m_handle, loc, 1, v); break;
	case GL_FLOAT_VEC4:
		glProgramUniform4fv(m_handle, loc, 1, v); break;

	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v(0))); break;
	case GL_DOUBLE_VEC2:
		glProgramUniform2dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC3:
		glProgramUniform3dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC4:
		glProgramUniform4dv(m_handle, loc, 1, v.as<double>()); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v(0) != 0.0f ? 1 : 0); break;
	case GL_BOOL_VEC2:
	{
		int D[2] = { v(0) != 0.0f ? 1 : 0 , v(1)!=0.0f ? 1 : 0}; break;
		glProgramUniform2iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC3:
	{
		int D[3] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0 }; break;
		glProgramUniform3iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC4:
	{
		int D[4] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0, v(3)!=0.0f ? 1 : 0 }; break;
		glProgramUniform4iv(m_handle, loc, 1, D); break;
	}
	case GL_INT:
		glProgramUniform1i(m_handle, loc, int(v(0))); break;
	case GL_INT_VEC2:
		glProgramUniform2iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_INT_VEC3:
		glProgramUniform3iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_INT_VEC4:
		glProgramUniform4iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v(0))); break;
	case GL_UNSIGNED_INT_VEC2:
		glProgramUniform2uiv(m_handle, loc, 1, v.as<GLuint>()); break;
	case GL_UNSIGNED_INT_VEC3:
		glProgramUniform3uiv(m_handle, loc, 1, v.as<GLuint>()); break;
	case GL_UNSIGNED_INT_VEC4:
		glProgramUniform4uiv(m_handle, loc, 1, v.as<GLuint>()); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to vec4\n", name.c_str());
		}
		return false;
	}
	return false;
}

bool lux::program::set_uniform(const std::string& name, const lux::color& v) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_FLOAT:
		glProgramUniform1fv(m_handle, loc, 1, v); break;
	case GL_FLOAT_VEC2:
		glProgramUniform2fv(m_handle, loc, 1, v); break;
	case GL_FLOAT_VEC3:
		glProgramUniform3fv(m_handle, loc, 1, v); break;
	case GL_FLOAT_VEC4:
		glProgramUniform4fv(m_handle, loc, 1, v); break;

	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v(0))); break;
	case GL_DOUBLE_VEC2:
		glProgramUniform2dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC3:
		glProgramUniform3dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC4:
		glProgramUniform4dv(m_handle, loc, 1, v.as<double>()); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v(0) != 0.0f ? 1 : 0); break;
	case GL_BOOL_VEC2:
	{
		int D[2] = { v(0) != 0.0f ? 1 : 0 , v(1) != 0.0f ? 1 : 0 }; break;
		glProgramUniform2iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC3:
	{
		int D[3] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0 }; break;
		glProgramUniform3iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC4:
	{
		int D[4] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0, v(3) != 0.0f ? 1 : 0 }; break;
		glProgramUniform4iv(m_handle, loc, 1, D); break;
	}
	case GL_INT:
		glProgramUniform1i(m_handle, loc, int(v(0))); break;
	case GL_INT_VEC2:
		glProgramUniform2iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_INT_VEC3:
		glProgramUniform3iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_INT_VEC4:
		glProgramUniform4iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v(0))); break;
	case GL_UNSIGNED_INT_VEC2:
		glProgramUniform2uiv(m_handle, loc, 1, v.as<GLuint>()); break;
	case GL_UNSIGNED_INT_VEC3:
		glProgramUniform3uiv(m_handle, loc, 1, v.as<GLuint>()); break;
	case GL_UNSIGNED_INT_VEC4:
		glProgramUniform4uiv(m_handle, loc, 1, v.as<GLuint>()); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to vec4\n", name.c_str());
		}
		return false;
	}
	return false;
}

bool lux::program::set_uniform(const std::string& name, const lux::vec4<int>& v) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_INT:
		glProgramUniform1i(m_handle, loc, v(0)); break;
	case GL_INT_VEC2:
		glProgramUniform2iv(m_handle, loc, 1, v); break;
	case GL_INT_VEC3:
		glProgramUniform3iv(m_handle, loc, 1, v); break;
	case GL_INT_VEC4:
		glProgramUniform4iv(m_handle, loc, 1, v); break;

	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v(0))); break;
	case GL_DOUBLE_VEC2:
		glProgramUniform2dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC3:
		glProgramUniform3dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC4:
		glProgramUniform4dv(m_handle, loc, 1, v.as<double>()); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_FLOAT:
		glProgramUniform1f(m_handle, loc, float(v(0))); break;
	case GL_FLOAT_VEC2:
		glProgramUniform2fv(m_handle, loc, 1, v.as<float>()); break;
	case GL_FLOAT_VEC3:
		glProgramUniform3fv(m_handle, loc, 1, v.as<float>()); break;
	case GL_FLOAT_VEC4:
		glProgramUniform4fv(m_handle, loc, 1, v.as<float>()); break;

	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v(0) != 0.0f ? 1 : 0); break;
	case GL_BOOL_VEC2:
	{
		int D[2] = { v(0) != 0.0f ? 1 : 0 , v(1) != 0.0f ? 1 : 0 }; break;
		glProgramUniform2iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC3:
	{
		int D[3] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0 }; break;
		glProgramUniform3iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC4:
	{
		int D[4] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0, v(3) != 0.0f ? 1 : 0 }; break;
		glProgramUniform4iv(m_handle, loc, 1, D); break;
	}
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, GLuint(v(0))); break;
	case GL_UNSIGNED_INT_VEC2:
		glProgramUniform2uiv(m_handle, loc, 1, v.as<GLuint>()); break;
	case GL_UNSIGNED_INT_VEC3:
		glProgramUniform3uiv(m_handle, loc, 1, v.as<GLuint>()); break;
	case GL_UNSIGNED_INT_VEC4:
		glProgramUniform4uiv(m_handle, loc, 1, v.as<GLuint>()); break;
#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to vec4\n", name.c_str());
		}
		return false;
	}
	return false;
}

bool lux::program::set_uniform(const std::string& name, const lux::vec4<uint32_t>& v) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name, loc, type)) return false;
	switch (type) {
	case GL_UNSIGNED_INT:
		glProgramUniform1ui(m_handle, loc, v(0)); break;
	case GL_UNSIGNED_INT_VEC2:
		glProgramUniform2uiv(m_handle, loc, 1, v); break;
	case GL_UNSIGNED_INT_VEC3:
		glProgramUniform3uiv(m_handle, loc, 1, v); break;
	case GL_UNSIGNED_INT_VEC4:
		glProgramUniform4uiv(m_handle, loc, 1, v); break;

	case GL_DOUBLE:
		glProgramUniform1d(m_handle, loc, double(v(0))); break;
	case GL_DOUBLE_VEC2:
		glProgramUniform2dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC3:
		glProgramUniform3dv(m_handle, loc, 1, v.as<double>()); break;
	case GL_DOUBLE_VEC4:
		glProgramUniform4dv(m_handle, loc, 1, v.as<double>()); break;

#ifdef AUTO_CONVERT_UNIFORMS
	case GL_FLOAT:
		glProgramUniform1f(m_handle, loc, float(v(0))); break;
	case GL_FLOAT_VEC2:
		glProgramUniform2fv(m_handle, loc, 1, v.as<float>()); break;
	case GL_FLOAT_VEC3:
		glProgramUniform3fv(m_handle, loc, 1, v.as<float>()); break;
	case GL_FLOAT_VEC4:
		glProgramUniform4fv(m_handle, loc, 1, v.as<float>()); break;

	case GL_BOOL:
		glProgramUniform1i(m_handle, loc, v(0) != 0.0f ? 1 : 0); break;
	case GL_BOOL_VEC2:
	{
		int D[2] = { v(0) != 0.0f ? 1 : 0 , v(1) != 0.0f ? 1 : 0 }; break;
		glProgramUniform2iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC3:
	{
		int D[3] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0 }; break;
		glProgramUniform3iv(m_handle, loc, 1, D); break;
	}
	case GL_BOOL_VEC4:
	{
		int D[4] = { v(0) != 0.0f ? 1 : 0, v(1) != 0.0f ? 1 : 0, v(2) != 0.0f ? 1 : 0, v(3) != 0.0f ? 1 : 0 }; break;
		glProgramUniform4iv(m_handle, loc, 1, D); break;
	}
	case GL_INT:
		glProgramUniform1i(m_handle, loc, int(v(0))); break;
	case GL_INT_VEC2:
		glProgramUniform2iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_INT_VEC3:
		glProgramUniform3iv(m_handle, loc, 1, v.as<int>()); break;
	case GL_INT_VEC4:
		glProgramUniform4iv(m_handle, loc, 1, v.as<int>()); break;

#endif
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to vec4\n", name.c_str());
		}
		return false;
	}
	return false;
}




bool lux::program::set_uniform(const std::string& name, const lux::mat4f& m, bool transpose) {
	GLint loc;
	GLenum type;
	if (!get_uniform(name,loc,type)) return false;
	switch (type) {
	case GL_FLOAT_MAT4:
		glProgramUniformMatrix4fv(m_handle, loc, 1, transpose, m);
		break;
	case GL_DOUBLE_MAT4:
	{
		double M[16];
		const float* pm = m;
		for (size_t n = 0; n < 16; n++) M[n] = (double)pm[n];
		glProgramUniformMatrix4dv(m_handle, loc, 1, transpose, M);
	}
	break;
	default:
		if (m_error != nullptr) {
			fprintf(m_error, "Error setting uniform '%s' to mat4\n", name.c_str());
		}
		return false;
	}
	return true;
}