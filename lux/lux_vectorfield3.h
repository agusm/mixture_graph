#ifndef __LUX_VECTORFIELD3_H__
#define __LUX_VECTORFIELD3_H__

#include<vector>
#include<cassert>
#include<functional>
#include<string>
#include"lux_image.h"	// for range_t
#include"lux_math.h"

namespace lux {
	enum BOUNDARY;
	inline int64_t map_coordinate(const int64_t& i, const int64_t& dim, const lux::BOUNDARY& b);

	template<class T> class coordinateframe;
	template<class T> quat4<T> slerp(const quat4<T>& q1, const quat4<T>& q2, const T& t);

	enum PARAMETER {
		ARC_LENGTH,
		TIME
	};

	// arc length samples
	template<class T>
	struct sample_t {
		vec4<T>		position;		// x,y,z, arc length
		quat4<T>	orientation;	// orientation as unit quaternion
		T			curvature;		// curvature
		T			torsion;		// torsion
		T			vel_magnitude;	// velocity magnitude
		T			time;			// physical time parameter

		inline T get_parameter(const PARAMETER& p = ARC_LENGTH) const {
			switch (p) {
			case ARC_LENGTH: return position(3);
			case TIME: return time;
			default: return T(0);
			}
		}

		// velocity = vel_magnitude*tangent
		inline vec4<T> get_tangent(void) const {
			return vec4<T>(
				T(1) - T(2) * (orientation(2)*orientation(2) + orientation(3)*orientation(3)), 
				T(2) * (orientation(1) * orientation(2) - orientation(3) * orientation(0)), 
				T(2) * (orientation(1) * orientation(3) + orientation(2) * orientation(0)), 
				T(1) // for integration, get_velocity
				);
				
		}
		inline vec4<T> get_velocity(void) const { // returns vx, vy, vz, ||v||
			return vel_magnitude*get_tangent();
		}
		// acceleration = curvature*normal
		inline vec4<T> get_normal(void) const {
			return vec4<T>(
				T(2) * (orientation(1) * orientation(2) + orientation(3) * orientation(0)), 
				T(1) - T(2) * (orientation(1) * orientation(1) + orientation(3) * orientation(3)), 
				T(2) * (orientation(2) * orientation(3) - orientation(1) * orientation(0)), 
				T(1) // for integration, get_acceleration
			);
		}
		inline vec4<T> get_acceleration(void) const {
			return curvature*get_normal();
		}
		inline vec4<T> get_binormal(void) const {
			return vec4<T>(
				T(2) * (orientation(1) * orientation(3) - orientation(2) * orientation(0)), 
				T(2) * (orientation(2) * orientation(3) + orientation(1) * orientation(0)), 
				T(1) - T(2) * (orientation(1) * orientation(1) + orientation(2) * orientation(2)), 
				T(0)
			);
		}
		// d/dt of orientation
		inline quat4<T> dQdt(void) const {
			return quat4<T>(
				0.5*(torsion*orientation(1) + curvature*orientation(3)),
				0.5*(curvature*orientation(2) - torsion*orientation(0)),
				0.5*(torsion*orientation(3) - curvature*orientation(1)),
				-0.5*(torsion*orientation(2) + curvature*orientation(0))
			);
		}
		inline quat4<T> dQconjdt(void) const {
			return quat4<T>(
				-0.5*(torsion*orientation(1) + curvature*orientation(3)),
				0.5*(torsion*orientation(0) + curvature*orientation(2)),
				0.5*(torsion*orientation(3) - curvature*orientation(1)),
				0.5*(curvature*orientation(0) - torsion*orientation(2))
				);
		}

		inline void construct(const vec4<T>& pos, const vec4<T>& vel, const vec4<T>& acc, const vec4<T>& jot, const T& phys_time = T(0)) {
			// position
			position = pos;
			// velocity magnitude
			vel_magnitude = vel.length(3);
			// physical time parameter
			time = phys_time;
			// curvature, torsion
			lux::vec4<T> va = vel^acc;
			curvature = va.length(3) / pow(vel_magnitude, T(3));
			torsion = (jot(0)*va(0) + jot(1)*va(1) + jot(2)*va(2)) / va.length2(3);
			// orientation
			lux::vec4<T> X(vel);	// tangent
			if (vel_magnitude != T(0)) X /= vel_magnitude;
			lux::vec4<T> Y(acc);	// normal
			Y.normalize(3);
			lux::vec4<T> Z = X^Y;	// binormal
			Z.normalize(3);
			Y = Z^X;
			Y.normalize(3);
			orientation(0) = 0.5*sqrt(std::max(0.0, 1.0 + X(0) + Y(1) + Z(2)));
			orientation(1) = 0.5*sqrt(std::max(0.0, 1.0 + X(0) - Y(1) - Z(2)));
			orientation(2) = 0.5*sqrt(std::max(0.0, 1.0 - X(0) + Y(1) - Z(2)));
			orientation(3) = 0.5*sqrt(std::max(0.0, 1.0 - X(0) - Y(1) + Z(2)));
			orientation(1) = lux::copysign(orientation(1), Z(1) - Y(2));
			orientation(2) = lux::copysign(orientation(2), X(2) - Z(0));
			orientation(3) = lux::copysign(orientation(3), Y(0) - X(1));
			orientation(0) = sqrt(std::max(0.0, 1.0 - orientation(1)*orientation(1) - orientation(2)*orientation(2) - orientation(3)*orientation(3)));
			/*
			// check
			mat4<T> R = orientation.asMatrix();
			double d = 0.0;
			for (size_t n = 0; n < 3; n++) {
				d += (R(0, n) - X(n))*(R(0, n) - X(n));
				d += (R(1, n) - Y(n))*(R(1, n) - Y(n));
				d += (R(2, n) - Z(n))*(R(2, n) - Z(n));
			}
			if (d > 1e-4) {
				printf("oops\n");
			}
			*/
		}
		inline void construct_arclength(const vec4<T>& pos, const vec4<T>& vel, const vec4<T>& acc, const vec4<T>& jot, const T& phys_time = T(0)) {
			static T tau_max = T(0);
			static T kappa_max = T(0);
			position = pos;
			vel_magnitude = T(1);
			time = phys_time;
			lux::vec4<T> va = vel^acc;
			curvature = va.length(3);
			T k2 = curvature*curvature;
			if (curvature == T(0)) torsion = T(0);
			else {
				torsion = T(1.0 / k2)*(jot(0)*va(0) + jot(1)*va(1) + jot(2)*va(2));
			}
			tau_max = std::max(tau_max, std::abs(torsion));
			kappa_max = std::max(kappa_max, curvature);
			lux::vec4<T> X(vel);	// tangent
			X.normalize();			// just to be safe
			lux::vec4<T> Y(acc);	// normal
			lux::vec4<T> Z = X^Y;	// binormal
			Z.normalize(3);
			Y = Z^X;
			Y.normalize(3);
			orientation(0) = 0.5*sqrt(std::max(0.0, 1.0 + X(0) + Y(1) + Z(2)));
			orientation(1) = 0.5*sqrt(std::max(0.0, 1.0 + X(0) - Y(1) - Z(2)));
			orientation(2) = 0.5*sqrt(std::max(0.0, 1.0 - X(0) + Y(1) - Z(2)));
			orientation(3) = 0.5*sqrt(std::max(0.0, 1.0 - X(0) - Y(1) + Z(2)));
			orientation(1) = lux::copysign(orientation(1), Z(1) - Y(2));
			orientation(2) = lux::copysign(orientation(2), X(2) - Z(0));
			orientation(3) = lux::copysign(orientation(3), Y(0) - X(1));
			orientation(0) = sqrt(std::max(0.0, 1.0 - orientation(1)*orientation(1) - orientation(2)*orientation(2) - orientation(3)*orientation(3)));
		}
	};

	template<class T>
	class vectorfield3 {
	public:
		typedef T base_type;
		vectorfield3(void);
		vectorfield3(const vectorfield3& other);
		vectorfield3(const size_t& dimx, const size_t& dimy, const size_t& dimz);
		~vectorfield3(void);
		bool empty(void) const;
		size_t size(void) const;
		void clear(void);
		void resize(size_t dimx, size_t dimy, size_t dimz);

		// 3D addressing
		inline const vec4<T>&		get_velocity(size_t i, size_t j, size_t k) const;			// (vx vy vz 1) / ||v||_2
		inline const vec4<T>&		get_acceleration(size_t i, size_t j, size_t k) const;		// (ax ay az 0)
		inline const quat4<T>&		get_orientation(size_t i, size_t j, size_t k) const;		// Frenet-Serret frame as quaternion
		inline const T&				get_curvature(size_t i, size_t j, size_t k) const;			// curvature
		inline const T&				get_torsion(size_t i, size_t j, size_t k) const;			// torsion
		inline void					set_velocity(size_t i, size_t j, size_t k, const vec4<T>& v);
		inline void					set_acceleration(size_t i, size_t j, size_t k, const vec4<T>& a);
		inline void					set_orientation(size_t i, size_t j, size_t k, const quat4<T>& q);
		inline void					set_kappa(size_t i, size_t j, size_t k, const T& kappa);
		inline void					set_tau(size_t i, size_t j, size_t k, const T& tau);

		// Linear addressing
		inline const vec4<T>&		get_velocity(size_t n) const;
		inline const vec4<T>&		get_acceleration(size_t n) const;
		inline const quat4<T>&		get_orientation(size_t n) const;
		inline const T&				get_curvature(size_t n) const;
		inline const T&				get_torsion(size_t n) const;
		inline void					set_velocity(size_t n, const vec4<T>& v);
		inline void					set_acceleration(size_t n, const vec4<T>& a);
		inline void					set_orientation(size_t n, const quat4<T>& q);
		inline void					set_curvature(size_t n, const T& kappa);
		inline void					set_torsion(size_t n, const T& tau);

		inline const BOUNDARY&		get_boundary(void) const;								// boundary
		inline const std::string&	get_name(void) const;									// name of the data set
		inline const range_t<typename T>&		get_domain(size_t n) const;								// nth domain range

		inline void set_boundary(const BOUNDARY& b);
		inline void set_name(const std::string& name);
		inline void set_domain(size_t n, const ranged_t& r);

		void update(void);

		vec4<T> sample_velocity(const lux::vec4<T>& pos) const;				// C1, tri-Catmull-Rom
		vec4<T> sample_acceleration(const lux::vec4<T>& pos) const;			// C0, tri-linear
		T sample_curvature(const lux::vec4<T>& pos) const;					// C0, tri-linear
		T sample_torsion(const lux::vec4<T>& pos) const;					// C0, tri-linear
		quat4<T> sample_orientation(const lux::vec4<T>& pos) const;			// C0, tri-slerp
		inline void get_sample(sample_t<T>& s, const vec4<T>& x) const {
			s.position = x;
			s.vel_magnitude = sample_velocity(x).length(3);
			s.curvature = sample_curvature(x);
			s.torsion = sample_torsion(x);
			s.orientation = sample_orientation(x);
		}

		size_t dim(size_t n) const;
		vectorfield3& operator=(const vectorfield3& other);
		typedef std::function<void(vec4<double>&, const double&, const double&, const double&)> fill_function; // io_vel, io_acc, x, y, z, dims
		void fill(const ranged_t& X, const ranged_t& Y, const ranged_t& Z, const fill_function& F);
		inline size_t linear_address(size_t i, size_t j, size_t k) const;
		void swap(vectorfield3& other);
		template<class S> vectorfield3<S> as(void) const;
		void ABC_field(const T& A = 1.0, const T& B = T(sqrt(2.0 / 3.0)), const T& C = T(sqrt(1.0 / 3.0)));
		inline bool in_volume(const lux::vec4<T>& x) const {
			if (x(0) < T(0) || x(1) < T(0) || x(2) < T(0)) return false;
			if (x(0) > T(m_dims[0] - 1) || x(1) > T(m_dims[1] - 1) || x(2) > T(m_dims[2] - 1)) return false;
			return true;
		}
		
		bool write(const std::string& name) const;
		bool write(FILE* stream) const;
		bool read(const std::string& name);
		bool read(FILE* stream);
	protected:
		// Everything uses voxel coordinates!
		std::vector<lux::vec4<T> > m_vel;			// phase space: normalized velocity, 1/velocity magnitude | infinity for velocity magnitude = 0
		std::vector<lux::vec4<T> > m_acc;			// acceleration
		std::vector<T> m_kappa;						// curvature
		std::vector<T> m_tau;						// torsion
		std::vector<quat4<T> > m_orientation;		// Frenet frame

		std::string m_name;
		size_t m_dims[3];
		lux::BOUNDARY m_boundary;
		lux::ranged_t m_domain[3];
		inline T scalar_trilerp(const std::vector<T>& data, const vec4<T>& pos) const;
		inline T scalar_tricubic(const std::vector<T>& data, const vec4<T>& pos) const;
		inline vec4<T> vector_trilerp(const std::vector<vec4<T> >& data, const vec4<T>& pos) const;
		inline vec4<T> vector_tricubic(const std::vector<vec4<T> >& data, const vec4<T>& pos) const;
		inline quat4<T> quaternion_trislerp(const std::vector<quat4<T> >& data, const vec4<T>& pos) const;
	};

	typedef vectorfield3<float> vectorfield3f;
	typedef vectorfield3<double> vectorfield3d;
};

#include"lux_sampler.h"			// for boundaries & coordinate mappers
#include"lux_coordinateframe.h"
#include"lux_HermiteCurveS3.h"	// for slerp

namespace std {
	template<class T>
	void swap(const lux::vectorfield3<T>& lhs, const lux::vectorfield3<T>& rhs) {
		lhs.swap(rhs);
	}
};

template<class T>
lux::vectorfield3<T>::vectorfield3(void) : m_boundary(lux::SAMPLER_REPEAT) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	for (size_t n = 0; n < 3; n++) m_domain[n] = range_t(0.0, 1.0);
}

template<class T>
lux::vectorfield3<T>::vectorfield3(const vectorfield3& other) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	*this = other;
}

template<class T>
lux::vectorfield3<T>::vectorfield3(const size_t& dimx, const size_t& dimy, const size_t& dimz) : m_boundary(lux::SAMPLER_REPEAT) {
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	for (size_t n = 0; n < 3; n++) m_domain[n] = range_t(0.0, 1.0);
	resize(dimx, dimy, dimz);
}

template<class T>
lux::vectorfield3<T>::~vectorfield3(void) {
	clear();
}
template<class T>
size_t lux::vectorfield3<T>::size(void) const {
	return m_vel.size();
}

template<class T>
bool lux::vectorfield3<T>::empty(void) const {
	return m_vel.empty();
}

template<class T>
void lux::vectorfield3<T>::clear(void) {
	m_vel.clear();
	m_acc.clear();
	m_orientation.clear();
	m_kappa.clear();
	m_tau.clear();
	m_name.clear();
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
}

template<class T>
void lux::vectorfield3<T>::resize(size_t dimx, size_t dimy, size_t dimz) {
	size_t nsize = dimx*dimy*dimz;
	if (nsize == 0) return clear();
	m_vel.clear();
	if (nsize != m_vel.size()) {
		m_vel.resize(nsize);
		m_acc.resize(nsize);
		m_orientation.resize(nsize);
		m_kappa.resize(nsize);
		m_tau.resize(nsize);
	}
	m_dims[0] = dimx; 
	m_dims[1] = dimy;
	m_dims[2] = dimz;
}

template<class T>
inline const lux::vec4<T>& lux::vectorfield3<T>::get_velocity(size_t i, size_t j, size_t k) const {
	return m_vel[linear_address(i, j, k)];
}

template<class T>
inline const lux::vec4<T>& lux::vectorfield3<T>::get_acceleration(size_t i, size_t j, size_t k) const {
	return m_acc[linear_address(i, j, k)];
}

template<class T>
inline const lux::quat4<T>& lux::vectorfield3<T>::get_orientation(size_t i, size_t j, size_t k) const {
	return m_orientation[linear_address(i, j, k)];
}

template<class T>
inline const T& lux::vectorfield3<T>::get_curvature(size_t i, size_t j, size_t k) const {
	return m_kappa[linear_address(i, j, k)];
}

template<class T>
inline const T& lux::vectorfield3<T>::get_torsion(size_t i, size_t j, size_t k) const {
	return m_tau[linear_address(i, j, k)];
}

template<class T>
inline void lux::vectorfield3<T>::set_velocity(size_t i, size_t j, size_t k, const vec4<T>& v) {
	m_vel[linear_address(i, j, k)] = v;
}

template<class T>
inline void lux::vectorfield3<T>::set_acceleration(size_t i, size_t j, size_t k, const vec4<T>& a) {
	m_acc[linear_address(i, j, k)] = a;
}

template<class T>
inline void lux::vectorfield3<T>::set_orientation(size_t i, size_t j, size_t k, const quat4<T>& q) {
	m_orientation[linear_address(i, j, k)] = q;
}

template<class T>
inline void lux::vectorfield3<T>::set_kappa(size_t i, size_t j, size_t k, const T& kappa) {
	m_kappa[linear_address(i, j, k)] = kappa;
}

template<class T>
inline void lux::vectorfield3<T>::set_tau(size_t i, size_t j, size_t k, const T& tau) {
	m_tau[linear_address(i, j, k)] = tau;
}

template<class T>
inline const lux::vec4<T>& lux::vectorfield3<T>::get_velocity(size_t n) const {
	assert("lux::vectorfield3::get_velocity -- invalid parameter" && n < m_vel.size());
	return m_vel[n];
}

template<class T>
inline const lux::vec4<T>& lux::vectorfield3<T>::get_acceleration(size_t n) const {
	assert("lux::vectorfield3::get_acceleration -- invalid parameter" && n < m_vel.size());
	return m_acc[n];
}

template<class T>
inline const lux::quat4<T>& lux::vectorfield3<T>::get_orientation(size_t n) const {
	assert("lux::vectorfield3::get_orientation -- invalid parameter" && n < m_vel.size());
	return m_orientation[n];
}

template<class T>
inline const T& lux::vectorfield3<T>::get_curvature(size_t n) const {
	assert("lux::vectorfield3::get_curvature -- invalid parameter" && n < m_vel.size());
	return m_kappa[n];
}

template<class T>
inline const T&	lux::vectorfield3<T>::get_torsion(size_t n) const {
	assert("lux::vectorfield3::get_torsion -- invalid parameter" && n < m_vel.size());
	return m_tau[n];
}

template<class T>
inline void lux::vectorfield3<T>::set_velocity(size_t n, const vec4<T>& v) {
	assert("lux::vectorfield3::set_velocity -- invalid parameter" && n < m_vel.size());
	m_vel[n] = v;
}

template<class T>
inline void lux::vectorfield3<T>::set_acceleration(size_t n, const vec4<T>& a) {
	assert("lux::vectorfield3::set_acceleration -- invalid parameter" && n < m_vel.size());
	m_acc[n] = a;
}

template<class T>
inline void	lux::vectorfield3<T>::set_orientation(size_t n, const quat4<T>& q) {
	assert("lux::vectorfield3::set_orientation -- invalid parameter" && n < m_vel.size());
	m_orientation[] = q;
}

template<class T>
inline void lux::vectorfield3<T>::set_curvature(size_t n, const T& kappa) {
	assert("lux::vectorfield3::set_curvature -- invalid parameter" && n < m_vel.size());
	m_kappa[n] = kappa;
}

template<class T>
inline void lux::vectorfield3<T>::set_torsion(size_t n, const T& tau) {
	assert("lux::vectorfield3::set_torsion -- invalid parameter" && n < m_vel.size());
	m_tau[n] = tau;
}

template<class T>
inline const lux::BOUNDARY& lux::vectorfield3<T>::get_boundary(void) const {
	return m_boundary;
}

template<class T>
inline const std::string& lux::vectorfield3<T>::get_name(void) const {
	return m_name;
}

template<class T>
inline const lux::range_t<typename T>&	lux::vectorfield3<T>::get_domain(size_t n) const {
	assert("lux::vectorfield3::get_domain -- invalid parameter" && n < 3);
	return m_domain[n];
}

template<class T>
inline void lux::vectorfield3<T>::set_boundary(const BOUNDARY& b) {
	m_boundary = b;
}

template<class T>
inline void lux::vectorfield3<T>::set_name(const std::string& name) {
	m_name = name;
}

template<class T>
inline void lux::vectorfield3<T>::set_domain(size_t n, const lux::ranged_t& r) {
	assert("lux::vectorfield3::set_domain -- invalid parameter" && n < 3);
	m_domain[n] = r;
}

template<class T>
void lux::vectorfield3<T>::update(void) {
	if (empty()) return;
	{
		for (auto& elem : m_vel) {
			elem(3) = elem.length(3);
			elem.normalize(3);
			//elem(3) = T(1);
		}
	}
	// fetch velocity from mapped coordinates
	auto fetch = [&](const int64_t& i, const int64_t& j, const int64_t& k) {
		if (i < 0 || j < 0 || k < 0) return &lux::vec4<T>::Zero;
		return (const vec4<T>*)m_vel.data() + linear_address(i, j, k);
	};
	// compute orientation from velocity and acceleration
	auto compute_orientation = [&](size_t n) {
		vec4<T> X(m_vel[n]); // tangent
		X.normalize(3);
		vec4<T> Y = m_acc[n]; // normal
		Y.normalize(3);
		vec4<T> Z = X^Y; // binormal
		Z.normalize(3);
		Y = Z^X;
		Y.normalize(3);
		quat4<T>& result(m_orientation[n]);
		result.real() = T(0.5*sqrt(std::max(0.0, 1.0 + X(0) + Y(1) + Z(2))));
		result.imag(0) = T(0.5*sqrt(std::max(0.0, 1.0 + X(0) - Y(1) - Z(2))));
		result.imag(1) = T(0.5*sqrt(std::max(0.0, 1.0 - X(0) + Y(1) - Z(2))));
		result.imag(2) = T(0.5*sqrt(std::max(0.0, 1.0 - X(0) - Y(1) + Z(2))));
		result.imag(0) = lux::copysign(result.imag(0), Z(1) - Y(2));
		result.imag(1) = lux::copysign(result.imag(1), X(2) - Z(0));
		result.imag(2) = lux::copysign(result.imag(2), Y(0) - X(1));
		result.real() = sqrt(std::max(T(0), T(1) - result.imag(0)*result.imag(0) - result.imag(1)*result.imag(1) - result.imag(2)*result.imag(2)));	
		return result;
	};

	// Compute acceleration and Frenet-Serret apparatus
	Eigen::Matrix<T, 3, 3, Eigen::ColMajor, 3, 3> M;
	Eigen::Matrix<T, 3, 1, Eigen::ColMajor, 3, 1> v;
	// All derivatives are in voxel space! That means that the curve will be parameterized in terms of voxel lengths
	T den[3];
	int64_t K[3], J[3], I[3];
	for (int64_t k = 0; k < int64_t(m_dims[2]); k++) {
		K[0] = map_coordinate(k - 1, m_dims[2], m_boundary);
		K[1] = map_coordinate(k, m_dims[2], m_boundary);
		K[2] = map_coordinate(k + 1, m_dims[2], m_boundary);
		den[2] = (m_boundary == lux::SAMPLER_REPEAT ? T(0.5) : T(1) / T(K[2] - K[0]));

		for (int64_t j = 0; j < int64_t(m_dims[1]); j++) {
			J[0] = map_coordinate(j - 1, m_dims[1], m_boundary);
			J[1] = map_coordinate(j, m_dims[1], m_boundary);
			J[2] = map_coordinate(j + 1, m_dims[1], m_boundary);
			den[1] = (m_boundary == lux::SAMPLER_REPEAT ? T(0.5) : T(1) / T(J[2] - J[0]));

			for (int64_t i = 0; i < int64_t(m_dims[0]); i++) {
				I[0] = map_coordinate(i - 1, m_dims[0], m_boundary);
				I[1] = map_coordinate(i, m_dims[0], m_boundary);
				I[2] = map_coordinate(i + 1, m_dims[0], m_boundary);
				den[0] = (m_boundary == lux::SAMPLER_REPEAT ? T(0.5) : T(1) / T(I[2] - I[0]));
				size_t addr = linear_address(i, j, k);

				const vec4<T>* support[27];
				for (size_t z = 0; z < 3; z++) {
					for (size_t y = 0; y < 3; y++) {
						for (size_t x = 0; x < 3; x++) {
							support[x+3*y+9*z] = fetch(I[x], J[y], K[z]);
						}
					}
				}
				
				// first order derivatives (velocity gradient tensor)
				vec4<T> dx = vec4<T>((*support[2 + 3 * 1 + 9 * 1]) - (*support[0 + 3 * 1 + 9 * 1]))*den[0];
				vec4<T> dy = vec4<T>((*support[1 + 3 * 2 + 9 * 1]) - (*support[1 + 3 * 0 + 9 * 1]))*den[1];
				vec4<T> dz = vec4<T>((*support[1 + 3 * 1 + 9 * 2]) - (*support[1 + 3 * 1 + 9 * 0]))*den[2];
				for (int r = 0; r < 3; r++) {
					M(r, 0) = ((*support[2 + 3 * 1 + 9 * 1])(r) - (*support[0 + 3 * 1 + 9 * 1])(r))*den[0];
					M(r, 1) = ((*support[1 + 3 * 2 + 9 * 1])(r) - (*support[1 + 3 * 0 + 9 * 1])(r))*den[1];
					M(r, 2) = ((*support[1 + 3 * 1 + 9 * 2])(r) - (*support[1 + 3 * 1 + 9 * 0])(r))*den[2];
					v(r) = m_vel[addr](r);
				}
				decltype(v) a = M*v;
				double alen = ::sqrt(a(0)*a(0) + a(1)*a(1) + a(2)*a(2));
				double dot = m_acc[addr](0)*m_vel[addr](0) + m_acc[addr](1)*m_vel[addr](1) + m_acc[addr](2)*m_vel[addr](2);
				// enforce orthogonality
				for (int c = 0; c < 3; c++) m_acc[addr](c) = a(c) - dot*m_vel[addr](c);
				
				// compute curvature and re-orthonormalize
				// kappa = |vel x acc| / |vel|^3
				// 1. magnitude of acceleration
				// 2. | vel x acc | / |vel|^3
				// OPT: tan(a.len) to compensate for non-spherical math...
				m_kappa[addr] = m_acc[addr].length(3); // normalized velocity!
				compute_orientation(addr);
				decltype(a) jerk;
				
				// second order derivatives
				// TODO: not accurate if boundary!=REPEAT 
				// voxel space, therefore:
				//			the differential stencil of Vxx,Vyy,Vzz is [1 -2 1]
				//			the differential stencil of Vxy, etc is composed of +-den[i]den[j]
				for (size_t c = 0; c < 3; c++) {
					// second order derivatives
					double center = -2.0*(*support[1 + 3 * 1 + 9 * 1])(c);
					double Vxx = ((*support[0 + 3 * 1 + 9 * 1])(c) + (*support[2 + 3 * 1 + 9 * 1])(c) + center);
					double Vyy = ((*support[1 + 3 * 0 + 9 * 1])(c) + (*support[1 + 3 * 2 + 9 * 1])(c) + center);
					double Vzz = ((*support[1 + 3 * 1 + 9 * 0])(c) + (*support[1 + 3 * 1 + 9 * 2])(c) + center);
					double Vxy = den[0]*den[1]*((*support[0 + 3 * 0 + 9 * 1])(c) + (*support[2 + 3 * 2 + 9 * 1])(c) - (*support[0 + 3 * 2 + 9 * 1])(c) - (*support[2 + 3 * 0 + 9 * 1])(c));
					double Vxz = den[0] * den[2] * ((*support[0 + 3 * 1 + 9 * 0])(c) + (*support[2 + 3 * 1 + 9 * 2])(c) - (*support[0 + 3 * 1 + 9 * 2])(c) - (*support[2 + 3 * 1 + 9 * 0])(c));
					double Vyz = den[1] * den[2] * ((*support[1 + 3 * 0 + 9 * 0])(c) + (*support[1 + 3 * 2 + 9 * 2])(c) - (*support[1 + 3 * 0 + 9 * 2])(c) - (*support[1 + 3 * 2 + 9 * 0])(c));
					//printf("%f %f %f\n", Vxy, Vxz, Vyz);
					jerk(c) = T(v(0)*(Vxx*v(0) + 2.0*Vxy*v(1)) + v(1)*(Vyy*v(1) + 2.0*Vyz*v(2)) + v(2)*(Vzz*v(2) + 2.0*Vxz*v(0)));
				}
				jerk += M*a;

				// torsion = det(v,a,j) / kappa^2
				double tau = jerk[0] * (v(1)*a(2) - v(2)*a(1)) + jerk[1] * (v(2)*a(0) - v(0)*a(2)) + jerk[2] * (v(0)*a(1) - v(1)*a(0));
				tau /= m_kappa[addr] * m_kappa[addr];
				m_tau[addr] = T(tau);
			}
		}
	}
}

template<class T>
size_t lux::vectorfield3<T>::dim(size_t n) const {
	assert("lux::vectorfield3::dim -- invalid parameter" && n < 3);
	return m_dims[n];
}

template<class T>
lux::vectorfield3<T>& lux::vectorfield3<T>::operator=(const vectorfield3& other) {
	m_vel = other.m_vel;
	m_acc = other.m_acc;
	m_orientation = other.m_orientation;
	m_kappa = other.m_kappa;
	m_tau = other.m_tau;
	m_name = other.m_name;
	m_boundary = other.m_boundary;
	for (size_t n = 0; n < 3; n++) {
		m_dims[n] = other.m_dims[n];
		m_domain[n] = other.m_domain[n];
	}
	return *this;
}

template<class T>
void lux::vectorfield3<T>::fill(const ranged_t& X, const ranged_t& Y, const ranged_t& Z, const fill_function& F) {
	if (empty()) return;
	static vec4d buf;
	size_t addr = 0;
	m_domain[0] = X;
	m_domain[1] = Y;
	m_domain[2] = Z;
	for (size_t k = 0; k < m_dims[2]; k++) {
		double z = Z.map(k, m_dims[2]+1);
		for (size_t j = 0; j < m_dims[1]; j++) {
			double y = Y.map(j, m_dims[1]+1);
			for (size_t i = 0; i < m_dims[0]; i++) {
				double x = X.map(i, m_dims[0]+1);
				for (size_t n = 0; n < 4; n++) buf(n) = double(m_vel[addr](n));
				F(buf, x, y, z);
				for (size_t n = 0; n < 4; n++) m_vel[addr](n) = buf[n];
				addr++;
			}
		}
	}
	update();
}

template<class T>
inline size_t lux::vectorfield3<T>::linear_address(size_t i, size_t j, size_t k) const {
	assert("lux::vectorfield3::linear_address -- invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	return (i + m_dims[0] * (j + m_dims[1] * k));
}

template<class T>
void lux::vectorfield3<T>::swap(vectorfield3& other) {
	m_vel.swap(other.m_vel);
	m_acc.swap(other.m_acc);
	m_name.swap(other.m_name);
	m_orientation.swap(other.m_orientation);
	m_kappa.swap(other.m_kappa);
	m_tau.swap(other.m_tau);
	for (size_t n = 0; n < 3; n++) std::swap(m_dims[n], other.m_dims[n]);
}

template<class T>
template<class S> lux::vectorfield3<S> lux::vectorfield3<T>::as(void) const {
	lux::vectorfield3<S> result;
	result.name() = m_name;
	if (empty()) return result;
	result.resize(m_dims[0], m_dims[1], m_dims[2]);
	for (size_t n = 0; n < m_vel.size(); n++) {
		vec4<S> v, a;
		quat4<S> q;
		for (size_t c = 0; c < 4; c++) {
			v(c) = S(m_vel[n](c));
			a(c) = S(m_acc[n](c));
			q(c) = S(m_orientation[n](c));
		}
		result.set_velocity(n, v);
		result.set_acceleration(n, a);
		result.set_orientation(n, a);
		result.set_curvature(n, S(m_kappa[n]));
		result.set_torsion(n, S(m_tau[n]));
	}
	return result;
}

template<class T>
void lux::vectorfield3<T>::ABC_field(const T& A, const T& B, const T& C) {
	ranged_t X(0.0, 2.0*M_PI);
	m_boundary = lux::SAMPLER_REPEAT;
	fill(X, X, X, 
		[&](vec4<T>& io, const double& x, const double& y, const double& z) {
			// velocity
			double sx = sin(x), sy = sin(y), sz = sin(z);
			double cx = cos(x), cy = cos(y), cz = cos(z);
			Eigen::Matrix<double, 3, 1, Eigen::ColMajor, 3, 1> v;
			io(0) = A*sz + C*cy;
			io(1) = B*sx + A*cz;
			io(2) = C*sy + B*cx;
			// velocity gradient
			//Eigen::Matrix<double, 3, 3, Eigen::ColMajor, 3, 3> M;
			//M(0, 0) = 0.0;		M(0, 1) = -C*sy;	M(0, 2) = A*cz;
			//M(1, 0) = B*cx;		M(1, 1) = 0.0;		M(1, 2) = -A*sz;
			//M(2, 0) = -B*sx;	M(2, 1) = C*cy;		M(2, 2) = 0.0;
			//for (size_t k = 0; k < 3; k++) io_vel[k] = v(k);
			//Eigen::Matrix<double,3,1,Eigen::ColMajor,3,1> r = M*v;
			//for (size_t k = 0; k < 3; k++) io_acc[k] = r(k);
		}
	);
	m_name = std::string("ABC");
}

template<class T>
inline T lux::vectorfield3<T>::scalar_trilerp(const std::vector<T>& data, const lux::vec4<T>& pos) const {
	if (empty()) return T(0);

	int64_t l, r;
	int64_t P[3][2];
	T w[3];
	for (size_t k = 0; k < 3; k++) {
		l = int64_t(floor(pos(k)));
		r = l + 1;
		w[k] = pos(k) - T(l);
		for (int64_t c = l; c <= r; c++)  P[k][c - l] = map_coordinate(c, m_dims[k], m_boundary);
	};

	auto fetch = [&](T& result, const int64_t& i, const int64_t& j, const int64_t& k) {
		if (i < 0 || j < 0 || k < 0) result = T(0);
		else result = data[linear_address(i, j, k)];
	};

	T t[8];
	size_t addr = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 2; j++) {
			for (size_t k = 0; k < 2; k++) {
				fetch(t[addr++],P[0][i], P[1][j], P[2][k]);
			}
		}
	}
	size_t off = 4;
	for (size_t d = 0; d < 3; d++) {
		for (size_t o = 0; o < off; o++) {
			t[o] += w[d]*(t[o + off]-t[o]);
		}
		off >>= 1;
	}
	return t[0];
}

template<class T>
inline lux::vec4<T> lux::vectorfield3<T>::vector_trilerp(const std::vector<lux::vec4<T> >& data, const lux::vec4<T>& pos) const {
	if (empty()) return lux::vec4<T>::Zero;
	int64_t l, r;
	int64_t P[3][2];
	T w[3];
	for (size_t k = 0; k < 3; k++) {
		l = int64_t(floor(pos(k)));
		r = l + 1;
		w[k] = pos(k) - T(l);
		for (int64_t c = l; c <= r; c++)  P[k][c - l] = map_coordinate(c, m_dims[k], m_boundary);
	};

	auto fetch = [&](vec4<T>& result, const int64_t& i, const int64_t& j, const int64_t& k) {
		if (i < 0 || j < 0 || k < 0) result(0) = result(1) = result(2) = result(3) = T(0);
		else result =  data[linear_address(i, j, k)];
	};

	lux::vec4<T> t[8];
	size_t addr = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 2; j++) {
			for (size_t k = 0; k < 2; k++) {
				fetch(t[addr++], P[0][i], P[1][j], P[2][k]);
			}
		}
	}
	size_t off = 4;
	for (size_t d = 0; d < 3; d++) {
		for (size_t o = 0; o < off; o++) {
			t[o] += w[d] * vec4<T>(t[o + off] - t[o]);
		}
		off >>= 1;
	}
	return t[0];
}

template<class T>
inline T lux::vectorfield3<T>::scalar_tricubic(const std::vector<T>&data, const lux::vec4<T>& pos) const {
	if (empty()) return T(0);
	int64_t l, r;
	int64_t P[3][4];
	T w[3];
	for (size_t k = 0; k < 3; k++) {
		l = int64_t(floor(pos(k))) - 1;
		r = l + 3;
		w[k] = pos(k) - T(l + 1);
		for (int64_t c = l; c <= r; c++) P[k][c - l] = map_coordinate(c, m_dims[k], m_boundary);
	}

	auto fetch = [&](T& result, const int64_t& i, const int64_t& j, const int64_t& k) {
		if (i < 0 || j < 0 || k < 0) result = T(0);
		else result = data[linear_address(i, j, k)];
	};

	lux::vec4<T> t[64];
	size_t addr = 0;
	for (int64_t i = 0; i < 4; i++) {
		for (int64_t j = 0; j < 4; j++) {
			for (int64_t k = 0; k < 4; k++) {
				fetch(t[addr++], P[0][i], P[1][j], P[2][k]);
			}
		}
	}
	size_t off = 16;
	for (size_t d = 0; d < 3; d++) {
		for (size_t o = 0; o < off; o++) {
			t[o] = lux::cubic(t[o], t[o + off], t[o + 2 * off], t[o + 3 * off], w[d]);
		}
		off >>= 2;
	}
	return t[0];
}

template<class T>
inline lux::vec4<T> lux::vectorfield3<T>::vector_tricubic(const std::vector<lux::vec4<T> >& data, const lux::vec4<T>& pos) const {
	if (empty()) return lux::vec4<T>::Zero;
	int64_t l, r;
	int64_t P[3][4];
	T w[3];
	for (size_t k = 0; k < 3; k++) {
		l = int64_t(floor(pos(k))) - 1;
		r = l + 3;
		w[k] = pos(k) - T(l + 1);
		for (int64_t c = l; c <= r; c++) P[k][c - l] = map_coordinate(c, m_dims[k], m_boundary);
	}

	auto fetch = [&](vec4<T>& result, const int64_t& i, const int64_t& j, const int64_t& k) {
		if (i < 0 || j < 0 || k < 0) result(0) = result(1) = result(2) = result(3) = T(0);
		else result = data[linear_address(i, j, k)];
	};

	lux::vec4<T> t[64];
	size_t addr = 0;
	for (int64_t i = 0; i < 4; i++) {
		for (int64_t j = 0; j < 4; j++) {
			for (int64_t k = 0; k < 4; k++) {
				fetch(t[addr++],P[0][i], P[1][j], P[2][k]);
			}
		}
	}
	
	size_t off = 16;
	for (size_t d = 0; d < 3; d++) {
		for (size_t o = 0; o < off; o++) {
			t[o] = lux::cubic(t[o], t[o + off], t[o + 2 * off], t[o + 3 * off], w[d]);
		}
		off >>= 2;
	}
	return t[0];
}

template<class T>
inline lux::quat4<T> lux::vectorfield3<T>::quaternion_trislerp(const std::vector<quat4<T> >& data, const lux::vec4<T>& pos) const {
	static const quat4<T> id(T(1),T(0),T(0),T(0));
	if (empty()) return id;

	int64_t l, r;
	int64_t P[3][2];
	T w[3];
	for (size_t k = 0; k < 3; k++) {
		l = int64_t(floor(pos(k)));
		r = l + 1;
		w[k] = pos(k) - T(l);
		for (int64_t c = l; c <= r; c++)  P[k][c - l] = map_coordinate(c, m_dims[k], m_boundary);
	};

	auto fetch = [&](quat4<T>& result, const int64_t& i, const int64_t& j, const int64_t& k) {
		if (i < 0 || j < 0 || k < 0) {
			result.real() = T(1);
			result.imag(0) = result.imag(1) = result.imag(2) = T(0);
		}
		else result = data[linear_address(i, j, k)];
	};

	quat4<T> t[8];
	size_t addr = 0;
	for (size_t i = 0; i < 2; i++) {
		for (size_t j = 0; j < 2; j++) {
			for (size_t k = 0; k < 2; k++) {
				fetch(t[addr++], P[0][i], P[1][j], P[2][k]);
			}
		}
	}
	size_t off = 4;
	for (size_t d = 0; d < 3; d++) {
		for (size_t o = 0; o < off; o++) {
			t[o] = slerp(t[o],t[o + off], w[d]);
		}
		off >>= 1;
	}
	return t[0];

}

template<class T>
lux::vec4<T> lux::vectorfield3<T>::sample_acceleration(const lux::vec4<T>& pos) const {
	return vector_trilerp(m_acc, pos);
}

template<class T>
lux::vec4<T> lux::vectorfield3<T>::sample_velocity(const lux::vec4<T>& pos) const {
	// approved and correct.
	//lux::mat4<T> O = quaternion_trislerp(m_orientation, pos).asMatrix();
	lux::vec4<T> result = vector_tricubic(m_vel, pos);
	//result(3) = T(1);
	//result.normalize(3);
	//result(3) = T(1);		// required for integrators
	//lux::vec4<T> result;
	//for (int i = 0; i < 3; i++) result(i) = O(i, 0);
	//result(3) = vector_trilerp(m_vel,pos)(3);
	return result;
}

template<class T>
T lux::vectorfield3<T>::sample_curvature(const lux::vec4<T>& pos) const {
	return scalar_trilerp(m_kappa, pos);
}

template<class T>
T lux::vectorfield3<T>::sample_torsion(const lux::vec4<T>& pos) const {
	return scalar_trilerp(m_tau, pos);
}

template<class T>
lux::quat4<T> lux::vectorfield3<T>::sample_orientation(const lux::vec4<T>& pos) const {
	return quaternion_trislerp(m_orientation, pos);
}

template<class T>
bool lux::vectorfield3<T>::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream);
	fclose(stream);
	return result;
}

template<class T>
bool lux::vectorfield3<T>::write(FILE* stream) const {
	if (stream == nullptr) return false;
	if (empty()) return false;
	uint64_t D[6] = { m_dims[0],m_dims[1],m_dims[2],uint64_t(m_boundary), m_name.length(), sizeof(T) };
	if (fwrite(D, sizeof(uint64_t), 6, stream) != 6) return false;
	if (fwrite(m_vel.data(), sizeof(vec4<T>), m_vel.size(), stream) != m_vel.size()) return false;
	if (fwrite(m_acc.data(), sizeof(vec4<T>), m_acc.size(), stream) != m_acc.size()) return false;
	if (fwrite(m_orientation.data(), sizeof(quat4<T>), m_orientation.size(), stream) != m_orientation.size()) return false;
	if (fwrite(m_kappa.data(), sizeof(T), m_kappa.size(), stream) != m_kappa.size()) return false;
	if (fwrite(m_tau.data(), sizeof(T), m_tau.size(), stream) != m_tau.size()) return false;
	if (fwrite(m_name.c_str(), sizeof(uint8_t), m_name.length(), stream) != m_name.length()) return false;
	double Dom[6] = { m_domain[0].rmax,m_domain[0].rmin,m_domain[1].rmax,m_domain[1].rmin,m_domain[2].rmax,m_domain[2].rmin };
	if (fwrite(Dom, sizeof(double), 6, stream) != 6) return false;
	return true;
}

template<class T>
bool lux::vectorfield3<T>::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read(stream);
	fclose(stream);
	return result;
}

template<class T>
bool lux::vectorfield3<T>::read(FILE* stream) {
	if (stream == nullptr) return false;
	uint64_t D[6];
	if (fread(D, sizeof(uint64_t), 6, stream) != 6) return false;
	if (sizeof(T) != D[5]) return false;
	resize(size_t(D[0]), size_t(D[1]), size_t(D[2]));
	m_boundary = lux::BOUNDARY(D[3]);
	if (fread(m_vel.data(), sizeof(vec4<T>), m_vel.size(), stream) != m_vel.size()) return false;
	if (fread(m_acc.data(), sizeof(vec4<T>), m_acc.size(), stream) != m_acc.size()) return false;
	if (fread(m_orientation.data(), sizeof(quat4<T>), m_orientation.size(), stream) != m_orientation.size()) return false;
	if (fread(m_kappa.data(), sizeof(T), m_kappa.size(), stream) != m_kappa.size()) return false;
	if (fread(m_tau.data(), sizeof(T), m_tau.size(), stream) != m_tau.size()) return false;
	char* ch = new char[D[4] + 1];
	if (fread(ch, sizeof(uint8_t), D[4], stream) != D[4]) {
		delete[] ch;
		return false;
	}
	ch[D[4]] = 0;
	m_name = std::string(ch);
	delete[] ch;
	double Dom[6];
	if (fread(Dom, sizeof(double), 6, stream) != 6) return false;
	for (size_t k = 0; k < 6; k += 2) {
		m_domain[k >> 1].rmax = Dom[k];
		m_domain[k >> 1].rmin = Dom[k + 1];
	}
	return true;
}

#endif
