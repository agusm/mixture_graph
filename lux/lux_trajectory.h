#ifndef __LUX_TRAJECTORY_H__
#define __LUX_TRAJECTORY_H__

#include"lux_math.h"			// vectors / matrices
#include"lux_color.h"			// colors
//#include"lux_interpolation.h"
#include<vector>
#include<cassert>
#include<string>
#include"Eigen/Dense"
#include"lux_weno.h"

namespace lux {
	enum CAP {
		CAP_NONE,
		CAP_SPHERE,
		CAP_DISK
	};

	template<class T> struct sample_t; // differential geometry sample: position, velocity, acceleration, orientation, curvature, torsion, parameter
	
	template<class T> inline quat4<T> slerp(const quat4<T>& q1, const quat4<T>& q2, const T& w); // spherical interpolation between quaternions

	template<class T>
	class trajectory {
	public:
		trajectory(void);
		trajectory(const trajectory& other);
		trajectory(const size_t& dim);
		~trajectory(void);
		void clear(void);
		size_t size(void) const;
		bool empty(void) const;
		void resize(size_t n);
		void push_back(const lux::sample_t<T>& s, const lux::color& rgb = lux::color(0.0f, 0.0f, 0.0f, 1.0f));

		inline const vec4<T>& get_position(size_t n) const;
		inline const vec4<T>& get_velocity(size_t n) const;
		inline const vec4<T>& get_acceleration(size_t n) const;
		inline const quat4<T>& get_orientation(size_t n) const;
		inline const T& get_curvature(size_t n) const;
		inline const T& get_torsion(size_t n) const;
		inline const color get_color(size_t n) const;
		inline const sample_t<T>& get_sample(size_t n) const {
			assert("lux::trajectory::get_sample -- invalid parameter" && n < m_samples.size());
			return m_samples[n];
		}
		inline void set_position(size_t n, const vec4<T>& x);
		inline void set_velocity(size_t n, const vec4<T>& v);
		inline void set_acceleration(size_t n, const vec4<T>& a);
		inline void set_orientation(size_t n, const quat4<T>& o);
		inline void set_curvature(size_t n, const T& kappa);
		inline void set_torsion(size_t n, const T& tau);
		inline void set_sample(size_t n, const sample_t<T>& s) {
			assert("lux::trajectory::set_sample -- invalid parameter" && n < m_samples.size());
			m_samples[n] = s;
		}

		vec4<T> sample_position(const T& t, const PARAMETER& p = ARC_LENGTH) const;			// C2, cubic spline
		vec4<T> sample_velocity(const T& t, const PARAMETER& p = ARC_LENGTH) const;			// C1, consistent from cubic spline
		vec4<T> sample_acceleration(const T& t, const PARAMETER& p = ARC_LENGTH) const;		// C0, linear
		T sample_curvature(const T& t, const PARAMETER& p = ARC_LENGTH) const;				// C0, linear
		T sample_torsion(const T& pos, const PARAMETER& p = ARC_LENGTH) const;				// C0, linear
		quat4<T> sample_orientation(const T& t, const PARAMETER& p = ARC_LENGTH) const;		// C0, slerp

		inline void frenet_frame(const T& t, lux::vec4<T>* pos = nullptr, lux::vec4<T>* tangent = nullptr, lux::vec4<T>* normal = nullptr, lux::vec4<T>* binormal = nullptr) const;

		
		inline void get_sample(sample_t<T>& s, const T& at, const PARAMETER& p = ARC_LENGTH) const;
			
		trajectory& operator=(const trajectory& other);
		std::string& name(void);
		const std::string& name(void) const;
		
	
		inline range_t<typename T> get_parameter_range(const lux::PARAMETER& p = ARC_LENGTH) const { 
			if (empty()) return range_t(T(0), T(0));
			return range_t(m_samples.front().get_parameter(p), m_samples.back().get_parameter(p));
		}

		trajectory<T> unfold(const T& w, const T& anchor=T(0));

		// append trajectory to this trajectory
		trajectory<T> operator+(const trajectory<T>& other) const;
		trajectory<T>& operator+=(const trajectory<T>& other);

		void swap(trajectory& other);
		
		typedef std::function<lux::color(const T& t, const T& s, const ranged_t& trange)> color_func;
		static const color_func default_color;
		static const range_t<typename T> default_range;

		bool refrenetieren(void);
		bool x_refrenetieren(void);

		bool export_obj(const std::string& name,				// name of file
						size_t nTess = 16,						// tesselation in cross section
						const T& radius = 1.0f,					// radius
						const color_func& F = default_color,	// t is the curve parameter, s the cross-section parameter normalized to [0,2Pi]
						const CAP& c = CAP_NONE,				// cap type
						const ranged_t& R = default_range,		// parameter range of trajectory to be exported
						const T& dt = T(-1)) const;				// step size along curve

		bool export_obj(FILE* stream,							// output stream
						size_t& count,							// number of emitted vertices, needs to be initialized to 0
						size_t nTess = 16,						// tesselation in cross section
						const T& radius = 1.0f,					// radius
						const color_func& F = default_color,	// t is the curve parameter, s the cross-section parameter normalized to [0,2Pi]
						const CAP& c = CAP_NONE,				// cap type
						const ranged_t& R = default_range,		// parameter range of trajectory to be exported
						const T& dt = T(-1)) const;				// step size along curve

		bool read(const std::string& name);
		bool read(FILE* stream);
		bool write(const std::string& name) const;
		bool write(FILE* stream) const;
	protected:
		std::vector<typename sample_t<T> > m_samples;
		std::vector<lux::color> m_color;
		std::string m_name;
		inline std::pair<size_t,size_t> find_range(const T& t, const PARAMETER& p = ARC_LENGTH) const;
		inline T clamp_parameter(const T& t, const PARAMETER& p = ARC_LENGTH) const;
	};

	typedef trajectory<float> trajectoryf;
	typedef trajectory<double> trajectoryd;
};

#include"lux_vectorfield3.h"


template<class T>
inline void lux::trajectory<T>::frenet_frame(const T& t, lux::vec4<T>* pos, lux::vec4<T>* tangent, lux::vec4<T>* normal, lux::vec4<T>* binormal) const {
	if (pos != nullptr) {
		*pos = sample_position(t);
	}
	if (tangent == nullptr && normal == nullptr && binormal == nullptr) return;
	lux::mat4<T> M = sample_orientation(t).asMatrix();
	if (tangent != nullptr) *tangent = lux::vec4<T>(M(0, 0), M(0, 1), M(0, 2), T(0));
	if (normal != nullptr) *normal = lux::vec4<T>(M(1, 0), M(1, 1), M(1, 2), T(0));
	if (binormal != nullptr) *binormal = lux::vec4<T>(M(2, 0), M(2, 1), M(2, 2), T(0));
}

template<class T>
typename const lux::trajectory<T>::color_func lux::trajectory<T>::default_color = [](const T& t, const T& s, const lux::range_t& trange) { return lux::color(1.0f, 1.0f, 1.0f, 1.0f); };

template<class T>
const lux::range_t<typename T> lux::trajectory<T>::default_range = ranged_t(std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max());

namespace std {
	template<class T>
	void swap(const lux::trajectory<T>& lhs, const lux::trajectory<T>& rhs) { lhs.swap(rhs); }
};

template<class T>
lux::trajectory<T>::trajectory(void) {
}

template<class T>
lux::trajectory<T>::trajectory(const trajectory& other) {
	*this = other;
}

template<class T>
lux::trajectory<T>::trajectory(const size_t& dim) {
	resize(dim);
}

template<class T>
lux::trajectory<T>::~trajectory(void) {
	clear();
}

template<class T>
void lux::trajectory<T>::clear(void) {
	m_samples.clear();
	m_color.clear();
	m_name.clear();
}

template<class T>
size_t lux::trajectory<T>::size(void) const {
	return m_samples.size();
}

template<class T>
bool lux::trajectory<T>::empty(void) const {
	return m_samples.empty();
}

template<class T>
void lux::trajectory<T>::resize(size_t n) {
	if (n == 0) return clear();
	if (n != m_samples.size()) {
		m_samples.resize(n);
		m_color.resize(n);
	}
}

template<class T>
void lux::trajectory<T>::push_back(const lux::sample_t<T>& s, const lux::color& rgb) {
	m_samples.push_back(s);
	m_color.push_back(rgb);
}


template<class T>
lux::trajectory<T>& lux::trajectory<T>::operator=(const trajectory& other) {
	m_samples = other.m_samples;
	m_color = other.m_color;
	m_name = other.m_name;
	return *this;
}

template<class T>
inline const lux::vec4<T>& lux::trajectory<T>::get_position(size_t n) const {
	assert("lux::trajectory::get_position -- invalid parameter" && n < m_samples.size());
	return m_samples[n].position;
}

template<class T>
inline const lux::vec4<T>& lux::trajectory<T>::get_velocity(size_t n) const {
	assert("lux::trajectory::get_velocity -- invalid parameter" && n < m_samples.size());
	return m_samples[n].velocity;
}

template<class T>
inline const lux::vec4<T>& lux::trajectory<T>::get_acceleration(size_t n) const {
	assert("lux::trajectory::get_acceleration -- invalid parameter" && n < m_samples.size());
	return m_samples[n].acceleration;

}
template<class T>
inline const lux::quat4<T>& lux::trajectory<T>::get_orientation(size_t n) const {
	assert("lux::trajectory::get_orientation -- invalid parameter" && n < m_samples.size());
	return m_samples[n].orientation;
}

template<class T>
inline const T& lux::trajectory<T>::get_curvature(size_t n) const {
	assert("lux::trajectory::get_curvature -- invalid parameter" && n < m_samples.size());
	return m_samples[n].curvature;
}
template<class T>
inline const T& lux::trajectory<T>::get_torsion(size_t n) const {
	assert("lux::trajectory::get_torsion -- invalid parameter" && n < m_samples.size());
	return m_samples[n].torsion;
}

template<class T>
inline const lux::color lux::trajectory<T>::get_color(size_t n) const {
	assert("lux::trajectory::get_color -- invalid parameter" && n < m_color.size());
	return m_color[n];
}

template<class T>
inline void lux::trajectory<T>::set_position(size_t n, const vec4<T>& x) {
	assert("lux::trajectory::set_position -- invalid parameter" && n < m_samples.size());
	m_samples[n].position = x;
}

template<class T>
inline void lux::trajectory<T>::set_velocity(size_t n, const vec4<T>& v) {
	assert("lux::trajectory::set_velocity -- invalid parameter" && n < m_samples.size());
	m_samples[n].velocity = v;
}

template<class T>
inline void lux::trajectory<T>::set_acceleration(size_t n, const vec4<T>& a) {
	assert("lux::trajectory::set_acceleration -- invalid parameter" && n < m_samples.size());
	m_samples[n].acceleration = a;
}

template<class T>
inline void lux::trajectory<T>::set_orientation(size_t n, const quat4<T>& o) {
	assert("lux::trajectory::set_orientation -- invalid parameter" && n < m_samples.size());
	m_samples[n].orientation = o;
}
template<class T>
inline void lux::trajectory<T>::set_curvature(size_t n, const T& kappa) {
	assert("lux::trajectory::set_curvature -- invalid parameter" && n < m_samples.size());
	m_samples[n].curvature = kappa;
}

template<class T>
inline void lux::trajectory<T>::set_torsion(size_t n, const T& tau) {
	assert("lux::trajectory::set_torsion -- invalid parameter" && n < m_samples.size());
	return m_samples[n].torsion;
}

template<class T>
inline std::pair<size_t,size_t> lux::trajectory<T>::find_range(const T& at, const lux::PARAMETER& p) const {
	size_t klo = 0;
	size_t khi = m_samples.size() - 1;
	while (khi - klo > 1) {
		size_t k = (khi + klo) >> 1;
		if (m_samples[k].get_parameter(p) > at) khi = k;
		else klo = k;
	}
	return std::make_pair(klo,khi);
}

template<class T>
inline T lux::trajectory<T>::clamp_parameter(const T& at,const lux::PARAMETER& p) const {
	return std::min(std::max(at, m_samples.front().get_parameter()), m_samples.back().get_parameter());
}

template<class T>
inline lux::vec4<T> lux::trajectory<T>::sample_position(const T& at, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return lux::vec4<T>(T(0), T(0), T(0), T(0));
	T t = clamp_parameter(at,p);
	std::pair<size_t,size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::sample_position -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	T b = T((t - tlo) / h);
	lux::vec4<T> P = a*slo.position + b*shi.position + ((a*a*a - a)*slo.get_acceleration() + (b*b*b - b)*shi.get_acceleration())*T(h*h / 6.0);
	P(3) = t;
	return P;
}

template<class T>
lux::vec4<T> lux::trajectory<T>::sample_velocity(const T& at, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return lux::vec4<T>(T(0), T(0), T(0), T(0));
	T t = clamp_parameter(at,p);
	std::pair<size_t, size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::sample_velocity -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	T b = T((t - tlo) / h);
	lux::vec4<T> result = (shi.position - slo.position) / T(h) + (T(1.0 - 3.0*a*a)*slo.acceleration + T(3.0*b*b - 1.0)*shi.acceleration)*T(h / 6.0);
	result(3) = t;
	return result;
}

template<class T>
lux::vec4<T> lux::trajectory<T>::sample_acceleration(const T& at, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return lux::vec4<T>(T(0), T(0), T(0), T(0));
	T t = clamp_parameter(at,p);
	std::pair<size_t, size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::sample_acceleration -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	lux::vec4<T> result = shi.acceleration + a*(slo.acceleration - shi.acceleration);
	result(3) = t;
	return result;
}

template<class T>
T lux::trajectory<T>::sample_curvature(const T& t, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return lux::vec4<T>(T(0), T(0), T(0), T(0));
	T t = clamp_parameter(at,p);
	std::pair<size_t, size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::sample_curvature -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	return shi.curvature + a*(slo.curvature - shi.curvature);
}

template<class T>
T lux::trajectory<T>::sample_torsion(const T& pos, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return lux::vec4<T>(T(0), T(0), T(0), T(0));
	T t = clamp_parameter(at,p);
	std::pair<size_t, size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::sample_curvature -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	return shi.torsion + a*(slo.torsion - shi.torsion);
}

template<class T>
lux::quat4<T> lux::trajectory<T>::sample_orientation(const T& at, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return lux::vec4<T>(T(0), T(0), T(0), T(0));
	T t = clamp_parameter(at,p);
	std::pair<size_t, size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::sample_orientation -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	return lux::slerp(shi.orientation, slo.orientation, a);
}

template<class T>
inline void lux::trajectory<T>::get_sample(lux::sample_t<T>& s, const T& at, const lux::PARAMETER& p) const {
	if (m_samples.size() < 2) return;
	T t = clamp_parameter(at,p);
	std::pair<size_t, size_t> r = find_range(t,p);
	const size_t& klo(r.first);
	const size_t& khi(r.second);
	const sample_t<T>& shi(m_samples[khi]);
	const sample_t<T>& slo(m_samples[klo]);
	T tlo = slo.get_parameter(p);
	T thi = shi.get_parameter(p);
	double h = thi - tlo;
	assert("lux::trajectory::get_sample -- invalid parameter" && h != T(0));
	T a = T((thi - t) / h);
	T b = T((t - tlo) / h);
	// position
	s.position = a*slo.position + b*shi.position + ((a*a*a - a)*slo.get_acceleration() + (b*b*b - b)*shi.get_acceleration())*T(h*h / 6.0);
	s.position(3) = t;
	// orientation
	s.orientation = lux::slerp(shi.orientation, slo.orientation, a);
	// physical time, curvature, torsion, velocity magnitude
	s.time = a*slo.time + b*shi.time;
	s.curvature = a*slo.curvature + b*shi.curvature;
	s.torsion = a*slo.torsion + b*shi.torsion;
	s.vel_magnitude = a*slo.vel_magnitude + b*shi.vel_magnitude;
}

template<class T>
lux::trajectory<T> lux::trajectory<T>::unfold(const T& w, const T& anchor) {
	// smoothstep from 1 to 0 on w parameter
	T param = w*w*(T(2)*w - T(3)) + T(1);
	if (param < T(1e-5)) param = T(0);
	trajectory<T> result(*this);
	range_t R = get_parameter_range();
	
	//if (w == T(0)) return result;

	// find anchor point
	T at = clamp_parameter(anchor);
	std::pair<size_t, size_t> r = find_range(at);
	size_t iAnchor = r.first;
	
	// update curvature and torsion
	for (size_t n = 0; n < m_samples.size(); n++) {
		result.m_samples[n].curvature *= param;
		result.m_samples[n].torsion *= param;
	}
	
	// forward integration of orientation and position
	sample_t<T>* last = nullptr;
	sample_t<T>* cur = &result.m_samples[iAnchor];
	mat4<T> M = cur->orientation.asMatrix();
	vec4<T> last_tangent(M(0, 0),M(0, 1), M(0, 2), T(1));
	for (size_t n = iAnchor + 1; n < m_samples.size(); n++) {
		last = cur;
		cur = &result.m_samples[n];
		T h = cur->position(3) - last->position(3);
		T kappa = last->curvature;
		T tau = last->torsion;
		// d/dt Q|_last
		//printf("last %f %f %f %f cur %f %f %f %f",last->orientation(0),last->orientation(1),last->orientation(2),last->orientation(3),cur->orientation(0),cur->orientation(1),cur)
		quat4<T> dQ_est = quat4<T>(cur->orientation - last->orientation) / h;
		quat4<T> Q = last->orientation;
		quat4<T> dQ = quat4<T>(
			T(0.5)*(tau*Q.imag(0) + kappa*Q.imag(2)),
			T(0.5)*(kappa*Q.imag(1)- tau*Q.real()),
			T(0.5)*(tau*Q.imag(2) - kappa*Q.imag(0)),
			-T(0.5)*(tau*Q.imag(1) + kappa*Q.real())
			);
		// integration of orientation
		cur->orientation = last->orientation+h*dQ;
		cur->orientation.normalize();
		double len = 0.0;
		T lastH = cur->position(3);
		for (size_t k = 0; k < 4; k++) len += cur->orientation(k)*cur->orientation(k);
		// integration of position
		cur->position = last->position + h*last_tangent;
		cur->position(3) = lastH;
		cur->vel_magnitude = T(1);
		//printf("len = %f, param = %f\n", len,cur->position(3));
		M = cur->orientation.asMatrix();
		for (int i = 0; i < 3; i++) last_tangent(i) = M(0,i);
	}
	cur = &result.m_samples[iAnchor];
	M = cur->orientation.asMatrix();
	last_tangent = vec4<T>(M(0, 0), M(0, 1), M(0, 2), T(1));
	for (size_t n = iAnchor - 1; n + 1 > 0; n--) {
		last = cur;
		cur = &result.m_samples[n];
		T h = cur->position(3) - last->position(3);
		T kappa = last->curvature;
		T tau = last->torsion;
		// d/dt Q|_last
		//printf("last %f %f %f %f cur %f %f %f %f",last->orientation(0),last->orientation(1),last->orientation(2),last->orientation(3),cur->orientation(0),cur->orientation(1),cur)
		quat4<T> dQ_est = quat4<T>(cur->orientation - last->orientation) / h;
		quat4<T> Q = last->orientation.conjugate();
		quat4<T> dQ = quat4<T>(
			-T(0.5)*(tau*Q.imag(0) + kappa*Q.imag(2)),
			T(0.5)*(tau*Q.real() + kappa*Q.imag(1)),
			T(0.5)*(tau*Q.imag(2) - kappa*Q.imag(0)),
			-T(0.5)*(tau*Q.imag(1) - kappa*Q.real())
			).conjugate();
		// integration of orientation
		cur->orientation = last->orientation + h*dQ;
		cur->orientation.normalize();
		double len = 0.0;
		T lastH = cur->position(3);
		for (size_t k = 0; k < 4; k++) len += cur->orientation(k)*cur->orientation(k);
		// integration of position
		cur->position = last->position + h*last_tangent;
		cur->position(3) = lastH;
		cur->vel_magnitude = T(1);
		//printf("len = %f, param = %f\n", len,cur->position(3));
		M = cur->orientation.asMatrix();
		for (int i = 0; i < 3; i++) last_tangent(i) = M(0, i);
	}
	return result;
}

template<class T>
std::string& lux::trajectory<T>::name(void) {
	return m_name;
}

template<class T>
const std::string& lux::trajectory<T>::name(void) const {
	return m_name;
}


template<class T>
void lux::trajectory<T>::swap(trajectory& other) {
	m_samples.swap(other.m_samples);
	m_color.swap(other.m_color);
	m_name.swap(other.m_name);
}

template<class T>
lux::trajectory<T> lux::trajectory<T>::operator+(const trajectory<T>& other) const {
	if (empty()) return other;
	if (other.empty()) return *this;
	lux::trajectory<T> result(*this);
	bool doubleZero = (m_samples.front().position(3) == T(0) || m_samples.back().position(3) == T(0)) 
						&& (other.m_samples.front().position(3) == T(0) || other.m_samples.back().position(3) == T(0));
	result.m_name += std::string(" + ") + other.m_name;
	for (size_t n = 0; n < other.size(); n++) {
		if (other.m_samples[n].position(3) != T(0) || !doubleZero) {
			result.m_samples.push_back(other.m_samples[n]);
			result.m_color.push_back(other.m_color[n]);
		}
	}
	// sorting
	std::vector<std::pair<T, size_t> > toSort;
	for (size_t n = 0; n < result.m_samples.size(); n++) toSort.push_back(std::make_pair(result.m_samples[n].position(3), n));
	std::sort(toSort.begin(), toSort.end());
	std::vector<sample_t<T> > S(toSort.size());
	std::vector<lux::color> C(toSort.size());
	for (size_t n = 0; n < toSort.size(); n++) {
		S[n] = result.m_samples[toSort[n].second];
		C[n] = result.m_color[toSort[n].second];
	}
	result.m_samples.swap(S);
	result.m_color.swap(C);
	return result;
}

template<class T>
lux::trajectory<T>& lux::trajectory<T>::operator+=(const trajectory<T>& other) {
	if (empty()) return *this = other;
	if (other.empty()) return *this;
	bool doubleZero = (m_samples.front().position(3) == T(0) || m_samples.back().position(3) == T(0))
		&& (other.m_samples.front().position(3) == T(0) || other.m_samples.back().position(3) == T(0));
	m_name += std::string(" + ") + other.m_name;
	for (size_t n = 0; n < other.size(); n++) {
		if (other.m_samples[n].position(3) != T(0) || !doubleZero) {
			m_samples.push_back.(other.m_samples[n]);
			m_color.push_back(other.m_color[n]);
		}
	}
	// sorting
	std::vector<std::pair<T, size_t> > toSort;
	for (size_t n = 0; n < result.m_samples.size(); n++) toSort.push_back(std::make_pair(result.m_samples[n].position(3), n));
	std::sort(toSort.begin(), toSort.end());
	std::vector<sample_t<T> > S(toSort.size());
	std::vector<lux::color> C(toSort.size());
	for (size_t n = 0; n < toSort.size(); n++) {
		S[n] = m_samples[toSort[n].second];
		C[n] = m_color[toSort[n].second];
	}
	m_samples.swap(S);
	m_color.swap(C);
	return *this;
}

template<class T>
bool lux::trajectory<T>::export_obj(const std::string& name, size_t nTess, const T& radius, const color_func& F, const CAP& c, const ranged_t& R, const T& dt) const {
	if (size() <2 ) return false;
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wt")) return false;
	size_t n = 0;
	bool result = export_obj(stream, n, nTess, radius, F, c, R, dt);
	fclose(stream);
	return result;
}

template<class T>
bool lux::trajectory<T>::export_obj(FILE* stream, size_t& count, size_t nTess, const T& radius, const color_func& F, const CAP& c, const ranged_t& R, const T& dt) const {
	// TODO: radius as parameter
	if (stream == nullptr) return false;
	if (size() < 2) return false;
	range_t r = get_parameter_range();
	r.rmin = std::max(R.rmin, r.rmin);
	r.rmax = std::min(R.rmax, r.rmax);
	vec4<T> P[4];
	lux::color clr;
	nTess = std::max(nTess, size_t(3));
	range_t K(0.0, 2 * M_PI);
	T s;
	std::vector<size_t> last_ring;
	std::vector<size_t> cur_ring;

	/// emits a faces on a ring from last_ring to cur_ring
	auto emit_ring = [&](void) {
		for (size_t k = 0; k < nTess; k++) {
			fprintf(stream, "f %zi//%zi %zi//%zi %zi//%zi\n",
				last_ring[k], last_ring[k],
				last_ring[(k + 1) % last_ring.size()], last_ring[(k + 1) % last_ring.size()],
				cur_ring[k], cur_ring[k]);
			fprintf(stream, "f %zi//%zi %zi//%zi %zi//%zi\n",
				last_ring[(k + 1) % last_ring.size()], last_ring[(k + 1) % last_ring.size()],
				cur_ring[(k + 1) % cur_ring.size()], cur_ring[(k + 1) % cur_ring.size()],
				cur_ring[k], cur_ring[k]);
		}
	};
	
	// initial cap
	switch (c) {
	case CAP_NONE: break;
	case CAP_DISK: // introduces nodes at perimeter 
	{
		frenet_frame(T(r.rmin), P, P + 1, P + 2, P + 3);
		s = 0.0f;
		clr = F(T(r.rmin), s,r);
		size_t center = count;
		fprintf(stream, "v %f %f %f %f %f %f\n", P[0](0), P[0](1), P[0](2), clr(0), clr(1), clr(2));
		fprintf(stream, "vn %f %f %f\n", -P[1](0), -P[1](1), -P[1](2));
		count++;
		for (size_t k = 0; k < nTess; k++) {
			T s = T(K.map(k, nTess + 1));
			clr = F(T(r.rmin), s,r);
			vec4<T> pos = P[0] + radius*(cos(s)*P[2] + sin(s)*P[3]);
			fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
			fprintf(stream, "vn %f %f %f\n", -P[1](0), -P[1](1), -P[1](2));
			fprintf(stream, "f %zi//%zi %zi//%zi %zi//%zi\n",
				center + 1, center + 1,
				center + 2 + ((k + 1) % nTess), center + 2 + ((k + 1) % nTess),
				center + 2 + k, center + 2 + k
			);
			count++;
		}
		break;
	}
	case CAP_SPHERE: // connected
	{
		frenet_frame(T(r.rmin), P, P + 1, P + 2, P + 3);
		s = 0.0f;
		clr = F(T(r.rmin) - 1.0f, s,r);
		size_t center = count;
		vec4<T> pos = P[0] - radius*P[1];
		fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
		fprintf(stream, "vn %f %f %f\n", -P[1](0), -P[1](1), -P[1](2));
		count++;
		range_t N(0.0, 1.0);
		// first ring at n=1
		T off = T(N.map(1, (nTess + 1) / 2));
		off *= off;
		for (size_t k = 0; k < nTess; k++) {
			T s = T(K.map(k, nTess + 1));
			clr = F(T(r.rmin) - 1.0f + off, s,r);
			T h = sqrt(T(1) - (T(1) - off)*(T(1) - off));
			vec4<T> pos = P[0] + radius*(-(T(1) - off)*P[1] + h*cos(s)*P[2] + h*sin(s)*P[3]);
			vec4<T> nrm = pos - P[0];
			nrm.normalize(3);
			fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
			fprintf(stream, "vn %f %f %f\n", nrm(0), nrm(1), nrm(2));
			fprintf(stream, "f %zi//%zi %zi//%zi %zi//%zi\n",
				center + 1, center + 1,
				center + 2 + ((k + 1) % nTess), center + 2 + ((k + 1) % nTess),
				center + 2 + k, center + 2 + k);
			count++;
			cur_ring.push_back(count);
		}
		// remaining rings
		for (size_t n = 2; n < (nTess + 1) / 2-1; n++) {
			off = T(N.map(n, (nTess + 1) / 2));
			off *= off;
			last_ring = cur_ring;
			cur_ring.clear();
			for (size_t k = 0; k < nTess; k++) {
				T s = T(K.map(k, nTess + 1));
				clr = F(T(r.rmin) - (1.0f - off), s,r);
				T h = sqrt(T(1) - (T(1) - off)*(T(1) - off));
				vec4<T> pos = P[0] +radius*(-(T(1) - off)*P[1] + h*cos(s)*P[2] + h*sin(s)*P[3]);
				vec4<T> nrm = pos - P[0];
				nrm.normalize(3);
				fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
				fprintf(stream, "vn %f %f %f\n", nrm(0), nrm(1), nrm(2));
				count++;
				cur_ring.push_back(count);
			}
			emit_ring();
		} // endfor remaining rings
		break;
	}
	}
	
	// middle segment
	T tincr = dt > T(0) ? dt : T(1);
	size_t steps = size_t((r.rmax - r.rmin) / tincr + T(1));
	for (size_t k = 0; k < steps; k++) {
		T t = T(r.map(k, steps));
		last_ring = cur_ring;
		cur_ring.clear();
		frenet_frame(t, P, P + 1, P + 2, P + 3); // Frenet-Serret Frame is orthonormal.
		for (size_t k = 0; k < nTess; k++) {
			T s = T(K.map(k, nTess + 1));
			clr = F(t, s, r);
			vec4<T> pos = P[0] + radius*(cos(s)*P[2] + sin(s)*P[3]);
			vec4<T> nrm = pos - P[0];
			nrm.normalize(3);
			fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
			fprintf(stream, "vn %f %f %f\n", nrm(0), nrm(1), nrm(2));
			count++;
			cur_ring.push_back(count);
		}
		if (!last_ring.empty()) emit_ring();
	}
	
	// end cap
	switch (c) {
	case CAP_NONE: break;
	case CAP_DISK: // introduces nodes at perimeter 
	{
		frenet_frame(T(r.rmax), P, P + 1, P + 2, P + 3);
		s = 0.0f;
		clr = F(T(r.rmax), s,r);
		size_t center = count;
		fprintf(stream, "v %f %f %f %f %f %f\n", P[0](0), P[0](1), P[0](2), clr(0), clr(1), clr(2));
		fprintf(stream, "vn %f %f %f\n", P[1](0), P[1](1), P[1](2));
		count++;
		for (size_t k = 0; k < nTess; k++) {
			T s = T(K.map(k, nTess + 1));
			clr = F(T(r.rmax), s,r );
			vec4<T> pos = P[0] + radius*(cos(s)*P[2] + sin(s)*P[3]);
			fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
			fprintf(stream, "vn %f %f %f\n", P[1](0), P[1](1), P[1](2));
			fprintf(stream, "f %zi//%zi %zi//%zi %zi//%zi\n",
				center + 1, center + 1,
				center + 2 + k, center + 2 + k,
				center + 2 + ((k + 1) % nTess), center + 2 + ((k + 1) % nTess)
			);
			count++;
		}
		break;
	}
	case CAP_SPHERE: // connected
	{
		frenet_frame(T(r.rmax), P, P + 1, P + 2, P + 3);
		s = 0.0f;
		clr = F(T(r.rmax) + 1.0f, s,r);
		size_t center = count;
		vec4<T> pos = P[0] + radius*P[1];
		fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
		fprintf(stream, "vn %f %f %f\n", P[1](0), P[1](1), P[1](2));
		count++;
		range_t N(0.0, 1.0);
		T off;
		// first rings
		for (size_t n = (nTess + 1) / 2 - 2; n >=2 ; n--) {
			off = T(N.map(n, (nTess + 1) / 2));
			off *= off;
			last_ring = cur_ring;
			cur_ring.clear();
			for (size_t k = 0; k < nTess; k++) {
				T s = T(K.map(k, nTess + 1));
				clr = F(T(r.rmax) + (1.0f - off), s,r);
				T h = sqrt(T(1) - (T(1) - off)*(T(1) - off));
				vec4<T> pos = P[0] + radius*((T(1) - off)*P[1] + h*cos(s)*P[2] + h*sin(s)*P[3]);
				vec4<T> nrm = pos - P[0];
				nrm.normalize(3);
				fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
				fprintf(stream, "vn %f %f %f\n", nrm(0), nrm(1), nrm(2));
				count++;
				cur_ring.push_back(count);
			}
			emit_ring();
		} // endfor first rings
		// last ring at n=1
		off = T(N.map(1, (nTess + 1) / 2));
		off *= off;
		for (size_t k = 0; k < nTess; k++) {
			T s = T(K.map(k, nTess + 1));
			clr = F(T(r.rmax) + (1.0f - off), s, r);
			T h = sqrt(T(1) - (T(1) - off)*(T(1) - off));
			vec4<T> pos = P[0] + radius*((T(1) - off)*P[1] + h*cos(s)*P[2] + h*sin(s)*P[3]);
			vec4<T> nrm = pos - P[0];
			nrm.normalize(3);
			fprintf(stream, "v %f %f %f %f %f %f\n", pos(0), pos(1), pos(2), clr(0), clr(1), clr(2));
			fprintf(stream, "vn %f %f %f\n", nrm(0), nrm(1), nrm(2));
			fprintf(stream, "f %zi//%zi %zi//%zi %zi//%zi\n",
			center + 1, center + 1,
			cur_ring[k], cur_ring[k],
			cur_ring[(k + 1) % nTess], cur_ring[(k + 1) % nTess]
		);
		count++;
		}
		break;
	}
	}
	return true;
}

template<class T>
bool lux::trajectory<T>::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read(stream);
	fclose(stream);
	return result;

}

template<class T>
bool lux::trajectory<T>::read(FILE* stream) {
	if (stream == nullptr) return false;
	uint64_t D[4];
	if (fread(D, sizeof(uint64_t), 4, stream) != 4) return false;
	if (D[3] != sizeof(T)) return false;
	resize(size_t(D[0]));
	if (fread(m_position.data(), sizeof(lux::vec4<T>), m_position.size(), stream) != m_position.size()) return false;
	if (fread(m_d2_position.data(), sizeof(lux::vec4<T>), m_d2_position.size(), stream) != m_d2_position.size()) return false;
	if (fread(m_color.data(), sizeof(lux::color), m_color.size(), stream) != m_color.size()) return false;
	if (D[1] != 0) {
		m_frames.resize(size_t(D[1]));
		if (fread(m_frames.data(), sizeof(lux::coordinateframe<T>), m_frames.size(), stream) != m_frames.size()) return false;
	}
	char* buf = new char[size_t(D[2]) + 1];
	if (fread(buf, sizeof(char), size_t(D[2]), stream) != size_t(D[2])) {
		delete[] buf;
		return false;
	}
	buf[size_t(D[2])] = 0;
	m_name = std::string(buf);
	delete[] buf;
	return true;
}

template<class T>
bool lux::trajectory<T>::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream);
	fclose(stream);
	return result;
}

template<class T>
bool lux::trajectory<T>::write(FILE* stream) const {
	if (stream == nullptr) return false;
	uint64_t D[4] = {
		uint64_t(m_position.size()),
		uint64_t(m_frames.size()),
		uint64_t(m_name.length()),
		sizeof(T)
	};
	if (m_position.size() != m_d2_position.size()) printf("Oh no!\n");
	if (fwrite(D, sizeof(uint64_t), 4, stream) != 4) return false;
	if (fwrite(m_position.data(), sizeof(lux::vec4<T>), m_position.size(), stream) != m_position.size()) return false;
	if (fwrite(m_d2_position.data(), sizeof(lux::vec4<T>), m_d2_position.size(), stream) != m_d2_position.size()) return false;
	if (fwrite(m_color.data(), sizeof(lux::color), m_color.size(), stream) != m_color.size()) return false;
	if (fwrite(m_name.c_str(), sizeof(char), m_name.length(), stream) != m_name.length()) return false;
	return true;
}

#include"Eigen/SparseQR"
#include"Eigen/IterativeLinearSolvers"

template<class T>
bool lux::trajectory<T>::refrenetieren(void) {
	if (m_samples.size() < 5) return false;

	// C3-spline:
	// p(t) = A(t)*p_i + B(t)*p_{i+1} + C(t)*j_i + D(t)*j_{i+1}
	// Add quartic polynomial q(t) = at^4 + bt^3 + ct^2 + dt to A*p_i + B*p_{i+1} s.t.
	// q(t_i) = 0
	// q(t_{i+1}) = 0
	// d^3/dt^3 q(t_i) = 1
	// d^3/dt^3 q(t_{i+1}) = 1
	// Let K:=	t_i^4		t_i^3		t_i^2		t_i
	//			t_{i+1}^4	t_{i+1}^3	t_{i+1}^2	t_{i+1}
	//			24t_i		6			0			0
	//			24t_{i+1}	6			0			0
	// q(t) = <t^4 | t^3 | t^2 | t>.MatrixInverse(K).<0,0,1,1>
	// d^3/dt^3 q(t) = <24t | 6 | 0 | 0>.MatrixInverse(K).<0,0,1,1>
	// Thus,
	// A(t) = (t_{i+1}-t) / (t_{i+1}-t_i)
	// B(t) = (t - t_i) / (t_{i+1}-t_i)
	// C(t) =  t*(t-t_{i+1}) (t-t_i) (t-3*t_{i+1}+t_i) / (24*(t_i-t_{i+1})
	// D(t) = -t*(t-t_{i+1}) (t-t_i) (t-3*t_i+t_{i+1}) / (24*(t_i-t_{i+1})

	// First order continuity:
	// A'(t_i)*p_i + B'(t_i)*p_{i+1} C'(t_i)*j_i + D'(t_{i+1})j_{i+1} = A'(t_

	// First order derivative of A,B,C,D
	// d/dt A(t) = -1/(t_{i+1}-t_i)
	// d/dt B(t) =  1/(t_{i+1}-t_i)
	// d/dt C(t) = (4t^3 - 12t^2*t_{i+1} -2t*t_i^2 + 6t*
	


	// re-parameterize (chordal)
	//for (size_t n = 1; n < m_samples.size(); n++) {
	//	m_samples[n].position(3) = m_samples[n - 1].position(3) + (vec4<T>(m_samples[n].position - m_samples[n - 1].position)).length(3);
	//}

	/*
	// C^3 spline
	typedef Eigen::SparseMatrix<double> mat_t;
	mat_t M(2 * m_samples.size(), m_samples.size());
	M.reserve(Eigen::VectorXi::Constant(2 * m_samples.size(), 3));
	Eigen::MatrixXd rhs(m_samples.size() * 2, 3);
	for (size_t n = 0; n < m_samples.size(); n++) {
		vec4<T> pl = n > 0 ? m_samples[n - 1].position : vec4<T>(T(2.0)*m_samples[0].position - m_samples[1].position);
		const vec4<T>& p(m_samples[n].position);
		vec4<T> pr = n + 1 < m_samples.size() ? m_samples[n + 1].position : vec4<T>(T(2.0)*m_samples.back().position - m_samples[m_samples.size() - 2].position);
		double tl = pl(3);
		double t = p(3);
		double tr = pr(3);
		double C1l = t*(2.0*t - tl) / 24.0, C1r = t*(2.0*t - 3.0*tr) / 24.0;
		double D1l = t*(2.0*t - 3.0*tl) / 24.0, D1r = t*(2.0*t - tr) / 24.0;
		double C2l = (3.0*t*t - 3 * t*tl + tl*tl) / (12.0*(t - tl)), C2r = (5.0*t*t - 9.0*t*tr + 3.0*tr*tr) / (12.0*(t - tr));
		double D2l = (5.0*t*t - 9.0*t*tl + 3 * tl*tl) / (12.0*(t - tl)), D2r = (3.0*t*t - 3.0*t*tr + tr*tr) / (12.0*(t - tr));
		double A1l = -1.0 / (t - tl), A1r = -1.0 / (tr - t);
		double B1l = 1.0 / (t - tl), B1r = 1.0 / (tr - t);

		if (n > 0) {
			M.insert(2 * n, n - 1) = C1l;
			M.insert(2 * n + 1, n - 1) = C2l;
		}
		M.insert(2 * n, n) = D1l - C1r;
		M.insert(2 * n + 1, n) = D2l - C2r;
		if (n + 1 < m_samples.size()) {
			M.insert(2 * n, n + 1) = -D1r;
			M.insert(2 * n + 1, n + 1) = -D2r;
		}

		for (size_t k = 0; k < 3; k++) {
			rhs(2 * n, k) = -A1l*pl(k) + (A1r - B1l)*p(k) + B1r*pr(k);
			rhs(2 * n + 1, k) = 0.0;
		}
	}
	M.makeCompressed();
	Eigen::LeastSquaresConjugateGradient<decltype(M)> S(M);
	decltype(rhs) J = S.solve(rhs);
	printf("%i iterations: %f error\n", S.iterations(), S.error());
	
	for (size_t n = 1; n < m_samples.size() - 1; n++) {
		vec4<T> jl(J(n - 1,0), J(n - 1, 1), J(n - 1, 2), T(0));
		vec4<T> j(J(n, 0), J(n, 1), J(n, 2), T(0));
		vec4<T> jr(J(n + 1, 0), J(n + 1, 1), J(n + 1, 2), T(0));
		const vec4<T>& pr(m_samples[n + 1].position);
		const vec4<T>& pl(m_samples[n - 1].position);
		const vec4<T>& p(m_samples[n].position);
		double tr = pr(3);
		double t = p(3);
		double tl = pl(3);
		double A1l = -1.0 / (t - tl), A1r = -1.0 / (tr - t);
		double B1l = 1.0 / (t - tl), B1r = 1.0 / (tr - t);
		double C1l = t*(2.0*t - tl) / 24.0, C1r = t*(2.0*t - 3.0*tr) / 24.0;
		double D1l = t*(2.0*t - 3.0*tl) / 24.0, D1r = t*(2.0*t - tr) / 24.0;
		double C2l = (3.0*t*t - 3 * t*tl + tl*tl) / (12.0*(t - tl)), C2r = (5.0*t*t - 9.0*t*tr + 3.0*tr*tr) / (12.0*(t - tr));
		double D2l = (5.0*t*t - 9.0*t*tl + 3 * tl*tl) / (12.0*(t - tl)), D2r = (3.0*t*t - 3.0*t*tr + tr*tr) / (12.0*(t - tr));
		vec4<T> vel = T(0.5)*(A1l*pl + B1l*p + C1l*jl + C1r*j + A1r*p + B1r*pr + C1r*j + D1r*jr);
		vec4<T> acc = T(0.5)*(C2l*jl + D2l*j + C2r*j + D2r*jr);
		m_samples[n].construct(m_samples[n].position, vel, acc, j, m_samples[n].time);
		printf("k=%f, t=%f\n", m_samples[n].curvature, m_samples[n].torsion);
	}
	
	return true;
	*/
	std::vector<vec4<T> > vel(m_samples.size());
	for (size_t n = 0; n < m_samples.size(); n++) {
		vel[n] = m_samples[n].get_velocity();
		vel[n](3) = m_samples[n].position(3);
		printf("%f %f %f %f\n", vel[n](0), vel[n](1), vel[n](2), vel[n](3));
	}
	std::vector<vec4<T> > dv2 = lux::cubic_spline(vel);
	/*
	typedef Eigen::SparseMatrix<double> mat_t;
	mat_t M(m_samples.size(), m_samples.size());
	M.reserve(Eigen::VectorXi::Constant(m_samples.size(),3));
	Eigen::MatrixXd rhs(m_samples.size(), 3);
	for (size_t n = 0; n < m_samples.size(); n++) {
		vec4<T> vl = n > 0 ? vel[n - 1] : vec4<T>(T(2.0)*vel[0] - vel[1]);
		const vec4<T>& v(vel[n]);
		vec4<T> vr = n + 1 < m_samples.size() ? vel[n + 1] : vec4<T>(T(2.0)*vel.back() - vel[m_samples.size() - 2]);
		double tl = vl(3);
		double t = v(3);
		double tr = vr(3);
		
		if (n > 0) M.insert(n,n-1) = (t - tl) / 6.0;
		M.insert(n,n) = (tr - tl) / 3.0;
		if (n + 1 < m_samples.size()) M.insert(n,n+1)=(tr - t) / 6.0;
		for (size_t k = 0; k < 3; k++) rhs(n, k) = (vr(k) - v(k)) / (tr - t) - (v(k) - vl(k)) / (t - tl);
	}
	M.makeCompressed();
	//Eigen::SparseQR<decltype(M),Eigen::COLAMDOrdering<int> > S(M);
	Eigen::ConjugateGradient<decltype(M)> S(M);
	decltype(rhs) jot = S.solve(rhs);
	printf("%i iterations: %f error\n", S.iterations(), S.error()); 

	for (size_t n = 1; n < m_samples.size()-1; n++) {
		vec4<T> jl(jot(n - 1, 0), jot(n - 1, 1), jot(n - 1, 2), T(0));
		vec4<T> j(jot(n, 0), jot(n, 1), jot(n, 2), T(0));
		vec4<T> jr(jot(n + 1, 0), jot(n + 1, 1), jot(n + 1, 2), T(0));
		const vec4<T>& vr(vel[n + 1]);
		const vec4<T>& vl(vel[n - 1]);
		const vec4<T>& v(vel[n]);
		vec4<T> al = vec4<T>(v - vl) / (v(3) - vl(3)) + (v(3) - vl(3)) / 6.0*al;
		vec4<T> ar = vec4<T>(vr - v) / (vr(3) - v(3)) - (vr(3) - v(3)) / 6.0*ar;
		vec4<T> a = T(0.5)*(vr + vl);
		m_samples[n].construct(m_samples[n].position, v, a, j, m_samples[n].time);
		printf("k=%f, t=%f\n", m_samples[n].curvature, m_samples[n].torsion);
	}
	*/
	for (size_t n = 0; n < m_samples.size() - 1; n++) {
		m_samples[n].construct(m_samples[n].position, vel[n], lux::cubic_int_diff1(vel, dv2, vel[n](3)), dv2[n], m_samples[n].time);
	}
	return true;
	
	/*
	vel[0] = m_samples[1].position - m_samples[0].position;
	vel.back() = m_samples.back().position - m_samples[m_samples.size() - 2].position;
	for (size_t n = 1; n < vel.size() - 1; n++) {
		T t1 = m_samples[n - 1].position(3) - m_samples[n].position(3);
		T t2 = m_samples[n + 1].position(3) - m_samples[n].position(3);
		T w1[3] = { t2 / (t1*(t2 - t1)),-(t1 + t2) / (t1*t2),t1 / (t2*(t1 - t2)) };
		T w2[3] = { 2.0 / (t1*(t1 - t2)),2.0 / (t1*t2),2.0 / (t2*(t2 - t1)) };
		vel[n] = w1[0] * m_samples[n - 1].position + w1[1] * m_samples[n].position + w1[2] * m_samples[n + 1].position;
		acc[n] = w2[0] * m_samples[n - 1].position + w2[1] * m_samples[n].position + w2[2] * m_samples[n + 1].position;
	}
	for (size_t n = 1; n < m_samples.size()-1; n++) {
		sample_t<T> s = get_sample(n);
		vec4<T> jot = T(0.5)*(acc[n+1]-acc[n-1]);
		s.construct(s.position, vel[n], acc[n], jot, s.time);
		set_sample(n, s);
		T k = s.curvature * vel[n].length(3);
		T t = s.torsion*vel[n].length(3);
		printf("%zi: v %f %f %f a %f %f %f j %f %f %f k %f t %f\n", n, vel[n](0),vel[n](1),vel[n](2),acc[n](0),acc[n](1),acc[n](2),jot(0),jot(1),jot(2),k,t);
	}*/
	return true;
	/*
	// reparameterize
	std::vector<double> s(m_samples.size());
	s[0] = T(0); 
	for (size_t n = 1; n < m_samples.size(); n++) {
		s[n] = s[n - 1] + T(1);// vec4<T>(m_samples[n].position - m_samples[n - 1].position).length(3);
	}

	// The following is essentially the analytic form of
	// MatrixInverse(VandermondeMatrix(<k[1],k[2],k[3],k[4],k[5]>).DiagonalMatrix(<1,1,1/2,1/6,1/24>)[2:4,:]
	// At the front, forward differences are made, whereas at the back, backward differences are used.
	// A chordal parameterization is used for this step.
	


	Eigen::Matrix<double, 5, 3, Eigen::ColMajor, 5, 3> S;
	Eigen::Matrix<double, 3, 5, Eigen::ColMajor, 3, 5> D;

	typedef Eigen::Matrix<double, 5, 5> mat5_t;
	mat5_t M;
	double k[5];

	for (size_t n = 0; n < m_samples.size(); n++) {
		size_t nl, nr;
		if (n < 2) {
			nl = 0; nr = 4;
		}
		else {
			if (n + 2 >= m_samples.size()) {
				nr = m_samples.size() - 1;
				nl = nr - 4;
			}
			else {
				nl = n - 2;
				nr = n + 2;
			}
		}
		for (size_t i=nl; i<=nr; i++) {
			//k[i+1-nl] = s[i] - s[n];
			k[i - nl] = s[i] - s[n];
			for (size_t j = 0; j < 3; j++) S(i-nl, j) = m_samples[i].position(j);
		}
		
		// Assemble Vandermonde matrix
		for (size_t i = 0; i<5; i++) {
			M(i, 0) = 1.0;
			for (size_t j = 1; j < 5; j++) {
				M(i, j) = M(i, j-1)*k[i] / double(j);
			}
		}
		Eigen::ColPivHouseholderQR<mat5_t> QR(M);
		decltype(S) der = QR.solve(S);


		
	
		vec4<T> vel(T(der(1, 0)), T(der(1, 1)), T(der(1, 2)), T(0));
		vec4<T> acc(T(der(2, 0)), T(der(2, 1)), T(der(2, 2)), T(0));
		vec4<T> jerk(T(der(3, 0)), T(der(3, 1)), T(der(3, 2)), T(0));
		//if (n < 5) printf("vel: %f %f %f\n", vel(0), vel(1), vel(2));
		//if (n < 5) printf("acc: %f %f %f\n", acc(0), acc(1), acc(2));
		//if (n < 5) printf("jot: %f %f %f\n", jerk(0), jerk(1), jerk(2));
		// keep original arc length and physical time intact
		m_samples[n].construct(m_samples[n].position, vel, acc, jerk, m_samples[n].time);

	
		//if (n < 5) printf("k=%f t=%f\n", m_samples[n].curvature, m_samples[n].torsion);
	}

	return true;
	*/
}

template<class T>
bool lux::trajectory<T>::x_refrenetieren(void) {
	if (m_samples.size() < 7) return false;

	vec4<T> F[7];
	auto rot = [&](const vec4<T>& p) {
		for (size_t n = 0; n < 6; n++) F[n] = F[n + 1];
		F[6] = p;
	};
	for (size_t n=0; n<6; n++) F[n+1] = m_samples[n].position;
	
	for (size_t n=3; n+3<m_samples.size(); n++) {
		lux::vec4<T> vel, acc, jot;
		rot(m_samples[n + 3].position);
		lux::weno7_diff(vel, acc, jot, F, 3);
		m_samples[n].construct(m_samples[n].position, vel, acc, jot, m_samples[n].time);
	}

	return true;
}

#endif
