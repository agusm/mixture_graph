#ifndef __LUX_WENO_H__
#define __LUX_WENO_H__

#include"lux_math.h"
namespace lux {
	// weighted essentially non-oscillatory interpolation of f[0]..f[6] at parameters t=0..t=6. Derivatives are evaluated at t=3
	template<class T> void weno7_diff(T& dt, T& d2t, T& d3t, const T f[7]);
	template<class T> void weno7_diff(vec4<T>& dt, vec4<T>& d2t, vec4<T>& d3t, const vec4<T> f[7], size_t dim = 4);

};


template<class T>
void lux::weno7_diff(T& dt, T& d2t, T& d3t, const T f[7]) {
	// 1. fit four cubic polynomials to intervals [0,3]; [1,4]; [2,5]; [3,6].
	// using Newton's method

	// 2. smoothness indicators: integral over all squared derivatives
	//							0 1 2 3 4 5 6 
	//		s[0] = int_2^3 ...  - - * * 
	//		s[1] = int_1^3 ...    - * * *
	//		s[2] = int_0^2 ...      * * * - 
	//		s[3] = int_0^1 ...        * * - -
	//	The derivatives will be estimated at x=3, which is in local coordinates
	//		p0(3), p1(2), p2(1), p3(0)

	double s[4] = {
		(f[0] * (244.0*f[0] - 1659.0*f[1] + 1854.0*f[2] - 683.0*f[3]) + f[1] * (2976.0*f[1] - 6927.0*f[2] + 2634.0*f[3]) + f[2] * (4326.0*f[2] - 3579.0*f[3]) + f[3] * f[3] * 814.0) / 180.0,
		(f[1] * (3169.0*f[1] - 19374.0*f[2] + 19014.0*f[3] - 5978.0*f[4]) + f[2] * (33441.0*f[2] - 70602.0*f[3] + 23094.0*f[4]) + f[3] * (41001.0*f[3] - 30414.0*f[4]) + f[4] * f[4] * 6649.0) / 2880.0,
		(f[2] * (6649.0*f[2] - 30414.0*f[3] + 23094.0*f[4] - 5978.0*f[5]) + f[3] * (41001.0*f[3] - 70602.0*f[4] + 19014.0*f[5]) + f[4] * (33441.0*f[4] - 19374.0*f[5]) + f[5] * f[5] * 3169.0) / 2880.0,
		(f[3] * (814.0*f[3] - 3579.0*f[4] + 2634.0*f[5] - 683.0*f[6]) + f[4] * (4326.0*f[4] - 6927.0*f[5] + 1854.0*f[6]) + f[5] * (2976.0*f[5] - 1659.0*f[6]) + f[6] * f[6] * 244.0) / 180.0,
	};

	// 3. optimal blends, will be evaluated at t = 3
	//	These are obtained by polynomial division: Let P(t) the full polynomial, p0..p3 the partial cubic polynomials.
	//	f[0] can only be found in p0 whereas f[6] can only be found in p3.
	//	First stage thus:
	//		C0 = 1 + quo( coeff(P(t),F_0) - coeff(p0(t),F_0) , coeff(p0(t),F_0)
	//		C3 = 1 + quo( coeff(P(t),F_6) - coeff(p3(t),F_6) , coeff(p3(t),F_6)
	//	Second stage
	//		Let Q = P(t)-C0(t)*p0(t)-C3(t)*p3(t)
	//		C1 = 1 + quo( coeff(Q(t),F_1) - coeff(p1(t),F_1) , coeff(p1(t),F_1)
	//		C2 = 1 + quo( coeff(Q(t),F_5) - coeff(p2(t),F_5) , coeff(p2(t),F_5)
	// Results:
	//	C0: 1+(-t^3+3t^2-26t)/120
	//	C1: (t^3-3t^2+10t)/40
	//	C2: (-t^3+7t^2-6t)/40
	//	C3: (t^3-3t^2+2t)/120
	double C[4] = { 7.0 / 20.0, 3.0 / 4.0, 9.0 / 20.0, 1.0 / 20.0 }; // evaluated at t=3
	// first order derivatives
	//	dC0 = (-3t^2+6t-26)/120
	//	dC1 = (3t^2-6t+10)/40
	//	dC2 = (-3t^2+14t-6)/40
	//	dC3 = (3t^2-6t+2)/120
	double dC[4] = { -7.0 / 24.0, 19.0 / 40.0, 9.0 / 40.0, 11.0 / 120.0 };	// evaluated at t=3
	// second order derivatives
	//	d2C0 = (1-t)/20
	//	d2C1 = (3t-3)/20
	//	d2C2 = (7-3t)/20
	//	d2C3 = (t-1)/20
	double d2C[4] = { -1.0 / 10.0, 3.0 / 10.0, -1.0 / 10.0, 1.0 / 10.0 };	// evaluated at t=3
	// third order derivatives
	//	d3C0 = -1/20
	//	d3C1 = 3/20
	//	d3C2 = -3/20
	//	d3C3 = 1/20
	double d3C[4] = { -1.0 / 20.0,3.0 / 20.0,-3.0 / 20.0,1.0 / 20.0 };

	static const double eps = 1e-6;

	// BLENDING WEIGHTS : A[n] = C[n]/(s[n]+eps)^2
	double A[4], dA[4], d2A[4], d3A[4], sum = 0.0, dsum = 0.0, d2sum = 0.0, d3sum = 0.0;
	for (size_t n = 0; n < 4; n++) {
		// TODO: why square s[n] again? It's already a quadratic function...
		s[n] = 1.0 / ::sqrt(s[n] + eps);// best performance...  factor 4x due to 4 weights -- all 
		A[n] = C[n] * s[n];
		dC[n] *= s[n];
		d2C[n] *= s[n];
		d3C[n] *= s[n];
		sum += A[n];		// sum of weights
		dsum += dC[n];		// first order derivative of sum of weights
		d2sum += d2C[n];	// second order derivative of sum of weights
		d3sum += d3C[n];	// third order derivative of sum of weights
	};
	// normalize weights and compute derivatives
	for (size_t n = 0; n < 4; n++) {
		// assumed correct
		dA[n] = dC[n] / sum - A[n] * dsum / (sum*sum);
		// assumed correct
		d2A[n] = d2C[n] / sum - 2.0*dC[n] / (sum*sum)*dsum + 2.0*A[n] * dsum*dsum / (sum*sum*sum) - A[n] * d2sum / (sum*sum);
		d3A[n] = d3C[n] / sum - 3.0*(d2C[n] * dsum + dC[n] * d2sum) / (sum*sum) - A[n] * d3sum / (sum*sum) + 6.0*(dC[n] * dsum*dsum + A[n] * dsum*d2sum) / (sum*sum*sum) - 6.0*A[n] * dsum*dsum*dsum / (sum*sum*sum*sum);
		A[n] /= sum;
	}
	// all candidates agree on position:
	// pos = f[3];
	// velocity estimates
	double vel[4] = { // evaluated at t=3
		(-2.0*f[0] + 9.0*f[1] - 18.0*f[2] + 11 * f[3]) / 6.0,
		(f[1] - 6.0*f[2] + 3.0*f[3] + 2.0*f[4]) / 6.0,
		(-2.0*f[2] - 3.0*f[3] + 6.0*f[4] - f[5]) / 6.0,
		(-11 * f[3] + 18.0*f[4] - 9.0*f[5] + 2.0*f[6]) / 6.0
	};
	// acceleration estimates
	double acc[4] = { // evaluated at t=3
		-f[0] + 4.0*f[1] - 5.0*f[2] + 2.0*f[3],
		f[2] - 2.0*f[3] + f[4],
		f[2] - 2.0*f[3] + f[4],
		2.0*f[3] - 5.0*f[4] + 4.0*f[5] - f[6]
	};
	// jot estimates
	double jot[4] = { // evaluated at t=3
		f[3] - 3.0*f[2] + 3.0*f[1] - f[0],
		f[4] - 3.0*f[3] + 3.0*f[2] - f[1],
		f[5] - 3.0*f[4] + 3.0*f[3] - f[2],
		f[6] - 3.0*f[5] + 3.0*f[4] - f[3]
	};

	// Now le assemblage:
	dt = d2t = d3t = 0.0;
	for (size_t n = 0; n < 4; n++) {
		// assumed correct
		dt += dA[n] * f[3] + A[n] * vel[n];
		//assumed correct
		d2t += d2A[n] * f[3] + 2.0*dA[n] * vel[n] + A[n] * acc[n];
		// assumed correct
		d3t += d3A[n] * f[3] + 3.0*d2A[n] * vel[n] + 3.0*dA[n] * acc[n] + A[n] * jot[n];
	}
}

template<class T>
void lux::weno7_diff(vec4<T>& dt, vec4<T>& d2t, vec4<T>& d3t, const vec4<T> f[7], size_t dim) {
	dim = std::min(dim, size_t(4));
	// 1. fit four cubic polynomials to intervals [0,3]; [1,4]; [2,5]; [3,6].
	// using Newton's method

	// 2. smoothness indicators: integral over all squared derivatives
	//							0 1 2 3 4 5 6 
	//		s[0] = int_2^3 ...  - - * * 
	//		s[1] = int_1^3 ...    - * * *
	//		s[2] = int_0^2 ...      * * * - 
	//		s[3] = int_0^1 ...        * * - -
	//	The derivatives will be estimated at x=3, which is in local coordinates
	//		p0(3), p1(2), p2(1), p3(0)

	for (size_t c = 0; c < dim; c++) {
		double s[4] = {
			(f[0](c) * (244.0*f[0](c) - 1659.0*f[1](c) + 1854.0*f[2](c) - 683.0*f[3](c)) + f[1](c) * (2976.0*f[1](c) - 6927.0*f[2](c) + 2634.0*f[3](c)) + f[2](c) * (4326.0*f[2](c) - 3579.0*f[3](c)) + f[3](c) * f[3](c) * 814.0) / 180.0,
			(f[1](c) * (3169.0*f[1](c) - 19374.0*f[2](c) + 19014.0*f[3](c) - 5978.0*f[4](c)) + f[2](c) * (33441.0*f[2](c) - 70602.0*f[3](c) + 23094.0*f[4](c)) + f[3](c) * (41001.0*f[3](c) - 30414.0*f[4](c)) + f[4](c) * f[4](c) * 6649.0) / 2880.0,
			(f[2](c) * (6649.0*f[2](c) - 30414.0*f[3](c) + 23094.0*f[4](c) - 5978.0*f[5](c)) + f[3](c) * (41001.0*f[3](c) - 70602.0*f[4](c) + 19014.0*f[5](c)) + f[4](c) * (33441.0*f[4](c) - 19374.0*f[5](c)) + f[5](c) * f[5](c) * 3169.0) / 2880.0,
			(f[3](c) * (814.0*f[3](c) - 3579.0*f[4](c) + 2634.0*f[5](c) - 683.0*f[6](c)) + f[4](c) * (4326.0*f[4](c) - 6927.0*f[5](c) + 1854.0*f[6](c)) + f[5](c) * (2976.0*f[5](c) - 1659.0*f[6](c)) + f[6](c) * f[6](c) * 244.0) / 180.0,
		};

		// 3. optimal blends, will be evaluated at t = 3
		//	These are obtained by polynomial division: Let P(t) the full polynomial, p0..p3 the partial cubic polynomials.
		//	f[0] can only be found in p0 whereas f[6] can only be found in p3.
		//	First stage thus:
		//		C0 = 1 + quo( coeff(P(t),F_0) - coeff(p0(t),F_0) , coeff(p0(t),F_0)
		//		C3 = 1 + quo( coeff(P(t),F_6) - coeff(p3(t),F_6) , coeff(p3(t),F_6)
		//	Second stage
		//		Let Q = P(t)-C0(t)*p0(t)-C3(t)*p3(t)
		//		C1 = 1 + quo( coeff(Q(t),F_1) - coeff(p1(t),F_1) , coeff(p1(t),F_1)
		//		C2 = 1 + quo( coeff(Q(t),F_5) - coeff(p2(t),F_5) , coeff(p2(t),F_5)
		// Results:
		//	C0: 1+(-t^3+3t^2-26t)/120
		//	C1: (t^3-3t^2+10t)/40
		//	C2: (-t^3+7t^2-6t)/40
		//	C3: (t^3-3t^2+2t)/120
		double C[4] = { 7.0 / 20.0, 3.0 / 4.0, 9.0 / 20.0, 1.0 / 20.0 }; // evaluated at t=3
		// first order derivatives
		//	dC0 = (-3t^2+6t-26)/120
		//	dC1 = (3t^2-6t+10)/40
		//	dC2 = (-3t^2+14t-6)/40
		//	dC3 = (3t^2-6t+2)/120
		double dC[4] = { -7.0 / 24.0, 19.0 / 40.0, 9.0 / 40.0, 11.0 / 120.0 };	// evaluated at t=3
		// second order derivatives
		//	d2C0 = (1-t)/20
		//	d2C1 = (3t-3)/20
		//	d2C2 = (7-3t)/20
		//	d2C3 = (t-1)/20
		double d2C[4] = { -1.0 / 10.0, 3.0 / 10.0, -1.0 / 10.0, 1.0 / 10.0 };	// evaluated at t=3
		// third order derivatives
		//	d3C0 = -1/20
		//	d3C1 = 3/20
		//	d3C2 = -3/20
		//	d3C3 = 1/20
		double d3C[4] = { -1.0 / 20.0,3.0 / 20.0,-3.0 / 20.0,1.0 / 20.0 };

		static const double eps = 1e-6;

		// BLENDING WEIGHTS : A[n] = C[n]/(s[n]+eps)^2
		double A[4], dA[4], d2A[4], d3A[4], sum = 0.0, dsum = 0.0, d2sum = 0.0, d3sum = 0.0;
		for (size_t n = 0; n < 4; n++) {
			// TODO: why square s[n] again? It's already a quadratic function...
			s[n] = 1.0 / ::sqrt(s[n] + eps);// best performance...  factor 4x due to 4 weights -- all 
			A[n] = C[n] * s[n];
			dC[n] *= s[n];
			d2C[n] *= s[n];
			d3C[n] *= s[n];
			sum += A[n];		// sum of weights
			dsum += dC[n];		// first order derivative of sum of weights
			d2sum += d2C[n];	// second order derivative of sum of weights
			d3sum += d3C[n];	// third order derivative of sum of weights
		};
		// normalize weights and compute derivatives
		for (size_t n = 0; n < 4; n++) {
			// assumed correct
			dA[n] = dC[n] / sum - A[n] * dsum / (sum*sum);
			// assumed correct
			d2A[n] = d2C[n] / sum - 2.0*dC[n] / (sum*sum)*dsum + 2.0*A[n] * dsum*dsum / (sum*sum*sum) - A[n] * d2sum / (sum*sum);
			d3A[n] = d3C[n] / sum - 3.0*(d2C[n] * dsum + dC[n] * d2sum) / (sum*sum) - A[n] * d3sum / (sum*sum) + 6.0*(dC[n] * dsum*dsum + A[n] * dsum*d2sum) / (sum*sum*sum) - 6.0*A[n] * dsum*dsum*dsum / (sum*sum*sum*sum);
			A[n] /= sum;
		}
		// all candidates agree on position:
		// pos = f[3];
		// velocity estimates
		double vel[4] = { // evaluated at t=3
			(-2.0*f[0](c) + 9.0*f[1](c) - 18.0*f[2](c) + 11 * f[3](c)) / 6.0,
			(f[1](c) - 6.0*f[2](c) + 3.0*f[3](c) + 2.0*f[4](c)) / 6.0,
			(-2.0*f[2](c) - 3.0*f[3](c) + 6.0*f[4](c) - f[5](c)) / 6.0,
			(-11 * f[3](c) + 18.0*f[4](c) - 9.0*f[5](c) + 2.0*f[6](c)) / 6.0
		};
		// acceleration estimates
		double acc[4] = { // evaluated at t=3
			-f[0](c) + 4.0*f[1](c) - 5.0*f[2](c) + 2.0*f[3](c),
			f[2](c) - 2.0*f[3](c) + f[4](c),
			f[2](c) - 2.0*f[3](c) + f[4](c),
			2.0*f[3](c) - 5.0*f[4](c) + 4.0*f[5](c) - f[6](c)
		};
		// jot estimates
		double jot[4] = { // evaluated at t=3
			f[3](c) - 3.0*f[2](c) + 3.0*f[1](c) - f[0](c),
			f[4](c) - 3.0*f[3](c) + 3.0*f[2](c) - f[1](c),
			f[5](c) - 3.0*f[4](c) + 3.0*f[3](c) - f[2](c),
			f[6](c) - 3.0*f[5](c) + 3.0*f[4](c) - f[3](c)
		};

		// Now le assemblage:
		dt(c) = d2t(c) = d3t(c) = 0.0;
		for (size_t n = 0; n < 4; n++) {
			// assumed correct
			dt(c) += dA[n] * f[3](c) + A[n] * vel[n];
			//assumed correct
			d2t(c) += d2A[n] * f[3](c) + 2.0*dA[n] * vel[n] + A[n] * acc[n];
			// assumed correct
			d3t(c) += d3A[n] * f[3](c) + 3.0*d2A[n] * vel[n] + 3.0*dA[n] * acc[n] + A[n] * jot[n];
		}
	}
}

#endif

