#ifndef __LUX_COLOR_H__
#define __LUX_COLOR_H__

/* LIST OF COLORMAPS
	01. Accents				Accent3  ... Accent8
	02. Blues				Blues3   ... Blues9
	03. Brown-Blue-Green	BrBG3    ... BrBG11
	04. Blue-Green			BuGn3    ... Bugn9
	05. Blue-Purple			BuPu3    ... BuPu9
	06. Dark2				Dark2_3  ... Dark2_8
	07. Green-Blue			GnBu3    ... GnBu9
	08. Greens				Greens3	 ... Greens9
	09. Greys				Greys3 	 ... Greys9
	10. Oranges				Oranges3 ... Oranges9
	11. Orange-Red			OrRd3	 ... OrRd9
	12. Paired				Paired3	 ... Paired12
	13. Pastel1				Pastel1_3... Pastel1_9
	14. Pastel2				Pastel2_3... Pastel2_8
*/

#include<cassert>
#include<string>
#include<unordered_map>
#include<inttypes.h>

namespace lux {
	template<class T> class vec4;
	class color {
	public:
		constexpr inline color(void) : m_data{ 0.0f,0.0f,0.0f,0.0f }  {}
		constexpr inline color(float r, float g, float b, float a = 1.0f) : m_data{ r,g,b,a } {}
		constexpr inline color(uint8_t r, uint8_t g, uint8_t b, uint8_t a=255) : m_data{ float(r) / 255.0f, float(g) / 255.0f, float(b) / 255.0f, float(a) / 255.0f } {}
		inline color(const color& other) { *this = other; }
		inline ~color(void) {}
		inline float& operator()(size_t n) {
			assert("lux::color() -- invalid parameter" && n < 4); return m_data[n];
		}
		inline operator float*(void) { return m_data; }
		inline operator const float*(void) const { return m_data; }
		inline float& red(void) { return m_data[0]; }
		inline float& green(void) { return m_data[1]; }
		inline float& blue(void) { return m_data[2]; }
		inline float& alpha(void) { return m_data[3]; }
		inline const float& operator()(size_t n) const {
			assert("lux::color() -- invalid parameter" && n < 4); return m_data[n];
		}
		inline const float& red(void) const { return m_data[0]; }
		inline const float& green(void) const { return m_data[1]; }
		inline const float& blue(void) const { return m_data[2]; }
		inline const float& alpha(void) const { return m_data[3]; }
		static const color Zero;
		template<class S>
		inline vec4<S> as(void) const {
			return vec4<S>(S(m_data[0]), S(m_data[1]), S(m_data[2]), S(m_data[3]));
		}
		inline friend color lerp(const color& a, const color& b, float w) {
			return color(
				a.m_data[0] + w*(b.m_data[0] - a.m_data[0]),
				a.m_data[1] + w*(b.m_data[1] - a.m_data[1]),
				a.m_data[2] + w*(b.m_data[2] - a.m_data[2]),
				a.m_data[3] + w*(b.m_data[3] - a.m_data[3])
			);
		}
	protected:
		float m_data[4];
	};

	class colormap {
	public:
		typedef typename std::unordered_map<std::string, typename std::vector<lux::color> >::const_iterator iterator;
		colormap(void);
		colormap(const std::string& name);
		colormap(const colormap& other);
		inline colormap(lux::colormap::iterator& it) { m_data = it; }
		~colormap(void);
		inline size_t size(void) const;
		inline const lux::color& operator()(size_t n) const;
		inline const lux::color& operator[](size_t n) const;
		inline colormap& operator=(const colormap& other);
		inline const std::string& name(void) const;
		inline bool valid(void) const;
		// STATIC ACCESS
		static bool has_map(const std::string& name);
		static size_t map_size(const std::string& name);
		static const lux::color& get_color(const std::string& name, size_t n);
		
		static inline iterator begin(void) {
			if (m_database.empty()) init();
			return m_database.cbegin();
		}
		static inline iterator end(void) {
			if (m_database.empty()) init();
			return m_database.cend();
		}
		static inline size_t nMaps(void) {
			if (m_database.empty()) init();
			return m_database.size();
		}
	protected:
		std::unordered_map<std::string, typename std::vector<lux::color> >::const_iterator m_data;
	private:
		static std::unordered_map<std::string, typename std::vector<lux::color> > m_database;
		static void init(void);
	};
};

#include"lux_math.h"

inline size_t lux::colormap::size(void) const {
	return m_data->second.size();
}

inline const lux::color& lux::colormap::operator()(size_t n) const {
	assert("lux::colormap::color -- invalid colormap" && m_data != m_database.end());
	assert("lux::colormap::color -- invalid parameter" && n < m_data->second.size());
	return m_data->second[n];
}

inline const lux::color& lux::colormap::operator[](size_t n) const {
	assert("lux::colormap::color -- invalid colormap" && m_data != m_database.end());
	assert("lux::colormap::color -- invalid parameter" && n < m_data->second.size());
	return m_data->second[n];
}

inline lux::colormap& lux::colormap::operator=(const colormap& other) {
	m_data = other.m_data;
	return *this;
}

inline const std::string& lux::colormap::name(void) const {
	assert("lux::colormap::name -- invalid colormap" && m_data != m_database.end());
	return m_data->first;
}

inline bool lux::colormap::valid(void) const {
	return m_data != m_database.end();
}

#endif
