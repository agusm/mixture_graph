#ifndef __LUX_HERMITE_CURVE_S3_H__
#define __LUX_HERMITE_CURVE_S3_H__

#include"lux_math.h"

// M-S. Kim and K-W. Nam,
// Hermite Interpolation of Solid Orientations with Circular Blending Quaternion Curves
// In J. of Visualization and Computer Animation, 7(2):95--110, 1996.

// own contribution: work out math for kappa, tau

namespace lux {
	template<class T> vec3<T> CircularCurve(const vec3<T>& q1, const vec3<T>& v1, const vec3<T>& q2, const T& t);
	template<class T> vec3<T> HermiteCurveS2(const vec3<T>& q1, const vec3<T>& q2, const vec3<T>& v1, const vec3<T>& v2, const T& t);
	template<class T> quat4<T> slerp(const quat4<T>& q1, const quat4<T>& q2, const T& t);
	template<class T> vec3<T> slerp(const vec3<T>& q1, const vec3<T>& q2, const T& t);
	template<class T> quat4<T> HermiteCurveS3(const quat4<T>& q1, const quat4<T>& q2, const quat4<T>& v1, const quat4<T>& v2, const T& t);
};

template<class T>
lux::vec3<T> lux::CircularCurve(const vec3<T>& q1, const vec3<T>& v1, const vec3<T>& q2, const T& t) {
	// 1. Initial setup
	vec3<T> n2(v1);					n2.normalize();
	vec3<T> n3 = vec3<T>(q2 - q1) ^ n2;	n3.normalize();
	vec3<T> n1 = n2^n3;

	// 2. Reduce the problem to a circle
	T z0 = n3.dot(q1);
	T r0 = n3.dot(q1);
	T s1 = v1.length();
	T theta1 = atan2(n2.dot(q2), n1.dot(q2));
	assert("lux::CircularCurve -- invalid input" && r0 != T(0));

	// 3. Generate point on the circular curve
	T b = s1 / r0;
	T a = theta1 - b;
	T theta = t*(a*t + b);
	return vec3<T>(r0*cos(theta), r0*sin(theta), z0);
}

template<class T>
lux::vec3<T> lux::HermiteCurveS2(const vec3<T>& q1, const vec3<T>& q2, const vec3<T>& v1, const vec3<T>& v2, const T& t) {
	vec3<T> C1 = CircularCurve<T>(q1, v1, q2, t);
	vec3<T> C2 = CircularCurve<T>(q2, -v2, q1, T(1) - t);
	return slerp(C1, C2, t);
}

template<class T>
lux::quat4<T> lux::HermiteCurveS3(const quat4<T>& q1, const quat4<T>& q2, const quat4<T>& v1, const quat4<T>& v2, const T& t) {
	// 1. Initial setup
	lux::quat4<T> n4 = lux::ternary_product<T>(v1, v2, (q2 - q1), true);
	lux::quat4<T> n3 = lux::ternary_product<T>(n4, v1, v2, true);
	lux::quat4<T> n1(v1); n1.normalize();
	lux::quat4<T> n2 = lux::ternary_product<T>(n3, n4, n1);

	// 2. Reduce the problem to S2
	T w0 = n4.dot(q1);				// Eq.(10)
	T r0 = T(::sqrt(1.0 - w0*w0));	// Eq.(13.5)
	lux::quat4<T> p0 = w0*n4;
	assert("lux::HermiteCurveS3 -- invalid input" && r0 != T(0));
	T ooR0 = T(1) / r0;
	vec3<T> q1t(T(0), n2.dot(q1), n3.dot(q1));			q1t *= ooR0;
	vec3<T> q2t(n1.dot(q2), n2.dot(q2), n3.dot(q2));	q2t *= ooR0;
	vec3<T> v1t(v1.length(), T(0), T(0));				v1t *= ooR0;
	vec3<T> v2t(n1.dot(v2), n2.dot(v2), T(0));			v2t *= ooR0;

	// 3. Call HermiteCurveS2
	vec3<T> Qt = HermiteCurveS2(q1t, q2t, v1t, v2t, t);
	Qt *= r0;

	// 4. Generate point at t
	return p0 + Qt(0)*n1 + Qt(1)*n2 + Qt(2)*n3;
}

template<class T>
inline lux::quat4<T> lux::slerp(const quat4<T>& q0, const quat4<T>& q1, const T& w) {
	if (w <= T(0)) return q0;
	if (w >= T(1)) return q1;

	quat4<T> q(q1);
	T dot = q0.dot(q1);
	if (dot < T(0)) {
		dot = -dot;
		q = -q;
	}
	if (dot > T(0.9995)) {
		q = q0 + w*quat4<T>(q1 - q);
		q.normalize();
		return q;
	}
	dot = std::min(T(1), std::max(T(0), dot));
	T theta_0 = acos(dot);
	T theta = theta_0*w;
	T s0 = cos(theta) - dot*sin(theta) / sin(theta_0);
	T s1 = sin(theta) / sin(theta_0);

	return s0*q0 + s1*q;
}

template<class T>
inline lux::vec3<T> lux::slerp(const vec3<T>& q0, const vec3<T>& q1, const T& w) {
	if (w <= T(0)) return q0;
	if (w >= T(1)) return q1;
	// interprets q0,q1 as versors

	vec3<T> q(q1);
	T dot = q0.dot(q1);
	if (dot < T(0)) {
		dot = -dot;
		q = -q;
	}
	if (dot > T(0.9995)) {
		q = q0 + w*vec3<T>(q1 - q);
		q.normalize();
		return q;
	}
	dot = std::min(T(1), std::max(T(0), dot));
	T theta_0 = acos(dot);
	T theta = theta_0*w;
	T s0 = cos(theta) - dot*sin(theta) / sin(theta_0);
	T s1 = sin(theta) / sin(theta_0);

	return s0*q0 + s1*q;
}


#endif

