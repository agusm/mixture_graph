#include"lux_auxiliary.h"
#include<stdio.h>
#include<stdlib.h>

bool lux::file_exists(const std::string& name) {
	FILE* stream = nullptr;
	if (!fopen_s(&stream, name.c_str(), "r")) {
		fclose(stream);
		return true;
	}
	return false;
}

size_t lux::find_file_slot(const std::string& mask) {
	char ch[256];
	int loc = 0;
	do {
		sprintf_s(ch, mask.c_str(), loc);
		loc++;
	} while (file_exists(std::string(ch)));
	return size_t(loc - 1);
}