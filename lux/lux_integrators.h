#ifndef __LUX_INTEGRATORS_H__
#define __LUX_INTEGRATORS_H__

#include"lux_math.h"

namespace lux {
	template<class T> class trajectory;
	template<class T> class vectorfield3;

	template<class T> trajectory<T> euler_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF);
	template<class T> trajectory<T> midpoint_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF);
	template<class T> trajectory<T> rk4_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF);
	template<class T> trajectory<T> fsa_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF);
};

#include"lux_trajectory.h"
#include"lux_vectorfield3.h"

template<class T>
lux::trajectory<T> lux::euler_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF) {
	lux::vec4<T> x(x0); x(3) = T(0);
	lux::sample_t<T> s;
	VF.get_sample(s, x);
	size_t n = 0;
	lux::trajectory<T> traj;
	while (VF.in_volume(x) && n < max_steps) {
		traj.push_back(s,lux::color(1.0f,1.0f,1.0f,1.0f));
		x += dt*s.velocity;
		VF.get_sample(s, x);
		n++;
	}
	return traj;
}


template<class T>
lux::trajectory<T> lux::midpoint_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF) {
	lux::vec4<T> x(x0); x(3) = T(0);
	lux::sample_t<T> s;
	VF.get_sample(s, x);
	size_t n = 0;
	lux::trajectory<T> traj;
	lux::vec4<T> vel;
	while (VF.in_volume(x) && n < max_steps) {
		traj.push_back(s, lux::color(1.0f,1.0f,1.0f,1.0f));
		// first step
		s.position += T(0.5)*dt*s.velocity;
		vel = VF.sample_velocity(s.position);
		// second step
		x += dt * vel;
		
		VF.get_sample(s, x);
		n++;
	}
	return traj;
}

template<class T>
lux::trajectory<T> lux::rk4_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF) {
	lux::vec4<T> x(x0); x(3) = T(0);
	lux::sample_t<T> s;
	VF.get_sample(s, x);
	lux::trajectory<T> traj;
	lux::vec4<T> k[4], vel;
	size_t n = 0;
	T physical_time = T(0);
	s.time = physical_time;
	double scale[3] = { VF.get_domain(0).rmax - VF.get_domain(0).rmin,VF.get_domain(1).rmax - VF.get_domain(1).rmin,VF.get_domain(2).rmax - VF.get_domain(2).rmin };
	while (VF.in_volume(x) && n < max_steps) {
		traj.push_back(s, lux::color(1.0f,1.0f,1.0f,1.0f));
		k[0] = VF.sample_velocity(x);
		k[1] = VF.sample_velocity(x + T(0.5)*dt*k[0]);
		k[2] = VF.sample_velocity(x + T(0.5)*dt*k[1]);
		k[3] = VF.sample_velocity(x + dt*k[2]);
		vel = (k[0] + T(2)*(k[1] + k[2]) + k[3]) / T(6);
		double v_mag = 0.0;
		for (size_t n = 0; n < 3; n++) {
			double dv = vel(3)*vel(n) / scale[n] * double(VF.dim(n));
			v_mag += dv*dv;
		}
		v_mag = ::sqrt(v_mag);
		// integrate with arc length parameter
		vel(3) = T(1);
		x += dt*vel;
		// integrate physical time
		VF.get_sample(s, x);
		physical_time += dt / v_mag;
		s.time = physical_time;
		n++;
	}
	return traj;
}

template<class T>
lux::trajectory<T> lux::fsa_trajectory(const lux::vec4<T>& x0, const T& dt, size_t max_steps, const lux::vectorfield3<T>& VF) {
	lux::vec4<T> x(x0); x(3) = T(0);
	lux::sample_t<T> s;
	VF.get_sample(s, x);
	lux::trajectory<T> traj;
	size_t n = 0;
	s.time = T(0);
	double scale[3] = { VF.get_domain(0).rmax - VF.get_domain(0).rmin,VF.get_domain(1).rmax - VF.get_domain(1).rmin,VF.get_domain(2).rmax - VF.get_domain(2).rmin };
	while (VF.in_volume(x) && n < max_steps) {
		traj.push_back(s, lux::color(1.0f, 1.0f, 1.0f, 1.0f));
		// integrate position
		lux::vec4<T> tng = s.get_tangent();
		s.position += dt*tng;
		// integrate orientation
		s.orientation += dt*s.dQdt();
		s.orientation.normalize();
		//printf("t=%f: %f %f %f / %f %f %f %f\n", s.position(3), s.position(0), s.position(1), s.position(2), s.orientation(0), s.orientation(1), s.orientation(2), s.orientation(3));
		// integrate physical time
		lux::vec4<T> vel = s.get_velocity();
		// map from physical velocity to voxels
		for (size_t n = 0; n < 3; n++) vel(n) = float(vel(n) / scale[n])*float(VF.dim(n));
		vel(3) = vel.length(3);
		s.time += dt / vel(3);
		s.curvature = VF.sample_curvature(s.position);
		s.torsion = VF.sample_torsion(s.position);
		s.vel_magnitude = VF.sample_velocity(s.position).length(3);
		n++;
	}
	return traj;
}

#endif

