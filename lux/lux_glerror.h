#ifndef __LUX_GLERROR_H__
#define __LUX_GLERROR_H__

#include"GL/glew.h"
#include<string>

namespace lux {
	extern std::string errstr(GLenum val);
};

#endif

