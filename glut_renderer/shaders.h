#ifndef __SHADERS_H__
#define __SHADERS_H__

constexpr const char vsStaticQuad[] = R"(// vertex shader: Static Quad
#version 420
in layout(location=0) vec2 position;
void main(void) {
	gl_Position = vec4(position.xy,0.0f,1.0f);
}
)";

constexpr const char fsStaticQuad[] = R"(// fragment shader: Static Quad
#version 420
out vec4 out_color;
uniform vec4 color;
void main(void) {
	out_color = color;
}
)";

constexpr const char vsRayEntry[] = R"(// vertex shader: Ray Entry setup
#version 420

in layout(location=0) vec3 input_position;
out vec3 object_position;
uniform mat4 ModelViewProjection;

void main(void) {
	object_position = input_position;
	gl_Position = ModelViewProjection*vec4(input_position,1.0f);
}
)";

constexpr const char fsRayEntry[] = R"(// fragment shader: Ray Entry setup
#version 420

in vec3 object_position;
uniform vec3 object_camera;
out float out_color;

void main(void) {
	if (gl_FrontFacing) {
		out_color = length(object_position-object_camera);
	} else {
		out_color = 0.0f;
	}
}	
)";

constexpr const char vsRayExit[] = R"(// vertex shader: Ray Exit & Raymarcher
#version 420

in layout(location=0) vec3 input_position;
out vec3 object_position;
uniform mat4 ModelViewProjection;

void main(void) {
	object_position = input_position;
	gl_Position = ModelViewProjection*vec4(input_position,1.0f);
}
)";

constexpr const char fsRayExit[] = R"(//fragment shader: Ray Exit & Raymarcher
#version 420

in vec3 object_position;
uniform layout(binding=0, r32f) readonly image2D entry;
uniform layout(binding=0) sampler3D volume;
uniform vec3 object_camera;
uniform vec4 background;
uniform bool enable_box;
uniform vec3 box_ll;
uniform vec3 box_ur;
uniform vec4 box_color;
out vec4 output_color;

vec2 ray_box(in vec3 dir) {
	vec3 t1 = (box_ll-object_camera)/dir;
	vec3 t2 = (box_ur-object_camera)/dir;
	vec3 tmin = min(t1,t2);
	vec3 tmax = max(t1,t2);
	float smin = max(max(tmin.x,tmin.y),tmin.z);
	float smax = min(min(tmax.x,tmax.y),tmax.z);
	return vec2(max(0.0f,smin),smax);
}

vec4 fetch_color(in vec3 pos) {
	vec4 color = vec4(1.0f,1.0f,1.0f,0.01f);
	color.a = texture(volume,pos).x;
	if (color.a<0.5f) color.a=0.0f;
	color.a*=0.1f;
	return color;
}

void main(void) {
	vec3 dir = object_position-object_camera;
	vec2 t = vec2(imageLoad(entry,ivec2(gl_FragCoord.xy)).r,length(dir));
	dir = normalize(dir);
	vec3 pos = object_camera + t.x*dir;
	output_color = vec4(0.5*pos+vec3(0.5,0.5,0.5),1.0);
}
/*
	
void main(void) {

	ivec3 voxel_dims = textureSize(volume,0);
	//gvec textureGrad(gsampler sampler, vec texCoord, gradvec dTdx, gradvec dTdy)

	vec3 dir = object_position-object_camera;
	vec2 t = vec2(imageLoad(entry,ivec2(gl_FragCoord.xy)).r,length(dir));
	dir = normalize(dir);

	vec3 entry = object_camera + t.x*dir;
	
	const float ds = 0.01f;

	vec4 accum = vec4(0.0f,0.0f,0.0f,0.0f);
	bool done = false;
	if (enable_box) {
		// ray-box intersect
		vec2 box_events = ray_box(dir);
		if (box_events.x<box_events.y) {
			
			// segment entry .. box_entry
			float len = box_events.x - t.x;
			for (float s = 0.0f; s<len; s+=ds) {		
				vec3 pos = 0.5f*mix(entry,object_position,s)+vec3(0.5f,0.5f,0.5f);
				vec4 in_color = fetch_color(pos);
				// compact form of "Under" operator
				accum += (1.0f-accum.a)*in_color.a*vec4(in_color.rgb,1.0f);
				if (accum.a>0.99f) {done = true; break;}
			}
	
			// segment box_entry .. box_exit
			if (!done) {
				accum += (1.0f-accum.a)*box_color.a*vec4(box_color.rgb,1.0f);
				len = box_events.y-box_events.x;
				for (float s = 0.0f; s<len; s+=ds) {		
					vec3 pos = 0.5f*mix(entry,object_position,s)+vec3(0.5f,0.5f,0.5f);
					vec4 in_color = fetch_color(pos);
					// compact form of "Under" operator
					accum += (1.0f-accum.a)*in_color.a*vec4(in_color.rgb,1.0f);
					if (accum.a>0.99f) { done = true; break;}
				}	
			}

			// segment box_exit .. exit
			if (!done) {
				accum += (1.0f-accum.a)*box_color.a*vec4(box_color.rgb,1.0f);
				len = t.y - box_events.y;
				for (float s = 0.0f; s<len; s+=ds) {		
					vec3 pos = 0.5f*mix(entry,object_position,s)+vec3(0.5f,0.5f,0.5f);
					vec4 in_color = fetch_color(pos);
					// compact form of "Under" operator
					accum += (1.0f-accum.a)*in_color.a*vec4(in_color.rgb,1.0f);
					if (accum.a>0.99f) {done = true; break;}
				}
			}	
			done = true;
		}
	}

	if (!done) {
		float len = t.y-t.x;
		for (float s = 0.0f; s<len; s+=ds) {		
			vec3 pos = 0.5f*mix(entry,object_position,s)+vec3(0.5f,0.5f,0.5f);
			vec4 in_color = fetch_color(pos);
			// compact form of "Under" operator
			accum += (1.0f-accum.a)*in_color.a*vec4(in_color.rgb,1.0f);
			if (accum.a>0.99f) break;
		}
	}
	// assuming background to be opaque
	accum += (1.0f-accum.a)*vec4(background.rgb,1.0f);
	output_color = accum;
}*/
)";

/* Ray-Box setup with analytic entry point -- slow!
constexpr const char vsRaymarcher[] = R"(// vertex shader: Ray Marcher
#version 420

in layout(location=0) vec3 input_position;
out vec3 object_position;
uniform mat4 ModelViewProjection;

void main(void) {
	object_position = input_position;
	gl_Position = ModelViewProjection*vec4(input_position,1.0f);
}
)";

constexpr const char fsRaymarcher[] = R"(// fragment shader: Ray Marcher
#version 420

in vec3 object_position;
out vec4 output_color;

uniform vec3 object_camera;
uniform vec3 box_ll;
uniform vec3 box_ur;
uniform vec3 volume_dims;
uniform vec4 background;

vec2 ray_box(in vec3 dir) {
	vec3 t1 = (box_ll-object_camera)/dir;
	vec3 t2 = (box_ur-object_camera)/dir;
	vec3 tmin = min(t1,t2);
	vec3 tmax = max(t1,t2);
	float smin = max(max(tmin.x,tmin.y),tmin.z);
	float smax = min(min(tmax.x,tmax.y),tmax.z);
	return vec2(max(0.0f,smin),smax);
}

float normalize_voxel(inout vec3 v) {
	vec3 av = abs(v);
	float nrm = max(av.x,max(av.y,av.z));
	v/=nrm;
	return nrm;
}

vec3 voxel_coords(vec3 object_coordinates) {
	return volume_dims*(object_coordinates-box_ll)/(box_ur-box_ll);
}

void main(void) {
	vec3 dir = object_position-object_camera;
	normalize(dir);
	vec2 t = ray_box(dir);
	vec3 entry = object_camera + t.x*dir;
	float len = t.y-t.x;

	vec4 accum = vec4(0.0f,0.0f,0.0f,0.0f);
	for (float s = 0.0f; s<len; s+=0.01f) {
		const vec4 in_color = vec4(1.0f,1.0f,1.0f,0.05f);
		// compact form of "Under" operator
		accum += (1.0f-accum.a)*in_color.a*vec4(in_color.rgb,1.0f);
	}
	// assuming background to be opaque
	accum += (1.0f-accum.a)*vec4(background.rgb,1.0f);
	output_color = accum;
}
)";
*/
#endif

