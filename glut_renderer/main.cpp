#include<stdlib.h>
#include<stdio.h>
#include<string>
#include<algorithm>
#include"lux.h"
#include"shaders.h"
#include"compressed_mipvolume.h"
#include"md5.h"
#include<omp.h>
#include<functional>
#include"glVolume.h"
#include"flags.h"



// TODO: 
// Box Selector

// FORCE OPTIMUS TO NVIDIA
extern "C" { _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001; }


// lux_math:			ok
// lux_arcball:			ok
// lux_camera:			ok, TODO panning: camera click, drag, release
// shaders:				ok, TODO consistent uniform settings
// box setup:			ok
// fbo:					ok
// volumes:				ok
// images:				ok
// distance_volume:		ok

// TODO: opacity adjustment
// TODO: length of ray in volume
// TODO: iso-surfaces
// TODO: scale-invariant volume rendering
// TODO: shifted tri-linear interpolation
// TODO: comparison with tri-cubic interpolation
// TODO: volume import

// RAY-TRACING SETUP SELECTOR
enum RaySetup {
	// Rasterize the backfaces of a unit cube, then compute a ray-box intersection per-fragment for the ray entry
	RS_ENTRY_ANALYTIC_EXIT_RASTER,
	// Rasterize the frontfaces of a  unit cube. Does not work if camera inside volume
	RS_ENTRY_RASTER,
	// Ours
	RS_ENTRY_EXIT_RASTER
};


//std::unordered_map<std::string, std::vector<lux::color> > lux::colormap::m_database;
lux::colormap lux_cm;


struct state_t {
	state_t(void);
	size_t						width;
	size_t						height;
	unsigned int				mode;
	std::string					name;
	lux::color					background;
	lux::color					box_color;
	CTimer						time;
	CTimer						fps_time;
	size_t						fps_count;
	int							swap_int;
	lux::arcballf				arcball;
	lux::cameraf				camera;
	bool						dpiAware;
	std::vector<lux::program*>	program;
	size_t						sel_program;
	lux::volume<float>			vol;
	lux::volume<uint8_t>		grad_vol;
	RaySetup					raysetup;
	GLuint						fbo;
	GLuint						raybuffer;
	GLuint						volume;
	int							window;
	bool						screenshot;
	lux::range_t<float>			box[3];
	bool						enable_box;
	compressed_mipvolume		data;
	glVolume					glData;
	std::string					data_file;
} renderstate;

#include<random>
void run_benchmark(void) {
	printf("DATA_SET         : %s\n", renderstate.data_file.c_str());
	printf("nLeafs           : %i\n", renderstate.data.get_nLeafs());
	printf("nLerpNodes       : %zi\n", renderstate.data.get_nLerpNodes());

	printf("sparse vector LUT: ");
	{
		const size_t nRuns = 32;
		CTimer ct0;
		for (size_t n = 0; n < nRuns; n++) renderstate.data.build_lookup_table(true, renderstate.glData.order());
		double dT = ct0.Query()*1000.0f / double(nRuns);
		printf("took %.6fms\n", dT);
	}

	printf("Range Queries with Error estimate\n");
	{
		std::mt19937 gen(0);
		static constexpr size_t nRuns = 100;
		for (size_t s = 8; s <= 8; s++) {
			uint32_t query[3] = { 1u << s,1u << s,1u << s };
			size_t nFetches = 0;
			sparse::vector<uint32_t, lux::errord> result;
			std::vector<lux::rangeu32_t> vR;
			for (size_t n = 0; n < nRuns; n++) {
				for (int i = 0; i < 3; i++) {
					lux::rangeu32_t R;
					std::uniform_int_distribution<uint32_t> D(0, renderstate.data.get_Dimension(i) - query[i]);
					R.vmin = D(gen);
					R.vmax = R.vmin + query[i] - 1;
					vR.push_back(R);
				}
			}
			printf("  %i x %i x %i [%i voxels]: ", query[0], query[1], query[2], query[0] * query[1] * query[2]);
			CTimer ctQ;
			for (size_t n = 0; n < nRuns; n++) {
				size_t f = renderstate.data.query_range(vR[3 * n], vR[3 * n + 1], vR[3 * n + 2], result);
				nFetches += f;
			}
			double dT = ctQ.Query()*1000.0 / double(nRuns);
			printf(" %.4fms, %.2f fetches (footprint) ",dT,double(nFetches)/double(nRuns));

			std::vector<sparse::vector<uint32_t, lux::errord> > partial(omp_get_max_threads());
			ctQ.Reset();
			for (size_t n = 0; n < nRuns; n++) {
				for (size_t k = 0; k < partial.size(); k++) partial[k].clear();
#pragma omp parallel for schedule(dynamic,2)
				for (int z = int(vR[3 * n + 2].vmin); z < int(vR[3 * n + 2].vmax); z++) {
					for (uint32_t y = vR[3 * n + 1].vmin; y < vR[3 * n + 1].vmax; y++) {
						for (uint32_t x = vR[3 * n].vmin; x < vR[3 * n].vmax; x++) {
							uint64_t id = renderstate.data.get_Voxel(x, y, z, 0);
							partial[omp_get_thread_num()] += renderstate.data.lookup_node(id);
						}
					}
				}
				result.clear();
				for (size_t n = 0; n < partial.size(); n++) result += partial[n];
			}
			double dTn = ctQ.Query()*1000.0 / double(nRuns);
			printf("%.4fms (naive) speedup %.2fx mem %.2f%%\n", dTn,dTn/dT,(double(query[0]*query[1]*query[2])-double(nFetches)/double(nRuns))/double(query[0]*query[1]*query[2])*100.0);
		}
	}
}

state_t::state_t(void) : 
	width(1024), height(1024), mode(GLUT_RGB | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH), 
	name("OpenGL"), fps_count(0), swap_int(0), dpiAware(true), sel_program(0), raybuffer(0), 
	screenshot(false) 
{
	raysetup = RS_ENTRY_ANALYTIC_EXIT_RASTER;
	for (int i = 0; i < 3; i++) {
		box[i].vmin = -0.5f;
		box[i].vmax = 0.5f;
	}
	enable_box = false;
	//background = lux::color(1.0f, 1.0f, 1.0f, 1.0f);
	background = lux_cm.get_color("Blues9", 8);
	box_color = lux_cm.get_color("YlOrRd9", 8);
	box_color(3) = 0.3f;
	//data_file = "Neocortex_sub.cmip";
	//data_file = "Hippocampus.cmip";
	//data_file = "CorradoHiQ.cmip";
	data_file = "Heinzl.cmip";

}

void OnStart(state_t& s) {
	s.camera.setFOV(60.0f);
	s.camera.setNear(0.001f);
	s.camera.setFar(100.0f);
}

constexpr const char* vertex_shader[] = { vsStaticQuad, vsRayEntry, vsRayExit };
constexpr const char* fragment_shader[] = { fsStaticQuad, fsRayEntry, fsRayExit };
constexpr const char* program_name[] = { "Static Quad", "RayEntrySetup" , "RayExitSetup" };
enum shader_id {
	SHADER_STATIC_QUAD = 0,
	SHADER_RAY_ENTRY = 1,
	SHADER_RAY_EXIT = 2,
};

uint32_t fast_read(bitstream<uint32_t>& stream, uint64_t pos, uint32_t bits) {
	bits = std::min(32u, bits);
	uint32_t result = 0;
	size_t addr = size_t(pos >> uint64_t(5));
	uint32_t offs = uint32_t(pos&uint64_t(0x1f));
	uint32_t cap = 32u - offs;
	uint32_t s = 0;
	uint32_t r = std::min(cap, bits);
	uint32_t m = (r == cap ? 0 : (uint32_t(1) << (offs + r))) + (0xFFFFFFFF << offs);
	result |= ((stream.data()[addr] & m) >> offs) << s;
	s += r;
	// next element
	if (bits > r) {
		bits -= r;
		uint32_t m = (uint32_t(1) << bits) - 1;
		result |= (stream.data()[addr+1] & m) << s;
	}
	return result;
}

void OnGLInit(void) {
	glewInit();
	renderstate.fps_time.Reset();
	renderstate.time.Reset();
	wglSwapIntervalEXT(renderstate.swap_int);
	glClearColor(renderstate.background(0), renderstate.background(1), renderstate.background(2), renderstate.background(3));

	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);
	glEnable(GL_CULL_FACE);

	glDepthFunc(GL_GEQUAL);
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);

	/// Add shaders here
	static_assert(sizeof(vertex_shader) == sizeof(fragment_shader) && sizeof(vertex_shader) == sizeof(program_name), "OnGLInit -- invalid combination of shaders");

	for (auto& p : renderstate.program) delete p;
	renderstate.program.clear();
	for (size_t n = 0; n < sizeof(vertex_shader) / sizeof(const char*); n++) {
		lux::program* p = new lux::program(program_name[n]);
		lux::shader s[2];
		if (vertex_shader[n] != nullptr) {
			s[0].compile(GL_VERTEX_SHADER, vertex_shader[n], lux::SOURCE_TEXT);
			if ((GLuint)s[0] == 0) {
				printf("Error compiling vertex shader\n");
				glutDestroyWindow(renderstate.window);
				glutExit();
				exit(0);
			}
			p->attach(s[0]);
		}
		if (fragment_shader[n] != nullptr) {
			s[1].compile(GL_FRAGMENT_SHADER, fragment_shader[n], lux::SOURCE_TEXT);
			if ((GLuint)s[1] == 0) {
				printf("Error compiling fragment shader\n");
				glutDestroyWindow(renderstate.window);
				glutExit();
				exit(0);
			}
			p->attach(s[1]);
		}
		p->link();
		if (!p->is_valid()) {
			printf("Error linking program '%s'\n", p->name().c_str());
			glutDestroyWindow(renderstate.window);
			glutExit();
			exit(0);
		}
		renderstate.program.push_back(p);
	}

	renderstate.program[SHADER_RAY_EXIT]->set_uniform("background", renderstate.background);
	renderstate.program[SHADER_RAY_EXIT]->set_uniform("box_color", renderstate.box_color);
	renderstate.vol.resize(64, 64, 64, 1);
	renderstate.vol.fill(
		lux::ranged_t(-1.0f, 1.0f),
		lux::ranged_t(-1.0f, 1.0f),
		lux::ranged_t(-1.0f, 1.0f),
		[](double* buf, const double& x, const double& y, const double& z, size_t comp) {
		buf[0] = 1.0 - sqrt(x*x + y*y + z*z) / sqrt(3.0);
	}
	);
	//for (int i = 0; i < 3; i++) {
	//	renderstate.box[i].vmin = 0;
	//	renderstate.box[i].vmax = uint32_t(renderstate.vol.dim(i));
	//}

	renderstate.vol.set_voxel_extents(1.0f, 1.0f, 1.0f);
	renderstate.vol.name() = std::string("spheres");
	renderstate.grad_vol = lux::gradient_volume<uint8_t>(renderstate.vol);

	glGenTextures(1, &renderstate.volume);
	glBindTexture(GL_TEXTURE_3D, renderstate.volume);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F,(GLsizei)renderstate.vol.dim(0), (GLsizei)renderstate.vol.dim(1), (GLsizei)renderstate.vol.dim(2), 0, GL_RED, GL_FLOAT, renderstate.vol.data());
	glBindTextureUnit(0, renderstate.volume);

	if (!renderstate.data.read(renderstate.data_file.c_str())) {
		printf("Error reading %s\n", renderstate.data_file.c_str());
		glutExit();
		exit(0);
	}
	renderstate.data.output();
	renderstate.glData.attach(renderstate.data);
	if (!renderstate.glData.upload_gpu()) {
		glutExit();
		exit(0);
	}
	renderstate.glData.background_color() = renderstate.background;

#ifdef BENCHMARK
	run_benchmark();
#endif

	lux::colormap CM("brbg11");// ("YlOrRd9");
	for (size_t n = 0; n < renderstate.glData.nLeafs(); n++) {
		renderstate.glData.leaf_color(n) = CM(n%CM.size());
		renderstate.glData.leaf_color(n)(3) = 0.3f;
	}
	renderstate.glData.leaf_color(0)(3) = 0.0f;

	/*std::mt19937 gen(1);
	for (size_t n = 0; n < renderstate.glData.nLeafs(); n++) {
		//std::uniform_real_distribution<float> D(0.0f, 1.0f);
		//lux::color clr(D(gen), D(gen), D(gen), 1.0f);
		lux::color white(1.0f, 1.0f, 1.0f, 1.0f);
		lux::color black(0.0f, 0.0f, 0.0f, 1.0f);
		renderstate.glData.leaf_color(n) = (rand()&1) ? white: black;
	}*/
	//renderstate.glData.leaf_color(0) = lux::color(0.8f, 0.8f, 0.8f, 0.0002f);
	renderstate.glData.gpu_update_color_lut();
	//renderstate.glData.requantize(5);
	//renderstate.glData.cpu_update_color_lut();

}

void OnKey(unsigned char ch, int x, int y) {
	switch (ch) {
	case 27: exit(0);
	case '+': renderstate.glData.lod_bias() += 0.2f; printf("bias: %f\n", renderstate.glData.lod_bias()); break;
	case '-': renderstate.glData.lod_bias() -= 0.2f; printf("bias: %f\n", renderstate.glData.lod_bias()); break;
	}
}

void OnSpecialKey(int ch, int x, int y) {
	switch (ch) {
	case GLUT_KEY_LEFT: renderstate.camera.pan(0.05f, 0.0f); break;
	case GLUT_KEY_RIGHT:renderstate.camera.pan(-0.05f, 0.0f); break;
	case GLUT_KEY_UP: renderstate.camera.pan(0.0f, -0.05f); break;
	case GLUT_KEY_DOWN: renderstate.camera.pan(0.0f, 0.05f); break;
	case GLUT_KEY_F1: renderstate.enable_box = !renderstate.enable_box; break;
	case GLUT_KEY_F11: renderstate.screenshot = true; break;
	case GLUT_KEY_F2:
		printf("Camera: eye: %f %f %f\n", renderstate.camera.getEye()(0), renderstate.camera.getEye()(1), renderstate.camera.getEye()(2));
		printf("Camera: up : %f %f %f\n", renderstate.camera.getUp()(0), renderstate.camera.getUp()(1), renderstate.camera.getUp()(2));
		printf("Camera: lat: %f %f %f\n", renderstate.camera.getCenter()(0), renderstate.camera.getCenter()(1), renderstate.camera.getCenter()(2));
		break;
	}
}

void OnMouse(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) renderstate.arcball.click(size_t(x), size_t(y));
		if (state == GLUT_UP) renderstate.arcball.release(size_t(x), size_t(y));
	}
}

void OnMotion(int x, int y) {
	renderstate.arcball.drag(size_t(x), size_t(y));
}

void OnMouseWheel(int wheel, int direction, int x, int y) {
	if (wheel != 0) return;
	float s = 1.05f - 0.1f*float(direction);
	renderstate.camera.zoom(s);
}

void OnReshape(int width, int height) {
	renderstate.width = size_t(std::max(1, width));
	renderstate.height = size_t(std::max(1, height));
	renderstate.arcball.reshape(renderstate.width, renderstate.height);
	renderstate.camera.reshape(width, height);
	renderstate.glData.reshape(width, height);
	renderstate.glData.compute_lod_scale(renderstate.camera.getFOV());
	glViewport(0, 0, width, height);

	// Resize raybuffer
	if (renderstate.fbo == 0) {
		glGenFramebuffers(1, &renderstate.fbo);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, renderstate.fbo);
	}
	else {
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, renderstate.fbo);
		glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0, 0, 0);
		glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, 0);
	}
	if (renderstate.raybuffer != 0) glDeleteTextures(1, &renderstate.raybuffer);
	glGetError();
	glGenTextures(1, &renderstate.raybuffer);
	glBindTexture(GL_TEXTURE_2D, renderstate.raybuffer);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderstate.raybuffer, 0);
	GLenum res = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (res != GL_FRAMEBUFFER_COMPLETE) {
		printf("Error: '%s' while resizing FBO to %zi x %zi\n", lux::errstr(res).c_str(), renderstate.width, renderstate.height);
		exit(0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTextureUnit(0, renderstate.volume);
}

void OnDisplay(void) {
	glClearColor(renderstate.background(0), renderstate.background(1), renderstate.background(2), renderstate.background(3));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	lux::mat4f ModelView = renderstate.camera.getView()*(renderstate.arcball.getOrientation().asMatrix());
	lux::vec4f campos = ModelView.inverse().col(3);
	lux::mat4f ModelViewProjection = renderstate.camera.getProjection()*ModelView;

	renderstate.glData.draw(glVolume::SHADER_CUBERILLE, renderstate.camera.getProjection(), ModelView);
	
	/*
	glBindFramebuffer(GL_FRAMEBUFFER, renderstate.fbo);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glCullFace(GL_BACK);
	renderstate.program[SHADER_RAY_ENTRY]->set_uniform("ModelViewProjection", ModelViewProjection);
	renderstate.program[SHADER_RAY_ENTRY]->set_uniform("object_camera", campos);
	lux::static_cube::draw((GLuint)*renderstate.program[SHADER_RAY_ENTRY]);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glBindImageTexture(0, renderstate.raybuffer, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
	if (renderstate.enable_box) {
		lux::vec4f box_ll(renderstate.box[0].vmin, renderstate.box[1].vmin, renderstate.box[2].vmin, 0);
		lux::vec4f box_ur(renderstate.box[0].vmax, renderstate.box[1].vmax, renderstate.box[2].vmax, 0);
		renderstate.program[SHADER_RAY_EXIT]->set_uniform("box_ll", box_ll);
		renderstate.program[SHADER_RAY_EXIT]->set_uniform("box_ur", box_ur);
	}

	renderstate.program[SHADER_RAY_EXIT]->set_uniform("enable_box", renderstate.enable_box);
	renderstate.program[SHADER_RAY_EXIT]->set_uniform("ModelViewProjection", ModelViewProjection);
	renderstate.program[SHADER_RAY_EXIT]->set_uniform("object_camera", campos);
	glCullFace(GL_FRONT);
	lux::static_cube::draw((GLuint)*renderstate.program[SHADER_RAY_EXIT]);
	*/
	++renderstate.fps_count;
	double dT = renderstate.fps_time.Query();
	if (dT > 1.0) {
		double fps = double(renderstate.fps_count) / dT;
		char ch[512];
		sprintf_s(ch, 512, "%s @ %.2ffps", renderstate.name.c_str(), fps);
		glutSetWindowTitle(ch);
		renderstate.fps_count = 0;
		renderstate.fps_time.Reset();
	}
	if (renderstate.screenshot) {
		uint8_t* buffer = new uint8_t[3 * renderstate.width*renderstate.height];
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glReadBuffer(GL_BACK);
		glReadPixels(0, 0, (GLsizei)renderstate.width, (GLsizei)renderstate.height, GL_RGB, GL_UNSIGNED_BYTE, buffer);
		size_t n = lux::find_file_slot("screenshots\\img%.4i.ppm");
		char ch[256];
		sprintf_s(ch, 256, "screenshots\\img%.4i.ppm", int(n));
		bool result = lux::ppm::write(ch, buffer, renderstate.width, renderstate.height, lux::ppm::UINT8);
		delete[] buffer;
		if (result) printf("screenshot '%s': success\n", ch);
		else printf("screenshot '%s': fail\n", ch);
		renderstate.screenshot = false;
	}
	glutSwapBuffers();
}

void OnCleanup(void) {
	lux::static_cube::clear();
	lux::static_quad::clear();
	for (auto& p : renderstate.program) delete p;
	renderstate.program.clear();
}

int main(int argc, char** argv) {
	atexit(OnCleanup);

	OnStart(renderstate);

	if (renderstate.dpiAware) SetProcessDPIAware();
	glutInit(&argc, argv);
	glutInitWindowSize((int)renderstate.width, (int)renderstate.height);
	glutInitDisplayMode(renderstate.mode);
	renderstate.window = glutCreateWindow(renderstate.name.c_str());

	OnGLInit();

	glutIdleFunc([](void) {glutPostRedisplay(); });
	glutReshapeFunc(OnReshape);
	glutDisplayFunc(OnDisplay);
	glutKeyboardFunc(OnKey);
	glutSpecialFunc(OnSpecialKey);
	glutMouseFunc(OnMouse);
	glutMotionFunc(OnMotion);
	glutMouseWheelFunc(OnMouseWheel);
	glutShowWindow();

	glutMainLoop();
	return 0;
}