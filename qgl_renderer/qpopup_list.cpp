#include <qpopup_list.hpp>


void qpopup_list::update_list(const std::vector< std::string>& l, const std::vector< std::size_t>& idx) {

	clear();
	m_action_index_map.clear();
	for (std::size_t i = 0; i < l.size(); ++i) {
		QAction* a = addAction(l[i].c_str());
		m_action_index_map[l[i].c_str()] = (idx.size()>= l.size() ? idx[i] : i);
		connect(this, SIGNAL(triggered(QAction*)), this, SLOT(change_current(QAction*)));
	}
}


void qpopup_list::update_current(const std::string& cur) {
	m_current_string = cur;
	m_current_index = m_action_index_map[cur.c_str()];
}