#ifndef _HISTOGRAM_GENERATOR_HPP_
#define _HISTOGRAM_GENERATOR_HPP_

#include <compressed_mipvolume.h>

class histogram_generator {

public:
    typedef sparse::vector<uint32_t, lux::errord> sparse_vector_t;

public:
  histogram_generator(compressed_mipvolume* cmip = nullptr);

  void set_compressed_mipvolume(compressed_mipvolume* cmip);

  void init(compressed_mipvolume* cmip, const lux::rangeu32_t& X, const lux::rangeu32_t& Y, const lux::rangeu32_t& Z);

  void aggregate(std::size_t nbins, std::vector<double>& out_hist, bool logscale= false);
  void update_attribute(const std::vector<double>& att); 
  void update_ranges(const lux::rangeu32_t & X, const lux::rangeu32_t& Y, const lux::rangeu32_t& Z);

  const std::size_t id_count() const { return m_id_hist.size();  }
  const double sigma() const { return m_sigma; }
  const double mu() const { return m_mu; }

  const double sum() const { return m_sum; }

protected:

  void compile_node_list(void);
  void compile_topo_ranges(void);
  
  inline void read_node(int n, uint32_t& ch0, uint32_t& ch1, double& idx) {
      ch0 = m_node_list[3 * n];
      ch1 = m_node_list[3 * n + 1];
      idx = double(m_node_list[3 * n + 2]) / double(1 << 20);
  }

  inline double lerp(double a, double b, double w) {
      return a + w * (b - a);
  }

protected:

  compressed_mipvolume* m_pVolume;
  std::vector<lux::rangeu32_t>	m_topo_order;
  std::vector<uint32_t>			m_node_list;

  sparse_vector_t               m_id_hist;

  std::vector<double>   m_leaf_attributes;
  lux::range_t<double>          m_attribute_range;
  double m_mu;
  double m_sigma;
  double m_sum;

};

#endif

