#include <qdraw_area.hpp>

#include <QPainter>
#include <QMouseEvent>

qdraw_area::qdraw_area(render_state* rs, QWidget *parent)
    : QWidget(parent)
{
  setAttribute(Qt::WA_StaticContents);
  render_state_ = rs;
  render_state_->modified = false;
  render_state_->modified = false;
  render_state_->scribbling = false;
  render_state_->pen_width = 1;
  render_state_->pen_color = Qt::blue;
  render_state_->last_point;
  render_state_->last_point;
}

bool qdraw_area::openImage(const QString &fileName)
{
    QPixmap loadedPixmap;
    if (!loadedPixmap.load(fileName)) {
        printf("Cannot load %s \n", fileName.toStdString().c_str());
        return false;
    }
#if 0
    QSize newSize = loadedImage.size().expandedTo(size());
    resizeImage(&loadedImage, newSize);
    render_state_->draw_area  = loadedImage;
#else
    QPainter painter(&render_state_->draw_area);
    painter.drawPixmap(0, 0, width(), height(), loadedPixmap);
#endif
    render_state_->modified = false;
    update();
    return true;
}

bool qdraw_area::saveImage(const QString &fileName, const char *fileFormat)
{
    QImage visibleImage = render_state_->draw_area;
    resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
      render_state_->modified = false;
      return true;
    }
    return false;
}

void qdraw_area::setPenColor(const QColor &newColor)
{
    render_state_->pen_color = newColor;
}

void qdraw_area::setPenWidth(int newWidth)
{
   render_state_->pen_width  = newWidth;
}

void qdraw_area::clearImage()
{
    lux::color c = render_state_->area_color;
    QPainter painter(this);
    //render_state_->draw_area.fill(qRgba(int(255*c[0]),int(255*c[1]),int(255*c[2]), 255));
    //render_state_->modified = true;
    painter.fillRect(rect(), qRgba(int(255 * c[0]), int(255 * c[1]), int(255 * c[2]), 255));
    update();
}


void qdraw_area::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        render_state_->last_point = event->pos();
        render_state_->scribbling = true;
    }
}

void qdraw_area::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && render_state_->scribbling)
        drawLineTo(event->pos());
    if (event->buttons() & Qt::RightButton) {
        // FIXME: add popup lists
    }
}

void qdraw_area::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && render_state_->scribbling) {
        drawLineTo(event->pos());
        render_state_->scribbling = false;
    }
}

void qdraw_area::draw_dirty_area(QPaintEvent* event) {

    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, render_state_->draw_area, dirtyRect);

}


void qdraw_area::paintEvent(QPaintEvent *event)
{
    const attribute_prop& att1 = render_state_->attribute_list[render_state_->current_attribute];
    const attribute_prop& att2 = render_state_->attribute_list[render_state_->current_second_attribute];
    const attribute_prop& attd = render_state_->attribute_list[render_state_->density_attribute_id];
    const attribute_prop& attn = render_state_->attribute_list[render_state_->current_nominal_attribute];

    std::string area_style = render_state_->area_coloring_styles[render_state_->area_coloring];
    std::string scatter_style = render_state_->scatter_coloring_styles[render_state_->scatter_coloring];
    std::string tf_style = render_state_->tf_styles[render_state_->current_style];

   

    if (area_style == "density") {
       // draw_kde();
    } else if (area_style == "uniform") {
        draw_filled_rectangle();
    } else if (area_style == "colormap2d") {   
        draw_color_gradient(att1, 0.5f);
        draw_color_gradient(att2, 0.5f, true);
    } else if (area_style == "colormap1") {
        draw_color_gradient(att1);
    }
    else if (area_style == "colormap2") {
        draw_color_gradient(att2, 1.0f, true);
    }

    draw_legend(att1);
    draw_legend(att2, true);
    //draw_dirty_area(event);
    //draw_text( 0.4f * width(), 0.05f * height(), attn.name);
   
    if (scatter_style != "none") {
        QString text; 
        if (scatter_style == "nominal") {
            text = attn.name.c_str();
        }
        else if (scatter_style == "tf") {
            if (tf_style == "attribute1") {
                text = att1.name.c_str();
            }
            else if (tf_style == "attribute2") {
                text = att2.name.c_str();
            }
            else if (tf_style == "attribute2d") {
                text = att1.name.c_str() + QString(" vs ") + att2.name.c_str();
            }
            else if (tf_style == "density") {
                text = attd.name.c_str();
            }
            else if (tf_style == "msc-interp") {
                text = "msc-interp";
            }
        }
        draw_text(0.4f * width(), 0.05f * height(), text);
        draw_scatter_points();
    }

}

void qdraw_area::draw_filled_rectangle() {
    clearImage();
}


void qdraw_area::draw_color_gradient(const attribute_prop& att, float alpha, bool vertical) {

    QPainter painter(this);
    QLinearGradient lin(0, 0, width(), 0);

    lux::colormap CM(att.color_scheme);

    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

    bool inverse = att.inverse;
    bool periodic = att.periodic;

    for (std::size_t k = 0; k < att.color_knots.size(); ++k) {
        std::size_t i = (periodic ? (k > CM.size() / 2 ? 2 * (CM.size() - 1 - k) : 2 * k) : k);
        i = (inverse ? CM.size() - 1 - i : i);
        lux::color c = CM(i);
        int r = c[0] * 255;
        int g = c[1] * 255;
        int b = c[2] * 255;
        int a = alpha * 255;
        float t = (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
        lin.setColorAt(t, QColor(r, g, b, a));
    }
    painter.setBrush(lin);
    painter.setBrush(lin);
    if (vertical) {
        painter.translate(0.0f, height());
        painter.rotate(-90.0f);
    }
    painter.drawRect(0.0f, 0.0f, width(), height());
    render_state_->modified = true;
    update();
}




void qdraw_area::draw_scatter_points() {

    QPainter painter(this);
    float w = float(width());
    float h = float(height());
    float glyph_size = std::min(w, h) / 200.0f;

    std::vector< QColor > color;
    const attribute_prop& attn = render_state_->attribute_list[render_state_->current_nominal_attribute];

    QImage draw_area;

    std::string scatter_coloring = render_state_->scatter_coloring_styles[render_state_->scatter_coloring];

    if (scatter_coloring == "constant") {
        QColor c;
        std::string style = render_state_->area_coloring_styles[render_state_->area_coloring];
        if (style == "uniform") {
            int r = int(render_state_->scatter_color[0] * 255);
            int g = int(render_state_->scatter_color[1] * 255);
            int b = int(render_state_->scatter_color[2] * 255);
            c  = QColor(r, g, b, 255);
        }
        else {
           c  = Qt::darkGray;
        }
        painter.setPen(QPen(c,int(floor(glyph_size))));
    }
    else if (scatter_coloring == "nominal") {
        lux::colormap CM(attn.color_scheme);
        for (std::size_t n = 0; n < CM.size(); ++n) {
            int r = int(CM[n][0] * 255);
            int g = int(CM[n][1] * 255);
            int b = int(CM[n][2] * 255);
            color.push_back(QColor(r, g, b, 255));
        }
    }
#if 0
    else if (scatter_coloring == "area") {
        draw_area = render_state_->draw_area;
    }
#endif

    const attribute_prop& att1 = render_state_->attribute_list[render_state_->current_attribute];
    const attribute_prop& att2 = render_state_->attribute_list[render_state_->current_second_attribute];

    bool xlog = (att1.scale == "logarithmic");
    float xrange = (xlog ? log(att1.color_knots[att1.color_knots.size() - 1]) - log(att1.color_knots[0]) : att1.color_knots[att1.color_knots.size() - 1] - att1.color_knots[0]);

    bool ylog = (att2.scale == "logarithmic");
    float yrange = (ylog ? log(att2.color_knots[att2.color_knots.size() - 1]) - log(att2.color_knots[0]) : att2.color_knots[att2.color_knots.size() - 1] - att2.color_knots[0]);


    for (std::size_t n = 0; n < render_state_->attribute.size(); ++n) {
        float cx = (xlog ? log(render_state_->attribute[n][att1.name]) - log(att1.knots[0]) : render_state_->attribute[n][att1.name] - att1.knots[0]) / xrange;
        float cy = (1.0f - (ylog ? log(render_state_->attribute[n][att2.name]) - log(att2.knots[0]) : render_state_->attribute[n][att2.name] - att2.knots[0]) / yrange);
        if (scatter_coloring == "nominal") {
            painter.setPen(QPen(color[render_state_->attribute[n][attn.name]], int(floor(glyph_size))));
        }
        else if (scatter_coloring == "tf") {
            lux::color c = render_state_->glData.get_leaf_color(n);
            painter.setPen(QPen(QColor(int(c[0] * 255.0), int(c[1] * 255.0), int(c[2] * 255.0)), floor(glyph_size)));
        }
        painter.drawPoint(w * cx, h * cy);
    }

}



void qdraw_area::resizeEvent(QResizeEvent *event)
{
    if (width() > render_state_->draw_area.width() || height() > render_state_->draw_area.height()) {
        int newWidth = qMax(width() + 128, render_state_->draw_area.width());
        int newHeight = qMax(height() + 128, render_state_->draw_area.height());
        resizeImage(&render_state_->draw_area, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}


void qdraw_area::drawLineTo(const QPoint &endPoint)
{
    QPainter painter(&render_state_->draw_area);
    painter.setPen(QPen(render_state_->pen_color, render_state_->pen_width, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(render_state_->last_point, endPoint);
    render_state_->modified = true;

    int rad = (render_state_->pen_width / 2) + 2;
    update(QRect(render_state_->last_point, endPoint).normalized()
                                     .adjusted(-rad, -rad, +rad, +rad));
    render_state_->last_point = endPoint;
}

void qdraw_area::resizeImage(QImage *image, const QSize &newSize)
{
    if (image->size() == newSize)
        return;

    QImage newImage(newSize, QImage::Format_ARGB32);
    newImage.fill(qRgba(255, 255, 255, 255));
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}

void qdraw_area::draw_kde() {

    // FIXME: Move to render state
#if 0
    if (render_state_->recompute_kde) {
        attribute_prop& attx = render_state_->attribute_list[render_state_->current_attribute];
        attribute_prop& atty = render_state_->attribute_list[render_state_->current_second_attribute];

        std::vector<float> xlim(2);
        xlim[0] = attx.knots[0];
        xlim[1] = attx.knots[attx.knots.size() - 1];

        std::vector<float> ylim(2);
        ylim[0] = atty.knots[0];
        ylim[1] = atty.knots[atty.knots.size() - 1];

        std::vector< std::vector<float> > data;
        for (std::size_t n = 0; n < render_state_->attribute.size(); ++n) {
            std::vector<float> p(2);
            p[0] = render_state_->attribute[n][attx.name];
            p[1] = render_state_->attribute[n][atty.name];
            data.push_back(p);
        }
        std::string fimage = "kde.png";
        render_state_->dimension_reduction.kde(data, xlim, ylim, fimage, "plasma");
        if (!openImage(fimage.c_str())) {
            printf("(EEE): Could not load %s", fimage.c_str());
        };
        render_state_->recompute_kde = false;
    }
#endif
    QPainter painter(this);
    painter.drawImage(rect(), render_state_->draw_area);
    update();
}


void qdraw_area::draw_legend(const attribute_prop& att,  bool vertical) {

    QPainter painter(this);
  
    float w = float(width());
    float h = float(height());
    if (vertical) {
        painter.translate(0.0f,h);
        painter.rotate(-90);
    }

    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
    std::string style = render_state_->area_coloring_styles[render_state_->area_coloring];
    if (style == "uniform") {
        pen.setColor(Qt::lightGray);
    }
    else {
        pen.setColor(Qt::darkGray);
    }
    painter.setPen(pen);
    painter.setFont(QFont("Helvetica", 8));

    float y_leg = (vertical ? 0.03f * h : 0.99f * h);

    painter.drawText(0.05f * w, y_leg, QString().setNum(att.color_knots[0], 'g', 2));

    for (std::size_t k = 1; k < 4; ++k) {
        float t = w * float(k) / float(4);
        QPoint p1(t, 0.0f);
        QPoint p2(t, h);
        painter.drawLine(p1, p2);
        float val = (logarithmic ? att.color_knots[0] * exp(float(k) / float(4) * range) : att.color_knots[0] + float(k) / float(4) * range);
        painter.drawText(t, y_leg, QString().setNum(val, 'g', 2));
    }
    float t = 0.9f * w;
    painter.drawText(t, y_leg, QString().setNum(att.color_knots[att.color_knots.size() - 1], 'g', 2));

    float y_name = (vertical ? 0.1f * h : 0.97f * h);
    painter.setFont(QFont("Helvetica", 14));
    painter.drawText(0.4f * w, y_name, att.name.c_str());
}


void qdraw_area::draw_text(float x, float y, const QString& text) {
    QPainter painter(this);
    QPen pen;
    std::string style = render_state_->area_coloring_styles[render_state_->area_coloring];
    if (style == "uniform") {
        pen.setColor(Qt::lightGray);
    }
    else {
        pen.setColor(Qt::darkGray);
    }
    painter.setPen(pen);
    painter.setFont(QFont("Helvetica", 14));
    painter.drawText(x, y, text);
}
