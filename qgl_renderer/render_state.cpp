#include <render_state.hpp>

#include <QtCore\qfile.h>
#include <random>

#include <json.hpp>

#include <QJsonDocument>
#include <QJsonArray>
#include "tsne.h"

#include "python_wrapper.hpp"
#include <set>

void render_state::init_settings() {

    width = 1600;
    height = 1200;
    name = "Mixture graph";
    fps_count = 0.0f;
    swap_int = 0;
    dpiAware = true;
    sel_program = 0;
    raybuffer = 0;
    screenshot = false;

    format.setSamples(16);
    format.setSwapInterval(0);

    for (int i = 0; i < 3; i++) {
        box[i].vmin = -0.5f;
        box[i].vmax = 0.5f;
    }

    scale = lux::vec3f(1.0f, 1.0f, 1.0f);

    enable_box = false;
    benchmark = false;

    animation_loop = false;

    animation_duration.clear();
    animation_duration.push_back(10.0);
    animation_duration.push_back(20.0);
    animation_duration.push_back(30.0);
    animation_duration.push_back(40.0);
    animation_duration.push_back(50.0);
    animation_duration.push_back(60.0);

    current_animation_duration = 0;

    overlay_text = false;


    lao_directions.clear();
    lao_directions.push_back(0);
    lao_directions.push_back(1);
    lao_directions.push_back(2);
    lao_directions.push_back(3);
    current_lao_directions = 0;

    alpha_constant = true;
    alpha.resize(2);
    alpha[0] = 0.0f;
    alpha[1] = 0.03f;


    lod_bias = -2.4f;
    shading_const = lux::vec3f(1.0f, 1.0f, 1.0f);

    roi_enabled = false;
    roi_dragging = false;
    roi_clipping = false;


    roi_half_edge = 0.25f;
    roi_center = lux::vec3f(0.0f, 0.0f, 0.0f);

    scatter_coloring_styles.push_back("none");
    scatter_coloring_styles.push_back("constant");
    scatter_coloring_styles.push_back("nominal");
    scatter_coloring_styles.push_back("tf");
    scatter_coloring = 0;

    area_coloring_styles.push_back("uniform");
    area_coloring_styles.push_back("colormap1");
    area_coloring_styles.push_back("colormap2");
    area_coloring_styles.push_back("colormap2d");
    area_coloring_styles.push_back("density");
    area_coloring_styles.push_back("msc-interp");
    area_coloring_styles.push_back("partition");

    area_coloring = 3;

    tf_styles.push_back("attribute1");
    tf_styles.push_back("attribute2");
    tf_styles.push_back("attribute2d");
    tf_styles.push_back("density");
    tf_styles.push_back("msc-interp");
    tf_styles.push_back("partition");

    current_style = 1;


    scatter_color = lux::colormap::get_color("blues9", 2);
    area_color = lux::colormap::get_color("greys9", 7);
    density_background_color = lux::colormap::get_color("blues9", 1);

    draw_image = "kde-msc.png";

    recompute_density = false;
    packing = true;

    density_attribute_id = -1;
    msc_sigma_attribute_id = -1;
    partition_attribute_id = -1;
    peak_attribute_id = -1;
    clusters_id = -1;
}

void render_state::init_color_schemes() {

    color_schemes.clear();
    color_schemes.push_back("YlOrRd9");
    color_schemes.push_back("BuPu9");
    color_schemes.push_back("BuGn9");
    color_schemes.push_back("brbg9");
    color_schemes.push_back("pastel1_9");
    color_schemes.push_back("reds9");
    color_schemes.push_back("greens9");
    color_schemes.push_back("greys9");
    color_schemes.push_back("gnbu9");
    color_schemes.push_back("orrd9");
    color_schemes.push_back("rdylbu9");
    color_schemes.push_back("oranges9");
    color_schemes.push_back("paired9");
    color_schemes.push_back("piyg9");
    color_schemes.push_back("prgn9");
    color_schemes.push_back("pubu9");
    color_schemes.push_back("pubugn9");
    color_schemes.push_back("puor9");
    color_schemes.push_back("purd9");
    color_schemes.push_back("purples9");
    color_schemes.push_back("rdbu9");
    color_schemes.push_back("rdgy9");
    color_schemes.push_back("rdpu9");
    color_schemes.push_back("rdylbu9");
    color_schemes.push_back("rdylgn9");
    color_schemes.push_back("set1_9");
    color_schemes.push_back("set3_9");
    color_schemes.push_back("spectral9");
    color_schemes.push_back("ylgn9");
    color_schemes.push_back("ylgnbu9");
    color_schemes.push_back("ylorbr9");
    color_schemes.push_back("graph1_9");

    current_color_scheme = 0;

    background = lux::colormap::get_color("Blues9", 8);
    box_color = lux::colormap::get_color("ylorrd9", 0);
    box_color(3) = 0.1f;
}

void render_state::init_attributes() {


    attribute_list.clear();

    attribute_prop anom;
    anom.name = "id";
    anom.type = "nominal";
    anom.color_scheme = "pastel1_9";
    anom.color_knots.resize(9);
    anom.knots.resize(9);
    anom.alpha.resize(9);
    std::fill(anom.alpha.begin(), anom.alpha.end(), 0.1f);
    attribute_list.push_back(anom);

    attribute_prop a;
    a.color_scheme = "brbg9";
    attribute_list.push_back(a);
    a.name = "length";
    a.type = "scalar";
    a.color_scheme = "bupu9";
    
    attribute_list.push_back(a);

    a.name = "diameter";
    a.type = "scalar";
    a.color_scheme = "pubu9";
    a.color_knots.resize(9);
    attribute_list.push_back(a);
    a.name = "nCCs";
    a.type = "scalar";
    a.color_scheme = "paired9";
    a.color_knots.resize(9);
    a.alpha[0] = 0.0f;
    a.alpha[1] = 1.0f;
    attribute_list.push_back(a);
    a.name = "dominantCC";
    a.type = "scalar";
    a.color_scheme = "spectral9";
    a.alpha[0] = 0.0f;
    a.alpha[1] = 1.0f;
    a.color_knots.resize(9);
    attribute_list.push_back(a);
    a.name = "volume";
    a.type = "scalar";
    a.scale = "logarithmic";
    a.color_scheme = "bugn9";
    a.color_knots.resize(9);
    attribute_list.push_back(a);

    a.name = "orientation_phi";
    a.type = "scalar";
    a.scale = "linear";
    a.color_scheme = "Greens9";
    a.color_knots.resize(9);
    a.color_knots[0] = -1.0f;
    a.color_knots[8] = 1.0f;
    a.alpha[0] = 0.0f;
    a.alpha[1] = 1.0f;
    a.knots[0] = -1.0f;
    a.knots[1] = 1.0f;
    a.periodic = true;
    attribute_list.push_back(a);

    a.name = "orientation_theta";
    a.type = "scalar";
    a.scale = "linear";
    a.color_scheme = "Blues9";
    a.color_knots.resize(9);
    a.color_knots[0] = -1.0f;
    a.color_knots[8] = 1.0f;
    a.alpha[0] = 0.0f;
    a.alpha[1] = 1.0f;
    a.knots[0] = -1.0f;
    a.knots[1] = 1.0f;
    a.periodic = true;
    attribute_list.push_back(a);

    current_attribute = 1;
    current_attribute_index = 0;
    current_second_attribute = 2;
    enable_second_attribute = false;

    current_nominal_attribute = 0;

}


void render_state::init_help() {

    help_text.clear();
    help_text.push_back("------------------------- HELP --------------------------");
    help_text.push_back("  Key_H: toggle render help");
    help_text.push_back("  Left: rotate scene");
    help_text.push_back("  Wheel: zoom in/out");
    help_text.push_back("  Right: change light direction");
    help_text.push_back("  CTRL + mouse: translate ROI");
    help_text.push_back("  Key_2: toggle 1D/2D transfer function");
    help_text.push_back("  Key_T: change animation duration");
    help_text.push_back("  Key_I: toggle render info");
    help_text.push_back("  Key_S: change color scheme (+SHIFT  for second attribute)");
    help_text.push_back("  Key_A: change data attribute (+SHIFT  for second attribute)");
    help_text.push_back("  Key_J: toggle increasing/decrasing color scale (+SHIFT  for second attribute)");
    help_text.push_back("  Key_K: toggle periodic color scale (+SHIFT  for second attribute)");
    help_text.push_back("  Key_L: toggle scale (logarithmic or linear) (+SHIFT  for second attribute)");
    help_text.push_back("  Key_Less: decrease ROI half length of 0.05");
    help_text.push_back("  Key_Greater: increase ROI half length of 0.05");
    help_text.push_back("  Key_R: toggle animation loop (start/stop)");
    help_text.push_back("  Key_Home: reset camera");
    help_text.push_back("  Key_C: reset animation path (+SHIFT for ROI clipping)");
    help_text.push_back("  Key_P: add current key-frame to animation path");
    help_text.push_back("  Key_Left: camera pan left");
    help_text.push_back("  Key_Right: camera pan right");;
    help_text.push_back("  Key_Up: camera pan up");
    help_text.push_back("  Key_Down: camera pan down");
    help_text.push_back("  Key_F11: take screenshot as data/img*.ppm");
    help_text.push_back("  Key_F1: toggle box rendering");
    help_text.push_back("  Key_O: toggle local ambient occlusion");
    help_text.push_back("  Key_N: toggle diffuse shading");
    help_text.push_back("  Key_F2: print current camera");
    help_text.push_back("  Key_Plus: increase lod bias");
    help_text.push_back("  Key_Minus: decrease lod bias");
    help_text.push_back("  Key_Escape: exit");
    help_text.push_back("  Key_W: write json data file");
    help_text.push_back("  Key_F: load json data file");
    help_text.push_back("  Key_Q: write config json file");
    help_text.push_back("---------------------------------------------------------");

    render_help = false;

}


void render_state::init_data(const std::string& json_file) {

    data_list.clear();
    data_list.push_back("vis22.cmip");
    data_list.push_back("mouse1.cmip"); 
    data_list.push_back("Hippocampus.cmip");    
    data_list.push_back("Heinzl.cmip"); 

    data_list.push_back("ok.cmip"); 
    data_list.push_back("mouse4.cmip");     
    data_list.push_back("mouse3.cmip");  
  
    data_list.push_back("Neocortex_sub.cmip");
 
    data_list.push_back("CorradoHiQ.cmip");
 
    current_data_file = 0;

    if (json_file.size()) {
        json_data_file = json_file;
        read_data_json(json_data_file);
    }
    else {
        normal_data_file = "vis22.nvol8";
        json_attribute_file = "vis22-test_object.json";

        if (json_attribute_file.size()) {
            read_attribute_json(json_attribute_file);
            create_dr_data();
        }
    }
}

void render_state::init(const std::string& json_file) {

    init_settings();
    init_color_schemes();
    init_attributes();
    init_help();
    init_data(json_file);

}


bool render_state::read_attribute_json(const std::string& json_file) {

    bool success = true;

    QJsonObject json;
    success = json::parse_file(json, json_file);

    if (!success) return success;

    int nsegm;
    success = json::read_value(json, "nSegments", nsegm);

    std::vector<float> v;
    success = json::read_vector(json, "scale", v);
    scale[0] = v[0];
    scale[1] = v[1];
    scale[2] = v[2];

    attribute_list.clear();

    attribute_prop anom;
    anom.name = "id";
    anom.type = "nominal";
    anom.color_scheme = color_schemes[rand() % color_schemes.size()];
    lux::colormap CM(anom.color_scheme);
    anom.color_knots.resize(CM.size());
    anom.knots.resize(CM.size());
    anom.alpha.resize(CM.size());
    for (std::size_t c = 0; c < anom.color_knots.size(); ++c) {
        anom.color_knots[c] =  c;
        anom.knots[c] = c;
        anom.alpha[c] = 0.2f;
    }

    attribute_list.push_back(anom);
 
    for (QJsonObject::const_iterator it = json.begin(); it != json.end(); ++it) {

        std::string name = it.key().toLatin1();
        success = json[name.c_str()].isArray();
        if (!success) {
            printf("Wrong array for attribute %s: number of segments %d \n", name.c_str(), json[name.c_str()].toArray().size());
            continue;
        }
        success = (json[name.c_str()].toArray().size() == nsegm);
        if (!success) {
            printf("Wrong number of values for attribute %s:  %d vs %d \n", name.c_str(), json[name.c_str()].toArray().size(), nsegm);
           continue;
        }

        if (json[name.c_str()].toArray().at(0).isDouble()) {
            std::vector<float> val;
            success = json::read_vector(json, name.c_str(), val);
            if (success) {

                attribute_prop att;
                att.name = name;
                att.color_scheme = color_schemes[rand() % color_schemes.size()];

                double vmin = *std::min_element(val.begin(), val.end());
                double vmax = *std::max_element(val.begin(), val.end());

                printf(" Attribute %s minmax: %.2f %.2f \n", att.name.c_str(), vmin, vmax);
                lux::colormap CM(att.color_scheme);

                if ((vmin - long(vmin)) == 0.0f
                    && (vmax - long(vmax)) == 0.0f
                    && std::size_t(vmax - vmin) <= CM.size()
                    && (val[val.size() / 2] - long(val[val.size() / 2])) == 0.0f) {
                    att.type = "nominal";
                }
                else {
                    att.type = "scalar";
                }

                if (att.name == "id") att.type = "nominal";;


                if (att.type == "nominal") {
                    std::size_t nelems = std::min(1 + std::size_t(vmax) - std::size_t(vmin), CM.size());
                    att.color_knots.resize(nelems);
                    att.knots.resize(nelems);
                    att.alpha.resize(nelems);
                    for (std::size_t i = 0; i < nelems; ++i) {
                        att.color_knots[i] = vmin + float(i);
                        att.knots[i] = vmin + float(i);
                        att.alpha[i] = 0.2f;
                    }
                }
                else {

                    att.knots[0] = vmin;
                    att.knots[att.knots.size() - 1] = vmax;

                    att.alpha[0] = 0.0f;
                    att.alpha[1] = 0.99f;

                    bool logarithmic = (vmin > 0.0f && vmax / vmin > 1.0e04);

                    if (logarithmic) att.scale = "logarithmic";


                    att.color_knots.resize(CM.size());
                    for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                        if (logarithmic) {
                            att.color_knots[c] = vmin * exp(float(c) / float(att.color_knots.size() - 1) * (log(vmax + 1.0f) - log(vmin + 1.0f)));
                        }
                        else {
                            att.color_knots[c] = vmin + float(c) / float(att.color_knots.size() - 1) * (vmax - vmin);
                        }
                    }

                }

                attribute_list.push_back(att);

                for (std::size_t j = 0; j < val.size(); ++j) {
                    if (attribute.find(j) != attribute.end()) {
                        attribute[j].insert(std::make_pair(att.name, val[j]));
                    }
                    else {
                        std::map< std::string, float> att_map;
                        att_map["id"] = j;
                        att_map["active"] = 1.0f;
                        att_map.insert(std::make_pair(att.name, val[j]));
                        attribute.insert(std::make_pair(j, att_map));
                    }
                }
            }
        }
        else if (name == "orientation" && json[name.c_str()].toArray().at(0).isArray()) {

            std::vector< std::vector<float> > val;

            QJsonArray arr = json[name.c_str()].toArray();

            success = (arr.size() == nsegm);

            if (success) {
                attribute_prop att;
                att.type = "scalar";
#if 0
                att.name = name + "_max";
                att.color_scheme = "ylorrd9";
                lux::colormap CM(att.color_scheme);
                att.color_knots.resize(CM.size());
                for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                    att.color_knots[c] = float(c) / float(att.color_knots.size() - 1);
                }
                att.knots[0] = 0.0f;
                att.knots[1] = 1.0f;
                att.alpha[0] = 0.0f;
                att.alpha[1] = 0.99f;

                attribute_list.push_back(att);

                att.name = name + "_phi";
                att.color_scheme = "bupu9";
                att.periodic = true;
                for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                    att.color_knots[c] = M_PI * float(c) / float(att.color_knots.size() - 1);
                }
                att.knots[0] = 0.0f;
                att.knots[1] = M_PI;
                att.alpha[0] = 0.0f;
                att.alpha[1] = 0.99f;

                attribute_list.push_back(att);

                att.name = name + "_theta";
                att.color_scheme = "bugn9";
                att.periodic = true;
                for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                    att.color_knots[c] = 0.5f * M_PI *  float(c) / float(att.color_knots.size() - 1);
                }
                att.knots[0] = 0.0f * M_PI;
                att.knots[1] = 0.5f * M_PI;
                att.alpha[0] = 0.0f;
                att.alpha[1] = 0.99f;
                attribute_list.push_back(att);
#else
                att.name = name + "_x";
                att.color_scheme = "ylorrd9";
                lux::colormap CM(att.color_scheme);
                att.color_knots.resize(CM.size());
                for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                    att.color_knots[c] = -1.0f + 2.0f * float(c) / float(att.color_knots.size() - 1);
                }
                att.knots[0] = -1.0f;
                att.knots[1] = 1.0f;
                att.alpha[0] = 0.0f;
                att.alpha[1] = 0.99f;

                attribute_list.push_back(att);

                att.name = name + "_y";
                att.color_scheme = "bupu9";
                att.periodic = true;
                att.color_knots.resize(CM.size());
                for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                    att.color_knots[c] = -1.0f + 2.0f * float(c) / float(att.color_knots.size() - 1);
                }
                att.knots[0] = -1.0f;
                att.knots[1] = 1.0f;
                att.alpha[0] = 0.0f;
                att.alpha[1] = 0.99f;

                attribute_list.push_back(att);

                att.name = name + "_z";
                att.color_scheme = "bugn9";
                att.periodic = true;
                for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
                    att.color_knots[c] = -1.0f + 2.0f *  float(c) / float(att.color_knots.size() - 1);
                }
                att.knots[0] = -1.0f;
                att.knots[1] = 1.0f;
                att.alpha[0] = 0.0f;
                att.alpha[1] = 0.99f;
                attribute_list.push_back(att);


#endif

                for (std::size_t i = 0; i < arr.size(); ++i) {
                    QJsonArray arr_i = arr.at(i).toArray();
                    std::vector<float> v;
                    for (std::size_t j = 0; j < arr_i.size(); ++j) {
                        v.push_back(float(arr_i.at(j).toDouble()));
                    }
                    val.push_back(v);
                }

#if 0
                for (std::size_t j = 0; j < val.size(); ++j) {
                    std::vector<float> n = val[j];
                    if (n[2] < 0.0f) {
                        n[0] *= -1.0f;
                        n[1] *= -1.0f;
                        n[2] *= -1.0f;
                    }
                    float phi = atan2f(n[1],n[0]);
                    if (phi < 0.0f) phi = (M_PI - phi);
                    float theta = atan2f(sqrt(n[0]*n[0] +n[1] * n[1]), n[2]);
                    if (theta < 0.0f) theta = (M_PI/2.0f - theta);

                    float omax = 0.0f;
                    for (std::size_t k = 0; k < val[j].size(); ++k) {
                        omax = std::max(omax, abs(val[j][k]));
                    }
                    if (attribute.find(j) != attribute.end()) {
                        attribute[j].insert(std::make_pair(name + "_max", omax));
                        attribute[j].insert(std::make_pair(name + "_phi", phi));
                        attribute[j].insert(std::make_pair(name + "_theta", theta));
                    }
                    else {
                        std::map< std::string, float> att_map;
                        att_map["id"] = j;
                        att_map["active"] = 1.0f;
                        att_map[name + "_max"] = omax;
                        att_map[name + "_phi"] = phi;
                        att_map[name + "_theta"] = theta;
                        attribute.insert(std::make_pair(j, att_map));
                    }
                }
#else
                for (std::size_t j = 0; j < val.size(); ++j) {
                    std::vector<float> n = val[j];
                    if (attribute.find(j) != attribute.end()) {
                        attribute[j].insert(std::make_pair(name + "_x", n[0]));
                        attribute[j].insert(std::make_pair(name + "_y", n[1]));
                        attribute[j].insert(std::make_pair(name + "_z", n[2]));
                    }
                    else {
                        std::map< std::string, float> att_map;
                        att_map["id"] = j;
                        att_map["active"] = 1.0f;
                        att_map[name + "_x"] = n[0];
                        att_map[name + "_y"] = n[1];
                        att_map[name + "_z"] = n[2];
                        attribute.insert(std::make_pair(j, att_map));
                    }
                }

#endif
            }
        }
    }
    
#if 0
    for (std::size_t n = 0; n < attribute.size(); ++n) {
        for (std::unordered_map< std::string, float>::const_iterator it = attribute[n].begin(); it != attribute[n].end(); ++it)
            printf("id: %d  attribute: %s value: %f \n", n, it->first.c_str(), it->second);
    }
#endif

    printf(" Filled attribute list  \n");
    return success;
}

bool render_state::read_config_json(const std::string& json_file) {

    QJsonObject json;
    bool success = json::parse_file(json, json_file);

    if (!success) return success;

    QString key = QString("width");
    int w;
    success = json::read_value(json, key, w);
    if (success) width = w;
    
    key = QString("height");
    int h;
    success = json::read_value(json, key, h);
    if (success) height = h;
    
    key = QString("lod_bias");
    double l;
    success = json::read_value(json, key, l);
    if (success) lod_bias = l;

    key = QString("shading_const");
    std::vector<float> k;
    success = json::read_vector(json, key, k);
    if (success) shading_const = lux::vec3f(k[0], k[1], k[2]);

    key = QString("current_lao_directions");
    int cld;
    success = json::read_value(json, key, cld);
    if (success) current_lao_directions = cld;

    key = QString("enable_shading");
    bool sh;
    success = json::read_value(json, key, sh);
    if (success) glData.enable_shading()  = sh;

    key = QString("roi_enabled");
    bool re;
    success = json::read_value(json, key, re);
    if (success) roi_enabled = re;

    key = QString("roi_half_edge");
    double rhe;
    success = json::read_value(json, key, rhe);
    if (success) roi_half_edge = rhe;

    key = QString("overlay_text");
    bool ot;
    success = json::read_value(json, key, ot);
    if (success) overlay_text = ot;

    key = QString("current_animation_duration");
    int cad;
    success = json::read_value(json, key, cad);
    if (success) current_animation_duration = cad;

    key = QString("format");
    success = json.contains(key);
    if (success) {
        int samples;
        success = json::read_value(json[key].toObject(), "samples", samples);
        if (success) format.setSamples(samples);
        int swap_interval;
        success = json::read_value(json[key].toObject(), "swap_interval", swap_interval);
        if (success) format.setSwapInterval(swap_interval);
    }

    key = QString("name");
    success = json::read_value(json, key, name);

    key = QString("background");
    std::vector<float> bg;
    success = json::read_vector(json, key, bg);
    if (success) background = lux::color(bg[0], bg[1], bg[2]);

    key = QString("box_color");
    std::vector<float> bc;
    success = json::read_vector(json, key, bc);
    if (success) box_color = lux::color(bc[0], bc[1], bc[2]);

    key = QString("arcball");
    std::vector<float> q;
    success = json::read_vector(json, key, q);
    if (success) arcball.setOrientation(lux::quat4f(q[0], q[1], q[2], q[3]));

    key = QString("light");
    success = json::read_vector(json, key, q);
    if (success) light_arcball.setOrientation(lux::quat4f(q[0], q[1], q[2], q[3]));

    key = QString("camera");
    success = json.contains(key);
    if (success) {
        double fov;
        success = json::read_value(json[key].toObject(), "fov", fov);
        if (success) camera.setFOV(fov);
        double n;
        success = json::read_value(json[key].toObject(), "near", n);
        if (success) camera.setNear(n);
        double f;
        success = json::read_value(json[key].toObject(), "far", f);
        if (success) camera.setFar(f);

        std::vector<float> v;
        success = json::read_vector(json[key].toObject(), "eye", v);
        if (success) camera.setEye(lux::vec4f(v[0], v[1], v[2], v[3]));
        success = json::read_vector(json[key].toObject(), "center", v);
        if (success) camera.setCenter(lux::vec4f(v[0], v[1], v[2], v[3]));
        success = json::read_vector(json[key].toObject(), "up", v);
        if (success) camera.setUp(lux::vec4f(v[0], v[1], v[2], v[3]));
    }

    key = QString("animation");
    success = json.contains(key);
    if (success) {
        std::vector<QJsonObject> o;
        success = json::read_vector(json, key, o);
        if (success) {
            animation_path.clear();
            target_path.clear();
            roi_path.clear();
            for (std::size_t i = 0; i < o.size(); ++i) {
                std::vector<float> e, a, c, l, rc;
                success = json::read_vector(o[i], "eye", e);
                success = json::read_vector(o[i], "arcball", a);
                success = json::read_vector(o[i], "center", c);
                success = json::read_vector(o[i], "light", l);
                success = json::read_vector(o[i], "roi-center", rc);


                if (success) {
                    lux::sample_t<float> s;
                    // HACK for performing linear interpolation of positions
                    s.curvature = 0.0f;

                    s.position = lux::vec4f(e[0], e[1], e[2], e[3]);
                    s.orientation = lux::vec4f(a[0], a[1], a[2], a[3]);
                    animation_path.push_back(s);

                    s.position = lux::vec4f(c[0], c[1], c[2], c[3]);
                    s.orientation = lux::vec4f(l[0], l[1], l[2], l[3]);
                    target_path.push_back(s);

                    s.position = lux::vec4f(rc[0], rc[1], rc[2], rc[3]);
                    roi_path.push_back(s);
                }
            }
        }
    }
    printf("Imported config file %s\n", json_file.c_str());
}


bool render_state::write_config_json(const std::string& json_file) {

    QFile save_file(json_file.c_str());

    if (!save_file.open(QIODevice::WriteOnly)) {
        printf("Couldn't open save file %s \n", json_file.c_str());
        return false;
    }

    QJsonObject config_obj;
    json::write_value(config_obj, "width", int(width));
    json::write_value(config_obj, "height", int(height));
    json::write_value(config_obj, "name", name);
    json::write_value(config_obj, "lod_bias", lod_bias);
    std::vector<float> k(3);
    k[0] = shading_const[0];
    k[1] = shading_const[1];
    k[2] = shading_const[2];
    json::write_vector(config_obj, "shading_const",k);

    k[0] = background[0];
    k[1] = background[1];
    k[2] = background[2];
    json::write_vector(config_obj, "background", k);

    k[0] = box_color[0];
    k[1] = box_color[1];
    k[2] = box_color[2];
    json::write_vector(config_obj, "box_color", k);

    json::write_value(config_obj, "current_lao_directions", int(current_lao_directions));
    json::write_value(config_obj, "enable_shading", glData.enable_shading());
    json::write_value(config_obj, "roi_enabled", roi_enabled);
    json::write_value(config_obj, "roi_half_edge", roi_half_edge);
    json::write_value(config_obj, "overlay_text", overlay_text);
    json::write_value(config_obj, "current_animation_duration", int(current_animation_duration));


    QJsonObject fmt_obj;
    json::write_value(fmt_obj, "samples", format.samples());
    json::write_value(fmt_obj, "swap_interval", format.swapInterval());
    config_obj.insert("format", fmt_obj);

    std::vector<float> q(4);
    q[0] = arcball.getOrientation()[0];
    q[1] = arcball.getOrientation()[1];
    q[2] = arcball.getOrientation()[2];
    q[3] = arcball.getOrientation()[3];
    json::write_vector(config_obj, "arcball", q);
    q[0] = light_arcball.getOrientation()[0];
    q[1] = light_arcball.getOrientation()[1];
    q[2] = light_arcball.getOrientation()[2];
    q[3] = light_arcball.getOrientation()[3];
    json::write_vector(config_obj, "light", q);

    QJsonObject cam_obj;
    json::write_value(cam_obj, "fov", camera.getFOV());
    json::write_value(cam_obj, "near", camera.getNear());
    json::write_value(cam_obj, "far", camera.getFar());
    q[0] = camera.getEye()[0];
    q[1] = camera.getEye()[1];
    q[2] = camera.getEye()[2];
    q[3] = camera.getEye()[3];
    json::write_vector(cam_obj, "eye", q);
    q[0] = camera.getCenter()[0];
    q[1] = camera.getCenter()[1];
    q[2] = camera.getCenter()[2];
    q[3] = camera.getCenter()[3];
    json::write_vector(cam_obj, "center", q);
    q[0] = camera.getUp()[0];
    q[1] = camera.getUp()[1];
    q[2] = camera.getUp()[2];
    q[3] = camera.getUp()[3];
    json::write_vector(cam_obj, "up", q);
    config_obj.insert("camera", cam_obj);

    QJsonArray anim_array;
    for (std::size_t i = 0; i < animation_path.size(); ++i) {
        QJsonObject sample_obj;
        lux::sample_t<float> s;
        s = animation_path.get_sample(i);
        q[0] = s.position[0];
        q[1] = s.position[1];
        q[2] = s.position[2];
        q[3] = s.position[3];
        json::write_vector(sample_obj, "eye", q);
        q[0] = s.orientation[0];
        q[1] = s.orientation[1];
        q[2] = s.orientation[2];
        q[3] = s.orientation[3];
        json::write_vector(sample_obj, "arcball", q);
        s = target_path.get_sample(i);
        q[0] = s.position[0];
        q[1] = s.position[1];
        q[2] = s.position[2];
        q[3] = s.position[3];
        json::write_vector(sample_obj, "center", q);
        q[0] = s.orientation[0];
        q[1] = s.orientation[1];
        q[2] = s.orientation[2];
        q[3] = s.orientation[3];
        json::write_vector(sample_obj, "light", q);
        s = roi_path.get_sample(i);
        q[0] = s.position[0];
        q[1] = s.position[1];
        q[2] = s.position[2];
        q[3] = s.position[3];
        json::write_vector(sample_obj, "roi-center", q);
        anim_array.append(sample_obj);
    }
    config_obj.insert("animation", anim_array);

    QJsonDocument save_doc(config_obj);
    save_file.write(save_doc.toJson());
    printf("Saved config to file %s \n", json_file.c_str());
    return true;

}

bool render_state::read_data_json(const std::string& json_file) {

    std::mt19937 gen(1);
    std::uniform_real_distribution<float> D(0.0f, 1.0f);

    QJsonObject json;
    bool success = json::parse_file(json, json_file);
    if (!success) return success;


    // Reading bounding box
    QString key = QString("bounding_box");
    success = json.contains(key);
    if ( success ) {
        success = json::read_range(json[key].toObject(), "x", box[0]);
        success = json::read_range(json[key].toObject(), "y", box[1]);
        success = json::read_range(json[key].toObject(), "z", box[2]);
    }
    if (!success) {
        printf(" Warning: problems with key %s\n", key.toLatin1().data());
    }
    
    key = QString("enable_packing");
    bool p;
    success = json::read_value(json, key, p);
    if (success) packing = p;

    key = QString("cmip_file");
    std::string file;
    success = json::read_value(json, key, file);
    if (success) {
        data_list.clear();
        data_list.push_back(file);
        current_data_file = 0;
    }

    key = QString("normal_file");
    success = json::read_value(json, key, file);
    if (success) {
        normal_data_file = file;
    }

    int a;
    key = QString("current_attribute");
    success = json::read_value(json, key, a);
    if (success) {
        current_attribute = a;
    }

    key = QString("current_second_attribute");
    success = json::read_value(json, key, a);
    if (success) {
        current_second_attribute = a;
    }


    key = QString("scale");
    std::vector<float> v;
    success = json::read_vector(json, key, v);
    scale[0] = v[0];
    scale[1] = v[1];
    scale[2] = v[2];

     
    key = QString("attribute_list");
    success = ( json.contains(key) && json[key].isArray());
    if (success) {
        attribute_list.clear();
        QJsonArray attr_array = json[key].toArray();
        for (std::size_t i = 0; i < attr_array.size(); ++i) {
            attribute_prop a;
            QJsonObject attr_json = attr_array.at(i).toObject();
            success = json::read_value(attr_json,"name", a.name );
            success = json::read_value(attr_json, "type", a.type);
            success = json::read_value(attr_json, "scale", a.scale);
            success = json::read_value(attr_json, "periodic", a.periodic);
            success = json::read_value(attr_json, "inverse", a.inverse);
            success = json::read_value(attr_json, "color_scheme", a.color_scheme);
            success = json::read_vector(attr_json, "color_knots", a.color_knots);
            success = json::read_vector(attr_json, "knots", a.knots);
            success = json::read_vector(attr_json, "alpha", a.alpha);

            attribute_list.push_back(a);
        }
        current_attribute = 0;
    }
    if (!success) {
        printf(" Warning: problems with key %s\n", key.toLatin1().data());
    }

    key = QString("segment_list");
    success = (json.contains(key) && json[key].isArray());
    if (success) {
        attribute.clear();
        QJsonArray seg_array = json[key].toArray();
        for (std::size_t i = 0; i < seg_array.size(); ++i) {
            QJsonObject seg = seg_array.at(i).toObject();
            std::map< std::string, float>  att_value;

            int value = i;
            success = json::read_value(seg_array.at(i).toObject(), "value", value);
            att_value["id"] = float(i);
            double val;
            success = json::read_value(seg, "id", val);
            if (success) {
                att_value["id"] = float(val);
            }
            success = json::read_value(seg_array.at(i).toObject(), "active", val);
            att_value["active"] = 1.0f;
            if (success) {
                att_value["active"] = float(val);
            }
            for (std::size_t a = 0; a < attribute_list.size(); ++a) {
                QString attname = QString(attribute_list[a].name.c_str());
                if (attribute_list[a].type != "vector") {
                    success = json::read_value(seg_array.at(i).toObject(), attname, val);
                    att_value[attribute_list[a].name] = (success ? val : D(gen));
                }
                else {
                    int curidx = 0;
                    while (success) {
                        success = json::read_value(seg_array.at(i).toObject(), attname + QString().setNum(curidx), val);
                        if (success) {
                            att_value[attribute_list[a].name + std::to_string(curidx)] = val;
                            curidx++;
                        }
                    }
                }
            }
            attribute[value] = att_value;
        }
    }

    printf("Loaded data from %s \n", json_file.c_str() );
    
    return success;

}

void render_state::create_dr_data() {

#if 0
    const att_map_t& dummy = attribute[0];
    if (dummy.find("tsne0") == dummy.end()) {
        printf("Computing t-SNE \n");
        compute_tsne();
    }
    if (dummy.find("pca0") == dummy.end()) {
        printf("Computing PCA \n");
        compute_pca();
    }
#endif

    const att_map_t& dummy = attribute[0];
    if (dummy.find("umap0") == dummy.end()) {
        printf("Computing UMAP \n");
        compute_umap();
    }

}


bool render_state::write_data_json(const std::string& json_file) {

    QFile save_file( json_file.c_str());

    if (!save_file.open(QIODevice::WriteOnly)) {
        printf("Couldn't open save file %s \n", json_file.c_str());
        return false;
    }

    QJsonObject data_obj;

    QJsonObject bb_obj;
    json::write_range(bb_obj, "x", box[0]);
    json::write_range(bb_obj, "y", box[1]);
    json::write_range(bb_obj, "z", box[2]);
    data_obj.insert("bounding_box", bb_obj);

    json::write_value(data_obj, "cmip_file", data_list[current_data_file]);
    json::write_value(data_obj, "normal_file", normal_data_file);
    json::write_value(data_obj, "current_attribute", int(current_attribute));
    json::write_value(data_obj, "current_second_attribute", int(current_second_attribute));
    json::write_value(data_obj, "enable_packing", packing);

    std::vector<float> v(3);
    v[0] = scale[0];
    v[1] = scale[1];
    v[2] = scale[2];
    json::write_vector(data_obj, "scale", v);

    QJsonArray att_array_obj;
    for (std::size_t i = 0; i < attribute_list.size(); ++i) {
        QJsonObject att_obj;
        json::write_value(att_obj, "name", attribute_list[i].name);
        json::write_value(att_obj, "color_scheme", attribute_list[i].color_scheme);
        json::write_value(att_obj, "type", attribute_list[i].type);
        json::write_value(att_obj, "scale", attribute_list[i].scale);
        json::write_value(att_obj, "periodic", attribute_list[i].periodic);
        json::write_value(att_obj, "inverse", attribute_list[i].inverse);
        json::write_vector(att_obj, "color_knots", attribute_list[i].color_knots);
        json::write_vector(att_obj, "knots", attribute_list[i].knots);
        json::write_vector(att_obj, "alpha", attribute_list[i].alpha);
        att_array_obj.append(att_obj);
    }
    data_obj.insert("attribute_list", att_array_obj);

    QJsonArray seg_array_obj;
    typedef std::map<std::string, float> att_map_t;
    typedef std::map< std::size_t, std::map<std::string, float> > seg_map_t;
    for (seg_map_t::const_iterator it = attribute.begin(); it != attribute.end(); ++it) {
        QJsonObject seg_obj;
        json::write_value(seg_obj, "value", int(it->first));
        for (att_map_t::const_iterator jit = it->second.begin(); jit != it->second.end(); ++jit) {
            json::write_value(seg_obj, jit->first.c_str(), double(jit->second));
        }
        seg_array_obj.append(seg_obj);
    }
    data_obj.insert("segment_list", seg_array_obj);

    QJsonDocument save_doc(data_obj);
    save_file.write(save_doc.toJson());
    printf("Saved data to file %s:  %uz attributes, %uz segments \n", json_file.c_str(), attribute_list.size(), attribute.size() );
    return true;
}



void render_state::update_color_map(const std::string& scheme, const std::string& name, const std::vector<float>& alpha,
    const std::vector<float>& knots, const std::vector<float>& color_knots, const std::string& type, const std::string& scale, bool periodic, bool inverse, bool blend) {

    bool interpolate = (type == "scalar");
     
    bool logarithmic = interpolate && (scale == "logarithmic");

    // Update colors in the leaves
    if (scheme.size()) {
        lux::colormap CM(scheme);// ("YlOrRd9");
        if (CM.size() < 2) {
            printf("Error: color scheme %s not existing. Keeping old one \n", scheme.c_str());
            return;
        }

        // Replicate colors
        if (!interpolate) {
            for (size_t n = 0; n < glData.nLeafs(); n++) {
                std::size_t id = std::size_t(attribute[n][name]);
                //printf("n: %d,  %s: %d \n", n, name.c_str(), id);
                lux::color c = CM(id % CM.size());
                c(3) = alpha[id % alpha.size()];
                glData.set_leaf_color(n, c);
            
            }
        }
        else {
            lux::trajectoryf color;
            for (std::size_t i = 0; i < color_knots.size(); ++i) {
                lux::sample_t<float> s;
                s.curvature = 0.0f;
                float t = color_knots[i];
                std::size_t c = (periodic ? (i > CM.size() / 2 ? 2 * (CM.size() - 1 - i) : 2 * i) : i);
                c = (inverse ? CM.size() - 1 - c : c);
                s.position = lux::vec4f(CM(c)(0), CM(c)(1), CM(c)(2), t);
                color.push_back(s);
            }
            for (size_t n = 0; n < glData.nLeafs(); ++n) {
                float t = attribute[n][name];
                lux::color c = glData.get_leaf_color(n);
                lux::vec4f cint = color.sample_position(t);
                c(0) = std::min(0.5f * ((blend ? c(0) : cint(0)) + cint(0)), 1.0f);
                c(1) = std::min(0.5f * ((blend ? c(1) : cint(1)) + cint(1)), 1.0f);
                c(2) = std::min(0.5f * ((blend ? c(2) : cint(2)) + cint(2)), 1.0f);
                glData.set_leaf_color(n, c);
            }
        }

    }

    if (interpolate) {
        if (alpha.size()) {
            lux::trajectoryf alpha_int;
            for (std::size_t i = 0; i < alpha.size(); ++i) {
                lux::sample_t<float> s;
                s.curvature = 0.0f;
                float t = knots[i];
                s.position = lux::vec4f(alpha[i], 0.0f, 0.0f, t);
                alpha_int.push_back(s);
            }
            for (size_t n = 0; n < glData.nLeafs(); ++n) {
                float t = attribute[n][name];
                float a = 1.0f; // attribute[n]["active"];
                float c = alpha_int.sample_position(t)(0);
                c = std::min(1.0f, std::max(c, 0.0f));
                lux::color col = glData.get_leaf_color(n);
                col(3) = a * (blend ? col(3) : 1.0f) * c;
                glData.set_leaf_color(n, col);  
            }
        }
    }
    lux::color col = glData.get_leaf_color(0);
    col(3) = 0.0f;
    glData.set_leaf_color(0, col);
    /*std::mt19937 gen(1);
      for (size_t n = 0; n < glData.nLeafs(); n++) {
      //std::uniform_real_distribution<float> D(0.0f, 1.0f);
      //lux::color clr(D(gen), D(gen), D(gen), 1.0f);
      lux::color white(1.0f, 1.0f, 1.0f, 1.0f);
      lux::color black(0.0f, 0.0f, 0.0f, 1.0f);
      glData.leaf_color(n) = (rand()&1) ? white: black;
      }*/
      //glData.leaf_color(0) = lux::color(0.8f, 0.8f, 0.8f, 0.0002f);
    glData.gpu_update_color_lut();

}


void render_state::update_transfer_function1d() {
    attribute_prop& att1 = attribute_list[current_attribute];
    update_color_map(att1.color_scheme, att1.name, att1.alpha, att1.knots, att1.color_knots, att1.type, att1.scale, att1.periodic, att1.inverse, false);
}




void render_state::update_transfer_function2d() {

    //update_transfer_function1d();
    std::string style = tf_styles[current_style];
#if 1
    if ( style == "density") {
        density_to_transfer_function();
    }
    else if (style == "msc-interp") {
        msc_interp_to_transfer_function();
    }
    else if (style == "attribute1") {
        attribute_prop& att1 = attribute_list[current_attribute];
        update_color_map(att1.color_scheme, att1.name, att1.alpha, att1.knots, att1.color_knots, att1.type, att1.scale, att1.periodic, att1.inverse, false);
    }
    else if (style == "attribute2") {
        attribute_prop& att1 = attribute_list[current_second_attribute];
        update_color_map(att1.color_scheme, att1.name, att1.alpha, att1.knots, att1.color_knots, att1.type, att1.scale, att1.periodic, att1.inverse, false);
    }
    else if (style == "attribute2d") {
        attribute_prop& att1 = attribute_list[current_attribute];
        update_color_map(att1.color_scheme, att1.name, att1.alpha, att1.knots, att1.color_knots, att1.type, att1.scale, att1.periodic, att1.inverse, false);
        attribute_prop& att2 = attribute_list[current_second_attribute];
        update_color_map(att2.color_scheme, att2.name, att2.alpha, att2.knots, att2.color_knots, att2.type, att2.scale, att2.periodic, att2.inverse, true);
    }
#endif

}


void render_state::update_density_attributes() {

    const attribute_prop& att1 = attribute_list[current_attribute];
    const attribute_prop& att2 = attribute_list[current_second_attribute];

    bool interp1 = (att1.type == "scalar");
    if (!interp1) return;
    bool interp2 = (att2.type == "scalar");
    if (!interp2) return;

    bool log1 = false; // interp1 && (att1.scale == "logarithmic");
    bool log2 = false; // interp2 && (att2.scale == "logarithmic");

    float xmin = att1.knots[0];
    if (log1) xmin = log(xmin);
    float xmax = att1.knots[att1.knots.size() - 1];
    if (log1) xmax = log(xmax);
    
    float ymin = att2.knots[0];
    if (log2) ymin = log(ymin);
    float ymax = att2.knots[att2.knots.size() - 1];
    if (log2) ymax = log(xmax);
 
    // FIXME: check with logarithmic attributes
    std::vector< std::vector<float> > data;
    for (std::size_t n = 0; n < attribute.size(); ++n) {
        std::vector<float> p(2);
        p[0] = attribute[n][att1.name];
        p[1] = attribute[n][att2.name];
        data.push_back(p);
    }
    dimension_reduction.set_xlim(xmin,xmax);
    dimension_reduction.set_ylim(ymin,ymax);

    dimension_reduction.compute_density(data);
    const std::vector< float>& density = dimension_reduction.density();

    
    printf(" Computed kde \n");

    // interpolation from density
    
    std::size_t w = dimension_reduction.width();
    std::size_t h = dimension_reduction.height();
 
#if 0
    for (std::size_t j = 0; j < h; ++j) {
        for (std::size_t i = 0; i < w; ++i) {
            printf(" %f ", density[j][i]);
        }
        printf("\n");
    }
#endif

 

    // FIXME
    float vmin = 1.0e10;
    float vmax = 0.0f;

    for (std::size_t n = 0; n < attribute.size(); ++n) {
        float v = (data[n][0] - xmin) / (xmax - xmin) * (h-1);
        float u = (data[n][1] - ymin) / (ymax - ymin) * (w - 1);
        //float v = (1.0f - (data[n][1] - ymin) / (ymax - ymin)) * (w-1);
        int u_floor = int(floor(u));
        int v_floor = int(floor(v));
        int id = v_floor * w + u_floor;

        float d_00 = density[id];
        float d_10 = density[id+1];
        float d_11 = density[id+w+1];
        float d_01 = density[id+w];
        float du = u - floor(u);
        float dv = v - floor(v);
        float d_u0 = (1.0f - du) * d_00 + du * d_10;
        float d_u1 = (1.0f - du) * d_01 + du * d_11;
        float d_uv = (1.0f - dv) * d_u0 + dv * d_u1;
        vmin = std::min(vmin, d_uv);
        vmax = std::max(vmax, d_uv);
        attribute[n]["density"] = d_uv;
    }
    printf(" Computed density attributes \n");

#if 1
    if (density_attribute_id == -1) {
        attribute_prop a;
        a.name = "density";
        a.type = "scalar";
        a.color_scheme = "Greens9";
        // Register 
        density_attribute_id = attribute_list.size();
        attribute_list.push_back(a);
    }
#endif

    attribute_prop& att = attribute_list[density_attribute_id];
    bool logarithmic = (vmin > 0.0f && vmax / vmin > 1.0e04);
    if (logarithmic) att.scale = "logarithmic";

    lux::colormap CM(att.color_scheme);
    att.color_knots.resize(CM.size());
    for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
        if (logarithmic) {
            att.color_knots[c] = vmin * exp(float(c) / float(att.color_knots.size() - 1) * (log(vmax + 1.0f) - log(vmin + 1.0f)));
        }
        else {
            att.color_knots[c] = vmin + float(c) / float(att.color_knots.size() - 1) * (vmax - vmin);
        }
    }
    att.knots.resize(2);
    att.knots[0] = vmin;
    att.knots[att.knots.size() - 1] = vmax;

    att.alpha.resize(2);
    att.alpha[0] = 0.49f;
    att.alpha[1] = 0.49f;

    for (std::size_t i = 0; i < attribute_list.size(); ++i) {
        printf(" Attribute [%d] =  %s \n",  int(i), attribute_list[i].name.c_str());
    }

    printf(" Attribute %s minmax: %f %f \n", att.name.c_str(), vmin, vmax);
    return;
}





void render_state::density_to_transfer_function() {

    if (density_attribute_id == -1) {
        update_density_attributes();
    }
    //printf(" density attribute id == %d \n", density_attribute_id);
    const attribute_prop& att1 = attribute_list[density_attribute_id];
    update_color_map(att1.color_scheme, att1.name, att1.alpha, att1.knots, att1.color_knots, att1.type, att1.scale, att1.periodic, att1.inverse, false);
   
}

void render_state::msc_interp_to_transfer_function() {
#if 0
    // from density compute msc points
    dimension_reduction.compute_morse_smale_complex();
    const pydr::vector_vectorf_t& msc = dimension_reduction.morse_smale_points();
#endif
    if (msc_sigma_attribute_id == -1 || peak_attribute_id == -1) {
        update_msc_attributes();
    }

    printf("peak attribute id =  %d \n", peak_attribute_id);
    printf("msc sigma attribute id =  %d \n", msc_sigma_attribute_id);
    const attribute_prop& attp = attribute_list[peak_attribute_id];
    const attribute_prop& atts = attribute_list[msc_sigma_attribute_id];

    lux::colormap CM(attp.color_scheme);// ("YlOrRd9");

    if (atts.alpha.size()) {
        lux::trajectoryf alpha_int;
        for (std::size_t i = 0; i < atts.alpha.size(); ++i) {
            lux::sample_t<float> s;
            s.curvature = 0.0f;
            float t = atts.knots[i];
            s.position = lux::vec4f(atts.alpha[i], 0.0f, 0.0f, t);
            alpha_int.push_back(s);
        }
        for (size_t n = 0; n < glData.nLeafs(); ++n) {
            std::size_t id = std::size_t(attribute[n][attp.name]);
            //printf("n: %d,  %s: %d \n", n, name.c_str(), id);
            lux::color col = CM(id % CM.size());
            float t = attribute[n][atts.name];
            lux::vec4f col_blend = (1.0f - t)*density_background_color.as<float>() + t*col.as<float>();
            float a = attp.alpha[id]; // attribute[n]["active"];
            float c = alpha_int.sample_position(t)(0);
            c = std::min(1.0f, std::max(c, 0.0f));
            col_blend(3) = a * c;
            col = lux::color(col_blend(0),col_blend(1),col_blend(2),col_blend(3));
            glData.set_leaf_color(n, col);
        }
    }

    lux::color col = glData.get_leaf_color(0);
    col(3) = 0.0f;
    glData.set_leaf_color(0, col);
    glData.gpu_update_color_lut();

}




void render_state::image_to_transfer_function2d() {

    attribute_prop& att1 = attribute_list[current_attribute];
    attribute_prop& att2 = attribute_list[current_second_attribute];

    bool interp1 = (att1.type == "scalar");
    if (!interp1) return;
    bool interp2 = (att2.type == "scalar");
    if (!interp2) return;

    bool log1 = interp1 && (att1.scale == "logarithmic");
    bool log2 = interp2 && (att2.scale == "logarithmic");

    float xmin = att1.knots[0];
    if (log1) xmin = log(xmin);
    float xmax = att1.knots[att1.knots.size() - 1];
    if (log1) xmax = log(xmax);
    std::vector<float> xlim(2);
    xlim[0] = xmin;
    xlim[1] = xmax;

    float ymin = att2.knots[0];
    if (log2) ymin = log(ymin);
    float ymax = att2.knots[att2.knots.size() - 1];
    if (log2) ymax = log(xmax);
    std::vector<float> ylim(2);
    ylim[0] = ymin;
    ylim[1] = ymax;

    float range1 = xmax-xmin;
    float range2 = ymax-ymin;

    if (recompute_density) {
        std::vector< std::vector<float> > data;
        for (std::size_t n = 0; n < attribute.size(); ++n) {
            std::vector<float> p(2);
            p[0] = attribute[n][att1.name];
            p[1] = attribute[n][att2.name];
            data.push_back(p);
        }

        dimension_reduction.kde_morse_smale(data, xlim, ylim, draw_image, "Set2", true);
        draw_area.load(draw_image.c_str());
        recompute_density = false;
    }

    const QRgb* img = (const QRgb*)(draw_area.constBits());
    int w = draw_area.width();
    int h = draw_area.height();

    //const float eps = 0.5f/ std::min(draw_area.width(), draw_area.height());
    // perform interpolation
    if (w > 0 && h > 0) {
        for (size_t n = 0; n < glData.nLeafs(); n++) {
            float x = std::max( 0.0f, std::min( 1.0f, (log1 ? log(attribute[n][att1.name]) - log(att1.knots[0]) : attribute[n][att1.name] - att1.knots[0])/range1));
            float y = std::max(0.0f, std::min(1.0f, 1.0f - (log2 ? log(attribute[n][att2.name]) - log(att2.knots[0]) : attribute[n][att2.name] - att2.knots[0])/range2));
            int id = w * int(y * (h - 1)) + int(x * (w - 1));
            QColor c = QColor(img[id]);
            //QColor c = draw_area.pixelColor(std::max(1, std::min(draw_area.width()-2, int(u*(draw_area.width()-1.5f)))), std::max(1, std::min(draw_area.height() - 2, int(v * (draw_area.height()-1.5f)))));
            if (c.red() < 10 && c.green() < 10 && c.blue() < 10) {
                printf("Black pixel at (%f,%f) \n", x, y);
            }
            lux::color col;
            col(0) = float(c.red()) / 255.0f;
            col(1) = float(c.green()) / 255.0f;
            col(2) = float(c.blue()) / 255.0f;
            col(3) = 1.0f;
            glData.set_leaf_color(n, col);
        }
    }

    if (att1.alpha.size() && att2.alpha.size()) {
        lux::trajectoryf alpha1_int;
        for (std::size_t i = 0; i < att1.alpha.size(); ++i) {
            lux::sample_t<float> s;
            s.curvature = 0.0f;
            float t = att1.knots[i];
            s.position = lux::vec4f(att1.alpha[i], 0.0f, 0.0f, t);
            alpha1_int.push_back(s);
        }
        lux::trajectoryf alpha2_int;
        for (std::size_t i = 0; i < att2.alpha.size(); ++i) {
            lux::sample_t<float> s;
            s.curvature = 0.0f;
            float t = att2.knots[i];
            s.position = lux::vec4f(att2.alpha[i], 0.0f, 0.0f, t);
            alpha2_int.push_back(s);
        }

        for (size_t n = 0; n < glData.nLeafs(); ++n) {
            float t1 = attribute[n][att1.name];
            float t2 = attribute[n][att2.name];
            float a = attribute[n]["active"];
            float c = a* alpha1_int.sample_position(t1)(0) * alpha2_int.sample_position(t2)(0);
            c = std::min(1.0f, std::max(c, 0.0f));
            lux::color col = glData.get_leaf_color(n);
            col(3) = c;
            glData.set_leaf_color(n,col);
        }
    }
    lux::color col = glData.get_leaf_color(0);
    col(3) = 0.0f;
    glData.set_leaf_color(0, col);

    glData.gpu_update_color_lut();

}





void render_state::compute_tsne() {
    // Parse attributes
    // Parse attributes
    std::size_t N = attribute.size();

    std::vector<std::string> scalar_attributes;

    for (std::size_t n = 0; n < attribute_list.size(); ++n) {
        if (attribute_list[n].type == "scalar" && attribute_list[n].input) {
            scalar_attributes.push_back(attribute_list[n].name);
        }
    }

    std::size_t D = scalar_attributes.size();

    std::vector< std::vector<float> > x;
    for (seg_map_t::const_iterator seg = attribute.begin(); seg != attribute.end(); ++seg) {
        std::vector<float> s;
        const att_map_t& att = seg->second;
        for (std::size_t a = 0; a < scalar_attributes.size(); ++a) {
            att_map_t::const_iterator it = att.find(scalar_attributes[a]);
            if (it != att.end()) {
                s.push_back(it->second);
            }
            else {
                s.push_back(0.0f);
                printf("Attributes %s missing for object %uz \n", scalar_attributes[a].c_str(), seg->first);
            }
        }
        x.push_back(s);
    }

    std::vector< std::vector<float> > y;
    dimension_reduction.tsne(x, y, 10.0f, 500);

    printf("(MMM): TSNE computed \n");
    std::size_t no_dims = 2;

    std::vector< int> cluster_labels;
    dimension_reduction.dbscan(y, cluster_labels, 50, 50);

    std::vector<int> l;
    for (std::size_t i = 0; i < N; ++i) {
        l.push_back(cluster_labels[i] + 1);
    }

    // Creat new attributes
    attribute_prop att;
    att.name = "tsne_clusters";
    att.color_scheme = "set2_8";

    int vmin = *std::min_element(l.begin(), l.end());
    int vmax = *std::max_element(l.begin(), l.end());

    printf(" Attribute %s minmax: %d %d \n", att.name.c_str(), vmin, vmax);
    lux::colormap CM(att.color_scheme);
    att.type = "nominal";

    std::size_t nsize = std::min(1 + std::size_t(vmax) - std::size_t(vmin), CM.size());
    att.color_knots.resize(nsize);
    att.knots.resize(nsize);
    att.alpha.resize(nsize);
    for (std::size_t i = 0; i < att.knots.size(); ++i) {
        att.color_knots[i] = vmin +  i;
        att.knots[i] = vmin + i;
        att.alpha[i] = 0.5f;
    }
    attribute_list.push_back(att);

    printf("Attribute %s size: %d \n", att.name.c_str(), int(nsize));

    // Update attributes
    for (std::size_t i = 0; i < N; ++i) {
        attribute[i].insert(std::make_pair(att.name, l[i]));
    }

    for (std::size_t d = 0; d < no_dims; ++d) {
        std::vector<double> x;

        for (std::size_t i = 0; i < N; ++i) {
            x.push_back(y[i][d]);
        }

        // Creat new attributes

        attribute_prop att;
        att.name = "tsne" + std::to_string(d);
        att.color_scheme = color_schemes[rand() % color_schemes.size()];
        att.input =  false;

        float vmin = *std::min_element(x.begin(), x.end());
        float vmax = *std::max_element(x.begin(), x.end());

        printf(" Attribute %s minmax: %.2f %.2f \n", att.name.c_str(), vmin, vmax);

        att.type = "scalar";

        att.knots[0] = vmin;
        att.knots[att.knots.size() - 1] = vmax;

        att.alpha[0] = 0.0f;
        att.alpha[1] = 0.99f;

        bool logarithmic = (vmin > 0.0f && vmax / vmin > 1.0e04);

        if (logarithmic) att.scale = "logarithmic";

        lux::colormap CM(att.color_scheme);
        att.color_knots.resize(CM.size());
        for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
            if (logarithmic) {
                att.color_knots[c] = vmin * exp(float(c) / float(att.color_knots.size() - 1) * (log(vmax + 1.0f) - log(vmin + 1.0f)));
            }
            else {
                att.color_knots[c] = vmin + float(c) / float(att.color_knots.size() - 1) * (vmax - vmin);
            }
        }
        attribute_list.push_back(att);

        // Update attributes
        for (std::size_t i = 0; i < N; ++i) {
            attribute[i].insert(std::make_pair(att.name, x[i]));
        }
    }
  
    printf("(MMM): TSNE attributes added\n");

}

void render_state::compute_pca() {
    // Parse attributes
    std::size_t N = attribute.size();

    std::vector<std::string> scalar_attributes;

    for (std::size_t n = 0; n < attribute_list.size(); ++n) {
        if (attribute_list[n].type == "scalar" && attribute_list[n].input) {
            scalar_attributes.push_back(attribute_list[n].name);
        }
    }

    std::size_t D = scalar_attributes.size();

    std::vector< std::vector<float> > x;
    for (seg_map_t::const_iterator seg = attribute.begin(); seg != attribute.end(); ++seg) {
        std::vector<float> s;
        const att_map_t& att = seg->second;
        for(std::size_t a = 0; a < scalar_attributes.size(); ++a) {
            att_map_t::const_iterator it = att.find(scalar_attributes[a]);
            if ( it != att.end() ) {
               s.push_back(it->second);
            }
            else {
                s.push_back(0.0f);
                printf("Attributes %s missing for object %uz \n", scalar_attributes[a].c_str(), seg->first);
            }
        }
        x.push_back(s);
    }
#if 0
    python_wrapper dimension_reduction("dimension_reduction", "C:/ProgramData/MiniConda3" );
    std::vector< std::vector<float> > y;
    dimension_reduction.set_func("compute_pca");
dimension_reduction.init_posargs(1);
dimension_reduction.set_posarg(0, helpers::vector_vector_to_nparray(x));
helpers::nparray_to_vector_vector(dimension_reduction.exec(), y);
#else
std::vector< std::vector<float> > y;
dimension_reduction.pca(x, y);
#endif


std::vector< int> cluster_labels;
dimension_reduction.dbscan(y, cluster_labels, 100, 100);

std::vector<int> l;
for (std::size_t i = 0; i < N; ++i) {
    l.push_back(cluster_labels[i] + 1);
}

// Creat new attributes
attribute_prop att;
att.name = "pca_clusters";
att.color_scheme = "set3_9";

int vmin = *std::min_element(l.begin(), l.end());
int vmax = *std::max_element(l.begin(), l.end());

printf(" Attribute %s minmax: %d %d \n", att.name.c_str(), vmin, vmax);
lux::colormap CM(att.color_scheme);
att.type = "nominal";

std::size_t nsize = std::min(1 + std::size_t(vmax) - std::size_t(vmin), CM.size());
att.color_knots.resize(nsize);
att.knots.resize(nsize);
att.alpha.resize(nsize);
for (std::size_t i = 0; i < att.knots.size(); ++i) {
    att.color_knots[i] = vmin + i;
    att.knots[i] = vmin + i;
    att.alpha[i] = 0.29;
}
attribute_list.push_back(att);

// Update attributes
for (std::size_t i = 0; i < N; ++i) {
    attribute[i].insert(std::make_pair(att.name, l[i]));
}


printf("(MMM): PCA computed \n");
std::size_t no_dims = 2;

for (std::size_t d = 0; d < no_dims; ++d) {
    std::vector<double> x;
    for (std::size_t i = 0; i < N; ++i) {
        x.push_back(y[i][d]);
    }

    // Creat new attributes
    attribute_prop att;
    att.name = "pca" + std::to_string(d);
    att.color_scheme = color_schemes[rand() % color_schemes.size()];

    float vmin = *std::min_element(x.begin(), x.end());
    float vmax = *std::max_element(x.begin(), x.end());

    printf(" Attribute %s minmax: %.2f %.2f \n", att.name.c_str(), vmin, vmax);

    att.type = "scalar";

    att.knots[0] = vmin;
    att.knots[att.knots.size() - 1] = vmax;

    att.alpha[0] = 0.0f;
    att.alpha[1] = 0.99f;
    att.input = false;

    bool logarithmic = (vmin > 0.0f && vmax / vmin > 1.0e04);

    if (logarithmic) att.scale = "logarithmic";

    lux::colormap CM(att.color_scheme);
    att.color_knots.resize(CM.size());
    for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
        if (logarithmic) {
            att.color_knots[c] = vmin * exp(float(c) / float(att.color_knots.size() - 1) * (log(vmax + 1.0f) - log(vmin + 1.0f)));
        }
        else {
            att.color_knots[c] = vmin + float(c) / float(att.color_knots.size() - 1) * (vmax - vmin);
        }

    }
    attribute_list.push_back(att);

    // Update attributes
    for (std::size_t i = 0; i < N; ++i) {
        attribute[i].insert(std::make_pair(att.name, x[i]));
    }
}
printf("(MMM): PCA attributes added\n");
}

/* ********************************************************************* */
// Test code: to move in some classes in the future

float safe_value(const int& id, const std::vector<float>& v, const float& val=0.0f) {
    return (id >= 0 && id < v.size() ? v[std::size_t(id)] : val);
}


double distance(const QPointF& a, const QPointF& b) {
    return sqrt((a.x() - b.x()) * (a.x() - b.x()) + (a.y() - b.y()) * (a.y() - b.y()));
}


double norm(const QPointF& a) {
    return distance(a, QPointF(0.0, 0.0));
}

void density_to_gradient(const std::vector<float>& density, const std::size_t w, std::vector< QPointF>& nabla) {

    // Central differences
    nabla.clear();
    for (int id = 0; id < density.size(); ++id) {
        float d_00 = safe_value(id - 1, density);
        float d_10 = safe_value(id + 1, density);
        float d_11 = safe_value(id + w, density);
        float d_01 = safe_value(id - w, density);
        QPointF v(0.5*double(d_10-d_00), 0.5*double(d_11-d_01));
        nabla.push_back(v);
    }
    printf("Computed gradient field \n");
}

int reached_peak(const QPointF& p, const std::vector<QPointF>& msc_peaks, float threshold  = 1.0e-2) {

    int closest_peak = -1;
    for (std::size_t i = 0; i < msc_peaks.size(); ++i) {
        float d = distance(p, msc_peaks[i]);
        if (d < threshold) {
            closest_peak = int(i);
            return closest_peak;
        }
    }
    return closest_peak;
}

// bilinear interpolation of  gradient
QPointF nabla_at(const QPointF& p, float xmin, float xmax, float ymin, float ymax, std::size_t w, std::size_t h,
    const std::vector< QPointF >& nabla) {

    QPointF nabla_uv = QPointF(0.0f, 0.0f);
    //printf(" p = (%f,%f) : xmin =%f, xmax =%f \n", p.x(), p.y(), xmin, xmax);
    if (p.x() < xmin || p.x() > xmax || p.y() < ymin || p.y() > ymax) {        
        return nabla_uv;
    }

    double v = (p.x() - xmin) / double(xmax-xmin) * double(h - 1);
    double u = (p.y() - ymin) / double(ymax-ymin) * double(w - 1);
    //double u = (1.0  - (p.y() - ymin) / double(ymax - ymin) ) * double(w - 1);

    int u_floor = int(floor(u));
    int v_floor = int(floor(v));
    int id = v_floor * w + u_floor;

    QPointF nabla_00 = nabla[id];
    QPointF nabla_10 = nabla[id + 1];
    QPointF nabla_11 = nabla[id + w + 1];
    QPointF nabla_01 = nabla[id + w];
    double du = u - floor(u);
    double dv = v - floor(v);
    QPointF nabla_u0 = (1.0-du) * nabla_00 + du * nabla_10;
    QPointF nabla_u1 = (1.0-du) * nabla_01 + du * nabla_11;
    nabla_uv = (1.0-dv) * nabla_u0 + dv * nabla_u1;
    return nabla_uv;
}

int streamline_from_to(const QPointF& seed, float step, float& length, float xmin, float xmax, float ymin, float ymax, std::size_t w, std::size_t h,
    const std::vector< QPointF >& nabla, const std::vector<QPointF>& critical_points) {
    length = 0.0;
    int closest = -1;
    QPointF p = seed;
    std::size_t step_count = 0;
    QPointF delta = nabla_at(p, xmin, xmax, ymin, ymax, w, h, nabla);
    //printf(" gradient == (%f, %f) \n ", delta.x(), delta.y());
    QPointF range(double(xmax - xmin), double(ymax - ymin));
    double scale = range.manhattanLength();
    double threshold = 0.01 * scale;
    while ( step_count <= 100) {
        closest = reached_peak(p, critical_points, threshold);
        if ( closest != -1 || norm(delta) < 1.0e-10) {
            break;
        }
#if 0 // Euler integrator
        p += step * delta;
        length += fabs(step) * norm(delta);
        delta = nabla_at(p, xmin, xmax, ymin, ymax, w, h, nabla);
#else  // RK-IV integrator
        QPointF k1 = delta;
        QPointF k2 = nabla_at(p + 0.5 * step * k1, xmin, xmax, ymin, ymax, w, h, nabla);
        QPointF k3 = nabla_at(p + 0.5 * step * k2, xmin, xmax, ymin, ymax, w, h, nabla);
        QPointF k4 = nabla_at(p + step * k3, xmin, xmax, ymin, ymax, w, h, nabla);
        QPointF dp = (step * scale / 6.0f) * (k1 + 2.0f * (k2 + k3) + k4);
        length += norm(dp);
        p += dp;
        delta = nabla_at(p, xmin, xmax, ymin, ymax, w, h, nabla);
#endif
        ++step_count;
    }
    // printf(" step: %d , p: (%f, %f), length: %f \n", int(step_count), p.x(), p.y(), length);
    //printf(" step count == %d \n", int(step_count));
    return closest;
}



// Assign peak according to distance: simple scheme
int  closest_critical_point(const QPointF& p, const std::vector<QPointF>& critical, float& dist_min) {
    dist_min = 1.0e10;
    int closest = -1;
    for (std::size_t i = 0; i < critical.size(); ++i) {
        float d = distance(p, critical[i]);
        if (dist_min > d) {
            dist_min = d;
            closest = int(i);
        }
    }
    return closest;
}
    


void render_state::compute_msc_attributes(const pydr::vector_vectorf_t& y, const std::vector<float>& xlim, const std::vector<float>& ylim, 
    const pydr::vectorf_t& density, std::size_t w, std::size_t h,  const std::vector< QPointF >& nabla, 
    const std::vector< QPointF>& msc_peaks, const std::vector< QPointF>& msc_valleys) {

    const float step = 10.0f;

    if (msc_sigma_attribute_id == -1) {
        attribute_prop att;
        att.name = "msc_sigma";
        att.color_scheme = "reds9";
        att.type = "scalar";
        att.color_knots.resize(9);
        for (std::size_t i = 0; i < 9; ++i) {
            att.color_knots[i] = float(i) / 8;
        }
        att.alpha.resize(2);
        att.alpha[0] = 0.2f;
        att.alpha[1] = 1.0f;
        att.knots.resize(2);
        att.knots[0] = 0.0f;
        att.knots[1] = 1.0;
        msc_sigma_attribute_id = attribute_list.size();
        attribute_list.push_back(att);
    }

    // Add MSC peaks
    // Creat new attributes
    if (peak_attribute_id == -1) {
        attribute_prop att;
        att.name = "msc_peak";
        att.color_scheme = "set1_9";
        att.type = "nominal";

        std::size_t nsize = msc_peaks.size();
        att.color_knots.resize(nsize);
        att.knots.resize(nsize);
        att.alpha.resize(nsize);
        for (std::size_t i = 0; i < att.knots.size(); ++i) {
            att.color_knots[i] = i;
            att.knots[i] = i;
            att.alpha[i] = 0.49f;
        }
        peak_attribute_id = attribute_list.size();
        attribute_list.push_back(att);
    }

    float xmin = xlim[0];
    float xmax = xlim[1];
    float ymin = ylim[0];
    float ymax = ylim[1];

    for (std::size_t n = 0; n < attribute.size(); ++n) {
        QPointF seed(y[n][0], y[n][1]);
        float length_to_peak = 0.0;
        int  closest_peak =  streamline_from_to(seed, step, length_to_peak, xmin, xmax, ymin, ymax,
            w, h, nabla, msc_peaks);
        float length_to_minimum = 0.0;
        int closest_minimum = streamline_from_to(seed, -step, length_to_minimum, xmin, xmax, ymin, ymax,
            w, h, nabla, msc_valleys);
        //printf(" Attributes [%d] = { d_min: %f, d_peak: %f, closest_min: %d, closest_max: %d } \n", int(n), length_to_minimum, length_to_peak, int(closest_minimum), int(closest_peak));
        //if (closest_minimum == -1) {
        //    closest_critical_point(seed, msc_valleys, length_to_minimum, closest_minimum);
        //}
        if (closest_peak == -1) {
            closest_peak = closest_critical_point(seed, msc_peaks, length_to_peak);
            closest_minimum = closest_critical_point(seed, msc_valleys, length_to_minimum);
            //printf("+Attributes [%d] = { d_min: %f, d_peak: %f, closest_min: %d, closest_max: %d } \n", int(n), length_to_minimum, length_to_peak, int(closest_minimum), int(closest_peak));
        }
        attribute[n]["msc_sigma"] = length_to_minimum / (length_to_peak + length_to_minimum);
        attribute[n]["msc_peak"] = closest_peak;
    }

#if 0
    // Assign peak according to distance: simple scheme
    for (std::size_t n = 0; n < attribute.size(); ++n) {
        QPointF p(y[n][0], y[n][1]);
        float dist_min = 1.0e10;
        std::size_t closest_peak = -1;
        for (std::size_t i = 0; i < msc_peaks.size(); ++i) {
            float d = distance(p, msc_peaks[i]);
            if (dist_min > d) {
                dist_min = d;
                closest_peak = i;
            }
        }
        attribute[n]["msc_peak"] = closest_peak;
    }
#endif
    printf(" Computed msc peak attributes \n");

}


/* ********************************************************************* */

void render_state::compute_umap() {
    // Parse attributes
    std::size_t N = attribute.size();

    std::vector<std::string> scalar_attributes;

    for (std::size_t n = 0; n < attribute_list.size(); ++n) {
        printf("Attribute %zu : %s \n", n, attribute_list[n].name.c_str());
        if (attribute_list[n].type == "scalar" && attribute_list[n].input) {
            printf("Attribute scalar %zu : %s \n", n, attribute_list[n].name.c_str());
            scalar_attributes.push_back(attribute_list[n].name);
        }
    }

    std::size_t D = scalar_attributes.size();

    std::vector< std::vector<float> > x;
    for (seg_map_t::const_iterator seg = attribute.begin(); seg != attribute.end(); ++seg) {
        std::vector<float> s;
        const att_map_t& att = seg->second;
        for (std::size_t a = 0; a < scalar_attributes.size(); ++a) {
            att_map_t::const_iterator it = att.find(scalar_attributes[a]);
            if (it != att.end()) {
                s.push_back(it->second);
            }
            else {
                s.push_back(0.0f);
                printf("Attributes %s missing for object %d \n", scalar_attributes[a].c_str(), int(seg->first));
            }
        }
        x.push_back(s);
    }
    

    std::vector< std::vector<float> > y;
    dimension_reduction.umap(x, y, 15, 0.1f);

    //printf("%f , %f, %f, %f \n", y[1][0], y[1][1], y[2][0], y[2][1]);

#if 0
    python_wrapper dimension_reduction("dimension_reduction", "C:/ProgramData/MiniConda3");
    std::vector< std::vector<float> > y;
    dimension_reduction.set_func("compute_umap");
    dimension_reduction.init_posargs(1);
    dimension_reduction.set_posarg(0, helpers::vector_vector_to_nparray(x));
    dimension_reduction.init_namedargs();
    dimension_reduction.set_namedarg("n_neighbors", helpers::pyval(10));
    dimension_reduction.set_namedarg("min_dist", helpers::pyval(0.5f));
    helpers::nparray_to_vector_vector(dimension_reduction.exec(), y);
#endif

    printf("(MMM): UMAP computed \n");
    std::size_t no_dims = 2;

    std::vector< int> l;
    lux::colormap CM("set1_9");

    dimension_reduction.dbscan(y, l, 20, 20);
   
    // Creat new attributes
    attribute_prop att;
    att.name = "umap_clusters";
    att.color_scheme = "set1_9";

    int vmin = *std::min_element(l.begin(), l.end());
    int vmax = *std::max_element(l.begin(), l.end());

    printf(" Attribute %s minmax: %d %d \n", att.name.c_str(), vmin, vmax);
    
    att.type = "nominal";

    std::size_t nsize = std::min(std::size_t(vmax) - std::size_t(vmin), CM.size());
    att.color_knots.resize(nsize);
    att.knots.resize(nsize);
    att.alpha.resize(nsize);
    for (std::size_t i = 0; i < att.knots.size(); ++i) {
        att.color_knots[i] = vmin +  i;
        att.knots[i] = vmin +  i;
        att.alpha[i] = 0.49f;
    }
    clusters_id = int(attribute_list.size());

    attribute_list.push_back(att);

    // Update attributes
    for (std::size_t i = 0; i < N; ++i) {
        attribute[i].insert(std::make_pair(att.name, l[i]));
    }

    float xmin = -3.0f;
    float xmax =  3.0f;
    float ymin = -3.0f;
    float ymax =  3.0f;


    for (std::size_t d = 0; d < no_dims; ++d) {
        std::vector<double> x;
        
        for (std::size_t i = 0; i < N; ++i) {
            x.push_back(y[i][d]);
        }

        // Creat new attributes

        attribute_prop att;
        att.name = "umap" + std::to_string(d);
        att.color_scheme = color_schemes[rand() % color_schemes.size()];

        float vmin = *std::min_element(x.begin(), x.end());
        if (d == 0) xmin = vmin;
        if (d == 1) ymin = vmin;

        float vmax = *std::max_element(x.begin(), x.end());
        if (d == 0) xmax = vmax;
        if (d == 1) ymax = vmax;

        printf(" Attribute %s minmax: %.2f %.2f \n", att.name.c_str(), vmin, vmax);

        att.type = "scalar";
        att.input = false;

        att.knots.resize(2);

        att.knots[0] = vmin;
        att.knots[att.knots.size() - 1] = vmax;

        att.alpha[0] = 0.0f;
        att.alpha[1] = 0.99f;

        bool logarithmic = (vmin > 0.0f && vmax / vmin > 1.0e04);

        if (logarithmic) att.scale = "logarithmic";

        lux::colormap CM(att.color_scheme);
        att.color_knots.resize(CM.size());
        for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
            if (logarithmic) {
                att.color_knots[c] = vmin * exp(float(c) / float(att.color_knots.size() - 1) * (log(vmax + 1.0f) - log(vmin + 1.0f)));
            }
            else {
                att.color_knots[c] = vmin + float(c) / float(att.color_knots.size() - 1) * (vmax - vmin);
            }
        }
        attribute_list.push_back(att);

        // Update attributes
        for (std::size_t i = 0; i < N; ++i) {
            attribute[i].insert(std::make_pair(att.name, x[i]));
        }
    }
    // Add also UMAP density

    dimension_reduction.set_xlim(xmin, xmax);
    dimension_reduction.set_ylim(ymin, ymax);
    dimension_reduction.width() = 128;
    dimension_reduction.height() = 128;

    dimension_reduction.compute_density(y);
    const std::vector< float>& density =  dimension_reduction.density();

    printf(" Computed kde \n");

    // interpolation from density
    std::size_t w = dimension_reduction.width();
    std::size_t h = dimension_reduction.height();

#if 0
    for (std::size_t j = 0; j < h; ++j) {
        for (std::size_t i = 0; i < w; ++i) {
            printf(" %f ", density[j][i]);
        }
        printf("\n");
    }
#endif


    float dmin = *std::min_element(density.begin(), density.end());
    float dmax = *std::max_element(density.begin(), density.end());
    printf(" Attribute %s minmax: %f %f \n", att.name.c_str(), dmin, dmax);

    dmin = 1.0e10f;
    dmax = 0.0f;
    for (std::size_t n = 0; n < attribute.size(); ++n) {
        float v = (y[n][0] - xmin) / (xmax - xmin) * (h-1);
        float u = (y[n][1] - ymin) / (ymax - ymin) * (w-1);
        //float v = ( 1.0f - (y[n][1] - ymin) / (ymax - ymin) ) * (w - 1);
        int u_floor = int(floor(u));
        int v_floor = int(floor(v));
        int id = v_floor * w + u_floor;

        float d_00 = density[id];
        float d_10 = density[id+1];
        float d_11 = density[id+w+1];
        float d_01 = density[id+w];
        float du = u - floor(u);
        float dv = v - floor(v);
        float d_u0 = (1.0f - du) * d_00 + du * d_10;
        float d_u1 = (1.0f - du) * d_01 + du * d_11;
        float d_uv = (1.0f - dv) * d_u0 + dv * d_u1;
        dmin = std::min(dmin, d_uv);
        dmax = std::max(dmax, d_uv);
        //printf("Density [ %d ] = %f \n", n, d_uv);
        attribute[n]["density"] = d_uv;
    }

    printf(" Computed umap density attributes \n");
    printf(" Attribute %s minmax: %f %f \n", att.name.c_str(), dmin, dmax);

    //attribute_prop att;
    att.name = "density";
    att.type = "scalar";
    att.color_scheme = "Greens9";

    printf(" Attribute %s minmax: %f %f \n", att.name.c_str(), dmin, dmax);

    att.knots.resize(2);

    att.knots[0] = dmin;
    att.knots[1] = dmax;

    att.alpha.resize(2);
    att.alpha[0] = 0.0f;
    att.alpha[1] = 0.5f;

    // FIXME
#if 1
    bool logarithmic = (dmin > 1.0e-10 && dmax / dmin > 1.0e04);
    if (logarithmic) {
        //att.scale = "logarithmic";
        printf("Density is logarithmic \n");
    }
#endif

    CM = lux::colormap(att.color_scheme);
    att.color_knots.resize(CM.size());
    for (std::size_t c = 0; c < att.color_knots.size(); ++c) {
        if (logarithmic) {
            att.color_knots[c] = dmin * exp(float(c) / float(att.color_knots.size() - 1) * (log(dmax + 1.0f) - log(dmin + 1.0f)));
        }
        else {
            att.color_knots[c] = dmin + float(c) / float(att.color_knots.size() - 1) * (dmax - dmin);
        }
    }
    density_attribute_id = attribute_list.size();
    attribute_list.push_back(att);

    printf("(MMM): UMAP attributes added\n");


    // Compute msc points and add attributes
    dimension_reduction.compute_morse_smale_complex();
    const pydr::vector_vectorf_t& msc = dimension_reduction.morse_smale_points();

    struct cmp {
        bool operator() (const QPointF& a, const QPointF& b) const {
            return  (a.x() < b.x()) || (a.x() == b.x() && a.y() < b.y());
        }
    };
    typedef std::set< QPointF, cmp> qpoint_set_t;

    qpoint_set_t msc_maxima;
    qpoint_set_t msc_minima;

    for (std::size_t i = 0; i < msc.size(); ++i) {
        //printf(" MSC [%d] == (%f, %f): type = (%f) \n", int(i), msc[i][0], msc[i][1], msc[i][2]);
        QPointF p(msc[i][1],msc[i][0]);
        if (msc[i][2] == 1.0f) {
            msc_maxima.insert(p);
        }
        else {
            msc_minima.insert(p);
        }
    }

    msc_peaks.clear();

    for (qpoint_set_t::const_iterator it = msc_maxima.begin(); it != msc_maxima.end(); ++it) {
        printf(" MSC max == (%f, %f) \n",  it->x(), it->y());
        msc_peaks.push_back(*it);
    }

    msc_valleys.clear();
    for (qpoint_set_t::const_iterator it = msc_minima.begin(); it != msc_minima.end(); ++it) {
        printf(" MSC min == (%f, %f) \n", it->x(), it->y());
        msc_valleys.push_back(*it);
    }

    std::vector< QPointF> nabla;
    density_to_gradient(density, w, nabla);
#if 0
    for (std::size_t i = 0; i < nabla.size(); ++i) {
        printf(" nabla[%d] = (%f, %f) \n", int(i), nabla[i].x(), nabla[i].y());
    }
#endif

    std::vector<float> xlim(2);
    xlim[0] = xmin;
    xlim[1] = xmax;
    std::vector<float> ylim(2);
    ylim[0] = ymin;
    ylim[1] = ymax;

    compute_msc_attributes(y, xlim, ylim, density, w, h, nabla, msc_peaks, msc_valleys);
 

    //exit(1);
}


void render_state::update_msc_attributes() {

    update_density_attributes();

    const std::vector<float>& density = dimension_reduction.density();

    // Compute msc points and add attributes
    dimension_reduction.compute_morse_smale_complex();

    printf("After msc computation \n");

    std::size_t w = dimension_reduction.width();
    std::size_t h = dimension_reduction.height();
    const pydr::vector_vectorf_t& msc = dimension_reduction.morse_smale_points();

    struct cmp {
        bool operator() (const QPointF& a, const QPointF& b) const {
            return  (a.x() < b.x()) || (a.x() == b.x() && a.y() < b.y());
        }
    };
    typedef std::set< QPointF, cmp> qpoint_set_t;

    qpoint_set_t msc_maxima;
    qpoint_set_t msc_minima;

    for (std::size_t i = 0; i < msc.size(); ++i) {
        //printf(" MSC [%d] == (%f, %f): type = (%f) \n", int(i), msc[i][0], msc[i][1], msc[i][2]);
        QPointF p(msc[i][0], msc[i][1]);
        if (msc[i][2] == 1.0f) {
            msc_maxima.insert(p);
        }
        else {
            msc_minima.insert(p);
        }
    }

    msc_peaks.clear();

    for (qpoint_set_t::const_iterator it = msc_maxima.begin(); it != msc_maxima.end(); ++it) {
        printf(" MSC max == (%f, %f) \n", it->x(), it->y());
        msc_peaks.push_back(*it);
    }

    msc_valleys.clear();
    for (qpoint_set_t::const_iterator it = msc_minima.begin(); it != msc_minima.end(); ++it) {
        printf(" MSC min == (%f, %f) \n", it->x(), it->y());
        msc_valleys.push_back(*it);
    }

    std::vector< QPointF> nabla;
    density_to_gradient(density, w, nabla);
#if 0
    for (std::size_t i = 0; i < nabla.size(); ++i) {
        printf(" nabla[%d] = (%f, %f) \n", int(i), nabla[i].x(), nabla[i].y());
    }
#endif
    const attribute_prop& att1 = attribute_list[current_attribute];
    const attribute_prop& att2 = attribute_list[current_second_attribute];

    bool interp1 = (att1.type == "scalar");
    if (!interp1) return;
    bool interp2 = (att2.type == "scalar");
    if (!interp2) return;

    bool log1 = false; // interp1 && (att1.scale == "logarithmic");
    bool log2 = false; // interp2 && (att2.scale == "logarithmic");

    float xmin = att1.knots[0];
    if (log1) xmin = log(xmin);
    float xmax = att1.knots[att1.knots.size() - 1];
    if (log1) xmax = log(xmax);

    float ymin = att2.knots[0];
    if (log2) ymin = log(ymin);
    float ymax = att2.knots[att2.knots.size() - 1];
    if (log2) ymax = log(xmax);

    // FIXME: check with logarithmic attributes
    std::vector< std::vector<float> > y;
    for (std::size_t n = 0; n < attribute.size(); ++n) {
        std::vector<float> p(2);
        p[0] = attribute[n][att1.name];
        p[1] = attribute[n][att2.name];
        y.push_back(p);
    }

    std::vector<float> xlim(2);
    xlim[0] = xmin;
    xlim[1] = xmax;
    std::vector<float> ylim(2);
    ylim[0] = ymin;
    ylim[1] = ymax;

    
    compute_msc_attributes(y, xlim, ylim, density, w, h, nabla, msc_peaks, msc_valleys);

}