#include <benchmark.hpp>
#include <random>
#include <histogram_generator.hpp>


void benchmark::run(render_state* rs) {
  
  if ( rs == nullptr) {
    printf(" Error: render state not existing. Benchmark failed \n");
    exit(-1);
  }

  printf("DATA_SET         : %s\n", rs->data_list[rs->current_data_file].c_str());
  printf("nLeafs           : %i\n", rs->data.get_nLeafs());
  printf("nLerpNodes       : %zi\n", rs->data.get_nLerpNodes());


  printf("Generating histograms: \n");
  {  
      typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
      histogram_generator hg(rs->glData.mipvolume());

      FILE* fp = fopen("histogram.csv","w");

      CTimer ct0;
      std::size_t nbins = 50;
      for (std::size_t i = 0; i < rs->attribute_list.size(); ++i) {
          attribute_prop att = rs->attribute_list[i];
          std::vector<double> v(rs->attribute.size());
          for (att_map_t::const_iterator it = rs->attribute.begin(); it != rs->attribute.end(); ++it)
              v[it->first] = (it->second).at(att.name.c_str());
          hg.update_attribute(v);
          std::vector<double> hist;
          hg.aggregate(nbins, hist, att.scale == "logarithmic");
          fprintf(fp,"%s", att.name.c_str());
          for (std::size_t b = 0; b < nbins; ++b) {
             fprintf(fp, ",%f", hist[b]);
          }
          fprintf(fp,"\n");
      }    
      fclose(fp);

      double dT = ct0.Query() * 1000.0f / double(rs->attribute_list.size());
      printf("took %.6fms\n", dT);
  }

  printf("sparse vector LUT: ");
  {
    const size_t nRuns = 32;
    CTimer ct0;
    for (size_t n = 0; n < nRuns; n++) rs->data.build_lookup_table(true, rs->glData.order());
    double dT = ct0.Query()*1000.0f / double(nRuns);
    printf("took %.6fms\n", dT);
  }

  printf("Range Queries with Error estimate\n");
  {
    std::mt19937 gen(0);
    static constexpr size_t nRuns = 100;
    for (size_t s = 8; s <= 8; s++) {
      uint32_t query[3] = { 1u << s,1u << s,1u << s };
      size_t nFetches = 0;
      sparse::vector<uint32_t, lux::errord> result;
      std::vector<lux::rangeu32_t> vR;
      for (size_t n = 0; n < nRuns; n++) {
	for (int i = 0; i < 3; i++) {
	  lux::rangeu32_t R;
	  std::uniform_int_distribution<uint32_t> D(0, rs->data.get_Dimension(i) - query[i]);
	  R.vmin = D(gen);
	  R.vmax = R.vmin + query[i] - 1;
	  vR.push_back(R);
	}
      }
      printf("  %i x %i x %i [%i voxels]: ", query[0], query[1], query[2], query[0] * query[1] * query[2]);
      CTimer ctQ;
      for (size_t n = 0; n < nRuns; n++) {
	size_t f = rs->data.query_range(vR[3 * n], vR[3 * n + 1], vR[3 * n + 2], result);
	nFetches += f;
      }
      double dT = ctQ.Query()*1000.0 / double(nRuns);
      printf(" %.4fms, %.2f fetches (footprint) ",dT,double(nFetches)/double(nRuns));

      std::vector<sparse::vector<uint32_t, lux::errord> > partial(omp_get_max_threads());
      ctQ.Reset();
      for (size_t n = 0; n < nRuns; n++) {
	for (size_t k = 0; k < partial.size(); k++) partial[k].clear();
#pragma omp parallel for schedule(dynamic,2)
	for (int z = int(vR[3 * n + 2].vmin); z < int(vR[3 * n + 2].vmax); z++) {
	  for (uint32_t y = vR[3 * n + 1].vmin; y < vR[3 * n + 1].vmax; y++) {
	    for (uint32_t x = vR[3 * n].vmin; x < vR[3 * n].vmax; x++) {
	      uint64_t id = rs->data.get_Voxel(x, y, z, 0);
	      partial[omp_get_thread_num()] += rs->data.lookup_node(id);
	    }
	  }
	}
	result.clear();
	for (size_t n = 0; n < partial.size(); n++) result += partial[n];
      }
      double dTn = ctQ.Query()*1000.0 / double(nRuns);
      printf("%.4fms (naive) speedup %.2fx mem %.2f%%\n", dTn,dTn/dT,
	     (double(query[0]*query[1]*query[2])-double(nFetches)/double(nRuns))/double(query[0]*query[1]*query[2])*100.0);
    }
  }


}
