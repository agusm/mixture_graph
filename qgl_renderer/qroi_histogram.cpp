#include <qroi_histogram.hpp>
#include <QPainter>
#include <QTimer>


qroi_histogram::qroi_histogram(render_state* rs,  QWidget *parent)
    : QWidget(parent)
{
  m_render_state = rs;
 
  setAttribute(Qt::WA_StaticContents);

  resize(600,300);

  QTimer * t = new QTimer(this);
  connect(t,SIGNAL(timeout()),this, SLOT(update()));
  t->start(50);

}


void qroi_histogram::paintEvent(QPaintEvent* event) {

    attribute_prop att = m_render_state->attribute_list[m_render_state->current_attribute];
    QString title = att.name.c_str();

    setWindowTitle(title);

    draw_histogram(att,  0.99f * height(), 0.9f * height(), 200);

}


void normalize(std::vector<double>& hist) {
    double hmax = *std::max_element(hist.begin(), hist.end());
    for (std::size_t i = 0; i < hist.size(); ++i) hist[i] /= hmax;
}

void qroi_histogram::draw_histogram(const attribute_prop& att, float y, float h, std::size_t nbins) {


    QPainter painter(this);

    QPen pen;
    bool logscale = (att.scale == "logarithmic");

    std::vector<double> hist;
    m_render_state->roi_hist.aggregate(nbins, hist, logscale);
    normalize(hist);

    float startx = 0.01f * width();
    float wtot = width() - 2.0f * startx;
    float wr = wtot / (nbins+1);

    lux::colormap CM(att.color_scheme);

    for (std::size_t b = 0; b < nbins; ++b) {
        float x = startx + float(b) / float(nbins - 1) * wtot;
        float hr = h * hist[b];

        lux::color col = CM(std::size_t(hist[b] * float(CM.size() - 1)));
        pen.setColor(QColor(255 * col(0), 255 * col(1), 255 * col(2)));
        painter.setPen(pen);
        QRect r(x, y - hr, wr, hr);
        painter.fillRect(r, QBrush(pen.color(), Qt::SolidPattern));
    }
}
