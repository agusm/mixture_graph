#ifndef _QDRAW_AREA_HPP_
#define _QDRAW_AREA_HPP_

#include <QWidget>
#include <render_state.hpp>

class qdraw_area: public QWidget {


Q_OBJECT

public:
  qdraw_area(render_state* rs = nullptr, QWidget *parent = nullptr);
  
  bool openImage(const QString &fileName);
  bool saveImage(const QString &fileName, const char *fileFormat);
  void setPenColor(const QColor &newColor);
  void setPenWidth(int newWidth);

  bool isModified() const { return render_state_->modified; }
  QColor penColor() const { return render_state_->pen_color; }
  int penWidth() const { return render_state_->pen_width; }

public Q_SLOTS:
  void clearImage();
 
protected:
  void mousePressEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;
  void paintEvent(QPaintEvent *event) override;
  void resizeEvent(QResizeEvent *event) override;

protected:
  void drawLineTo(const QPoint &endPoint);
  void resizeImage(QImage *image, const QSize &newSize);
  void draw_dirty_area(QPaintEvent* event);
  void draw_scatter_points();
  void draw_kde();
  void draw_filled_rectangle();
  void draw_color_gradient(const attribute_prop& att, float alpha = 1.0f, bool vertical = false);

  void draw_text(float x, float y, const QString& text);
  void draw_legend(const attribute_prop& att,  bool vertical = false);
  
protected:
  render_state* render_state_;

};


#endif
