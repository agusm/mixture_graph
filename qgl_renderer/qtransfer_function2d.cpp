#include <qtransfer_function2d.hpp>
#include <QPainter>
#include <QTimer>
#include <QMouseEvent>


qtransfer_function2d::qtransfer_function2d(render_state* rs,  QWidget *parent)
    : QWidget(parent)
{
  m_render_state = rs;
 
  setAttribute(Qt::WA_StaticContents);

  int width = 1024;
  int height = 1024;
  m_border_ratio = 0.01f;
  m_area_ratio = 0.7f;
  
  resize(width,height);

  m_draw_area_widget = new qdraw_area(rs, this);
  int draw_area_x = int( (1.0f - m_area_ratio - m_border_ratio) * width);
  int draw_area_y = int( m_border_ratio * height);
  int draw_area_w = int(m_area_ratio * width);
  int draw_area_h = int(m_area_ratio * height);

  m_draw_area_widget->move(QPoint(draw_area_x, draw_area_y));
  m_draw_area_widget->resize(draw_area_w, draw_area_h);


  m_color_scheme_popup = new qpopup_list(m_render_state->color_schemes);
  connect(m_color_scheme_popup, SIGNAL(changed_selection(int)), this, SLOT(change_color_scheme()));
 
  m_color_scheme_second_popup = new qpopup_list(m_render_state->color_schemes);
  connect(m_color_scheme_second_popup, SIGNAL(changed_selection(int)), this, SLOT(change_color_scheme_second()));

  std::vector<std::string> att_list_scalar;
  std::vector<std::size_t> att_idx_scalar;

  std::vector<std::string> att_list_nominal;
  std::vector<std::size_t> att_idx_nominal;

  for (std::size_t i = 0; i < m_render_state->attribute_list.size(); ++i) {
      if (m_render_state->attribute_list[i].type == "scalar") {
          att_list_scalar.push_back(m_render_state->attribute_list[i].name);
          att_idx_scalar.push_back(i);
      } else if (m_render_state->attribute_list[i].type == "nominal") {
          att_list_nominal.push_back(m_render_state->attribute_list[i].name);
          att_idx_nominal.push_back(i);
      }

  }

  m_scatter_style_popup = new qpopup_list(m_render_state->scatter_coloring_styles);
  m_scatter_style_popup->update_current(m_render_state->scatter_coloring_styles[m_render_state->scatter_coloring]);
  connect(m_scatter_style_popup, SIGNAL(changed_selection(int)), this, SLOT(change_scatter_style(int)));
  m_scatter_style_button = new QPushButton("Scatter",this);
  m_scatter_style_button->setGeometry(QRect(QPoint(0.05f * width, 0.75f * height), QPoint(0.15f * width, 0.81f * height)));
  m_scatter_style_button->setMenu(m_scatter_style_popup);


  m_area_style_popup = new qpopup_list(m_render_state->area_coloring_styles);
  m_area_style_popup->update_current(m_render_state->area_coloring_styles[m_render_state->area_coloring]);
  connect(m_area_style_popup, SIGNAL(changed_selection(int)), this, SLOT(change_area_style(int)));
  m_area_style_button = new QPushButton("Background", this);
  m_area_style_button->setGeometry(QRect(QPoint(0.16f * width, 0.75f * height), QPoint(0.26f * width, 0.81f * height)));
  m_area_style_button->setMenu(m_area_style_popup);

  m_attribute_popup = new qpopup_list(att_list_scalar, att_idx_scalar);
  m_attribute_popup->update_current(m_render_state->attribute_list[m_render_state->current_attribute].name);
  connect(m_attribute_popup, SIGNAL(changed_selection(int)), this, SLOT(change_attribute(int)));
  m_attribute_button = new QPushButton("X Att", this);
  m_attribute_button->setGeometry(QRect(QPoint(0.05f * width, 0.82f * height), QPoint(0.15f * width, 0.88f * height)));
  m_attribute_button->setMenu(m_attribute_popup);

  m_attribute_second_popup = new qpopup_list(att_list_scalar, att_idx_scalar);
  m_attribute_second_popup->update_current(m_render_state->attribute_list[m_render_state->current_second_attribute].name);
  connect(m_attribute_second_popup, SIGNAL(changed_selection(int)), this, SLOT(change_attribute_second(int)));
  m_second_attribute_button = new QPushButton("Y Att", this);
  m_second_attribute_button->setGeometry(QRect(QPoint(0.16f * width, 0.82f * height), QPoint(0.26f * width, 0.88f * height)));
  m_second_attribute_button->setMenu(m_attribute_second_popup);

  m_tf_style_popup = new qpopup_list(m_render_state->tf_styles);
  m_tf_style_popup->update_current(m_render_state->tf_styles[m_render_state->current_style]);
  connect(m_tf_style_popup, SIGNAL(changed_selection(int)), this, SLOT(change_tf_style(int)));
  m_tf_style_button = new QPushButton("TF type", this);
  m_tf_style_button->setGeometry(QRect(QPoint(0.05f * width, 0.89f * height), QPoint(0.15f * width, 0.96f * height)));
  m_tf_style_button->setMenu(m_tf_style_popup);

  m_attribute_nominal_popup = new qpopup_list(att_list_nominal, att_idx_nominal);
  m_attribute_nominal_popup->update_current(m_render_state->attribute_list[m_render_state->current_nominal_attribute].name);
  connect(m_attribute_nominal_popup, SIGNAL(changed_selection(int)), this, SLOT(change_attribute_nominal(int)));

  m_update_button = new QPushButton("Update", this);
  m_update_button->setGeometry(QRect(QPoint(0.16f * width, 0.89f * height), QPoint(0.26f * width, 0.96f * height)));

  connect(m_update_button, SIGNAL(clicked()), this, SLOT(update_attributes()));

  QTimer * t = new QTimer(this);
  connect(t,SIGNAL(timeout()),this, SLOT(update()));
  // FIXME --> really necessary??
  connect(t, SIGNAL(timeout()), m_draw_area_widget, SLOT(update()));
  t->start(50);

  m_active_color_knot = -1;
  m_active_second_color_knot = -1;

  m_active_alpha_knot = -1;
  m_active_second_alpha_knot = -1;
  m_active_peak_alpha = -1;

  m_active_alpha_height = -1;
  m_active_second_alpha_height = -1;

}


void qtransfer_function2d::change_attribute(int a) {
    m_render_state->current_attribute = a;
    //if (m_render_state->tf_styles[m_render_state->current_style] == "density")   m_render_state->recompute_density = true;
    m_render_state->update_transfer_function2d();
}



void qtransfer_function2d::change_color_scheme() {
    std::string tf_style = m_render_state->tf_styles[m_render_state->current_style];
    attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
    attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
    attribute_prop& attp = m_render_state->attribute_list[m_render_state->peak_attribute_id];
    attribute_prop& att = (tf_style == "density") ? attd : (tf_style == "msc-interp" ? attp : att1);
    att.color_scheme = m_color_scheme_popup->current_string();
    m_render_state->update_transfer_function2d();
}


void qtransfer_function2d::update_attributes() {
    std::string style = m_render_state->tf_styles[m_render_state->current_style];
    if (style == "density") { m_render_state->update_density_attributes(); }
    if (style == "msc-interp") { m_render_state->update_msc_attributes(); }
    m_render_state->update_transfer_function2d();
}


void qtransfer_function2d::change_attribute_second(int a) {
    m_render_state->current_second_attribute = a;
    //if (m_render_state->tf_styles[m_render_state->current_style] == "density")   m_render_state->recompute_density = true;
    m_render_state->update_transfer_function2d();
}

void qtransfer_function2d::change_attribute_nominal(int a) {
    m_render_state->current_nominal_attribute = a;
  //  m_render_state->update_transfer_function2d();
}


void qtransfer_function2d::change_color_scheme_second() {
    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
    att.color_scheme = m_color_scheme_second_popup->current_string();
    m_render_state->update_transfer_function2d();
}


void qtransfer_function2d::change_area_style(int a) {
    m_render_state->area_coloring = a;
    //m_render_state->update_transfer_function2d();
}

void qtransfer_function2d::change_scatter_style(int a) {
    m_render_state->scatter_coloring = a;
}

void qtransfer_function2d::change_tf_style(int a) {
    m_render_state->current_style = a;
    //if (m_render_state->tf_styles[m_render_state->current_style] == "density")   m_render_state->recompute_density = true;
    m_render_state->update_transfer_function2d();
}





void qtransfer_function2d::push_alpha_knot(attribute_prop& att, float xins) {

    std::map<float, float> alpha_map;
    float xprev = 0.0f;
    bool inserted = false;
    bool logarithmic = (att.scale == "logarithmic");

    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        float x = att.knots[k];
        float y = att.alpha[k];
        alpha_map[x] = y;
        if (!inserted) {
            if (x < xins) xprev = x;
            if (x > xins) {
                float yprev = alpha_map[xprev];
                float yins = (logarithmic ? yprev + (log(xins) - log(xprev)) * (y - yprev) / (log(x) - log(xprev)) : yprev + (xins - xprev) * (y - yprev) / (x - xprev) );
                alpha_map[xins] = yins;
                inserted = true;
            }
        }
    }
    att.knots.resize(alpha_map.size());
    att.alpha.resize(alpha_map.size());
    std::size_t k = 0;
    for (std::map<float, float>::const_iterator it = alpha_map.begin(); it != alpha_map.end(); ++it) {
        att.knots[k] = it->first;
        att.alpha[k] = it->second;
        ++k;
    }
}



void qtransfer_function2d::pop_alpha_knot(attribute_prop& att, std::size_t knot) {

    std::map<float, float> alpha_map;
    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        if (k != knot) {
            float x = att.knots[k];
            float y = att.alpha[k];
            alpha_map[x] = y;
        }
    }
    att.knots.resize(alpha_map.size());
    att.alpha.resize(alpha_map.size());
    std::size_t k = 0;
    for (std::map<float, float>::const_iterator it = alpha_map.begin(); it != alpha_map.end(); ++it) {
        att.knots[k] = it->first;
        att.alpha[k] = it->second;
        ++k;
    }
}



void qtransfer_function2d::mousePressEvent(QMouseEvent* event) {

    float x_thr_second = width() * (1.0f - m_area_ratio - 1.5f * m_border_ratio);
    float y_thr_second = height() * (m_area_ratio + 1.5f * m_border_ratio);

    float x_thr_first = x_thr_second;
    float y_thr_first = y_thr_second;

    float w_second = 0.5f * width() * (1.0f - m_area_ratio - 4.0f * m_border_ratio);
    float h_second = height() * m_area_ratio;

    float w_first = width() * m_area_ratio;
    float h_first = 0.5f * height() * (1.0f - m_area_ratio - 4.0f * m_border_ratio);

    float dw = 2.0f * width() * m_border_ratio;
    float dh = 2.0f * height() * m_border_ratio;


    float x_color_thr = 0.5f * x_thr_second;
    float y_color_thr = height() * (1.0f - 1.5f * m_border_ratio) - h_first;

    std::string tf_style = m_render_state->tf_styles[m_render_state->current_style];

    if (event->button() == Qt::LeftButton) {
        if (event->y() < y_thr_second && event->x() < x_thr_first) {
            //  printf("Left button: %d %d \n", event->x(), event->y());
            if (  tf_style == "attribute2d") {
                attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
                bool logarithmic = (att.scale == "logarithmic");
                float x_color_thr = 0.5f * x_thr_second;
                float y0 = height() * (m_border_ratio + m_area_ratio);

                if (event->x() > x_color_thr) {
                    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
                    for (std::size_t k = 1; k < att.color_knots.size() - 1; ++k) {
                        float yk = y0 - h_second * (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
                        float d = fabs(yk - event->y());
                        if (d < 10.0f) {
                            m_active_second_color_knot = k;
                            m_old_y = event->y();
                            break;
                        }
                    }
                }
                else {
                    float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
                    float x0 = width() * m_border_ratio;
                    for (std::size_t k = 0; k < att.knots.size(); ++k) {
                        float yk = y0 - h_second * (logarithmic ? log(att.knots[k]) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
                        float d = fabs(yk - event->y());
                        if (d < 10.0f) {
                            m_active_second_alpha_knot = k;
                            m_old_y = event->y();
                            break;
                        }
                    }
                    if (m_active_second_alpha_knot >= 0) {
                        float xk = x0 + w_second * (1.0f - att.alpha[m_active_second_alpha_knot]);
                        float d = fabs(xk - event->x());
                        if (d < 10.0f) {
                            m_active_second_alpha_height = m_active_second_alpha_knot;
                            m_active_second_alpha_knot = -1;
                            m_old_x = event->x();
                        }
                        if (m_active_second_alpha_knot == (att.knots.size() - 1) || m_active_second_alpha_knot == 0) {
                            m_active_second_alpha_knot = -1;
                        }
                    }
                    else {
                        float y = (logarithmic ? att.knots[0] * exp(float(y0 - event->y()) * range / h_second) : att.knots[0] + float(y0 - event->y()) * range / h_second);
                        push_alpha_knot(att, y);
                    }
                }
            }
        }
        else if (event->y() > y_thr_first && event->x() > x_thr_first) {
            attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
            attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
            attribute_prop& attp = m_render_state->attribute_list[m_render_state->peak_attribute_id];
            attribute_prop& atts = m_render_state->attribute_list[m_render_state->msc_sigma_attribute_id];

            
            float y_color_thr = height() * (1.0f - 1.5f * m_border_ratio) - h_first;
            float x0 = width() * (1.0f - m_border_ratio) - w_first;
            if (event->y() < y_color_thr) {
                if (tf_style == "msc-interp") {
                    attribute_prop& att = attp;
                    float dx = w_first / float(att.knots.size());
                    for (std::size_t k = 0; k < att.knots.size(); ++k) {
                        float xlo = x0 + k * dx;
                        float xhi = x0 + (k + 1) * dx;
                        float x = event->x();
                        if (xlo < x && x < xhi) {
                            m_active_peak_alpha = k;
                            m_old_y = event->y();
                            break;
                        }
                    }
                    printf(" Active alpha height = %d \n", m_active_peak_alpha);
                }
                else {
                    attribute_prop& att = (tf_style  == "density") ? attd : att1;
                    bool logarithmic = (att.scale == "logarithmic");
                    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
                    for (std::size_t k = 1; k < att.color_knots.size() - 1; ++k) {
                        float xk = x0 + w_first * (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
                        float d = fabs(xk - event->x());
                        if (d < 10.0f) {
                            m_active_color_knot = k;
                            m_old_x = event->x();
                            break;
                        }
                    }
                }
            }
            else {
                attribute_prop& att = (tf_style == "density") ? attd : (tf_style == "msc-interp" ? atts :att1 );
                bool logarithmic = (att.scale == "logarithmic");
                float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
                float y0 = height() * (1.0f - m_border_ratio);
                for (std::size_t k = 0; k < att.knots.size(); ++k) {
                    float xk = x0 + w_first * (logarithmic ? log(att.knots[k]) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
                    float d = fabs(xk - event->x());
                    if (d < 10.0f) {
                        m_active_alpha_knot = k;
                        m_old_x = event->x();
                        break;
                    }
                }
                if (m_active_alpha_knot >= 0) {
                    float yk = y0 - h_first * att.alpha[m_active_alpha_knot];
                    float d = fabs(yk - event->y());
                    if (d < 10.0f) {
                        m_active_alpha_height = m_active_alpha_knot;
                        m_active_alpha_knot = -1;
                        m_old_y = event->y();
                    }
                    if (m_active_alpha_knot == (att.knots.size() - 1) || m_active_alpha_knot == 0) {
                        m_active_alpha_knot = -1;
                    }
                }
                else {
                    float x = (logarithmic ? att.knots[0] * exp(float(event->x() - x0) * range / w_first) : att.knots[0] + float(event->x() - x0) * range / w_first);
                    push_alpha_knot(att, x);
                }
            }
        }
    }
    if (event->button() == Qt::RightButton) {
        if (event->y() < y_thr_second) {
            if (event->x() < x_color_thr) {
                attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
                bool logarithmic = (att.scale == "logarithmic");
                float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
                float y0 = height() * (m_border_ratio + m_area_ratio);

                for (std::size_t k = 1; k < att.knots.size() - 1; ++k) {
                    float yk = y0 - h_second * (logarithmic ? log(att.knots[k]) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
                    float d = fabs(yk - event->y());
                    if (d < 10.0f) {
                        pop_alpha_knot(att, k);
                        std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
                        m_render_state->update_transfer_function2d();
                    }
                }
            }
            else if (event->x() < x_thr_second) {
                if (m_render_state->tf_styles[m_render_state->current_style] == "attribute2d") {
                    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
                    m_color_scheme_second_popup->update_current(att.color_scheme);
                    m_color_scheme_second_popup->popup(event->globalPos());
                }
            }
            else if (event->x() < x_thr_second + 4.0f * dw && event->y() > 2.0f * dh) {
                if (m_render_state->tf_styles[m_render_state->current_style] == "attribute2d") {
                    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
                    m_attribute_second_popup->update_current(att.name);
                    m_attribute_second_popup->popup(event->globalPos());
                }
            }
            else if (event->x() < width() - 2.0f * dw) {
                if (event->y() < dh) {
                    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_nominal_attribute];
                    m_attribute_nominal_popup->update_current(att.name);
                    m_attribute_nominal_popup->popup(event->globalPos());
                }
                else if (event->y() > y_thr_second - dh) {
                    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
                    m_attribute_popup->update_current(att.name);
                    m_attribute_popup->popup(event->globalPos());
                }
            }
        }
        else if (event->x() > x_thr_first) {
            if (event->y() > y_color_thr) {
                attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
                attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
                attribute_prop& atts = m_render_state->attribute_list[m_render_state->msc_sigma_attribute_id];
                attribute_prop& att = (tf_style == "density") ? attd : ( tf_style == "msc-interp" ? atts : att1);
                bool logarithmic = (att.scale == "logarithmic");
                float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
                float x0 = width() * (1.0f - m_border_ratio) - w_first;
                for (std::size_t k = 1; k < att.knots.size() - 1; ++k) {
                    float xk = x0 + w_first * (logarithmic ? log(att.knots[k]) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
                    float d = fabs(xk - event->x());
                    if (d < 10.0f) {
                        pop_alpha_knot(att, k);
                        m_render_state->update_transfer_function2d();
                    }
                }
            }
            else {
                attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
                attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
                attribute_prop& atts = m_render_state->attribute_list[m_render_state->msc_sigma_attribute_id];
                attribute_prop& att = (tf_style == "density")? attd : (tf_style == "msc-interp" ? atts : att1);
                printf(" Changing color scheme for att %s \n", att.name);
                m_color_scheme_popup->update_current(att.color_scheme);
                m_color_scheme_popup->popup(event->globalPos());
            }
        }
    }
}



void qtransfer_function2d::mouseMoveEvent(QMouseEvent* event) {

    float w_second = 0.5f * width() * (1.0f - m_area_ratio - 4.0f * m_border_ratio);
    float h_second = height() * m_area_ratio;

    float w_first = width() * m_area_ratio;
    float h_first = 0.5f * height() * (1.0f - m_area_ratio - 4.0f * m_border_ratio);

    float dk = 0.0f;
    std::string tf_style = m_render_state->tf_styles[m_render_state->current_style];
    if (m_active_color_knot >= 0) {
        attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
        attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
        attribute_prop& att = (tf_style == "density")? attd :  att1;
        bool logarithmic = (att.scale == "logarithmic");

        float range = (logarithmic ?  log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
        dk =  range * float(event->x() - m_old_x) / w_first;
         if (logarithmic) {
            att.color_knots[m_active_color_knot] *= exp(dk);
            att.color_knots[m_active_color_knot] = std::max(att.color_knots[m_active_color_knot - 1] * exp(0.001f * range), std::min(att.color_knots[m_active_color_knot + 1] * exp(-0.001f * range), att.color_knots[m_active_color_knot]));

        }
        else {
            att.color_knots[m_active_color_knot] += dk;
            att.color_knots[m_active_color_knot] = std::max(att.color_knots[m_active_color_knot - 1] + 0.001f * range, std::min(att.color_knots[m_active_color_knot + 1] - 0.001f * range, att.color_knots[m_active_color_knot]));
        }
        printf("knot: %f, dk: %f \n", att.color_knots[m_active_color_knot], dk);
    }
    if (m_active_peak_alpha >= 0) {
        attribute_prop& att = m_render_state->attribute_list[m_render_state->peak_attribute_id];
        dk = float(m_old_y - event->y()) / h_first;
        if (att.alpha[m_active_peak_alpha] < 0.05f) dk *= 0.1f;
        att.alpha[m_active_peak_alpha] += dk;
        att.alpha[m_active_peak_alpha] = std::max(0.0f, std::min(0.999f, att.alpha[m_active_peak_alpha]));
        printf("alpha [%d] : %f, dk: %f \n", int(m_active_peak_alpha), att.alpha[m_active_peak_alpha], dk);
    }
    if (m_active_alpha_height >= 0) {
        attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
        attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
        attribute_prop& attp = m_render_state->attribute_list[m_render_state->peak_attribute_id];
        attribute_prop& atts = m_render_state->attribute_list[m_render_state->msc_sigma_attribute_id];

        attribute_prop& att = (tf_style == "density") ? attd : ( tf_style == "msc-interp"?  atts : att1 );
        dk =  float(m_old_y - event->y()) / h_first;
        if (att.alpha[m_active_alpha_height] < 0.05f) dk *= 0.1f;
        att.alpha[m_active_alpha_height] += dk;
        att.alpha[m_active_alpha_height] = std::max(0.0f, std::min(0.999f, att.alpha[m_active_alpha_height]));
        printf("alpha [%d] : %f, dk: %f \n", int(m_active_alpha_height), att.alpha[m_active_alpha_height], dk);
    }
    if (m_active_alpha_knot >= 0) {
        attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
        attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
        attribute_prop& atts = m_render_state->attribute_list[m_render_state->msc_sigma_attribute_id];
        attribute_prop& att = (tf_style == "density") ? attd : (tf_style == "msc-interp" ? atts : att1);
        bool logarithmic = (att.scale == "logarithmic");

        float range = (logarithmic ? log(att.knots[att.knots.size() - 1] ) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
        dk = range * float(event->x() - m_old_x) /w_first;
        if (logarithmic) {
            att.knots[m_active_alpha_knot] *= exp(dk);
            att.knots[m_active_alpha_knot] = std::max(att.knots[m_active_alpha_knot - 1] * exp(0.001f * range), std::min(att.knots[m_active_alpha_knot + 1] * exp(-0.001f * range), att.knots[m_active_alpha_knot]));

        } else {
            att.knots[m_active_alpha_knot] += dk;
            att.knots[m_active_alpha_knot] = std::max(att.knots[m_active_alpha_knot - 1] + 0.001f * range, std::min(att.knots[m_active_alpha_knot + 1] - 0.001f * range, att.knots[m_active_alpha_knot]));
        }
    }

    if (m_render_state->tf_styles[m_render_state->current_style] == "attribute2d") {
        if (m_active_second_color_knot >= 0) {
            attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
            bool logarithmic = (att.scale == "logarithmic");

            float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
            dk = range * float(m_old_y - event->y()) / h_second;
            if (logarithmic) {
                att.color_knots[m_active_second_color_knot] *= exp(dk);
                att.color_knots[m_active_second_color_knot] = std::max(att.color_knots[m_active_second_color_knot - 1]
                    * exp(0.001f * range), std::min(att.color_knots[m_active_second_color_knot + 1]
                        * exp(-0.001f * range), att.color_knots[m_active_second_color_knot]));

            }
            else {
                att.color_knots[m_active_second_color_knot] += dk;
                att.color_knots[m_active_second_color_knot] = std::max(att.color_knots[m_active_second_color_knot - 1] + 0.001f * range,
                    std::min(att.color_knots[m_active_second_color_knot + 1] - 0.001f * range, att.color_knots[m_active_second_color_knot]));
            }
            // printf("knot: %f, dk: %f \n", att.color_knots[m_active_color_knot], dk);

        }

        if (m_active_second_alpha_height >= 0) {
            attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
            if (m_active_second_alpha_height < att.alpha.size()) {
                dk = float(m_old_x - event->x()) / w_second;
                if (att.alpha[m_active_second_alpha_height] < 0.05f) dk *= 0.1f;
                att.alpha[m_active_second_alpha_height] += dk;
                att.alpha[m_active_second_alpha_height] = std::max(0.0f, std::min(0.999f, att.alpha[m_active_second_alpha_height]));
                printf("alpha: %f, dk: %f \n", att.alpha[m_active_second_alpha_height], dk);
            }
        }
        if (m_active_second_alpha_knot >= 0) {
            attribute_prop& att = m_render_state->attribute_list[m_render_state->current_second_attribute];
            if (m_active_second_alpha_knot < att.knots.size()) {
                bool logarithmic = (att.scale == "logarithmic");

                float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
                dk = range * float(m_old_y - event->y()) / h_second;
                if (logarithmic) {
                    att.knots[m_active_second_alpha_knot] *= exp(dk);
                    att.knots[m_active_second_alpha_knot] = std::max(att.knots[m_active_second_alpha_knot - 1] * exp(0.001f * range), std::min(att.knots[m_active_second_alpha_knot + 1] * exp(-0.001f * range), att.knots[m_active_second_alpha_knot]));
                }
                else {
                    att.knots[m_active_second_alpha_knot] += dk;
                    att.knots[m_active_second_alpha_knot] = std::max(att.knots[m_active_second_alpha_knot - 1] + 0.001f * range, std::min(att.knots[m_active_second_alpha_knot + 1] - 0.001f * range, att.knots[m_active_second_alpha_knot]));
                }
                printf("knot: %f, dk: %f \n", att.knots[m_active_second_alpha_height], dk);
            }
        }
    }

    m_old_x = event->x();
    m_old_y = event->y();

    if (dk != 0.0f ) {
        m_render_state->update_transfer_function2d();
    }
}
    
void qtransfer_function2d::mouseReleaseEvent(QMouseEvent* event) {
        m_active_color_knot = -1;
        m_active_alpha_height = -1;
        m_active_alpha_knot = -1;
        m_active_second_color_knot = -1;
        m_active_second_alpha_height = -1;
        m_active_peak_alpha = -1;
        m_active_second_alpha_knot = -1;
}
    


void qtransfer_function2d::draw_color_fill(const attribute_prop& att, float x, float y, float w, float  h) {

    QPainter painter(this);
    
    lux::colormap CM(att.color_scheme);

    bool inverse = att.inverse;
    bool periodic = att.periodic;
    float xmin = 0.025f * width();
    float xmax = 0.95f * width();
    //printf("Color knots size == %d", int(att.color_knots.size()));
    float dx = w / float(att.color_knots.size());
    painter.translate(QPoint(x, y));
    for (std::size_t k = 0; k < att.color_knots.size(); ++k) {
        std::size_t i = (periodic ? (k > CM.size() / 2 ? 2 * (CM.size() - 1 - k) : 2 * k) : k);
        i = (inverse ? CM.size() - 1 - i : i);
        lux::color c = CM(i);
        int r = c[0] * 255;
        int g = c[1] * 255;
        int b = c[2] * 255;
        QColor col(r, g, b, 255);
        //float x = xmin + float(k) * dx;
        painter.setBrush(col);
        //painter.setPen(c);
        painter.drawRect(0.0f, 0.0f, dx, h);
        painter.translate(QPoint(dx, 0.0f));
    }
}


void qtransfer_function2d::draw_alpha_levels(const attribute_prop& att, float x, float y, float w, float h) {

    QPainter painter(this);

    QPen pen;

    //float xmin = 0.025f * width();
    //float xmax = 0.95f * width();
    float dx = w / float(att.knots.size());

    painter.translate(QPoint(x, y));
    for (std::size_t k = 0; k < att.knots.size(); ++k) {

        if (m_active_alpha_height == k) {
            pen.setWidth(4);
            pen.setColor(Qt::yellow);
        }
        else {
            pen.setWidth(4);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);
        float x1 = dx;
        float x2 = x1 + dx;
        QPoint p0(0,  h);
        QPoint p1(0,  h * (1.0f - att.alpha[k]));
        QPoint p2(dx,  h * (1.0f - att.alpha[k]));
        QPoint p3(dx,  h);
        painter.drawLine(p0, p1);
        painter.drawLine(p1, p2);
        painter.drawLine(p2, p3);
        painter.drawLine(p3, p0);
        painter.translate(QPoint(dx, 0.0f));
    }
}


void qtransfer_function2d::draw_alpha_fill(const attribute_prop& att, float x, float y, float w, float  h) {

    QPainter painter(this);

    //float xmin = 0.025f;
    //float xmax = 0.95f;
    float dx = w / float(att.knots.size());
    painter.translate(QPoint(x, y));
    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        float alpha = att.alpha[k];
        int r = 0 * 255;
        int g = 0 * 255;
        int b = 0 * 255;
        int a = (1.0f - alpha) * 255;
        QColor c(r, g, b, a);
        //float x = xmin + float(k) * dx;
        painter.setBrush(c);
        //painter.setPen(c);
        painter.drawRect(0.0f, 0.0f, dx, h);
        painter.translate(QPoint(dx, 0.0f));
    }
}



void qtransfer_function2d::paintEvent(QPaintEvent* event) {

    
#if 0
    attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
    //QString title = att1.name.c_str();
    //if (att1.type == "vector") title += QString().setNum(m_render_state->current_attribute_index);

    attribute_prop& att2 = m_render_state->attribute_list[m_render_state->current_second_attribute];
    //title += QString(" vs ") + QString(att2.name.c_str());
#endif

    QString  style = m_render_state->tf_styles[m_render_state->current_style].c_str();

    QString title = style;

    setWindowTitle(title);


    const attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
    const attribute_prop& att2 = m_render_state->attribute_list[m_render_state->current_second_attribute];
    const attribute_prop& attd = m_render_state->attribute_list[m_render_state->density_attribute_id];
    const attribute_prop& atts = m_render_state->attribute_list[m_render_state->msc_sigma_attribute_id];
    const attribute_prop& attp = m_render_state->attribute_list[m_render_state->peak_attribute_id];
    const attribute_prop& attn = m_render_state->attribute_list[m_render_state->current_nominal_attribute];

    float br = m_border_ratio;
    float ar = m_area_ratio;

    float whr = 0.5f*(1.0f-ar-4.0f * br);

    float bw = br * width();
    float bh = br * height();
    float aw = ar * width();
    float ah = ar * height();
    float ww = aw;
    float wh = whr * height();

    float ax = 3.0f * bw + 2.0f * wh;
    float ay = bh;

#if 0
    //  Output colormap area
    if (m_render_state->area_coloring == render_state::area_coloring_t::KDE) {
        //draw_kde(ax,ay,aw,ah);
    } else if (m_render_state->area_coloring == render_state::area_coloring_t::COLORMAP) {
        draw_color_gradient(att1, ax, ay, aw, ah, 0.5f);
        draw_color_gradient(att2, ax, ay + ah, aw, ah, 0.5f, true);
    }
    else if (m_render_state->area_coloring == render_state::area_coloring_t::UNIFORM) {
        draw_filled_rectangle( ax, ay, aw, ah);
    }

    if (m_render_state->scatter_coloring != render_state::scatter_coloring_t::NONE)
        draw_scatter_points(ax, ay, aw, ah);
   
 //   draw_alpha_gradient(att1, ax, ay, aw, ah);
 //   draw_alpha_gradient(att2, ax, ay+ah, aw, ah, true);
    draw_text(ax + 0.4f*aw, ay + 2.0f*bh, attn.name);
   
    draw_legend(att1, ax, ay, aw, ah);
    draw_legend(att2, ax, ay+ah, aw, ah, true);

#endif 
   

    // first attribute
    float fx = ax;
    float fy = 2*bh + ah;

    if (style == "density") {
        draw_color_gradient(attd, fx, fy, ww, wh);
        draw_color_lines(attd, fx, fy, ww, wh);
    }
    else if (style == "msc-interp") {
        draw_color_fill(attp, fx, fy, ww, wh);
        draw_alpha_levels(attp, fx, fy, ww, wh);
        draw_alpha_fill(attp, fx, fy, ww, wh);
    }  else {
        draw_color_gradient(att1, fx, fy, ww, wh);
        draw_color_lines(att1, fx, fy, ww, wh);
    }


    fy += (bh + wh);
    if (m_render_state->tf_styles[m_render_state->current_style] == "density") {
        draw_alpha_gradient(attd, fx, fy, ww, wh);
        draw_alpha_lines(attd, fx, fy, ww, wh);
    }
    else if (style == "msc-interp") {
        draw_alpha_gradient(atts, fx, fy, ww, wh);
        draw_alpha_lines(atts, fx, fy, ww, wh);
    }
    else {
        draw_alpha_gradient(att1, fx, fy, ww, wh);
        draw_alpha_lines(att1, fx, fy, ww, wh);
        //draw_histogram(att1, fx, fy, ww, wh);
    }

    if (m_render_state->tf_styles[m_render_state->current_style] == "attribute2d") {
        
        float sx = wh + 2.0f * bw;
        float sy = bh + ah;

        // second attribute
        draw_color_gradient(att2, sx, sy, ww, wh, 1.0f, true);
        draw_color_lines(att2, sx, sy, ww, wh, true);
        sx = bw;
        draw_alpha_gradient(att2, sx, sy, ww, wh, true);
        draw_alpha_lines(att2, sx, sy, ww, wh, true);
        //draw_histogram(att2, sx, sy, ww, wh, true);
    }

}


void qtransfer_function2d::draw_kde(float x, float y, float w, float h) {

    // FIXME: Move to render state
    if (m_render_state->recompute_density) {
        attribute_prop& attx = m_render_state->attribute_list[m_render_state->current_attribute];
        attribute_prop& atty = m_render_state->attribute_list[m_render_state->current_second_attribute];

        std::vector<float> xlim(2);
        xlim[0] = attx.knots[0];
        xlim[1] = attx.knots[attx.knots.size() - 1];

        std::vector<float> ylim(2);
        ylim[0] = atty.knots[0];
        ylim[1] = atty.knots[atty.knots.size() - 1];

        std::vector< std::vector<float> > data;
        for (std::size_t n = 0; n < m_render_state->attribute.size(); ++n) {
            std::vector<float> p(2);
            p[0] = m_render_state->attribute[n][attx.name];
            p[1] = m_render_state->attribute[n][atty.name];
            data.push_back(p);
        }

        std::string fimage = "kde.png";
        m_render_state->dimension_reduction.kde(data, xlim, ylim, fimage, "plasma");
        m_render_state->draw_area.load(fimage.c_str());
        m_render_state->recompute_density = false;
    }
    
    QPainter painter(this);
    QPixmap pixmap = QPixmap::fromImage(m_render_state->draw_area);
    painter.drawPixmap(x,y,w,h, pixmap);
}


void qtransfer_function2d::draw_text(float x, float y, const std::string& text) {
    QPainter painter(this);
    QPen pen;
    pen.setColor(Qt::darkGray);
    painter.setPen(pen);
    painter.setFont(QFont("Helvetica", 14));
    painter.drawText(x, y, text.c_str());
}

void qtransfer_function2d::resizeEvent(QResizeEvent* event) {

}

void qtransfer_function2d::draw_alpha_gradient(const attribute_prop& att, float x, float y, float w, float h, bool vertical) {

    QPainter painter(this);

    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.knots[att.knots.size() - 1] ) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);

    QLinearGradient lin(0, 0, w, 0);
 
    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        float alpha = att.alpha[k];
        int r = 0 * 255;
        int g = 0 * 255;
        int b = 0 * 255;
        int a = (1.0f - alpha) * 255;
        float t = (logarithmic ? log(att.knots[k]) - log(att.knots[0]) :  att.knots[k] - att.knots[0]) / range;
        lin.setColorAt(t, QColor(r, g, b, a));
    }
    painter.setBrush(lin);
    painter.translate(QPoint(x, y));
    if (vertical) painter.rotate(-90.0f);
    painter.drawRect(0.0f, 0.0f, w, h);
}

void qtransfer_function2d::draw_filled_rectangle(float ax, float ay, float aw, float  ah) {
    QPainter painter(this);
    lux::color c = m_render_state->area_color;
    int r = c[0] * 255;
    int g = c[1] * 255;
    int b = c[2] * 255;
    QColor col(r, g, b, 255);
    painter.setBrush(col);
    painter.drawRect(ax, ay, aw, ah);
}

void qtransfer_function2d::draw_color_gradient(const attribute_prop& att, float x, float y, float w, float h, float alpha, bool vertical) {

    QPainter painter(this);
    QLinearGradient lin(0, 0, w, 0);

    lux::colormap CM(att.color_scheme);
 
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1] ) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

    bool inverse = att.inverse;
    bool periodic = att.periodic;

    for (std::size_t k = 0; k < att.color_knots.size(); ++k) {
        std::size_t i = (periodic ? (k > CM.size() / 2 ? 2 * (CM.size() - 1 - k) : 2 * k) : k);
        i = (inverse ? CM.size() - 1 - i : i);
        lux::color c = CM(i);
        int r = c[0] * 255;
        int g = c[1] * 255;
        int b = c[2] * 255;
        int a = alpha * 255;
        float t = (logarithmic ?  log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
        lin.setColorAt(t, QColor(r, g, b, a));
    }
    painter.setBrush(lin);
    painter.translate(QPoint(x, y));
    if (vertical) painter.rotate(-90.0f);
    painter.drawRect(0.0f, 0.0f, w, h);
    //if (vertical) painter.rotate(90);
}


void qtransfer_function2d::draw_scatter_points(float x, float y, float w, float h) {

    QPainter painter(this);
    painter.translate(x, y);
    float glyph_size = std::min(w, h) / 100.0f;

    std::vector< QColor > color;
    const attribute_prop& attn = m_render_state->attribute_list[m_render_state->current_nominal_attribute];

    //QImage draw_area;
    std::string scatter_coloring = m_render_state->scatter_coloring_styles[m_render_state->scatter_coloring];

    std::string style = m_render_state->tf_styles[m_render_state->current_style];


    if (scatter_coloring == "constant") {
        int r = int(m_render_state->scatter_color[0] * 255);
        int g = int(m_render_state->scatter_color[1] * 255);
        int b = int(m_render_state->scatter_color[2] * 255);
        painter.setPen(QPen(QColor(r, g, b, 255), int(floor(glyph_size))));
    }
    else if (scatter_coloring == "nominal") {    
        lux::colormap CM(attn.color_scheme);
        for (std::size_t n = 0; n < CM.size(); ++n) {
            int r = int(CM[n][0] * 255);
            int g = int(CM[n][1] * 255);
            int b = int(CM[n][2] * 255);
            color.push_back(QColor(r, g, b, 255));
        }
    }
   
    const attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
    const attribute_prop& att2 = m_render_state->attribute_list[m_render_state->current_second_attribute];

    bool xlog = (att1.scale == "logarithmic");
    float xrange = (xlog ? log(att1.color_knots[att1.color_knots.size() - 1]) - log(att1.color_knots[0]) : att1.color_knots[att1.color_knots.size() - 1] - att1.color_knots[0]);

    bool ylog = (att2.scale == "logarithmic");
    float yrange = (ylog ? log(att2.color_knots[att2.color_knots.size() - 1]) - log(att2.color_knots[0]) : att2.color_knots[att2.color_knots.size() - 1] - att2.color_knots[0]);
 
    for (std::size_t n = 0; n < m_render_state->attribute.size(); ++n) {
        float cx = (xlog ? log(m_render_state->attribute[n][att1.name]) - log(att1.knots[0]) : m_render_state->attribute[n][att1.name] - att1.knots[0]) / xrange;
        float cy = (1.0f - (ylog ? log(m_render_state->attribute[n][att2.name]) - log(att2.knots[0]) : m_render_state->attribute[n][att2.name] - att2.knots[0]) / yrange);
        if (scatter_coloring == "nominal") {
            painter.setPen(QPen(color[m_render_state->attribute[n][attn.name]], int(floor(glyph_size))));
        }
        else if (scatter_coloring == "tf") {
            lux::color c = m_render_state->glData.get_leaf_color(n);
            painter.setPen(QPen(QColor( int(c[0]*255.0), int(c[1]*255.0), int(c[2]*255.0)), floor(glyph_size)));
        }
        painter.drawPoint(w * cx, h * cy);
    }
#if 0
    else if (scatter_coloring == "area") {
        draw_area = m_render_state->draw_area;
    }


    bool xlog = (att1.scale == "logarithmic");
    float xrange = (xlog ? log(att1.color_knots[att1.color_knots.size() - 1]) - log(att1.color_knots[0]) : att1.color_knots[att1.color_knots.size() - 1] - att1.color_knots[0]);

    bool ylog = (att2.scale == "logarithmic");
    float yrange = (ylog ? log(att2.color_knots[att2.color_knots.size() - 1]) - log(att2.color_knots[0]) : att2.color_knots[att2.color_knots.size() - 1] - att2.color_knots[0]);

    for (std::size_t n = 0; n < m_render_state->attribute.size(); ++n) {
        float cx = (xlog ? log(m_render_state->attribute[n][att1.name]) - log(att1.knots[0]) : m_render_state->attribute[n][att1.name] - att1.knots[0]) / xrange;
        float cy = (1.0f-(ylog ? log(m_render_state->attribute[n][att2.name]) - log(att2.knots[0]) : m_render_state->attribute[n][att2.name] - att2.knots[0]) / yrange);
        if (scatter_coloring == "nominal") {
            painter.setPen(QPen(color[m_render_state->attribute[n][attn.name]], int(floor(glyph_size))));
        }
        else if (scatter_coloring == "area" && draw_area.width() > 0 && draw_area.height()) {
            painter.setPen(QPen( draw_area.pixelColor(int(cx*(draw_area.width()-1)),int(cy*(draw_area.height()-1))),floor(glyph_size)));
        }  
        painter.drawPoint(w*cx, h*cy);
    }
#endif
}


void qtransfer_function2d::draw_histogram(const attribute_prop& att, float x, float y, float w, float h, bool vertical) {
    QPainter painter(this);

    QPen pen;
    bool logscale = (att.scale == "logarithmic");

    pen.setWidth( 4 );
    pen.setColor(Qt::magenta);
    painter.setPen(pen);
    
    const double max_height = 0.2;
    std::vector<double>::const_iterator it = (vertical ? m_render_state->att_hist_second.begin() : m_render_state->att_hist.begin());
    std::size_t nbins = (vertical ? m_render_state->att_hist_second.size() : m_render_state->att_hist.size());
    double max_val = 0.0;
    for (std::size_t b = 1; b < nbins; ++b) {
        max_val = std::max(max_val, *it);
        ++it;
    }

    //printf("Hist max val == %f \n", max_val);

    painter.translate(QPoint(x, y));
    if (vertical) painter.rotate(-90);
    for (std::size_t b = 1; b < nbins; ++b) {
        float t = float(b-1) / float(nbins - 1);
        QPoint p1(w * t, h * (1.0f - *it)); // / max_val * max_height));
        t = float(b) / float(nbins - 1);
        ++it;
        QPoint p2(w * t, h * (1.0f - *it)); // / max_val * max_height));
        painter.drawLine(p1, p2);
    }
}


void qtransfer_function2d::draw_alpha_lines(const attribute_prop& att, float x, float y, float w, float h, bool vertical) {

    QPainter painter(this);
    painter.translate(QPoint(x, y));
    if (vertical) painter.rotate(-90);


    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);

    for (std::size_t k = 1; k < (att.knots.size() - 1); ++k) {
 
        bool active = (vertical ? m_active_second_alpha_knot == k :  m_active_alpha_knot == k);
        if (active) {
            pen.setWidth(4);
            pen.setColor(Qt::yellow);
        }
        else {
            pen.setWidth(2);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);

        float t = (logarithmic ? log(att.knots[k] ) - log(att.knots[0] ) : att.knots[k] - att.knots[0] ) / range;
        QPoint p1  = QPoint(w*t, 0.0f);
        QPoint p2  = QPoint(w*t, h);
        if (vertical) {
            if (m_active_second_alpha_knot == k) {
                painter.setFont(QFont("Helvetica", 12));
                painter.drawText(p1.x(), h / 2.0f, QString().setNum(att.knots[k], 'g', 3));
            }
        }
        else {
            if (m_active_alpha_knot == k) {
                painter.setFont(QFont("Helvetica", 12));
                painter.drawText(p1.x(), h / 2.0f, QString().setNum(att.knots[k], 'g', 3));
            }
        }
        painter.drawLine(p1, p2);
    }

    for (std::size_t k = 1; k < att.knots.size(); ++k) {

        float t1 = (logarithmic ? log(att.knots[k - 1]) - log(att.knots[0]) : att.knots[k - 1] - att.knots[0]) / range;
        QPoint p1 =  QPoint(w*t1, h*(1-att.alpha[k-1])); 
        float t2 = (logarithmic ? log(att.knots[k] ) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
        QPoint p2 = QPoint(w*t2,  h*(1 - att.alpha[k]));

        bool active = (m_active_alpha_height >= 0 && (m_active_alpha_height == k || m_active_alpha_height == (k - 1)));
        if ( vertical) active =  (m_active_second_alpha_height >= 0 && (m_active_second_alpha_height == k || m_active_second_alpha_height == (k - 1)));

        if (active) {
            pen.setWidth(4);
            pen.setColor(Qt::green);
        }
        else {
            pen.setWidth(2);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);

        if (vertical) {
            if (m_active_second_alpha_height == (k-1) ) {
                painter.setFont(QFont("Helvetica", 12));
                painter.drawText(p1.x(), p1.y(), QString().setNum(att.alpha[k-1], 'g', 3));
            }
        }
        else {
            if (m_active_alpha_height == (k-1) ) {
                painter.setFont(QFont("Helvetica", 12));
                painter.drawText(p1.x(), p1.y(), QString().setNum(att.alpha[k-1], 'g', 3));
            }
        }
        painter.drawLine(p1, p2);
    }

}
    
void qtransfer_function2d::draw_legend(const attribute_prop& att, float x, float y, float w, float h, bool vertical) {

    QPainter painter(this);
    painter.translate(QPoint(x, y));
    if (vertical) painter.rotate(-90);

    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1] ) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
    pen.setColor(Qt::darkGray);
    painter.setPen(pen);
    painter.setFont(QFont("Helvetica", 8));

    float y_leg = (vertical ? 0.03f*h : 0.99f *h);

    painter.drawText(0.05f*w, y_leg, QString().setNum(att.color_knots[0], 'g', 2));

    for (std::size_t k = 1; k < 4; ++k) { 
        float t =  w * float(k) / float(4);
        QPoint p1(t, 0.0f);
        QPoint p2(t, h);
        painter.drawLine(p1, p2);
        float val = (logarithmic ? att.color_knots[0] * exp(float(k) / float(4) * range) : att.color_knots[0] + float(k) / float(4) * range);
        painter.drawText(t, y_leg, QString().setNum(val, 'g', 2));
    }
    float t = 0.9f * w;
    painter.drawText(t, y_leg, QString().setNum(att.color_knots[att.color_knots.size() - 1], 'g', 2));

    float y_name = (vertical ? 0.1f * h : 0.97f * h);
    painter.setFont(QFont("Helvetica", 14));
    painter.drawText(0.4f*w, y_name, att.name.c_str());
}


void qtransfer_function2d::draw_color_lines(const attribute_prop& att, float x, float y, float w, float h, bool vertical) {
    
    QPainter painter(this);
    painter.translate(QPoint(x, y));
    if (vertical) painter.rotate(-90);

    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1] ) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

    for (std::size_t k = 1; k < (att.color_knots.size()-1); ++k) {

        bool active = (vertical ? m_active_second_color_knot == k : m_active_color_knot == k);

        if (active) {
            pen.setWidth(4);
            pen.setColor(Qt::yellow);
        }
        else {
            pen.setWidth(1);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);
        QPoint p1( w* (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range, 0.0f);
        QPoint p2( w * (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range, h);
        if (vertical) {
            if (m_active_second_color_knot == k) {
                painter.setFont(QFont("Helvetica", 12));
                painter.drawText(p1.x(), h/2.0f, QString().setNum(att.color_knots[k], 'g', 3));
            }
        }
        else {
            if (m_active_color_knot == k) {
                painter.setFont(QFont("Helvetica", 12));
                painter.drawText(p1.x(), h / 2.0f, QString().setNum(att.color_knots[k], 'g', 3));
            }
        }
        painter.drawLine(p1,p2);
    }

}



void qtransfer_function2d::keyPressEvent(QKeyEvent* ev) {
    switch (ev->key()) {
    case Qt::Key_H: {
        m_render_state->render_help = !m_render_state->render_help;
        if (m_render_state->render_help) {
            for (std::size_t s = 0; s < m_render_state->help_text.size(); s++) {
                printf("%s\n", m_render_state->help_text[s].c_str());
            }
        }
    } break;
    case Qt::Key_P: {
        m_render_state->scatter_coloring = (m_render_state->scatter_coloring + 1)% m_render_state->scatter_coloring_styles.size();
        printf("Scatter coloring: %s \n", m_render_state->scatter_coloring_styles[m_render_state->scatter_coloring].c_str());
    } break;
    case Qt::Key_R: {
        m_render_state->area_coloring = (m_render_state->area_coloring + 1 )% m_render_state->area_coloring_styles.size();  
        printf("Area coloring: %s \n", m_render_state->area_coloring_styles[m_render_state->area_coloring].c_str());
    } break;
    case Qt::Key_O: {
        m_render_state->current_lao_directions = (m_render_state->current_lao_directions + 1) % m_render_state->lao_directions.size();
        m_render_state->glData.lao_directions() = (m_render_state->lao_directions[m_render_state->current_lao_directions]);
        printf("Local ambient occlusion: %s \n", (m_render_state->glData.lao_directions() ? "ON" : "OFF"));
        if (m_render_state->glData.lao_directions()) printf("Directions: %d \n", m_render_state->glData.lao_directions());
    } break;
    case Qt::Key_N: {
        m_render_state->glData.enable_shading() = !(m_render_state->glData.enable_shading());
        printf("Diffuse shading: %s\n", (m_render_state->glData.enable_shading() ? "ON" : "OFF"));
        if (m_render_state->glData.enable_shading()) printf(" Light dir: %.2f %.2f %.2f \n", m_render_state->glData.light_dir()[0],
            m_render_state->glData.light_dir()[1], m_render_state->glData.light_dir()[2]);
    } break;
    case Qt::Key_S: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);

        m_render_state->current_color_scheme = (m_render_state->current_color_scheme + 1) % m_render_state->color_schemes.size();
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.color_scheme = m_render_state->color_schemes[m_render_state->current_color_scheme];
        lux::colormap CM(att.color_scheme);// ("YlOrRd9");
        if (CM.size() != att.color_knots.size()) {
            bool logarithmic = (att.scale == "logarithmic");
            float b = att.color_knots[0];
            float e = att.color_knots[att.color_knots.size() - 1];
            float r = (logarithmic ? log(e) - log(b) : e - b);
            att.color_knots.resize(CM.size());
            for (std::size_t i = 0; i < CM.size(); ++i) {
                att.color_knots[i] = (logarithmic ? b * exp(float(i) / float(CM.size() - 1) * r) : b + r * float(i) / float(CM.size() - 1));
            }
        }
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;

    case Qt::Key_A: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);

        typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
        if (update_second_attribute) {
            m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
        }
        else {
            m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
        }

        if (m_render_state->enable_second_attribute && m_render_state->current_attribute == m_render_state->current_second_attribute) {
            if (update_second_attribute) {
                m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
            }
            else {
                m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
            }
        }
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        //     std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
        std::vector<double> v(m_render_state->attribute.size());
        for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
            v[it->first] = (it->second).at(att.name.c_str());

        if (!update_second_attribute && m_render_state->roi_enabled) {
            m_render_state->roi_hist.update_attribute(v);
        }

        m_render_state->hist_gen.update_attribute(v);
        if (update_second_attribute) {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att.scale == "logarithmic");
        }
        else {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att.scale == "logarithmic");
        }

        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;


    case Qt::Key_J: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::unordered_map< std::size_t, std::unordered_map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.inverse = !att.inverse;
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_K: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::unordered_map< std::size_t, std::unordered_map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.periodic = !att.periodic;
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_L: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.scale = (att.scale == "linear" ? "logarithmic" : "linear");
        //     std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
        std::vector<double> v(m_render_state->attribute.size());
        for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
            v[it->first] = (it->second).at(att.name.c_str());
        m_render_state->hist_gen.update_attribute(v);
        if (update_second_attribute) {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att.scale == "logarithmic");
        }
        else {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att.scale == "logarithmic");
        }
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_Escape: exit(0);
    }

}