#ifndef _QTRANSFER_FUNCTION2D_HPP_
#define _QTRANSFER_FUNCTION2D_HPP_

#include <QWidget>
#include <render_state.hpp>
#include <histogram_generator.hpp>
#include <qpopup_list.hpp>
#include <QPushButton>
#include <qdraw_area.hpp>

class qtransfer_function2d : public QWidget
{
    Q_OBJECT

public:
    qtransfer_function2d(render_state* rs = nullptr,  QWidget *parent = nullptr);


public Q_SLOTS:
    void change_color_scheme();
    void change_attribute(int a);
    void change_color_scheme_second();
    void change_attribute_second(int a);
    void change_attribute_nominal(int a);
    void change_area_style(int a);
    void change_scatter_style(int a);
    void change_tf_style(int a);
    void update_attributes();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void keyPressEvent(QKeyEvent* ev) override;

protected:
    void draw_histogram(const attribute_prop& att, float x, float y, float w, float h,  bool vertical = false);

    void draw_text(float x, float y, const std::string&);
    void draw_filled_rectangle(float ax, float ay, float aw, float  ah);
    void draw_alpha_lines(const attribute_prop& att, float x, float y, float w, float h, bool vertical = false);
    void draw_alpha_gradient(const attribute_prop& att, float x, float y, float w, float h, bool vertical = false);
    void draw_legend(const attribute_prop& att, float x, float y, float w, float h, bool vertical = false);
    void draw_color_gradient(const attribute_prop& att, float x, float y, float w, float h, float alpha = 1.0f, bool vertical = false);
    void draw_color_lines(const attribute_prop& att, float x, float y, float w, float h, bool vertical = false);
    
    void push_alpha_knot(attribute_prop& att, float x);
    void pop_alpha_knot(attribute_prop& att, std::size_t knot);

    void draw_scatter_points(float x, float y, float w, float h);
    void draw_kde(float x, float y, float w, float h);

    void draw_color_fill(const attribute_prop& att, float x, float y, float w, float  h);
    void draw_alpha_fill(const attribute_prop& att, float x, float y, float w,  float  h);
    void draw_alpha_levels(const attribute_prop& att, float x, float y, float w, float h);

protected:
  
  render_state*  m_render_state;
  
  qpopup_list* m_color_scheme_popup;
  qpopup_list* m_attribute_popup;

  qpopup_list* m_color_scheme_second_popup;
  qpopup_list* m_attribute_second_popup;

  qpopup_list* m_attribute_nominal_popup;

  qdraw_area* m_draw_area_widget;

  qpopup_list* m_scatter_style_popup;
  QPushButton* m_scatter_style_button;
  qpopup_list* m_area_style_popup;
  QPushButton* m_area_style_button;

  QPushButton* m_attribute_button;
  QPushButton* m_second_attribute_button;

  QPushButton* m_tf_style_button;
  qpopup_list* m_tf_style_popup;

  QPushButton* m_update_button;

  int   m_active_color_knot;
  int   m_active_second_color_knot;

  int m_active_alpha_knot;
  int m_active_second_alpha_knot;

  int         m_active_alpha_height;
  int         m_active_second_alpha_height;
  int         m_active_peak_alpha;


  int m_old_x;
  int m_old_y;

  // design coefficients
  float m_border_ratio;
  float m_area_ratio;

};





#endif
