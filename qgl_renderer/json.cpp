
#include <json.hpp>
#include <QJsonArray>
#include <QFile>
#include <QJsonDocument>

bool json::parse_file(QJsonObject& json, const std::string& json_file) {

    QFile load_file(json_file.c_str());
    if (!load_file.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open json file %s", json_file.c_str());
        return false;
    }

    QByteArray file_data = load_file.readAll();

    QJsonParseError err;
    QJsonDocument json_doc = QJsonDocument::fromJson(file_data, &err);

    if (err.error != QJsonParseError::NoError) {
        printf("error = %s \n", err.errorString().toLatin1().constData());
        return false;
    }

    json = json_doc.object();

    for (QJsonObject::const_iterator it = json.begin(); it != json.end(); ++it) {
        printf("key: %s \n", it.key().toLatin1().constData());
    }
    return true;
}


void json::write_vector(QJsonObject& obj, const QString& key, const std::vector<std::string>& v) {
    QJsonArray arr;
    for (std::size_t i = 0; i < v.size(); ++i) {
        arr.append(QString(v[i].c_str()));
    }
    obj[key] = arr;
}


void json::write_vector(QJsonObject& obj, const QString& key, const std::vector<float>& v) {
    QJsonArray arr;
    for (std::size_t i = 0; i < v.size(); ++i) {
         arr.append(v[i]);
    }
    obj[key] = arr;
}

void json::write_vector(QJsonObject& obj, const QString& key, const std::vector<double>& v) {
    QJsonArray arr;
    for (std::size_t i = 0; i < v.size(); ++i) {
        arr.append(v[i]);
    }
    obj[key] = arr;
}

void json::write_vector(QJsonObject& obj, const QString& key, const std::vector<int>& v) {
    QJsonArray arr;
    for (std::size_t i = 0; i < v.size(); ++i) {
        arr.append(v[i]);
    }
    obj[key] = arr;
}

void json::write_vector(QJsonObject& obj, const QString& key, const std::vector<std::size_t>& v) {
    QJsonArray arr;
    for (std::size_t i = 0; i < v.size(); ++i) {
        arr.append(qint64(v[i]));
    }
    obj[key] = arr;
}


void json::write_value(QJsonObject& obj, const QString& key, const std::string& v) {
    obj[key] = QString(v.c_str());
}

void json::write_value(QJsonObject& obj, const QString& key, double v) {
    obj[key] = v;
}

void json::write_value(QJsonObject& obj, const QString& key, int v) {
    obj[key] = v;
}

void json::write_value(QJsonObject& obj, const QString& key, bool v) {
    obj[key] = v;
}


void json::write_range(QJsonObject& obj, const QString& key, const lux::range_t<float>& v) {
    QJsonArray arr;
    arr.append(v.vmin);
    arr.append(v.vmax);
    obj[key] = arr;
}



bool json::read_range(const QJsonObject& obj, const QString& key, lux::range_t<float>& range) {
    bool result =  true;
    if (obj.contains(key)) {
        if (obj[key].isArray()) {
            QJsonArray r = obj[key].toArray();
            if (r.size() == 2) {
                range.vmin = r.at(0).toDouble();
                range.vmax = r.at(1).toDouble();
            }
            else {
                printf(" warning: too long range at %s \n", key.toLatin1().constData());
                result = false;
            }
        }
        else {
            printf("warning: key %s is not array \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;

    }
    return result;
}

bool json::read_value(const QJsonObject& obj, const QString& key, std::string& str) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isString()) {
            str = obj[key].toString().toLatin1();
        }
        else {
            printf("warning: key %s is not string \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;
    }
    return result;
}


bool json::read_value(const QJsonObject& obj, const QString& key, double& f) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isDouble()) {
            f = obj[key].toDouble();
        }
        else {
            printf("warning: key %s is not value \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;
    }
    return result;
}

bool json::read_value(const QJsonObject& obj, const QString& key, bool& f) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isBool()) {
            f = obj[key].toBool();
        }
        else {
            printf("warning: key %s is not value \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;
    }
    return result;
}



bool json::read_value(const QJsonObject& obj, const QString& key, int& f) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isDouble()) {
            f = obj[key].toInt();
        }
        else {
            printf("warning: key %s is not value \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;
    }
    return result;
}

bool json::read_vector(const QJsonObject& obj, const QString& key, std::vector<double>& v) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isArray()) {
            QJsonArray arr = obj[key].toArray();
            v.clear();
            v.resize(arr.size());
            for( std::size_t i = 0; i < v.size(); ++i) {
                v[i] = arr.at(i).toDouble();
            }
        }
        else {
            printf("warning: key %s is not array \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;

    }
    return result;
}

bool json::read_vector(const QJsonObject& obj, const QString& key, std::vector<float>& v) {
    std::vector<double> vdouble;
    bool result = read_vector(obj, key, vdouble);
    if (result) {
        v.resize(vdouble.size());
        std::copy(vdouble.begin(), vdouble.end(), v.begin());
    }
    return result;
}




bool json::read_vector(const QJsonObject& obj, const QString& key, std::vector<int>& v) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isArray()) {
            QJsonArray arr = obj[key].toArray();
            v.clear();
            v.resize(arr.size());
            for (std::size_t i = 0; i < v.size(); ++i) {
                v[i] = arr.at(i).toInt();
            }
        }
        else {
            printf("warning: key %s is not array \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;

    }
    return result;
}

bool json::read_vector(const QJsonObject& obj, const QString& key, std::vector<std::size_t>& v) {
    std::vector<int> vint;
    bool result = read_vector(obj, key, vint);
    if (result) {
        v.resize(vint.size());
        std::copy(vint.begin(), vint.end(), v.begin());
    }
    return result;
}


bool json::read_vector(const QJsonObject& obj, const QString& key, std::vector<std::string>& v) {
    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isArray()) {
            QJsonArray arr = obj[key].toArray();
            v.clear();
            v.resize(arr.size());
            for (std::size_t i = 0; i < v.size(); ++i) {
                v[i] = arr.at(i).toString().toLatin1();
            }
        }
        else {
            printf("warning: key %s is not array \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;

    }
    return result;
}

bool json::read_vector(const QJsonObject& obj, const QString& key, std::vector<QJsonObject>& v) {

    bool result = true;
    if (obj.contains(key)) {
        if (obj[key].isArray()) {
            QJsonArray arr = obj[key].toArray();
            v.clear();
            v.resize(arr.size());
            for (std::size_t i = 0; i < v.size(); ++i) {
                v[i] = arr.at(i).toObject();
            }
        }
        else {
            printf("warning: key %s is not array \n", key.toLatin1().constData());
            result = false;
        }
    }
    else {
        printf("warning: key %s is not existing \n", key.toLatin1().constData());
        result = false;

    }
    return result;
}
