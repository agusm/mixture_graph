

#include <render_state.hpp>
#include <qgl_volume_renderer_window.hpp>
#include <QApplication>
#include <iostream>
#include <WinUser.h>

extern "C" { _declspec(dllexport) DWORD NvOptimusEnablement = 1; }

std::unordered_map<std::string, std::vector<lux::color> > lux::colormap::m_database;

int main(int argc, char **argv)
{
    SetProcessDPIAware();
    std::string config_json = "config.json";

    QApplication::setAttribute(Qt::AA_UseDesktopOpenGL);

    QApplication app(argc, argv);
 
    std::string data_json = "";
    if (argc > 1) {
        data_json = std::string(argv[1]);
    }

    render_state rs(data_json);
    rs.read_config_json(config_json);

    qgl_volume_renderer_window window(&rs); 
    window.setFormat(rs.format);

    window.start();
    window.show();
    return app.exec();
}

