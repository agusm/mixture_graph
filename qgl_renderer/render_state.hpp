#ifndef _RENDER_STATE_HPP
#define _RENDER_STATE_HPP

#include <GL/glew.h>
#include"lux.h"
#include"shaders.h"
#include"compressed_mipvolume.h"
#include"md5.h"
#include<omp.h>
#include<functional>
#include"glVolume.h"
#include"flags.h"

#include <QSurfaceFormat>
#include <QPixmap>

#include <json.hpp>
#include <histogram_generator.hpp>

#include <pydr.hpp>

class attribute_prop {

public:
    attribute_prop() {
        name = "active";
        color_scheme = "bupu9";
        type = "scalar";
        scale = "linear";
        knots = std::vector<float>(2, 1.0f);
        knots[0] = 0.0f;
        color_knots.resize(2);
        color_knots[0] = 0.0f;
        alpha = std::vector<float>(2, 0.5f);
        alpha[0] = 0.0f;
        periodic = false;
        inverse = false;
        input = true;
    }

    std::string name;
    std::string color_scheme;
    std::string type;
    std::string scale;
    std::vector<float> color_knots;
    std::vector<float> knots;
    std::vector<float> alpha;
    bool periodic;
    bool inverse;
    bool input;

};



class render_state {

#if 0
public:
    typedef enum class scatter_coloring_enum {NONE=0, CONSTANT, NOMINAL, AREA } scatter_coloring_t;
    typedef enum class area_coloring_enum {UNIFORM=0, COLORMAP, KDE} area_coloring_t;
#endif

public:
    typedef std::map<std::string, float> att_map_t;
    typedef std::map< std::size_t, att_map_t > seg_map_t;

protected:

  // HACK for command line without options
  void init(const std::string& json_file="");
  void init_settings();
  void init_color_schemes();
  void init_attributes();
  void init_help();
  void init_data(const std::string& json_file = "");

public:
  render_state(const std::string json_file="") {
    init(json_file);
  }; 


  bool read_data_json(const std::string & json_file);
  bool write_data_json(const std::string& json_file);
  bool read_attribute_json(const std::string& json_file);

  bool read_config_json(const std::string& json_file);
  bool write_config_json(const std::string& json_file);

  void update_color_map(const std::string& scheme, const std::string& attribute, const std::vector<float>& alpha,
      const std::vector<float>& knots, const std::vector<float>& color_knots, const std::string& type, const std::string& scale, bool periodic, bool inverse, bool blend= false);

  void update_transfer_function1d();
  void update_transfer_function2d();

  void image_to_transfer_function2d();

  void create_dr_data();
  void compute_tsne();
  void compute_pca();
  void compute_umap();

  void update_density_attributes();
  void update_msc_attributes();


  void density_to_transfer_function();
  void msc_interp_to_transfer_function();

  void compute_msc_attributes(const pydr::vector_vectorf_t& y, const std::vector<float>& xlim, const std::vector<float>& ylim,
      const pydr::vectorf_t& density, std::size_t w, std::size_t h, const std::vector< QPointF >& nabla, const std::vector< QPointF>& msc_peaks, const std::vector< QPointF>& msc_minima);

public:
  size_t                        width;
  size_t			height;
  QSurfaceFormat                format;
  // unsigned int			mode;
  std::string			name;
  lux::color			background;
  lux::color			box_color;
  CTimer			time;
  CTimer			fps_time;
  size_t			fps_count;
  double            fps;
  int				swap_int;
  lux::arcballf			arcball;
  lux::arcballf       light_arcball;
  lux::cameraf			camera;
  bool                          benchmark;
  bool				dpiAware;
  std::vector<lux::program*>	program;
  size_t			sel_program;
  lux::volume<float>		vol;
  lux::volume<float>	  	norm_vol;
  lux::volume<uint8_t>		grad_vol;

  GLuint			fbo;
  GLuint			raybuffer;
  GLuint			volume;
  int				window;
  bool				screenshot;

  std::vector<std::size_t> lao_directions;
  std::size_t current_lao_directions;


  bool               animation_loop;
 
  std::vector<double>  animation_duration;

  std::size_t         current_animation_duration;
  lux::trajectoryf   animation_path;   // containing eye plus arcball
  lux::trajectoryf   target_path;      // containing target plus light orientation
  lux::trajectoryf  roi_path; // containing roi_ll and roi_ur

  CTimer              animation_time;

  bool overlay_text;

  bool roi_enabled;
  float roi_half_edge;
  lux::vec3f roi_center;
  bool roi_dragging;
  bool roi_clipping;


  float lod_bias;
  lux::vec3f shading_const;

  lux::range_t<float>		box[3];
  bool				enable_box;
  compressed_mipvolume		data;
  normal_volume        normal_data;
  lux::vec3f scale;


  glVolume			glData;
  std::vector<std::string>			data_list;
 
  std::size_t current_data_file;
  std::string			normal_data_file;

  std::vector<std::string> color_schemes;
  size_t                   current_color_scheme;
  std::vector<float>       alpha;
  bool                   alpha_constant;

  std::vector<std::string>   help_text;
  bool render_help;

  // FIXME --> to be encapsulated in some specific class, glData or whatever. Data structure: ID --> (ATTRIBUTE NAME, ATTRIBUTE VALUE)
  seg_map_t attribute;
  std::vector<attribute_prop> attribute_list;
  std::size_t current_attribute;
  std::size_t current_attribute_index;

  bool enable_second_attribute;
  std::size_t current_second_attribute;

  std::size_t current_nominal_attribute;

  std::string  json_data_file;
  std::string  json_attribute_file;

  histogram_generator hist_gen;
  std::vector<double>  att_hist;
  std::vector<double>  att_hist_second;

  histogram_generator roi_hist;

  // FIXME: to be added to config
  pydr dimension_reduction;

  std::vector<std::string> scatter_coloring_styles;
  std::size_t scatter_coloring;
  lux::color scatter_color;
  std::vector<std::string>  area_coloring_styles;
  std::size_t area_coloring;
  lux::color area_color;

  std::vector<std::string> tf_styles;
  std::size_t current_style;
  lux::color density_background_color;
  int density_attribute_id;
  int partition_attribute_id;
  int peak_attribute_id;
  int msc_sigma_attribute_id;
  int clusters_id;
  std::vector< QPointF> msc_peaks;
  std::vector< QPointF> msc_valleys;

  bool packing;

  bool recompute_density;
  bool recompute_msc_interp;
  QImage draw_area;
  std::string draw_image;
  bool modified;
  bool scribbling;
  int pen_width;
  QColor pen_color;
  QPoint last_point;

};


#endif
