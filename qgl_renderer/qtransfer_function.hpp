#ifndef _QTRANSFER_FUNCTION_HPP_
#define _QTRANSFER_FUNCTION_HPP_

#include <QWidget>
#include <render_state.hpp>
#include <histogram_generator.hpp>
#include <qpopup_list.hpp>

class qtransfer_function : public QWidget
{
    Q_OBJECT

public:
    qtransfer_function(render_state* rs = nullptr,  QWidget *parent = nullptr);


protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void keyPressEvent(QKeyEvent* ev) override;

protected:
    void draw_histogram(const attribute_prop& att, float y, float h, std::size_t nbins);

    void draw_alpha_lines(const attribute_prop& att, float y, float h);
    void draw_alpha_gradient(const attribute_prop& att, float y, float h);
    void draw_legend(const attribute_prop& att, float y, float h);
    void draw_color_gradient(const attribute_prop& att, float y, float h);
    void draw_color_lines(const attribute_prop& att, float y, float h);

    void draw_color_fill(const attribute_prop& att, float y, float  h);
    void draw_alpha_fill(const attribute_prop& att, float y, float  h);
    void draw_alpha_levels(const attribute_prop& att, float y, float h);

    void push_alpha_knot(attribute_prop& att, float x);
    void pop_alpha_knot(attribute_prop& att, std::size_t knot);

public Q_SLOTS:
    void change_color_scheme();
    void change_attribute(int a);

protected:
  
  render_state*  m_render_state;
  qpopup_list* m_color_scheme_popup;
  qpopup_list* m_attribute_popup;
  
  int   m_active_color_knot;
  int m_active_alpha_knot;
  int         m_active_alpha_height;

  int m_old_x;
  int m_old_y;

};





#endif
