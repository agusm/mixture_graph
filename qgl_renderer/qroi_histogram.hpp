#ifndef _QROI_HISTOGRAM_HPP_
#define _QROI_HISTOGRAM_HPP_

#include <QWidget>
#include <render_state.hpp>
#include <histogram_generator.hpp>

class qroi_histogram: public QWidget {

      Q_OBJECT

public:
    qroi_histogram(render_state* rs = nullptr,  QWidget *parent = nullptr);

protected:

  void draw_histogram(const attribute_prop& att, float y, float h, std::size_t nbins);

  
protected:
    void paintEvent(QPaintEvent *event) override;
 
protected:
    render_state* m_render_state;

};


#endif
