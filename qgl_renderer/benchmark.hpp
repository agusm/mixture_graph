#ifndef _BENCHMARK_HPP_
#define _BENCHMARK_HPP_

#include <render_state.hpp>

class benchmark {
public:
   void run(render_state* rs);
};

#endif
