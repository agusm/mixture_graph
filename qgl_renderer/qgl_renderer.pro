include(../mixture_graph.pri)

TEMPLATE=app
  
requires(qtHaveModule(gui))
QT += gui widgets
CONFIG += no_keywords windeployqt 

## config for external python
INCLUDEPATH += C:/ProgramData/Miniconda3/include C:/ProgramData/Miniconda3/pkgs/numpy-1.18.4-py37hae9e721_0/Lib/site-packages/numpy/core/include
LIBS+= -LC:/ProgramData/Miniconda3/libs -lpython37 -LC:/ProgramData/Miniconda3/pkgs/numpy-1.18.4-py37hae9e721_0/Lib/site-packages/numpy\core\lib -lnpymath

## config for external eigen and glew 
INCLUDEPATH += ../ext
LIBS+= -L../ext/GL -lglew32
##LIBS+= -L../ext/GL -lfreeglut

## config for lux
INCLUDEPATH += ../lux
LIBS+= -L$$DESTDIR -llux

## config for base
INCLUDEPATH += ../base
LIBS+= -L$$DESTDIR -lbase

## config for dr
INCLUDEPATH += ../dr
LIBS+= -L$$DESTDIR -ldr

DEPENDPATH*= ../lux
DEPENDPATH*= ../base
DEPENDPATH*= ../dr

HEADERS += \
qrender_config.hpp \
qdraw_area.hpp \
histogram_generator.hpp \
json.hpp \
render_state.hpp \
benchmark.hpp \
qgl_volume_renderer_window.hpp \
qtransfer_function.hpp \
qtransfer_function2d.hpp\
qroi_histogram.hpp \
qpopup_list.hpp \
shaders.h

SOURCES += \
qrender_config.cpp \
qdraw_area.cpp \
histogram_generator.cpp \
json.cpp \
render_state.cpp \
benchmark.cpp \
qgl_volume_renderer_window.cpp \
qtransfer_function.cpp \
qtransfer_function2d.cpp\
qroi_histogram.cpp \
qpopup_list.cpp \ 
qgl_renderer_main.cpp \

TARGET = qgl_renderer

QMAKE_POST_LINK = windeployqt --compiler-runtime $$DESTDIR
