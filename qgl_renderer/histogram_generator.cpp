#include <histogram_generator.hpp>
#include <omp.h>

histogram_generator::histogram_generator(compressed_mipvolume* cmip) {
	m_pVolume = cmip;
	if (cmip != nullptr) {
		set_compressed_mipvolume(cmip);
	}
}

void histogram_generator::init(compressed_mipvolume* cmip, const lux::rangeu32_t& X, const lux::rangeu32_t& Y, const lux::rangeu32_t& Z) {

	if (cmip == nullptr) return;
	m_pVolume = cmip;
	compile_topo_ranges();
	compile_node_list();
	m_pVolume->build_lookup_table(true, m_topo_order);
	update_ranges(X, Y, Z);
}


void histogram_generator::set_compressed_mipvolume(compressed_mipvolume* cmip) {

	if (cmip == nullptr) return;

	m_pVolume = cmip;
	compile_topo_ranges();
	compile_node_list();
	m_pVolume->build_lookup_table(true, m_topo_order);

	lux::rangeu32_t X, Y, Z;
	X.vmin = 0;
	X.vmax = m_pVolume->get_Dimension(0)-1;
	Y.vmin = 0;
	Y.vmax = m_pVolume->get_Dimension(1)-1;
	Z.vmin = 0;
	Z.vmax = m_pVolume->get_Dimension(2)-1;
	update_ranges(X, Y, Z);
}

void histogram_generator::update_ranges(const lux::rangeu32_t& X, const lux::rangeu32_t& Y, const lux::rangeu32_t& Z) {

	if (m_pVolume == nullptr) return;

	m_mu = 0.0;
	m_sigma = 0.0;

	size_t vol = size_t(X.vmax - X.vmin) * size_t(Y.vmax - Y.vmin) * size_t(Z.vmax - Z.vmin);
	double one_over_vol = 1.0 / double(vol);
	size_t f = m_pVolume->query_range(X, Y, Z, m_id_hist);
	for (sparse_vector_t::iterator it = m_id_hist.begin(); it != m_id_hist.end(); ++it) {
		if (it->first) {
			m_mu += it->second.mu();
			m_sigma += it->second.sigma();
		}
		it->second.mu() *= one_over_vol;
		it->second.sigma() *= one_over_vol;
	}
}


void histogram_generator::aggregate(std::size_t nbins,  std::vector<double>& out_hist, bool logscale) {

	out_hist.clear();
	out_hist.resize(nbins);
	double one_over_range = (logscale?  1.0f/(log(m_attribute_range.vmax)-log(m_attribute_range.vmin)) : 1.0f / (m_attribute_range.vmax - m_attribute_range.vmin)  );
	for (sparse_vector_t::iterator it = m_id_hist.begin(); it != m_id_hist.end(); ++it) {
		if (it->first) {
			std::size_t bin = (logscale ? log(m_leaf_attributes[it->first]) - log(m_attribute_range.vmin) : m_leaf_attributes[it->first] - m_attribute_range.vmin) * one_over_range * (nbins - 1);
			out_hist[bin] += it->second.mu();
		}
	}
}

void histogram_generator::update_attribute(const std::vector<double>& att) {
  m_leaf_attributes.resize(att.size());
  std::copy(att.begin(),att.end(), m_leaf_attributes.begin());
  m_sum = 0.0f;
  for (sparse_vector_t::iterator it = m_id_hist.begin(); it != m_id_hist.end(); ++it) {
	  if( it->first) m_sum += m_leaf_attributes[it->first];
  }
  // minmax
  m_attribute_range.vmin = *std::min_element(att.begin(), att.end());
  m_attribute_range.vmax = *std::max_element(att.begin(), att.end());
}


void histogram_generator::compile_topo_ranges(void) {
	m_topo_order.clear();
	if (m_pVolume == nullptr) return;
	size_t nLeafs = m_pVolume->get_nLeafs();
	size_t nLerps = m_pVolume->get_nLerpNodes();
	if (nLeafs + nLerps == 0) return;
	std::vector<uint32_t> order(nLeafs + nLerps, std::numeric_limits<uint32_t>::max());
	for (size_t n = 0; n < nLeafs; n++) order[n] = 0;
	for (uint32_t n = 0; n < nLerps; n++) {
		uint64_t ch0, ch1;
		uint32_t idx;
		m_pVolume->get_Node(n, ch0, ch1, idx);
		order[n + nLeafs] = std::max(order[ch0], order[ch1]) + 1;
	}
	m_topo_order.push_back(lux::rangeu32_t(0u, uint32_t(nLeafs)));
	uint32_t pos = uint32_t(nLeafs);
	uint32_t p0 = uint32_t(nLeafs);
	uint32_t level = 1;
	while (pos < order.size()) {
		if (order[pos] > level) {
			m_topo_order.push_back(lux::rangeu32_t(p0, pos));
			p0 = pos;
			level++;
		}
		else pos++;
	}
	m_topo_order.push_back(lux::rangeu32_t(p0, uint32_t(order.size())));
}


void histogram_generator::compile_node_list(void) {
	m_node_list.clear();
	if (m_pVolume == nullptr) return;
	size_t nLerps = m_pVolume->get_nLerpNodes();
	m_node_list.resize(3 * nLerps);
#pragma omp parallel for schedule(dynamic,32)
	for (int n = 0; n<int(nLerps); n++) {
		uint64_t ch0, ch1;
		uint32_t idx;
		m_pVolume->get_Node(n, ch0, ch1, idx);
		m_node_list[3 * n] = uint32_t(ch0);
		m_node_list[3 * n + 1] = uint32_t(ch1);
		m_node_list[3 * n + 2] = uint32_t(m_pVolume->get_Code(idx) * double(1 << 20));
	}
}
