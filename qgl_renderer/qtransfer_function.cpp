#include <qtransfer_function.hpp>
#include <QPainter>
#include <QTimer>
#include <QMouseEvent>

qtransfer_function::qtransfer_function(render_state* rs,  QWidget *parent)
    : QWidget(parent)
{
  m_render_state = rs;
 
  setAttribute(Qt::WA_StaticContents);

  resize(800,600);

  m_color_scheme_popup = new qpopup_list(m_render_state->color_schemes);
  connect(m_color_scheme_popup, SIGNAL(changed_selection(int)), this, SLOT(change_color_scheme()));

  std::vector<std::string> att_list;
  std::vector<std::size_t> att_idx;
  for (std::size_t i = 0; i < m_render_state->attribute_list.size(); ++i) {
      if (m_render_state->attribute_list[i].type != "vector") {
          att_list.push_back(m_render_state->attribute_list[i].name);
          att_idx.push_back(i);
      }
  }
  m_attribute_popup = new qpopup_list(att_list, att_idx);
  m_attribute_popup->update_current(m_render_state->attribute_list[m_render_state->current_attribute].name);
  connect(m_attribute_popup, SIGNAL(changed_selection(int)), this, SLOT(change_attribute(int)));


  QTimer * t = new QTimer(this);
  connect(t,SIGNAL(timeout()),this, SLOT(update()));
  t->start(50);

  m_active_color_knot = -1;
  m_active_alpha_knot = -1;
  m_active_alpha_height = -1;

  m_old_x = 0;
  m_old_y = 0;
}



void qtransfer_function::change_attribute(int a) {
    m_render_state->current_attribute = a;
    m_render_state->update_transfer_function1d();
}



void qtransfer_function::change_color_scheme() {
    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
    att.color_scheme = m_color_scheme_popup->current_string();
    m_render_state->update_transfer_function1d();
}


void qtransfer_function::push_alpha_knot(attribute_prop& att, float xins) {

    if (att.type == "nominal") return;

    std::map<float, float> alpha_map;
    float xprev = 0.0f;
    bool inserted = false;
    bool logarithmic = (att.scale == "logarithmic");

    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        float x = att.knots[k];
        float y = att.alpha[k];
        alpha_map[x] = y;
        if (!inserted) {
            if (x < xins) xprev = x;
            if (x > xins) {
                float yprev = alpha_map[xprev];
                float yins = (logarithmic ? yprev + (log(xins) - log(xprev)) * (y - yprev) / (log(x) - log(xprev)) : yprev + (xins - xprev) * (y - yprev) / (x - xprev) );
                alpha_map[xins] = yins;
                inserted = true;
            }
        }
    }
    att.knots.resize(alpha_map.size());
    att.alpha.resize(alpha_map.size());
    std::size_t k = 0;
    for (std::map<float, float>::const_iterator it = alpha_map.begin(); it != alpha_map.end(); ++it) {
        att.knots[k] = it->first;
        att.alpha[k] = it->second;
        ++k;
    }
}



void qtransfer_function::pop_alpha_knot(attribute_prop& att, std::size_t knot) {

    if(att.type == "nominal") return;

    std::map<float, float> alpha_map;
    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        if (k != knot) {
            float x = att.knots[k];
            float y = att.alpha[k];
            alpha_map[x] = y;
        }
    }
    att.knots.resize(alpha_map.size());
    att.alpha.resize(alpha_map.size());
    std::size_t k = 0;
    for (std::map<float, float>::const_iterator it = alpha_map.begin(); it != alpha_map.end(); ++it) {
        att.knots[k] = it->first;
        att.alpha[k] = it->second;
        ++k;
    }
}



void qtransfer_function::mousePressEvent(QMouseEvent* event) {

    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
    
    if (event->button() == Qt::LeftButton) {
      //  printf("Left button: %d %d \n", event->x(), event->y());
        attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
        if (att.type == "nominal") {
            float xmin = 0.025f;
            float xmax = 0.95f;
            float dx = (xmax - xmin) / float(att.knots.size());
            if (event->y() > 0.65 * height()) {
                for (std::size_t k = 0; k < att.knots.size(); ++k) {
                    float xlo = (xmin + k * dx) * width();
                    float xhi = (xmin + (k + 1) * dx) * width();
                    float x = event->x();
                    if (xlo < x && x < xhi) {
                        m_active_alpha_height = k;
                        m_old_y = event->y();
                        break;
                    }
                }
            }
        }
        else {
            bool logarithmic = (att.scale == "logarithmic");
            if (event->y() < 0.35f * height()) {
                float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

                for (std::size_t k = 1; k < att.color_knots.size() - 1; ++k) {
                    float xk = 0.025f * width() + 0.95f * width() * (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
                    float d = fabs(xk - event->x());
                    if (d < 10.0f) {
                        m_active_color_knot = k;
                        m_old_x = event->x();
                        //printf("Active color knot: %d \n", m_active_color_knot);
                        break;
                    }
                }
            }
            else if (event->y() > 0.65f * height()) {
                float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);

                for (std::size_t k = 0; k < att.knots.size(); ++k) {
                    float xk = 0.025f * width() + 0.95f * width() * (logarithmic ? log(att.knots[k]) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
                    float d = fabs(xk - event->x());
                    if (d < 10.0f) {
                        m_active_alpha_knot = k;
                        m_old_x = event->x();
                        //printf("Active color knot: %d \n", m_active_color_knot);
                        break;
                    }
                }
                if (m_active_alpha_knot >= 0) {
                    float yk = 0.95f * height() - 0.3f * height() * att.alpha[m_active_alpha_knot];
                    float d = fabs(yk - event->y());
                    if (d < 10.0f) {
                        m_active_alpha_height = m_active_alpha_knot;
                        m_active_alpha_knot = -1;
                        m_old_y = event->y();
                    }
                    if (m_active_alpha_knot == (att.knots.size() - 1) || m_active_alpha_knot == 0) {
                        m_active_alpha_knot = -1;
                    }
                }
                else {
                    float x = (logarithmic ? att.knots[0] * exp(float(event->x() - 0.025f * width()) * range / (0.95f * width())) : att.knots[0] + float(event->x() - 0.025f * width()) * range / (0.95f * width()));
                    push_alpha_knot(att, x);
                }
            }
        }
    }

    if (event->button() == Qt::RightButton) {
        if (event->y() < 0.35f * height()) {
            attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
            m_color_scheme_popup->update_current(att.color_scheme);
            m_color_scheme_popup->popup(event->globalPos());
        } else if (event->y() < 0.65f * height()) {
            attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
            m_attribute_popup->update_current(att.name);
            m_attribute_popup->popup(event->globalPos());
        } else   {
            attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
            bool logarithmic = (att.scale == "logarithmic");
            float range = (logarithmic ? log(att.knots[att.knots.size() - 1] ) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
            for (std::size_t k = 1; k < att.knots.size() - 1; ++k) {
                float xk = 0.025f * width() + 0.95f * width() * (logarithmic ? log(att.knots[k]) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
                float d = fabs(xk - event->x());
                if (d < 10.0f) {
                    pop_alpha_knot(att,k);
                    std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
                    m_render_state->update_transfer_function1d();
                }
            }
        }
    }
}

void qtransfer_function::mouseMoveEvent(QMouseEvent* event) {

    attribute_prop& att = m_render_state->attribute_list[m_render_state->current_attribute];
    float dk = 0.0f;

    if (att.type == "nominal") {
        if (m_active_alpha_height >= 0) {
            dk = float(m_old_y - event->y()) / float(0.3f * height());
            if (att.alpha[m_active_alpha_height] < 0.05f) dk *= 0.1f;
            att.alpha[m_active_alpha_height] += dk;
            att.alpha[m_active_alpha_height] = std::max(0.0f, std::min(0.99f, att.alpha[m_active_alpha_height]));
           // printf("knot: %f, dk: %f \n", att.color_knots[m_active_alpha_knot], dk);
        }
    }
    else {
        bool logarithmic = (att.scale == "logarithmic");

        if (m_active_color_knot >= 0) {
            float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
            dk = range * float(event->x() - m_old_x) / float(0.95f * width());
            if (logarithmic) {
                att.color_knots[m_active_color_knot] *= exp(dk);
                att.color_knots[m_active_color_knot] = std::max(att.color_knots[m_active_color_knot - 1] * exp(0.001f * range), std::min(att.color_knots[m_active_color_knot + 1] * exp(-0.001f * range), att.color_knots[m_active_color_knot]));

            }
            else {
                att.color_knots[m_active_color_knot] += dk;
                att.color_knots[m_active_color_knot] = std::max(att.color_knots[m_active_color_knot - 1] + 0.001f * range, std::min(att.color_knots[m_active_color_knot + 1] - 0.001f * range, att.color_knots[m_active_color_knot]));
            }
            // printf("knot: %f, dk: %f \n", att.color_knots[m_active_color_knot], dk);

        }
        if (m_active_alpha_height >= 0) {
            dk = float(m_old_y - event->y()) / float(0.3f * height());
            if (att.alpha[m_active_alpha_height] < 0.05f) dk *= 0.1f;
            att.alpha[m_active_alpha_height] += dk;
            att.alpha[m_active_alpha_height] = std::max(0.0f, std::min(0.999f, att.alpha[m_active_alpha_height]));
            // printf("knot: %f, dk: %f \n", att.color_knots[m_active_color_knot], dk);
        }
        if (m_active_alpha_knot >= 0) {
            float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);
            dk = range * float(event->x() - m_old_x) / float(0.95f * width());
            if (logarithmic) {
                att.knots[m_active_alpha_knot] *= exp(dk);
                att.knots[m_active_alpha_knot] = std::max(att.knots[m_active_alpha_knot - 1] * exp(0.001f * range), std::min(att.knots[m_active_alpha_knot + 1] * exp(-0.001f * range), att.knots[m_active_alpha_knot]));

            }
            else {
                att.knots[m_active_alpha_knot] += dk;
                att.knots[m_active_alpha_knot] = std::max(att.knots[m_active_alpha_knot - 1] + 0.001f * range, std::min(att.knots[m_active_alpha_knot + 1] - 0.001f * range, att.knots[m_active_alpha_knot]));
            }
        }
    }
    m_old_x = event->x();
    m_old_y = event->y();

    if (dk != 0.0f ) {
        m_render_state->update_transfer_function1d();
    }
}
    
void qtransfer_function::mouseReleaseEvent(QMouseEvent* event) {

        m_active_color_knot = -1;
        m_active_alpha_height = -1;
        m_active_alpha_knot = -1;
}
    
void qtransfer_function::paintEvent(QPaintEvent* event) {

    attribute_prop att = m_render_state->attribute_list[m_render_state->current_attribute];
    QString title = att.name.c_str();

    setWindowTitle(title);

    if (att.type == "nominal") {
        draw_color_fill(att, 0.05f * height(), 0.3f * height());
        //draw_color_lines(att, 0.05f * height(), 0.3f * height());

        draw_color_fill(att, 0.4f * height(), 0.2f * height());
        draw_alpha_fill(att, 0.4f * height(), 0.2f * height());
        draw_legend(att, 0.4f * height(), 0.2f * height());

        draw_alpha_fill(att, 0.65f * height(), 0.3f * height());
        draw_alpha_levels(att, 0.65f * height(), 0.3f * height());
    }
    else {
        draw_color_gradient(att, 0.05f * height(), 0.3f * height());
        draw_color_lines(att, 0.05f * height(), 0.3f * height());

        draw_color_gradient(att, 0.4f * height(), 0.2f * height());
        draw_alpha_gradient(att, 0.4f * height(), 0.2f * height());
        draw_legend(att, 0.4f * height(), 0.2f * height());


        draw_alpha_gradient(att, 0.65f * height(), 0.3f * height());
        draw_alpha_lines(att, 0.65f * height(), 0.3f * height());
        draw_histogram(att, 0.65f * height(), 0.3f * height(), 50);
    }
}
   

void qtransfer_function::draw_alpha_fill(const attribute_prop& att, float y, float  h) {

    QPainter painter(this);

    float xmin = 0.025f;
    float xmax = 0.95f;
    float dx = (xmax - xmin) / float(att.knots.size());

    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        float alpha = att.alpha[k];
        int r = 0 * 255;
        int g = 0 * 255;
        int b = 0 * 255;
        int a = (1.0f - alpha) * 255;
        QColor c(r, g, b, a);
        float x = xmin + float(k) * dx;
        painter.setBrush(c);
        //painter.setPen(c);
        painter.drawRect(x * width(), y,  dx * width(), h);
    }
}

void qtransfer_function::draw_color_fill(const attribute_prop& att, float y, float  h) {

    QPainter painter(this);

    lux::colormap CM(att.color_scheme);
   
    bool inverse = att.inverse;
    bool periodic = att.periodic;
    float xmin = 0.025f * width();
    float xmax = 0.95f * width();
    //printf("Color knots size == %d", int(att.color_knots.size()));
    float dx = (xmax - xmin) / float(att.color_knots.size());

    for (std::size_t k = 0; k < att.color_knots.size(); ++k) {
        std::size_t i = (periodic ? (k > CM.size() / 2 ? 2 * (CM.size() - 1 - k) : 2 * k) : k);
        i = (inverse ? CM.size() - 1 - i : i);
        lux::color c = CM(i);
        int r = c[0] * 255;
        int g = c[1] * 255;
        int b = c[2] * 255;
        QColor col(r, g, b, 255);
        float x = xmin + float(k)*dx;
        painter.setBrush(col);
        //painter.setPen(c);
        painter.drawRect( x, y, dx, h);
    }
}



void qtransfer_function::resizeEvent(QResizeEvent* event) {

}

void qtransfer_function::draw_alpha_gradient(const attribute_prop& att, float y, float h) {

    QPainter painter(this);

    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.knots[att.knots.size() - 1] ) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);


    QLinearGradient lin(0.025f * float(width()), y, 0.975f * float(width()), y);

    for (std::size_t k = 0; k < att.knots.size(); ++k) {
        float alpha = att.alpha[k];
        int r = 0 * 255;
        int g = 0 * 255;
        int b = 0 * 255;
        int a = (1.0f - alpha) * 255;

        float x = (logarithmic ? log(att.knots[k]) - log(att.knots[0]) :  att.knots[k] - att.knots[0]) / range;
        lin.setColorAt(x, QColor(r, g, b, a));
    }
    painter.setBrush(lin);
    painter.drawRect(0.025f * width(), y, 0.95f * width(), h);

}

void qtransfer_function::draw_color_gradient(const attribute_prop& att, float y, float h) {

    QPainter painter(this);
    lux::colormap CM(att.color_scheme);
    QLinearGradient lin(0.025f * float(width()), y, 0.975f * float(width()), y);

    bool logarithmic = (att.scale == "logarithmic");
    bool inverse = att.inverse;
    bool periodic = att.periodic;
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1] ) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

    for (std::size_t k = 0; k < att.color_knots.size(); ++k) {
        std::size_t i = (periodic ? (k > CM.size() / 2 ? 2 * (CM.size() - 1 - k) : 2 * k) : k);
        i= (inverse ? CM.size() - 1 - i : i);
        lux::color c = CM(i);
        int r = c[0] * 255;
        int g = c[1] * 255;
        int b = c[2] * 255;
        float x = (logarithmic ?  log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
        lin.setColorAt(x, QColor(r, g, b));
    }
    painter.setBrush(lin);
    painter.drawRect(0.025f * width(), y, 0.95f * width(), h);

}


void qtransfer_function::draw_histogram(const attribute_prop& att, float y, float h, std::size_t nbins) {
    QPainter painter(this);

    QPen pen;
    bool logscale = (att.scale == "logarithmic");

    pen.setWidth( 4 );
    pen.setColor(Qt::darkMagenta);
    painter.setPen(pen);

    for (std::size_t b = 1; b < nbins; ++b) {
        float t = float(b-1) / float(nbins - 1);
        QPoint p1(0.025f * width() + 0.95f * width() * t, y + h*(1.0f - m_render_state->att_hist[b-1]));
        t = float(b) / float(nbins - 1);
        QPoint p2(0.025f * width() + 0.95f * width() * t, y + h*(1.0f - m_render_state->att_hist[b]));
        painter.drawLine(p1, p2);
    }
}

void qtransfer_function::draw_alpha_levels(const attribute_prop& att, float y, float h) {

    QPainter painter(this);

    QPen pen;

    float xmin = 0.025f * width();
    float xmax = 0.95f * width();
    float dx = (xmax - xmin) / float(att.knots.size());

    for (std::size_t k = 0; k < att.knots.size(); ++k) {

        if (m_active_alpha_height == k) {
            pen.setWidth(4);
            pen.setColor(Qt::yellow);
        }
        else {
            pen.setWidth(4);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);
        float x1 = xmin + k * dx;
        float x2 = x1 + dx;
        QPoint p0(x1 , y + h);
        QPoint p1(x1, y + h * (1.0f - att.alpha[k]));
        QPoint p2(x2 , y + h * (1.0f - att.alpha[k]));
        QPoint p3(x2, y + h);
        painter.drawLine(p0, p1);
        painter.drawLine(p1, p2);
        painter.drawLine(p2, p3);
        painter.drawLine(p3, p0);
    }
}


void qtransfer_function::draw_alpha_lines(const attribute_prop& att, float y, float h) {

    QPainter painter(this);

    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.knots[att.knots.size() - 1]) - log(att.knots[0]) : att.knots[att.knots.size() - 1] - att.knots[0]);

    for (std::size_t k = 1; k < (att.knots.size() - 1); ++k) {
 
        if (m_active_alpha_knot == k) {
            pen.setWidth(4);
            pen.setColor(Qt::yellow);
        }
        else {
            pen.setWidth(2);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);

        float t = (logarithmic ? log(att.knots[k] ) - log(att.knots[0] ) : att.knots[k] - att.knots[0] ) / range;
 
        QPoint p1(0.025f * width() + 0.95f * width() * t, y);
        QPoint p2(0.025f * width() + 0.95f * width() * t, y+h);
        if (m_active_alpha_knot == k) {
            painter.setFont(QFont("Helvetica", 12));
            painter.drawText(p1.x(), y + h / 2.0f, QString().setNum(att.knots[k], 'g', 3));
        }
        painter.drawLine(p1, p2);
    }

    for (std::size_t k = 1; k < att.knots.size(); ++k) {

        float t1 = (logarithmic ? log(att.knots[k - 1]) - log(att.knots[0]) : att.knots[k - 1] - att.knots[0]) / range;
        QPoint p1(0.025f * width() + 0.95f * width() * t1, y + h*(1.0f - att.alpha[k - 1]));
        float t2 = (logarithmic ? log(att.knots[k] ) - log(att.knots[0]) : att.knots[k] - att.knots[0]) / range;
        QPoint p2(0.025f * width() + 0.95f * width() * t2,  y + h*(1.0f - att.alpha[k]));

        if (m_active_alpha_height >= 0 && (m_active_alpha_height == k || m_active_alpha_height == (k - 1))) {
            pen.setWidth(4);
            pen.setColor(Qt::green);
        }
        else {
            pen.setWidth(2);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);
        if (m_active_alpha_height == k) {
            painter.setFont(QFont("Helvetica", 12));
            painter.drawText(p2.x(), p2.y(), QString().setNum(att.alpha[k], 'g', 3));
        }
        painter.drawLine(p1, p2);
    }

}
    
void qtransfer_function::draw_legend(const attribute_prop& att, float y, float h) {

    QPainter painter(this);

    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1] ) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);
    pen.setColor(Qt::white);
    painter.setPen(pen);
    painter.setFont(QFont("Helvetica", 8));

    float x = 0.025f * width();
    painter.drawText(x, y+ 0.99f * h, QString().setNum(att.color_knots[0], 'g', 2));

    for (std::size_t k = 1; k < 4; ++k) { 
        x = 0.95f * width() * float(k) / float(4);
        QPoint p1(x, y);
        QPoint p2(x, y + h);
        painter.drawLine(p1, p2);
        float val = (logarithmic ? att.color_knots[0] * exp(float(k) / float(4) * range) : att.color_knots[0] + float(k) / float(4) * range);
        painter.drawText(x, y+ 0.99f * h, QString().setNum(val, 'g', 2));
    }
    x = 0.91f * width();
    painter.drawText(x, y+ 0.99f * h, QString().setNum(att.color_knots[att.color_knots.size() - 1], 'g', 2));
}


void qtransfer_function::draw_color_lines(const attribute_prop& att, float y, float h) {
    
    QPainter painter(this);

    QPen pen;
    bool logarithmic = (att.scale == "logarithmic");
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1] ) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

    for (std::size_t k = 1; k < (att.color_knots.size()-1); ++k) {
        if (m_active_color_knot == k) {
            pen.setWidth(4);
            pen.setColor(Qt::yellow);
        }
        else {
            pen.setWidth(1);
            pen.setColor(Qt::white);
        }
        painter.setPen(pen);

        QPoint p1(0.025f * width() + 0.95f * width() * (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range, y);
        QPoint p2(0.025f * width() + 0.95f * width() * (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range, y+h);
        if (m_active_color_knot == k) {
            painter.setFont(QFont("Helvetica", 12));
            painter.drawText(p1.x(),y+h/2.0f, QString().setNum(att.color_knots[k], 'g', 3));
        }
        painter.drawLine(p1,p2);
    }


}


void qtransfer_function::keyPressEvent(QKeyEvent* ev) {
    switch (ev->key()) {
    case Qt::Key_H: {
        m_render_state->render_help = !m_render_state->render_help;
        if (m_render_state->render_help) {
            for (std::size_t s = 0; s < m_render_state->help_text.size(); s++) {
                printf("%s\n", m_render_state->help_text[s].c_str());
            }
        }
    } break;
    case Qt::Key_O: {
        m_render_state->current_lao_directions = (m_render_state->current_lao_directions + 1) % m_render_state->lao_directions.size();
        m_render_state->glData.lao_directions() = (m_render_state->lao_directions[m_render_state->current_lao_directions]);
        printf("Local ambient occlusion: %s \n", (m_render_state->glData.lao_directions() ? "ON" : "OFF"));
        if (m_render_state->glData.lao_directions()) printf("Directions: %d \n", int(m_render_state->glData.lao_directions()));
    } break;
    case Qt::Key_N: {
        m_render_state->glData.enable_shading() = !(m_render_state->glData.enable_shading());
        printf("Diffuse shading: %s\n", (m_render_state->glData.enable_shading() ? "ON" : "OFF"));
        if (m_render_state->glData.enable_shading()) printf(" Light dir: %.2f %.2f %.2f \n", m_render_state->glData.light_dir()[0],
            m_render_state->glData.light_dir()[1], m_render_state->glData.light_dir()[2]);
    } break;
    case Qt::Key_S: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        m_render_state->current_color_scheme = (m_render_state->current_color_scheme + 1) % m_render_state->color_schemes.size();
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.color_scheme = m_render_state->color_schemes[m_render_state->current_color_scheme];
        lux::colormap CM(att.color_scheme);// ("YlOrRd9");
        if (CM.size() != att.color_knots.size()) {
            bool logarithmic = (att.scale == "logarithmic");
            float b = att.color_knots[0];
            float e = att.color_knots[att.color_knots.size() - 1];
            float r = (logarithmic ? log(e) - log(b) : e - b);
            att.color_knots.resize(CM.size());
            for (std::size_t i = 0; i < CM.size(); ++i) {
                att.color_knots[i] = (logarithmic ? b * exp(float(i) / float(CM.size() - 1) * r) : b + r * float(i) / float(CM.size() - 1));
            }
        }
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_A: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);

        typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
        if (update_second_attribute) {
            m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
        }
        else {
            m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
        }

        if (m_render_state->enable_second_attribute && m_render_state->current_attribute == m_render_state->current_second_attribute) {
            if (update_second_attribute) {
                m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
            }
            else {
                m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
            }
        }
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        //     std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
        std::vector<double> v(m_render_state->attribute.size());
        for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
            v[it->first] = (it->second).at(att.name.c_str());

        m_render_state->hist_gen.update_attribute(v);

        if (m_render_state->roi_enabled) {
            m_render_state->roi_hist.update_attribute(v);
        }

        if (update_second_attribute) {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att.scale == "logarithmic");
        }
        else {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att.scale == "logarithmic");
        }

        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_J: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::unordered_map< std::size_t, std::unordered_map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.inverse = !att.inverse;
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_K: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::unordered_map< std::size_t, std::unordered_map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.periodic = !att.periodic;
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_L: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.scale = (att.scale == "linear" ? "logarithmic" : "linear");
        //     std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
        std::vector<double> v(m_render_state->attribute.size());
        for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
            v[it->first] = (it->second).at(att.name.c_str());
        m_render_state->hist_gen.update_attribute(v);
        if (update_second_attribute) {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att.scale == "logarithmic");
        }
        else {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att.scale == "logarithmic");
        }
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_Escape: exit(0);
    }

}
