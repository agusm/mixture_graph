#ifndef _QPOPUP_LIST_HPP_
#define _QPOPUP_LIST_HPP_

#include <QMenu>
#include <unordered_map>


class qpopup_list : public QMenu {
  Q_OBJECT
public:
  qpopup_list( const std::vector<std::string>& l, const std::vector<std::size_t>& idx = std::vector<std::size_t>()) {
    update_list(l,idx);
  }

public: 
  void update_list( const std::vector<std::string>& l, const std::vector<std::size_t>& idx = std::vector<std::size_t>());
 
  void update_current(const std::string& s);

  const std::size_t& current_index() const {
    return m_current_index;
  }

  const std::string& current_string() const {
      return m_current_string;
  }

Q_SIGNALS:
    void changed_selection(int index);

public Q_SLOTS:
    void change_current(QAction* a) {
        m_current_index = m_action_index_map[a->text()];
        m_current_string = a->text().toLatin1();
        Q_EMIT changed_selection(m_current_index);
    }
    
protected:
  std::unordered_map< QString, std::size_t> m_action_index_map;  
  std::size_t m_current_index;
  std::string m_current_string;
};

  
#endif
