#include <qgl_volume_renderer_window.hpp>
#include <benchmark.hpp>
#include <QTimer>
#include <QtGui\qopenglfunctions.h>
#include <QOpenGLExtraFunctions>
#include <shaders.h>

#include <QMouseEvent>
#include <QKeyEvent>
#include <QPainter>
#include <random>
#include <algorithm>

#include<QJsonDocument>
#include <normal_volume.h>
#include <QFileDialog>


void qgl_volume_renderer_window::start() {
    
  if(m_render_state != nullptr) {
      resize(m_render_state->width, m_render_state->height);
  
      //reset_camera();
    
     QTimer * t = new QTimer(this);
     connect(t,SIGNAL(timeout()),this, SLOT(update()));
     t->start(0);

     m_tf = new qtransfer_function(m_render_state);

     m_rc = new qrender_config(m_render_state);
     m_rc->show();

     m_tf2d = new qtransfer_function2d(m_render_state);

     if (m_render_state->enable_second_attribute) {
         m_tf2d->show();
     }
     else {
         m_tf->show();
     }

     m_roi_hist = new qroi_histogram(m_render_state);
     if (m_render_state->roi_enabled) {
         m_roi_hist->show();
     }

  } else {
     printf(" Error: invalide render state\n");
    exit(-1);
  }
}  

void qgl_volume_renderer_window::reset_camera() {

    // add to state
    m_render_state->camera.setFOV(60.0f);
    m_render_state->camera.setNear(0.001f);
    m_render_state->camera.setFar(100.0f);

    m_render_state->camera.setEye(lux::vec4f(0.0f, 0.0f, -3.0f, 1.0f));
    m_render_state->camera.setCenter(lux::vec4f(0.0f, 0.0f, 0.0f, 1.0f));
    m_render_state->camera.setUp(lux::vec4f(0.0f, 1.0f, 0.0f, 0.0f));

    m_render_state->arcball.setOrientation(lux::vec4f(1.0f, 0.0f, 0.0f, 0.0f));
    m_render_state->light_arcball.setOrientation(lux::vec4f(1.0f, 0.0f, 0.0f, 0.0f));
}


void qgl_volume_renderer_window::initializeGL() {

  glewInit();
  glc.initializeOpenGLFunctions();
  glx.initializeOpenGLFunctions();

  m_render_state->fps_time.Reset();
  m_render_state->time.Reset();
 
  glClearColor(m_render_state->background(0), m_render_state->background(1), m_render_state->background(2), m_render_state->background(3));

  glFrontFace(GL_CW);
  glCullFace(GL_FRONT);
  glEnable(GL_CULL_FACE);

  glDepthFunc(GL_GEQUAL);
  glDepthMask(GL_FALSE);
  glDisable(GL_DEPTH_TEST);

  update_data();

  // FIXME --> from render state

 
}


void qgl_volume_renderer_window::update_data() {


    if (!m_render_state->data.read(m_render_state->data_list[m_render_state->current_data_file].c_str())) {
        printf("Error reading %s.  Exiting..... \n", m_render_state->data_list[m_render_state->current_data_file].c_str());
        exit(0);
    }


    // Fixme --> Normal volume and other attributes for rendering (AO ???)
    normal_volume* norm_ptr = nullptr;
    
    if (m_render_state->normal_data.read(m_render_state->normal_data_file)) {
        printf(" Read volume normals from %s\n", m_render_state->normal_data_file.c_str());
        norm_ptr = &m_render_state->normal_data;
    } else {
        printf("Error reading %s\n", m_render_state->normal_data_file.c_str());
    }

    m_render_state->data.output();
    m_render_state->glData.attach(m_render_state->data, norm_ptr);
    if (!m_render_state->glData.upload_gpu()) {
        printf("Error: could not upload gpu\n");
        exit(0);
    }

    m_render_state->glData.background_color() = m_render_state->background;
    m_render_state->glData.box_color() = m_render_state->box_color;


    /// Fixme --> where this one????
    if (m_render_state->benchmark) {
        benchmark b;
        b.run(m_render_state);
        m_render_state->benchmark = false;
    }

    init_attributes();

    // Update colors in the leaves
    attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
   
    m_render_state->hist_gen.set_compressed_mipvolume(&m_render_state->data);

    lux::rangeu32_t dummy(0, 63);
    m_render_state->roi_hist.init(&m_render_state->data, dummy, dummy, dummy);


    typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;

    // Update colors in the leaves
    //std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");

    std::vector<double> v(m_render_state->attribute.size());
    for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
        v[it->first] = (it->second).at(att1.name.c_str());
    m_render_state->roi_hist.update_attribute(v);


    m_render_state->hist_gen.update_attribute(v);
    m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att1.scale == "logarithmic");

    attribute_prop& att2 = m_render_state->attribute_list[m_render_state->current_second_attribute];
    for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
        v[it->first] = (it->second).at(att2.name.c_str());
    m_render_state->hist_gen.update_attribute(v);
    m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att2.scale == "logarithmic");
 
    if (m_render_state->enable_second_attribute) {
        m_render_state->update_transfer_function2d();
    }
    else {
        m_render_state->update_transfer_function1d();
    }

    m_render_state->glData.lod_bias() = m_render_state->lod_bias;
    m_render_state->glData.shading_const() = m_render_state->shading_const;

}


void qgl_volume_renderer_window::init_attributes() {

    if (m_render_state->attribute.size() != 0) return;

    std::mt19937 gen(1);
    std::uniform_real_distribution<float> D(0.0f, 1.0f);

    for (std::size_t a = 0; a < m_render_state->attribute_list.size(); ++a) {
        attribute_prop& att = m_render_state->attribute_list[a];
        if (att.type == "nominal") {
            att.knots[0] = 0.0f;
            att.knots[1] = float (m_render_state->glData.nLeafs() - 1.0f);
        }
    }

    // Iterate over leaves
    for (size_t n = 0; n < m_render_state->glData.nLeafs(); n++) {
        std::map<std::string, float> att;
        for (std::size_t a = 1; a < m_render_state->attribute_list.size(); ++a) {
            att[m_render_state->attribute_list[a].name] = D(gen);
        }
        att["id"] = n;
        att["active"] = 1.0f;
        m_render_state->attribute[n] = att;
    }
    printf("Generated random attributes \n");
}





// FIXME --> move to a class for screenshot
void qgl_volume_renderer_window::take_screenshot() {

  uint8_t* buffer = new uint8_t[3 * m_render_state->width*m_render_state->height];
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glReadBuffer(GL_BACK);
  glReadPixels(0, 0, (GLsizei)m_render_state->width, (GLsizei)m_render_state->height, GL_RGB, GL_UNSIGNED_BYTE, buffer);
  size_t n = lux::find_file_slot("screenshots\\img%.4i.ppm");
  char ch[256];
  sprintf_s(ch, 256, "screenshots\\img%.4i.ppm", int(n));
  bool result = lux::ppm::write(ch, buffer, m_render_state->width, m_render_state->height, lux::ppm::UINT8);
  delete[] buffer;
  if (result) printf("screenshot '%s': success\n", ch);
  else printf("screenshot '%s': fail\n", ch);

}


void qgl_volume_renderer_window::draw_bounding_box(const lux::mat4f& MV, const lux::vec3f& c, float he) {

    glc.glUseProgram(0);
    glc.glBindVertexArray(0);
    glc.glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glDisable(GL_LIGHTING);
    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glLineWidth(4.0f);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMultMatrixf(m_render_state->camera.getProjection());
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glMultMatrixf(MV);
 
    lux::vec3f ll = c - lux::vec3f(he, he, he);
    lux::vec3f ur = c + lux::vec3f(he, he, he);

    glColor3f(m_render_state->box_color(0),  m_render_state->box_color(1), m_render_state->box_color(2));

    glBegin(GL_LINE_LOOP);
    glVertex3f(ll[0], ll[1], ll[2]);
    glVertex3f(ur[0], ll[1], ll[2]);
    glVertex3f(ur[0], ur[1], ll[2]);
    glVertex3f(ll[0], ur[1], ll[2]);
    glEnd();

    glBegin(GL_LINE_LOOP);
    glVertex3f(ll[0], ll[1], ur[2]);
    glVertex3f(ur[0], ll[1], ur[2]);
    glVertex3f(ur[0], ur[1], ur[2]);
    glVertex3f(ll[0], ur[1], ur[2]);
    glEnd();

    glBegin(GL_LINE_LOOP);
    glVertex3f(ll[0], ll[1], ur[2]);
    glVertex3f(ll[0], ll[1], ll[2]);
    glVertex3f(ll[0], ur[1], ll[2]);
    glVertex3f(ll[0], ur[1], ur[2]);
    glEnd();


    glBegin(GL_LINE_LOOP);
    glVertex3f(ur[0], ll[1], ur[2]);
    glVertex3f(ur[0], ur[1], ur[2]);
    glVertex3f(ur[0], ur[1], ll[2]);
    glVertex3f(ur[0], ll[1], ll[2]);
    glEnd();


    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

}


void qgl_volume_renderer_window::paintGL() {

  glPushAttrib(GL_ALL_ATTRIB_BITS);

  glClearColor(m_render_state->background(0), m_render_state->background(1), m_render_state->background(2), m_render_state->background(3));
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (m_render_state->animation_loop && m_render_state->animation_path.size() > 1) {
      double Ta = m_render_state->animation_time.Query();
      if (Ta > m_render_state->animation_duration[m_render_state->current_animation_duration]) {
          Ta = 0.0f;
          m_render_state->animation_time.Reset();
      }
      float tau = float(Ta / m_render_state->animation_duration[m_render_state->current_animation_duration]) * float(m_render_state->animation_path.size() - 1);
      lux::vec4f e = m_render_state->animation_path.sample_position(tau);
      e[3] = 1.0f;
      m_render_state->camera.setEye(e);
      lux::vec4f t = m_render_state->target_path.sample_position(tau);
      t[3] = 1.0f;
      m_render_state->camera.setCenter(t);
      m_render_state->arcball.setOrientation(m_render_state->animation_path.sample_orientation(tau));
      m_render_state->light_arcball.setOrientation(m_render_state->target_path.sample_orientation(tau));

      lux::vec4f rc = m_render_state->roi_path.sample_position(tau);
      rc[3] = 1.0f;
      m_render_state->roi_center = lux::vec3f(rc[0],rc[1],rc[2]);

  } 

  lux::mat4f V = m_render_state->camera.getView();
  lux::mat4f M = m_render_state->arcball.getOrientation().asMatrix();
  lux::vec3f Dv(float(m_render_state->glData.mipvolume()->get_Dimension(0)-1.0f), float(m_render_state->glData.mipvolume()->get_Dimension(1)-1.0f), float(m_render_state->glData.mipvolume()->get_Dimension(2)-1.0f));

  lux::vec3f scale = lux::vec3f(0.5f * m_render_state->scale[0] * Dv(0),
      0.5f * m_render_state->scale[1] * Dv(1),
     0.5f * m_render_state->scale[2] * Dv(2));

  lux::mat4f S = lux::mat4f::Scale(lux::vec4f( 1.0f, scale[1]/scale[0],scale[2]/scale[0], 1.0f));


  lux::mat4f P = m_render_state->camera.getProjection();
  lux::mat4f  ModelView =   V * M * S;
  
  lux::mat4f L = m_render_state->light_arcball.getOrientation().asMatrix();

  //lux::vec4f light = L * V.inverse() * lux::vec4f(0.0f, 0.0f, -1.0f, 0.0f);
  lux::mat4f MV = M * V;
  lux::vec4f light = MV.inverse_transpose() * L * lux::vec4f(0.0f, 0.0f, 1.0f, 1.0f);
  m_render_state->glData.light_dir() = lux::vec3f(light[0],light[1],light[2]).normalized();

  m_render_state->glData.shading_const() = m_render_state->shading_const;
  m_render_state->glData.lod_bias() = m_render_state->lod_bias;

  if (m_render_state->roi_clipping) {
      lux::vec3f he = lux::vec3f(m_render_state->roi_half_edge, m_render_state->roi_half_edge, m_render_state->roi_half_edge);
      lux::vec4f c = lux::vec4f(m_render_state->roi_center[0], m_render_state->roi_center[1], m_render_state->roi_center[2], 1.0f);
      lux::vec4f c_w = MV.inverse_transpose() * c;
      lux::vec3f ll = lux::vec3f(c[0],c[1],c[2]) - he;
      lux::vec3f ur = lux::vec3f(c[0], c[1], c[2]) + he;
      lux::vec3f urc = lux::vec3f(std::min(1.0f, std::max(-1.0f, ur[0])), std::min(1.0f, std::max(-1.0f, ur[1])), std::min(1.0f, std::max(-1.0f, ur[2])));
      lux::vec3f llc = lux::vec3f(std::min(1.0f, std::max(-1.0f, ll[0])), std::min(1.0f, std::max(-1.0f, ll[1])), std::min(1.0f, std::max(-1.0f, ll[2])));
      m_render_state->glData.box_ur() = urc;
      m_render_state->glData.box_ll() = llc;
  }
  else {
      m_render_state->glData.box_ur() = lux::vec3f(1.0f, 1.0f, 1.0f);
      m_render_state->glData.box_ll() = lux::vec3f(-1.0f, -1.0f, -1.0f);
  }


  lux::mat4f ModelViewProjection = m_render_state->camera.getProjection()*ModelView;

  m_render_state->glData.draw(glVolume::SHADER_CUBERILLE, m_render_state->camera.getProjection(), ModelView);

  ++m_render_state->fps_count;
  double dT = m_render_state->fps_time.Query();
 
  if (m_render_state->screenshot) {
      take_screenshot();
      m_render_state->screenshot = false;
  }

  if (dT > 1.0) {
    m_render_state->fps = double(m_render_state->fps_count) / dT;
    char ch[512];
    sprintf_s(ch, 512, "%s @ %.2ffps", m_render_state->name.c_str(), m_render_state->fps);
    setTitle(QString(ch));
    m_render_state->fps_count = 0;
    m_render_state->fps_time.Reset();
  }
  

  if (m_render_state->enable_box) {
      if (m_render_state->roi_enabled) {
          draw_bounding_box(ModelView, m_render_state->roi_center, m_render_state->roi_half_edge);
      }
      draw_bounding_box(ModelView, lux::vec3f(0.0f,0.0f,0.0f), 1.0f);

  }

  glPopAttrib();



  if (m_render_state->render_help) {
      int fontsize = std::max(8, int(float(m_render_state->height) /(2.5f*float(m_render_state->help_text.size()+1.0f))));
      int hspace = 5 * fontsize /2;
      for (std::size_t s = 0; s < m_render_state->help_text.size(); s++) {
          render_text(QString(m_render_state->help_text[s].c_str()), m_render_state->width/20, (s+1)*hspace, Qt::white, QFont("Helvetica", fontsize));
      }
  }
  else if (m_render_state->overlay_text) {
      int fontsize = std::max(6, int(24.0f * float(m_render_state->width) / 1600.0f));
      int hspace = 2*fontsize;

      attribute_prop& att1 = m_render_state->attribute_list[m_render_state->current_attribute];
      attribute_prop& att2 = m_render_state->attribute_list[m_render_state->current_second_attribute];

      QString text = QString("DAT:") + QString(m_render_state->data_list[m_render_state->current_data_file].c_str()) + QString(", ");
      text += QString("DIM:") + QString().setNum(m_render_state->glData.mipvolume()->get_Dimension(0))
          + QString("X") + QString().setNum(m_render_state->glData.mipvolume()->get_Dimension(1))
          + QString("X") + QString().setNum(m_render_state->glData.mipvolume()->get_Dimension(2));
      render_text(text, 10, hspace, Qt::lightGray, QFont("Arial", fontsize));

      text = QString("ANIM:") + QString(m_render_state->animation_loop ? " ON" : "OFF") + QString("(T=")
          + QString().setNum(m_render_state->animation_duration[m_render_state->current_animation_duration], 'g', 2) + QString(")");
      render_text(text, m_render_state->width * 3/4, hspace, Qt::green, QFont("Arial", fontsize));

      text = QString("SH:") + QString(m_render_state->glData.enable_shading() ? " ON, " : "OFF, ");
      text += QString("AO:") + QString(m_render_state->glData.lao_directions() ? " ON" : "OFF");
      render_text(text, m_render_state->width * 3/4, 2 * hspace, Qt::green, QFont("Arial", fontsize));

      text = QString("FPS:") + QString().setNum(m_render_state->fps, 'f', 1);
      render_text(text, 10, m_render_state->height - hspace, Qt::green, QFont("Arial", fontsize)) ;


      if (m_render_state->roi_enabled) {
          lux::vec3f he = lux::vec3f(m_render_state->roi_half_edge, m_render_state->roi_half_edge, m_render_state->roi_half_edge);
          lux::vec4f c = lux::vec4f(m_render_state->roi_center[0], m_render_state->roi_center[1], m_render_state->roi_center[2], 1.0f);
          lux::vec4f c_w = MV.inverse_transpose() * c;
          lux::vec3f ll = lux::vec3f(c[0], c[1], c[2]) - he;
          lux::vec3f ur = lux::vec3f(c[0], c[1], c[2]) + he;
          lux::vec3f urc = lux::vec3f(std::min(1.0f, std::max(-1.0f, ur[0])), std::min(1.0f, std::max(-1.0f, ur[1])), std::min(1.0f, std::max(-1.0f, ur[2])));
          lux::vec3f llc = lux::vec3f(std::min(1.0f, std::max(-1.0f, ll[0])), std::min(1.0f, std::max(-1.0f, ll[1])), std::min(1.0f, std::max(-1.0f, ll[2])));
          lux::rangeu32_t X = lux::rangeu32_t(uint32_t(Dv(0) * (0.5f + 0.5f * llc(0))), uint32_t(Dv(0) * (0.5f + 0.5f * urc(0))));
          lux::rangeu32_t Y = lux::rangeu32_t(uint32_t(Dv(1) * (0.5f + 0.5f * llc(1))), uint32_t(Dv(1) * (0.5f + 0.5f * urc(1))));
          lux::rangeu32_t Z = lux::rangeu32_t(uint32_t(Dv(2) * (0.5f + 0.5f * llc(2))), uint32_t(Dv(2) * (0.5f + 0.5f * urc(2))));
          m_render_state->roi_hist.update_ranges(X, Y, Z);
      
          text = QString("# ids = ") + QString().setNum(m_render_state->roi_hist.id_count());
          render_text(text, 0.7 * m_render_state->width, m_render_state->height - 4 * hspace, Qt::red, QFont("Arial", fontsize));

          text = QChar(0x03BC) + QString("_avg = ") + QString().setNum(m_render_state->roi_hist.mu() / m_render_state->roi_hist.id_count(), 'g', 2);
          render_text(text, 0.7 * m_render_state->width,  m_render_state->height - 3 * hspace, Qt::red, QFont("Arial", fontsize));

          text = QChar(0x03C3) + QString("_avg = ") + QString().setNum(m_render_state->roi_hist.sigma() / m_render_state->roi_hist.id_count(), 'g', 2);
          render_text(text, 0.7 * m_render_state->width, m_render_state->height - 2 * hspace, Qt::red, QFont("Arial", fontsize));


          text = QString(att1.name.c_str()).left(4) + QString("_avg = ") + QString().setNum(m_render_state->roi_hist.sum() / m_render_state->roi_hist.id_count(), 'g', 2);
          render_text(text, 0.7 * m_render_state->width, m_render_state->height -  hspace, Qt::red, QFont("Arial", fontsize));

      }
      else {
          //text = QString("ATT:") + (att1.name.size() > 10 ? (QString(att1.name.c_str()).left(4) + QString("..") + QString(att1.name.c_str()).right(4)) : QString(att1.name.c_str()));
          //if (m_render_state->enable_second_attribute) text += QString(" vs ") + (att2.name.size() > 10 ? (QString(att2.name.c_str()).left(4) + QString("..") + QString(att2.name.c_str()).right(4)) : QString(att2.name.c_str()));
          //text += QString(", ");
          text = QString("Empty space skipping: ") + (m_render_state->glData.empty_space_skipping() ? QString("ON") : QString("OFF"));
          //      text += QChar(945) + QString(":(") + QString().setNum(att.knots[0], 'f', 2) + QString(",") + QString().setNum(att.knots[1], 'f', 2) +
          //           QString("):[") + QString().setNum(att.alpha[0], 'f', 2) + QString(",") + QString().setNum(att.alpha[1], 'f', 2)+ QString("], ");
          //text += QString("LOD:") + QString().setNum(m_render_state->glData.lod_bias(), 'f', 1);
          //text += QString("COL:") + QString(att.color_scheme.c_str());
          render_text(text, m_render_state->width/2, m_render_state->height - hspace, Qt::red, QFont("Arial",fontsize));

      }

      //draw_color_gradient(att1, m_render_state->width / 2, 0.7f * m_render_state->height, 0.3f * m_render_state->width, 0.2f * m_render_state->height);

   }

}


void qgl_volume_renderer_window::render_text(const QString& text, int x, int y, const QColor&c, const QFont& font) {

    // Clean OpenGL status --> release shaders, vbo, framebuffer
    glc.glUseProgram(0);
    glc.glBindVertexArray(0);
    glc.glBindFramebuffer(GL_FRAMEBUFFER, 0);

    QPainter painter;
    painter.begin(this);
    painter.setPen(c);
    painter.setFont(font);
    painter.drawText(x, y, text);
    painter.end();

}

void qgl_volume_renderer_window::resizeGL(int w, int h) {

  m_render_state->width = size_t(std::max(1, w));
  m_render_state->height = size_t(std::max(1, h));
  m_render_state->arcball.reshape(m_render_state->width, m_render_state->height);
  m_render_state->light_arcball.reshape(m_render_state->width, m_render_state->height);

  m_render_state->camera.reshape(m_render_state->width, m_render_state->height);
  m_render_state->glData.reshape(m_render_state->width, m_render_state->height);
  m_render_state->glData.compute_lod_scale(m_render_state->camera.getFOV());
  glViewport(0, 0, m_render_state->width, m_render_state->height);

  // Resize raybuffer
  if (m_render_state->fbo == 0) {
    glc.glGenFramebuffers(1, &m_render_state->fbo);
    glc.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_render_state->fbo);
  }
  else {
    glc.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_render_state->fbo);
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0, 0, 0);
    glc.glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, 0);
  }
  if (m_render_state->raybuffer != 0) glDeleteTextures(1, &m_render_state->raybuffer);
  glGetError();
  glGenTextures(1, &m_render_state->raybuffer);
  glBindTexture(GL_TEXTURE_2D, m_render_state->raybuffer);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, m_render_state->width, m_render_state->height, 0, GL_RED, GL_FLOAT, nullptr);
  glBindTexture(GL_TEXTURE_2D, 0);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_render_state->raybuffer, 0);
  GLenum res = glc.glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (res != GL_FRAMEBUFFER_COMPLETE) {
    printf("Error: '%s' while resizing FBO to %zi x %zi\n", lux::errstr(res).c_str(), m_render_state->width, m_render_state->height);
    exit(0);
  }
  glc.glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glBindTextureUnit(0, m_render_state->volume);

}


void qgl_volume_renderer_window::mousePressEvent(QMouseEvent *ev) {
  
  if (ev->modifiers() == Qt::ControlModifier && ev->button() == Qt::LeftButton) {
          m_render_state->roi_dragging = true;
          m_roi_x = ev->x();
          m_roi_y = ev->y();
          return;
   }

  if (ev->button() == Qt::LeftButton) {
    m_render_state->arcball.click(size_t(ev->x()), size_t(ev->y()));
  }
  if (ev->button() == Qt::RightButton) {
      m_render_state->light_arcball.click(size_t(ev->x()), size_t(ev->y()));
  }

}


void qgl_volume_renderer_window::mouseReleaseEvent(QMouseEvent* ev) {

  if (ev->modifiers() == Qt::ControlModifier && ev->button() == Qt::LeftButton) {
      m_render_state->roi_dragging = false;
      return;
  }

  if (ev->button() == Qt::LeftButton) {
    m_render_state->arcball.release(size_t(ev->x()), size_t(ev->y()));
  }
  if (ev->button() == Qt::RightButton) {
      m_render_state->light_arcball.release(size_t(ev->x()), size_t(ev->y()));
  }
}


void qgl_volume_renderer_window::mouseMoveEvent(QMouseEvent* ev) {
  if (m_render_state->roi_dragging) {
      float dy = 2.0f * float(m_roi_y- ev->y())/float(height());
      float dx = 2.0f * float(ev->x() - m_roi_x)/float(width());

      lux::mat4f V = m_render_state->camera.getView();
      lux::mat4f M = m_render_state->arcball.getOrientation().asMatrix();

      lux::mat4f MV = M * V;
      lux::vec3f c = m_render_state->roi_center;
      lux::vec4f cw = MV * lux::vec4f(c(0), c(1), c(2), 1.0);
      cw += lux::vec4f(dx, dy, 0.0f, 0.0f);
      lux::vec4f cl = MV.inverse() * cw;

      m_render_state->roi_center = lux::vec3f(cl(0), cl(1), cl(2));
      m_render_state->roi_center[0] = std::min(1.0f, std::max(m_render_state->roi_center[0], -1.0f));
      m_render_state->roi_center[1] = std::min(1.0f, std::max(m_render_state->roi_center[1], -1.0f));
      m_render_state->roi_center[2] = std::min(1.0f, std::max(m_render_state->roi_center[2], -1.0f));

      m_roi_x = ev->x();
      m_roi_y = ev->y();

      return;
  }


  m_render_state->arcball.drag(size_t(ev->x()), size_t(ev->y()));
  m_render_state->light_arcball.drag(size_t(ev->x()), size_t(ev->y()));
}

void qgl_volume_renderer_window::wheelEvent(QWheelEvent* ev) {
    float dir = (ev->angleDelta().y() > 0 ? 1.0f : -1.0f);
    float s = 1.05f - 0.1f * dir;
    m_render_state->camera.zoom(s);
}


void qgl_volume_renderer_window::keyPressEvent(QKeyEvent *ev) {
    switch (ev->key()) {
    case Qt::Key_H: {
        m_render_state->render_help = !m_render_state->render_help;
        if (m_render_state->render_help) {
            for (std::size_t s = 0; s < m_render_state->help_text.size(); s++) {
                printf("%s\n", m_render_state->help_text[s].c_str());
            }
        }
    } break;
    case Qt::Key_2: {
        m_render_state->enable_second_attribute = !m_render_state->enable_second_attribute;
        if (m_render_state->enable_second_attribute) {
            m_tf->hide();
            m_tf2d->show();
            if (m_render_state->current_attribute == m_render_state->current_second_attribute) {
                m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
            }
            m_render_state->update_transfer_function2d();
        }
        else {
            m_tf2d->hide();
            m_tf->show();
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_O: {
        m_render_state->current_lao_directions = (m_render_state->current_lao_directions + 1) % m_render_state->lao_directions.size();
        m_render_state->glData.lao_directions() = (m_render_state->lao_directions[m_render_state->current_lao_directions]);
        printf("Local ambient occlusion: %s \n", (m_render_state->glData.lao_directions() ? "ON" : "OFF"));
        if (m_render_state->glData.lao_directions()) printf("Directions: %d \n", m_render_state->glData.lao_directions());
    } break;
    case Qt::Key_N: {
        m_render_state->glData.enable_shading() = !(m_render_state->glData.enable_shading());
        printf("Diffuse shading: %s\n", (m_render_state->glData.enable_shading() ? "ON" : "OFF"));
        if (m_render_state->glData.enable_shading()) printf(" Light dir: %.2f %.2f %.2f \n", m_render_state->glData.light_dir()[0],
            m_render_state->glData.light_dir()[1], m_render_state->glData.light_dir()[2]);
    } break;
    case Qt::Key_T: {
        m_render_state->current_animation_duration = (m_render_state->current_animation_duration + 1) % m_render_state->animation_duration.size();
    } break;
    case Qt::Key_I: {
        m_render_state->overlay_text = !m_render_state->overlay_text;
    } break;
    case Qt::Key_S: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);

        m_render_state->current_color_scheme = (m_render_state->current_color_scheme + 1) % m_render_state->color_schemes.size();
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute] );
        att.color_scheme = m_render_state->color_schemes[m_render_state->current_color_scheme];
        lux::colormap CM(att.color_scheme);// ("YlOrRd9");
        if (CM.size() != att.color_knots.size()) {
            bool logarithmic = (att.scale == "logarithmic");
            float b = att.color_knots[0];
            float e = att.color_knots[att.color_knots.size() - 1];
            float r = (logarithmic ? log(e) - log(b) : e - b);
            att.color_knots.resize(CM.size());
            for (std::size_t i = 0; i < CM.size(); ++i) {
                att.color_knots[i] = (logarithmic ? b * exp(float(i) / float(CM.size() - 1) * r) : b + r * float(i) / float(CM.size() - 1));
            }
        }
        m_render_state->box_color = CM[CM.size()-1];

        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;

    case Qt::Key_A: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);

        typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
#if 0
        attribute_prop att = m_render_state->attribute_list[m_render_state->current_attribute];
        if (att.type == "vector") {
            m_render_state->current_attribute_index++;
            std::string name = att.name +  std::to_string(m_render_state->current_attribute_index);
            if (m_render_state->attribute[0].find(name) == m_render_state->attribute[0].end()) {
                m_render_state->current_attribute_index = 0;
                m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
            }
        }
        else {
            m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
        }
#endif
        if (update_second_attribute) {
            m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
        }
        else {
            m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
        }
        
        if (m_render_state->enable_second_attribute && m_render_state->current_attribute == m_render_state->current_second_attribute) {
            if (update_second_attribute) {
                m_render_state->current_second_attribute = (m_render_state->current_second_attribute + 1) % m_render_state->attribute_list.size();
            }
            else {
                m_render_state->current_attribute = (m_render_state->current_attribute + 1) % m_render_state->attribute_list.size();
            }
        }
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
   //     std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
        std::vector<double> v(m_render_state->attribute.size());
        for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
            v[it->first] = (it->second).at(att.name.c_str());

        if (!update_second_attribute && m_render_state->roi_enabled) {
            m_render_state->roi_hist.update_attribute(v);
        }

        m_render_state->hist_gen.update_attribute(v);
        if (update_second_attribute) {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att.scale=="logarithmic");
        }
        else {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att.scale == "logarithmic");
        }

        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }


    } break;
  
    case Qt::Key_D: {
        m_render_state->current_data_file = (m_render_state->current_data_file + 1) % m_render_state->data_list.size();
        update_data();
    } break;

    case Qt::Key_J: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::unordered_map< std::size_t, std::unordered_map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.inverse = !att.inverse;
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_K: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::unordered_map< std::size_t, std::unordered_map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.periodic = !att.periodic;
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;
    case Qt::Key_L: {
        bool update_second_attribute = m_render_state->enable_second_attribute && (ev->modifiers() & Qt::ShiftModifier);
        typedef std::map< std::size_t, std::map<std::string, float> > att_map_t;
        attribute_prop& att = (update_second_attribute ? m_render_state->attribute_list[m_render_state->current_second_attribute] : m_render_state->attribute_list[m_render_state->current_attribute]);
        att.scale = (att.scale=="linear"? "logarithmic" :"linear");
        //     std::string name = att.name + (att.type == "vector" ? std::to_string(m_render_state->current_attribute_index) : "");
        std::vector<double> v(m_render_state->attribute.size());
        for (att_map_t::const_iterator it = m_render_state->attribute.begin(); it != m_render_state->attribute.end(); ++it)
            v[it->first] = (it->second).at(att.name.c_str());
        m_render_state->hist_gen.update_attribute(v);
        if (update_second_attribute) {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist_second, att.scale == "logarithmic");
        }
        else {
            m_render_state->hist_gen.aggregate(50, m_render_state->att_hist, att.scale == "logarithmic");
        }
        if (m_render_state->enable_second_attribute) {
            m_render_state->update_transfer_function2d();
        }
        else {
            m_render_state->update_transfer_function1d();
        }
    } break;

    case Qt::Key_Less: {
        m_render_state->roi_half_edge = std::max(0.05f, m_render_state->roi_half_edge - 0.05f);
    }            break;
    case Qt::Key_Greater: {
        m_render_state->roi_half_edge = std::min(1.0f, m_render_state->roi_half_edge + 0.05f);
    }                    break;
    case Qt::Key_R: {
        if (ev->modifiers() && Qt::ShiftModifier) {
            m_render_state->roi_enabled = !m_render_state->roi_enabled;
            if (m_render_state->roi_enabled) {
                m_roi_hist->show();
            }
            else {
                m_roi_hist->hide();
            }
        }
        else {
            m_render_state->animation_loop = !m_render_state->animation_loop;
            if (m_render_state->animation_loop) {
                m_render_state->animation_time.Reset();
            }
            printf("Animation loop %s\n", (m_render_state->animation_loop ? "ON" : "OFF"));
        }
    } break;
    case Qt::Key_Home: {
        reset_camera();
    } break;
    case Qt::Key_C: {
        if (ev->modifiers() && Qt::ShiftModifier) {
            m_render_state->roi_clipping = !m_render_state->roi_clipping;
        }
        else {
 //           lux::mat4f V = m_render_state->camera.getView();
//            lux::mat4f M = m_render_state->arcball.getOrientation().asMatrix();

            m_render_state->animation_path.clear();
            m_render_state->target_path.clear();
            m_render_state->roi_path.clear();
            printf("Animation path reset\n");
        }
    } break;
    case Qt::Key_P: {
        lux::sample_t<float> s;

        // HACK for performing linear interpolation of positions
        s.curvature = 0.0f;

        lux::vec4f eye_time = m_render_state->camera.getEye();
        eye_time[3] = float(m_render_state->animation_path.size());

        s.position = eye_time;
        s.orientation = m_render_state->arcball.getOrientation();
        m_render_state->animation_path.push_back(s);

        lux::vec4f target_time = m_render_state->camera.getCenter();
        target_time[3] = float(m_render_state->target_path.size());
        s.orientation = m_render_state->light_arcball.getOrientation();

        s.position = target_time;
        m_render_state->target_path.push_back(s);

        lux::vec3f rc = m_render_state->roi_center;
        s.position = lux::vec4f(rc[0],rc[1],rc[2], m_render_state->roi_path.size());

        m_render_state->roi_path.push_back(s);

        printf("Added node %f\n", s.get_parameter());
        printf("Camera: eye: %f %f %f\n", m_render_state->camera.getEye()(0),
            m_render_state->camera.getEye()(1), m_render_state->camera.getEye()(2));
        printf("Camera: target: %f %f %f\n", m_render_state->camera.getCenter()(0),
            m_render_state->camera.getCenter()(1), m_render_state->camera.getCenter()(2));
        printf("Arcball orientation:%f %f %f %f\n", m_render_state->arcball.getOrientation()(0),
            m_render_state->arcball.getOrientation()(1), m_render_state->arcball.getOrientation()(2),
            m_render_state->arcball.getOrientation()(3));
        printf("Light orientation:%f %f %f %f\n", m_render_state->light_arcball.getOrientation()(0),
            m_render_state->light_arcball.getOrientation()(1), m_render_state->light_arcball.getOrientation()(2),
            m_render_state->light_arcball.getOrientation()(3));
        printf("ROI center :%f %f %f\n", rc(0),rc(1),rc(2));

    } break;
  case Qt::Key_Left: m_render_state->camera.pan(0.05f, 0.0f); break;
  case Qt::Key_Right: m_render_state->camera.pan(-0.05f, 0.0f); break;
  case Qt::Key_U: {
      m_render_state->packing = !m_render_state->packing;
      printf("Packing :  %s\n", (m_render_state->packing ? "ON" : "OFF"));
      m_render_state->glData.pack(m_render_state->packing);
  }break;

  case Qt::Key_Up: m_render_state->camera.pan(0.0f, -0.05f); break;
  case Qt::Key_Down: m_render_state->camera.pan(0.0f, 0.05f); break;
  case Qt::Key_F11: m_render_state->screenshot = true; break;
  case Qt::Key_F1: m_render_state->enable_box = !m_render_state->enable_box; break;
  case Qt::Key_F2:
    printf("Camera: eye: %f %f %f\n", m_render_state->camera.getEye()(0),
	   m_render_state->camera.getEye()(1), m_render_state->camera.getEye()(2));
    printf("Camera: up : %f %f %f\n", m_render_state->camera.getUp()(0),
	   m_render_state->camera.getUp()(1), m_render_state->camera.getUp()(2));
    printf("Camera: lat: %f %f %f\n", m_render_state->camera.getCenter()(0),
	   m_render_state->camera.getCenter()(1), m_render_state->camera.getCenter()(2));
    break;
  case Qt::Key_Plus: m_render_state->lod_bias += 0.2f; printf("bias: %f\n", m_render_state->lod_bias); break;
  case Qt::Key_Minus: m_render_state->lod_bias -= 0.2f; printf("bias: %f\n", m_render_state->lod_bias); break;
  case Qt::Key_W: {
      QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save data file"),
          m_render_state->json_data_file.c_str(),
          tr("Json (*.json)"));
      std::string json_file = fileName.toLatin1();
      m_render_state->write_data_json(json_file); 
  } break;
  case Qt::Key_F: {
      QString fileName = QFileDialog::getOpenFileName(nullptr, tr("Open data file"),
          "test.json",
          tr("Json (*.json)"));
      std::string json_file = fileName.toLatin1();      
      m_render_state->read_data_json(json_file);
      update_data();
  } break;
  case Qt::Key_Q: {
      QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save config file"),
          "config.json",
          tr("Json (*.json)"));
      std::string json_file = fileName.toLatin1(); 
      m_render_state->write_config_json(json_file);
  } break;
  case Qt::Key_E: {
      m_render_state->glData.empty_space_skipping() = !m_render_state->glData.empty_space_skipping();
      printf("Empty space skipping:  %s\n", (m_render_state->glData.empty_space_skipping() ? "ON" : "OFF"));
  } break;
  case Qt::Key_Escape: exit(0);
  }
  
}


void qgl_volume_renderer_window::draw_color_gradient(const attribute_prop& att, float x, float y, float w, float h) {

    // Clean OpenGL status --> release shaders, vbo, framebuffer
    glc.glUseProgram(0);
    glc.glBindVertexArray(0);
    glc.glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glc.glActiveTexture(GL_TEXTURE0);

    QPainter painter;
    painter.begin(this);
    lux::colormap CM(att.color_scheme);
    QLinearGradient lin = QLinearGradient(x, y, x+w, y);

    bool logarithmic = (att.scale == "logarithmic");
    bool inverse = att.inverse;
    bool periodic = att.periodic;
    float range = (logarithmic ? log(att.color_knots[att.color_knots.size() - 1]) - log(att.color_knots[0]) : att.color_knots[att.color_knots.size() - 1] - att.color_knots[0]);

    for (std::size_t k = 0; k < att.color_knots.size(); ++k) {
        std::size_t i = (periodic ? (k > CM.size() / 2 ? 2 * (CM.size() - 1 - k) : 2 * k) : k);
        i = (inverse ? CM.size() - 1 - i : i);
        lux::color c = CM(i);
        int r = c[0] * 255;
        int g = c[1] * 255;
        int b = c[2] * 255;
        float x = (logarithmic ? log(att.color_knots[k]) - log(att.color_knots[0]) : att.color_knots[k] - att.color_knots[0]) / range;
        lin.setColorAt(x, QColor(r, g, b));
    }
    painter.setBrush(lin);
    painter.drawRect(x, y, w, h);
    painter.end();
}

