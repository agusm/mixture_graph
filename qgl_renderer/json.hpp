#ifndef  _JSON_HPP_
#define  _JSON_HPP_

#include <QJsonObject>
#include <QString>
#include <vector>
#include <lux.h>


class json {


public:

    static  bool parse_file(QJsonObject& json, const std::string& json_file);

  static  bool read_vector(const QJsonObject& obj, const QString& key, std::vector<std::string>& v);
  static  bool read_vector(const QJsonObject& obj, const QString& key, std::vector<int>& v);
  static  bool read_vector(const QJsonObject& obj, const QString& key, std::vector<std::size_t>& v);
  static  bool read_vector(const QJsonObject& obj, const QString& key, std::vector<float>& v);
  static  bool read_vector(const QJsonObject& obj, const QString& key, std::vector<double>& v);
  static  bool read_vector(const QJsonObject& obj, const QString& key, std::vector<QJsonObject>& v);



  static  bool read_value(const QJsonObject& obj, const QString& key, std::string& v);
  static  bool read_value(const QJsonObject& obj, const QString& key, int& v);
  static  bool read_value(const QJsonObject& obj, const QString& key, double& v);
  static  bool read_value(const QJsonObject& obj, const QString& key, bool& v);


  static  bool read_range(const QJsonObject& obj, const QString& key, lux::range_t<float>& range);

  static  void write_vector( QJsonObject& obj, const QString& key, const std::vector<std::string>& v);
  static  void write_vector( QJsonObject& obj, const QString& key, const std::vector<int>& v);
    static  void write_vector( QJsonObject& obj, const QString& key, const std::vector<std::size_t>& v);
    static  void write_vector( QJsonObject& obj, const QString& key, const std::vector<float>& v);
    static  void write_vector( QJsonObject& obj, const QString& key, const std::vector<double>& v);

    static  void write_value( QJsonObject& obj, const QString& key, const std::string& v);
    static  void write_value( QJsonObject& obj, const QString& key, int v);
    static  void write_value( QJsonObject& obj, const QString& key, double v);
    static  void write_value(QJsonObject& obj, const QString& key, bool v);


    static  void write_range( QJsonObject& obj, const QString& key, const lux::range_t<float>& range);
};

#endif
