#ifndef _QGL_VOLUME_RENDERER_WINDOW_HPP_
#define _QGL_VOLUME_RENDERER_WINDOW_HPP_

#include <render_state.hpp>
#include <QOpenGLWindow>
#include <QOpenGLExtraFunctions>
#include <QOpenGLFunctions_4_5_Compatibility>
#include <QLabel>
#include <qtransfer_function.hpp>
#include <qtransfer_function2d.hpp>
#include <qrender_config.hpp>
#include <qroi_histogram.hpp>

class qgl_volume_renderer_window : public QOpenGLWindow {
    Q_OBJECT
public:
   enum shader_id {
        SHADER_STATIC_QUAD = 0,
        SHADER_RAY_ENTRY = 1,
        SHADER_RAY_EXIT = 2,
    };



public:
  qgl_volume_renderer_window(render_state * s = nullptr, QOpenGLWindow::UpdateBehavior updateBehavior = QOpenGLWindow::NoPartialUpdate): QOpenGLWindow(updateBehavior) {
    attach_render_state(s);
  };


  void start();

protected:

  // Interaction
  void mousePressEvent(QMouseEvent *ev) override;
  void mouseReleaseEvent(QMouseEvent *ev) override;
  void mouseMoveEvent(QMouseEvent *ev) override;
  void wheelEvent(QWheelEvent* ev) override;

  void keyPressEvent(QKeyEvent *ev) override;
  
  void reset_camera();

  void take_screenshot();
  void render_text(const QString& text, int x = 0, int y = 20, const QColor& c = Qt::white, const QFont& font = QFont("Helvetica",20));
  // Render update
  void initializeGL() override;
  void paintGL() override;
  void resizeGL(int w, int h) override;

  void init_attributes();
  
  void update_data();

  void draw_color_gradient(const attribute_prop& att, float x,float y, float w, float h);

  void draw_bounding_box(const lux::mat4f& MV, const lux::vec3f& c, float he);

  void attach_render_state(render_state* s) {
    m_render_state = s;
  }
  
protected:
  QOpenGLExtraFunctions glx;
  QOpenGLFunctions_4_5_Compatibility glc;
  render_state* m_render_state;
  qtransfer_function* m_tf;
  qtransfer_function2d* m_tf2d;

  qroi_histogram* m_roi_hist;

  qrender_config* m_rc;

protected: 
    int m_roi_x;
    int m_roi_y;

};

#endif


