#ifndef _QRENDER_CONFIG_HPP_
#define _QRENDER_CONFIG_HPP_

#include <QWidget>
#include <render_state.hpp>



class qrender_config : public QWidget
{
  Q_OBJECT

public:
  qrender_config(render_state* rs = nullptr,  QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent* event) override {
        draw_text(0.7f * width(), 0.3f * height(), QString("Ka = ") + QString().setNum(m_render_state->shading_const[0]));
        draw_text(0.7f * width(), 0.5f * height(), QString("Kd = ") + QString().setNum(m_render_state->shading_const[1]));
        draw_text(0.7f * width(), 0.7f * height(), QString("Ks = ") + QString().setNum(m_render_state->shading_const[2]));
    }

    void draw_text(int x, int y, const QString& txt); 

public Q_SLOTS:
    void change_ka(int v) {
        m_render_state->shading_const[0] = float(v) / 100.0f;
        update();
    }

    void change_kd(int v) {
        m_render_state->shading_const[1] = float(v) / 100.0f;
        update();
    }

    void change_ks(int v) {
        m_render_state->shading_const[2] = float(v) / 100.0f;
        update();
    }
 
protected:
  render_state* m_render_state;

};

#endif
