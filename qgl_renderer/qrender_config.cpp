#include <qrender_config.hpp>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QSlider>
#include <QPainter>

qrender_config::qrender_config(render_state* rs,  QWidget *parent)
    : QWidget(parent)
{
  m_render_state = rs;
  setAttribute(Qt::WA_StaticContents);
  
  setWindowTitle("Render config");

  resize(600,300);
 
  QGroupBox* group_box = new QGroupBox(tr("Shading Constants"), this);
  group_box->setGeometry(0.05f*width(), 0.05f*height(), 0.65f*width(), 0.9f*height());

  QSlider* slider_ka = new QSlider(Qt::Horizontal, this);
  slider_ka->setGeometry(0.1f*width(), 0.1f*height(), 0.5f*width(), 0.3f*height());
  slider_ka->setFocusPolicy(Qt::StrongFocus);
  slider_ka->setTickPosition(QSlider::TicksBothSides);
  slider_ka->setMinimum(0);
  slider_ka->setMaximum(100);
  slider_ka->setTickInterval(10);
  slider_ka->setSingleStep(1);
  slider_ka->setValue(100 * m_render_state->shading_const[0]);

  QSlider* slider_kd = new QSlider(Qt::Horizontal, this);
  slider_kd->setFocusPolicy(Qt::StrongFocus);
  slider_kd->setGeometry(0.1f * width(), 0.4f * height(), 0.5f * width(), 0.3f * height());
  slider_kd->setTickPosition(QSlider::TicksBothSides);
  slider_kd->setMinimum(0);
  slider_kd->setMaximum(100);
  slider_kd->setTickInterval(10);
  slider_kd->setSingleStep(1);
  slider_kd->setValue(100 * m_render_state->shading_const[1]);


  QSlider* slider_ks = new QSlider(Qt::Horizontal, this);
  slider_ks->setGeometry(0.1f * width(), 0.7f * height(), 0.5f * width(), 0.3f * height());
  slider_ks->setFocusPolicy(Qt::StrongFocus);
  slider_ks->setTickPosition(QSlider::TicksBothSides);
  slider_ks->setMinimum(0);
  slider_ks->setMaximum(100);
  slider_ks->setTickInterval(10);
  slider_ks->setSingleStep(1);
  slider_ks->setValue(100 * m_render_state->shading_const[2]);

  QVBoxLayout* vbox = new QVBoxLayout;
  vbox->addWidget(slider_ka);
  vbox->addWidget(slider_kd);
  vbox->addWidget(slider_ks);
  group_box->setLayout(vbox);

  connect(slider_ka, &QSlider::valueChanged,
      this, &qrender_config::change_ka);

  connect(slider_kd, &QSlider::valueChanged,
      this, &qrender_config::change_kd);

  connect(slider_ks, &QSlider::valueChanged,
      this, &qrender_config::change_ks);

}


void qrender_config::draw_text(int x, int y, const QString& txt) {
    QPainter painter(this);
    QPen pen;
    pen.setColor(Qt::blue);
    painter.setPen(pen);
    painter.setFont(QFont("Helvetica", 18));
    painter.drawText(x, y, txt);
}
