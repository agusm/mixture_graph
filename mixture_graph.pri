
CONFIG += release console
DESTDIR=../bin

QMAKE_CXXFLAGS_RELEASE += -openmp -std:c++17 -fp:fast
QMAKE_CFLAGS_RELEASE += -openmp -std:c++17 -fp:fast 
