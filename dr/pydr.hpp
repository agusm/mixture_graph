#ifndef _PYDR_HPP_
#define _PYDR_HPP_

#include <python_wrapper.hpp>

class pydr {

public:
    typedef std::vector<float> vectorf_t;
    typedef std::vector<vectorf_t> vector_vectorf_t;

    typedef std::vector<int> vectori_t;
    typedef std::vector<vectori_t> vector_vectori_t;

public:


  pydr() {
    python_wrapper_.init("dimension_reduction", "C:/ProgramData/Miniconda3" );
    xlim_.resize(2);
    ylim_.resize(2);
    
    xlim_[0] = -3.0; 
    xlim_[1] = 3.0;
    ylim_[0] = -3.0;
    ylim_[1] = 3.0;

    width_ = 128;
    height_ = 128;
    printf("dimension reduction initialized \n");
  }


 
public: // Hooks to python functions

  const vectorf_t& density() const { return density_; }
  const vector_vectorf_t& morse_smale_points() const { return morse_smale_points_; }
  const vector_vectori_t& partitions() const { return partitions_; }

  std::size_t& width()  { return width_; }
  std::size_t& height()  { return height_; }

  const std::size_t& width() const { return width_;  }
  const std::size_t& height() const { return height_; }

  const vectorf_t& xlim() const { return xlim_; }
  void set_xlim(float xmin, float xmax) { xlim_[0] = xmin; xlim_[1] = xmax; }

  const vectorf_t& ylim() const { return ylim_; }
  void set_ylim(float ymin, float ymax) { ylim_[0] = ymin; ylim_[1] = ymax; }


  void pca( const vector_vectorf_t& x, vector_vectorf_t& y );  

  void umap( const vector_vectorf_t& x, vector_vectorf_t& y, int nneighbors = 10, float dist = 0.5f);
  
  void tsne( const vector_vectorf_t& x, vector_vectorf_t& y, float perplexity = 40.0f, int niter = 200);
  

  void kmeans(  const vector_vectorf_t& x,  vectori_t& l, int nclasses);
  
  void dbscan(  const vector_vectorf_t& x, vectori_t& l, int min_samples = 10, int min_cluster_size = 10);
  
  void compute_density(const vector_vectorf_t& x);

  void kde(const vector_vectorf_t& x, const vectorf_t& xlim, const vectorf_t& ylim, const std::string& fname="", const std::string& cmap="");

 
  void compute_morse_smale_complex(int k_neigbors=100, float persistence = 0.0);

  void kde_morse_smale(const vector_vectorf_t& x, const vectorf_t& xlim, const vectorf_t& ylim, const std::string& fimage = "", const std::string& cmap = "", bool geodesic = false,  float gamma = 2.0f, int k_neighbors = 100,
      int resolution = 128);

  


protected:

  python_wrapper  python_wrapper_;

 
  std::size_t width_;
  std::size_t height_;

  std::vector<float> xlim_;
  std::vector<float> ylim_;

  std::vector< float > density_;
  std::vector<  std::vector<int> > partitions_;
  std::vector< std::vector<float> > morse_smale_points_;

};

#endif
