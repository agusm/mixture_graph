
#include <python_wrapper.hpp>
#include <stdio.h>
#include <vector>
#include <stdexcept>
#include <map>


void python_wrapper::init(const std::string& module, const std::string& path) {
  Py_SetPythonHome((const wchar_t*)path.c_str());
  Py_Initialize();
  _import_array();

  PyObject* pname = PyUnicode_DecodeFSDefault(module.c_str());
  /* Error checking of pName left out */
  if( pname == NULL ) {
    printf("Module %s not valid: exiting. \n", module.c_str());
    return;
  }
  pmodule_ = PyImport_Import(pname);
  if( pmodule_ == NULL ) {
    PyErr_Print();
    printf("Module %s not imported: exiting. \n", module.c_str());
    return;
  }
  Py_DECREF(pname);
}


python_wrapper::~python_wrapper() {
    if (pmodule_) {
        Py_DECREF(pmodule_);
        pmodule_ == NULL;
    }
    if (pfunc_) {
        Py_DECREF(pfunc_);
        pfunc_ == NULL;
    }
    if (pvalue_) {
        Py_DECREF(pvalue_);
        pvalue_ == NULL;
    }
    if (pposargs_) {
        Py_DECREF(pposargs_);
        pposargs_ == NULL;
    }
    if (pnamedargs_) {
        Py_DECREF(pnamedargs_);
        pnamedargs_ == NULL;
    }

  if ( Py_FinalizeEx() < 0 ) {
    printf("Exiting python interpreter in bad state \n");
  }			    
}


void python_wrapper::exec() {

    printf("Entering exec \n");

    if (pvalue_) {
        printf("Decref pvalue = %x \n", pvalue_);
        Py_DECREF(pvalue_);
        pvalue_ = NULL;
    }
   
    if (pfunc_ == NULL) {
        printf("(EEE): Function cannot be executed \n");
        return;
    }
    printf("Before import array \n");
    
    if (pposargs_ == NULL) {
        init_posargs(0);
    }

    printf("Call  \n");
    pvalue_ = PyObject_Call(pfunc_, pposargs_, pnamedargs_);
  
    printf("After the call \n");
    if (pposargs_) {
        Py_DECREF(pposargs_);
        pposargs_ = NULL;
    }
    if (pnamedargs_) {
        Py_DECREF(pnamedargs_);
        pnamedargs_ = NULL;
    }

    if (pvalue_ == NULL) {
        Py_DECREF(pfunc_);
        PyErr_Print();
        printf("Call failed. \n");
        pfunc_ = NULL;
    }
    printf(" pvalue  = %x ", pvalue_);
    
}


void python_wrapper::set_func( const std::string& method) {

   printf("Function init: %s \n", method.c_str());

   if (pmodule_ == NULL) {
    printf("Module is not valid: exiting. \n");
    return;
  }
  
  pfunc_ = PyObject_GetAttrString(pmodule_, method.c_str());
  /* pFunc is a new reference */
  if (pfunc_ == NULL || ! PyCallable_Check(pfunc_) ) {
    if (PyErr_Occurred()) PyErr_Print();     
    printf("Cannot find function \"%s\"\n", method);
  }

}



// =====
// LISTS
// =====

PyObject* helpers::vector_to_list(const std::vector<float> &data) {
  PyObject* listObj = PyList_New( data.size() );
  if (!listObj) {
    printf("Unable to allocate memory for Python list");
    return NULL;
  }
  for (unsigned int i = 0; i < data.size(); i++) {
    PyObject *num = PyFloat_FromDouble( (double) data[i]);
    if (!num) {
      Py_DECREF(listObj);
      printf("Data not allocated");
      return NULL;
    }
    PyList_SET_ITEM(listObj, i, num);
  }
  return listObj;
}

// ======
// TUPLES
// ======

PyObject* helpers::vector_to_tuple(const std::vector<float> &data) {
  PyObject* tuple = PyTuple_New( data.size() );
  if (!tuple) {
    printf("Unable to allocate memory for Python tuple \n");
    return NULL;
  }
  for (unsigned int i = 0; i < data.size(); i++) {
    PyObject *num = PyFloat_FromDouble( (double) data[i]);
    if (!num) {
      Py_DECREF(tuple);
      printf("Unable to allocate memory for Python tuple. \n");
      return NULL;
    }
    PyTuple_SET_ITEM(tuple, i, num);
  }
  return tuple;
}

PyObject* helpers::vector_vector_to_list(const std::vector< std::vector< float > > &data) {
  PyObject*  listObj = PyList_New( data.size() );
  if (!listObj) {
    printf("Unable to allocate memory for Python list of lists. \n");
    return NULL;
  }
  for (unsigned int i = 0; i < data.size(); i++) {
    PyObject* subList =  vector_to_list(data[i]);
    if (!subList) {
      Py_DECREF(listObj);
      printf("Unable to allocate memory for Python sublist. \n");
      return NULL;
    }
    PyList_SET_ITEM(listObj, i, subList);
  }
  return listObj;
}


PyObject* helpers::vector_vector_to_tuple(const std::vector< std::vector< float > > &data) {
  PyObject* tuple = PyTuple_New( data.size() );
  if (!tuple) {
    printf("Unable to allocate memory for Python tuple \n");
    return NULL;
  }
  for (unsigned int i = 0; i < data.size(); i++) {
    PyObject* subTuple = vector_to_tuple(data[i]);  
    if (!subTuple) {
      Py_DECREF(tuple);
      printf("Unable to allocate memory for Python sub tuple. \n");
      return NULL;
    }
    PyTuple_SET_ITEM(tuple, i, subTuple);
  }
  return tuple;
}

// PyObject -> Vector
void  helpers::list_tuple_to_vector(PyObject* incoming,  std::vector<float>& data) {
  data.clear();
  if (PyTuple_Check(incoming)) {
    for(Py_ssize_t i = 0; i < PyTuple_Size(incoming); i++) {
      PyObject *value = PyTuple_GetItem(incoming, i);
      data.push_back( PyFloat_AsDouble(value) );
    }
  } else {
    if (PyList_Check(incoming)) {
      for(Py_ssize_t i = 0; i < PyList_Size(incoming); i++) {
	PyObject *value = PyList_GetItem(incoming, i);
	data.push_back( PyFloat_AsDouble(value) );
      }
    } else {
      printf("Passed PyObject pointer was not a list or tuple! \n");
    }
  }
}

// PyObject -> Vector
void  helpers::list_tuple_to_vector(PyObject* incoming, std::vector<int>& data) {
  data.clear();
  if (PyTuple_Check(incoming)) {
    for(Py_ssize_t i = 0; i < PyTuple_Size(incoming); i++) {
      PyObject *value = PyTuple_GetItem(incoming, i);
      data.push_back( PyFloat_AsDouble(value) );
    }
  } else {
    if (PyList_Check(incoming)) {
      for(Py_ssize_t i = 0; i < PyList_Size(incoming); i++) {
	PyObject *value = PyList_GetItem(incoming, i);
	data.push_back( PyFloat_AsDouble(value) );
      }
    } else {
      printf("Passed PyObject pointer was not a list or tuple! \n");
    }
    Py_DECREF(incoming);
  }
  
}

PyObject* helpers::map_to_dict( const std::map<std::string, float>& params) {

  if ( params.empty() ) {
    return NULL;
  }
  PyObject*  dict = PyDict_New();
  if (!dict) {
    printf("Could not allocate dict object \n");
    return NULL;
  }
  for (std::map<std::string, float>::const_iterator it = params.begin();it != params.end(); ++it) {
    if ( PyDict_SetItemString( dict, it->first.c_str(), PyFloat_FromDouble( (double) it->second ) ) < 0 ) {
      printf("Could not set pair %s, %f \n", it->first.c_str(), it->second);
    };
  }
  return dict;
}

PyObject* helpers::vector_vector_to_nparray(const std::vector< std::vector<float> >& vec){

    //_import_array();
   // rows not empty
    printf("Vector vector conversion start. \n");
    PyObject* vec_array = NULL;
    if (vec.empty()) {
        printf("Input vector is empty. \n");
        return vec_array;
    }
    size_t nRows = vec.size();
    if (vec[0].empty()) {
        printf("Input subvector is empty. \n");
        return vec_array;
    }
        
    size_t nCols = vec[0].size();
    npy_intp dims[2] = {nRows, nCols};
    
    vec_array =  PyArray_SimpleNew(2, dims, PyArray_FLOAT);
    if (vec_array == NULL) {
        printf("nparray not %d x %d allocated. \n", nRows, nCols);
        return vec_array;
    }
    float *vec_array_pointer = (float*) PyArray_DATA(vec_array);
    printf("Before memory copy. \n");
    for (size_t iRow=0; iRow < nRows; ++iRow){
       if( vec[iRow].size() != nCols) {
             Py_DECREF(vec_array); // delete
             printf("Cannot convert vector<vector<float>> to np.array, since c++ matrix shape is not uniform.\n");
             return NULL;
        }
	    std::copy(vec[iRow].begin(),vec[iRow].end(),vec_array_pointer+iRow*nCols);
     }
     printf("Vector vector conversion end. \n");
     return vec_array;
}

PyObject* helpers::vector_to_nparray(const std::vector<float>& vec){

   // rows not empty
   if( !vec.empty() ){

       size_t nRows = vec.size();
       npy_intp dims[1] = {nRows};

       PyObject* vec_array =  PyArray_SimpleNew(1, dims,  PyArray_FLOAT);
       float *vec_array_pointer = (float*) PyArray_DATA(vec_array);

       std::copy(vec.begin(),vec.end(),vec_array_pointer);
       return vec_array;

       // no data at all
   } else {
      npy_intp dims[1] = {0};
      return  PyArray_ZEROS(1, dims, PyArray_FLOAT, 0);
   }

}


void helpers::nparray_to_vector_vector(PyObject* incoming,  std::vector< std::vector< float> > & data) {
  _import_array();
  printf("Vector vector conversion start. \n");
  data.clear();
  if (PyArray_Check(incoming)) {
    PyArrayObject* arr_obj = (PyArrayObject*) incoming;
    if ( arr_obj->nd != 2 ) {
      printf("Error: nparray is not 2D \n");
    }
    std::vector<npy_intp> dimensions(2);
    dimensions[0] = PyArray_DIM(arr_obj, 0);
    dimensions[1] = PyArray_DIM(arr_obj, 1);

    for (npy_intp i = 0; i < dimensions[0]; ++i) {
      std::vector<float> v_i(arr_obj->dimensions[1]);
      for (npy_intp j = 0; j < dimensions[1]; ++j) {
          double* item = ((double*)PyArray_GETPTR2(arr_obj, i, j));
          v_i[j] = float(*item);
      }
      data.push_back( v_i);
      //printf(" %f ", data[i][0]);
    }
    //Py_DECREF(incoming);
  } else {
    printf("(WWW): Incoming object is not an array. \n");
  }
  printf("Vector vector conversion end. \n");
}


void helpers::nparray_to_vector(PyObject* incoming,  std::vector< float>  & data) {
  _import_array();
  data.clear();
  if (PyArray_Check(incoming)) {

      PyArrayObject* arr_obj = (PyArrayObject*)incoming;
      if (arr_obj->nd != 1) {
          printf("Error: nparray is not 1D \n");
      }
      npy_intp dim = PyArray_DIM(arr_obj, 0);
      printf("Dim = %d \n", int(dim));
      // Change this way to access the data
      data.resize(dim);
      for (npy_intp i = 0; i < dim; ++i) {
          double* internal_data = ((double*)PyArray_GETPTR1(arr_obj, i));
          data[i] = float( *internal_data);
         // printf(" %f ", data[i]);
      }
  }  else {
    printf("(WWW): Incoming object is not an array. \n");
  }
}

void helpers::nparray_to_vector( PyObject* incoming,  std::vector< int >  & data) {
  _import_array();
  data.clear();
  if (PyArray_Check(incoming)) {
    PyArrayObject* arr_obj = (PyArrayObject*) incoming;
    if ( arr_obj->nd != 1 ) {
      printf("Error: nparray is not 1D \n");
    }
    npy_intp dim = PyArray_DIM(arr_obj, 0);
    //printf("Dim = %d \n", int(dim));
    // Change this way to access the data
    data.resize(dim);
    for (npy_intp i = 0; i < dim; ++i) {
        long* internal_data = ((long*)PyArray_GETPTR1(arr_obj, i));
        data[i] = int(*internal_data);
        //printf(" %d ", data[i]);
    }
  } else {
    printf("(WWW): Incoming object is not an array. \n");
  }
}
