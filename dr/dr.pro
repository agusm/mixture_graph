include(../mixture_graph.pri)

CONFIG+= staticlib 
TEMPLATE= lib


HEADERS= \
python_wrapper.hpp pydr.hpp sptree.h  tsne.h  vptree.h  

INCLUDEPATH += C:/ProgramData/Miniconda3/include C:/ProgramData/Miniconda3/pkgs/numpy-1.18.4-py37hae9e721_0/Lib/site-packages/numpy/core/include
#LIBS+= -LC:/ProgramData/Miniconda3/libs -lpython37


SOURCES= \
python_wrapper.cpp pydr.cpp sptree.cpp  tsne.cpp 

TARGET=dr

#include(../external_dep.pri)

