#include <pydr.hpp>


void pydr::pca(const pydr::vector_vectorf_t& x, pydr::vector_vectorf_t& y ) {
  python_wrapper_.set_func("compute_pca");
  python_wrapper_.init_posargs(1);
  python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
  python_wrapper_.exec();
  PyObject* res = python_wrapper_.return_value();
  if (res != NULL) {
      helpers::nparray_to_vector_vector(res, y);
  }
  else {
      printf("(EEE):  Invalid result for python function \n");
  }
}


void pydr::umap( const pydr::vector_vectorf_t& x, pydr::vector_vectorf_t& y, int nneighbors, float dist) {
    python_wrapper_.set_func("compute_umap");
    python_wrapper_.init_posargs(1);
    python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
    python_wrapper_.init_namedargs();
    python_wrapper_.set_namedarg("n_neighbors", helpers::pyval(nneighbors));
    python_wrapper_.set_namedarg("min_dist", helpers::pyval(dist));
    python_wrapper_.exec();
    PyObject* res = python_wrapper_.return_value();
    if (res != NULL) {
        helpers::nparray_to_vector_vector(res, y);
    }
    else {
        printf("(EEE):  Invalid result for python function \n");
    }
}

void pydr::tsne( const pydr::vector_vectorf_t& x, pydr::vector_vectorf_t& y,float perplexity, int niter) {
    python_wrapper_.set_func("compute_tsne");
    python_wrapper_.init_posargs(1);
    python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
    python_wrapper_.init_namedargs();
    python_wrapper_.set_namedarg("perplexity", helpers::pyval(perplexity));
    python_wrapper_.set_namedarg("n_iter", helpers::pyval(niter));
    python_wrapper_.exec();
    PyObject* res = python_wrapper_.return_value();
    if (res != NULL) {
        helpers::nparray_to_vector_vector(res, y);
    }
    else {
        printf("(EEE):  Invalid result for python function \n");
    }
}


void pydr::dbscan( const pydr::vector_vectorf_t& x, pydr::vectori_t& l, int min_samples, int min_cluster_size  ) {
  python_wrapper_.set_func("hdbscan_clusters");
  python_wrapper_.init_posargs(1);
  python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
  python_wrapper_.init_namedargs();
  python_wrapper_.set_namedarg("min_samples", helpers::pyval(min_samples));
  python_wrapper_.set_namedarg("min_cluster_size", helpers::pyval(min_cluster_size));
  python_wrapper_.exec();
  PyObject* res = python_wrapper_.return_value();
  if (res != NULL) {
      helpers::nparray_to_vector(res, l);
  }
  else {
      printf("(EEE):  Invalid result for python function \n");
  }
}

void pydr::kmeans( const pydr::vector_vectorf_t& x, pydr::vectori_t& l, int n_clusters) {
  python_wrapper_.set_func("kmeans_clusters");
  python_wrapper_.init_posargs(1);
  python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
  python_wrapper_.init_namedargs();
  python_wrapper_.set_namedarg("n_clusters", helpers::pyval(n_clusters));
  python_wrapper_.exec();
  PyObject* res = python_wrapper_.return_value();
  if (res != NULL) {
      helpers::nparray_to_vector(res, l);
  }
  else {
      printf("(EEE):  Invalid result for python function \n");
  }
  printf("Kmeans computed \n");
}

void pydr::kde(const pydr::vector_vectorf_t& x, const pydr::vectorf_t& xlim, const pydr::vectorf_t& ylim, const std::string& fimage, const std::string& cmap) {
    python_wrapper_.init_posargs(1);
    python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
    python_wrapper_.init_namedargs();
    python_wrapper_.set_namedarg("xlim", helpers::vector_to_tuple(xlim) );
    python_wrapper_.set_namedarg("ylim", helpers::vector_to_tuple(ylim));
    if (fimage != "") python_wrapper_.set_namedarg("fimage", helpers::pyval(fimage));
    if (cmap != "") python_wrapper_.set_namedarg("cmap", helpers::pyval(cmap));
    python_wrapper_.exec();
    PyObject* res = python_wrapper_.return_value();
    if ( res == NULL ) printf("Issues with KDE computation \n");
}

void pydr::compute_density(const pydr::vector_vectorf_t& x)
{
    python_wrapper_.set_func("compute_kde");
    python_wrapper_.init_posargs(1);
    python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
    python_wrapper_.init_namedargs();
    python_wrapper_.set_namedarg("xlim", helpers::vector_to_tuple(xlim_));
    python_wrapper_.set_namedarg("ylim", helpers::vector_to_tuple(ylim_));
    python_wrapper_.set_namedarg("width", helpers::pyval(int(width_)));
    python_wrapper_.set_namedarg("height", helpers::pyval(int(height_)));
    python_wrapper_.exec();
    PyObject* res = python_wrapper_.return_value();
    if (res != NULL) {
        helpers::nparray_to_vector(res, density_);
    }
    else {
        printf("(EEE):  Invalid result for python function \n");
    }
}


void pydr::kde_morse_smale(const pydr::vector_vectorf_t& x, const pydr::vectorf_t& xlim, const pydr::vectorf_t& ylim, const std::string& fimage, const std::string& cmap, bool geodesic, float gamma, int k_neighbors,
    int resolution) {
    python_wrapper_.set_func("kde_msc");
    python_wrapper_.init_posargs(1);
    python_wrapper_.set_posarg(0, helpers::vector_vector_to_nparray(x));
    python_wrapper_.init_namedargs();
    python_wrapper_.set_namedarg("xlim", helpers::vector_to_tuple(xlim));
    python_wrapper_.set_namedarg("ylim", helpers::vector_to_tuple(ylim));
    python_wrapper_.set_namedarg("gamma", helpers::pyval(gamma));
    python_wrapper_.set_namedarg("k_neighbors", helpers::pyval(k_neighbors));
    python_wrapper_.set_namedarg("resolution", helpers::pyval(resolution));
    python_wrapper_.set_namedarg("geodesic", helpers::pyval(geodesic));
    if (fimage != "") python_wrapper_.set_namedarg("fimage", helpers::pyval(fimage));
    if (cmap != "") python_wrapper_.set_namedarg("cmap", helpers::pyval(cmap));
    python_wrapper_.exec();
    PyObject* res = python_wrapper_.return_value();
    if (res == NULL) printf("(EEE): Issues with KDE - Morse Smale computation \n");
}


void pydr::compute_morse_smale_complex( int k_neighbors, float persistence) {
    python_wrapper_.set_func("compute_msc");
    python_wrapper_.init_posargs(1);
    python_wrapper_.set_posarg(0, helpers::vector_to_nparray(density_));
    python_wrapper_.init_namedargs();
    python_wrapper_.set_namedarg("xlim", helpers::vector_to_tuple(xlim_));
    python_wrapper_.set_namedarg("ylim", helpers::vector_to_tuple(ylim_));
    python_wrapper_.set_namedarg("width", helpers::pyval(int(width_)));
    python_wrapper_.set_namedarg("height", helpers::pyval(int(height_)));
    python_wrapper_.set_namedarg("k_neighbors", helpers::pyval(k_neighbors));
    python_wrapper_.set_namedarg("persistence_level", helpers::pyval(persistence));
    python_wrapper_.exec();
    PyObject* res = python_wrapper_.return_value();
    if (res != NULL) {
        helpers::nparray_to_vector_vector(res, morse_smale_points_);
    }
}
