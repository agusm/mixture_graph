#ifndef _PYTHON_WRAPPER_HPP_
#define _PYTHON_WRAPPER_HPP_

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <string>
#include <vector>
#include <map>
#include <numpy/arrayobject.h>

class helpers {

public: // static helpers
    static inline PyObject* pyval(float v) { return PyFloat_FromDouble(v); }
    static inline PyObject* pyval(int v) { return PyLong_FromLong(v); }
    static inline PyObject* pyval(const std::string& v) {
        return  Py_BuildValue("s#", v.c_str(), v.size()); 
    } 
    static inline PyObject* pyval(bool v) { return (v ? Py_True: Py_False); }

    static inline double to_double( PyObject* o) { return PyFloat_AsDouble(o); }
    static inline long to_long( PyObject* o) { return PyLong_AsLong(o); }
    static inline bool to_bool( PyObject* o) { return (o == Py_True); }
    static inline std::string  to_string( PyObject* o) { return std::string(PyBytes_AsString(o)); }

    static PyObject* vector_to_list(const std::vector<float>& data);
    static PyObject* vector_to_tuple(const std::vector<float>& data);
    static PyObject* vector_vector_to_tuple(const std::vector< std::vector< float > >& data);
    static PyObject* vector_vector_to_list(const std::vector< std::vector< float > >& data);
    static void  list_tuple_to_vector( PyObject* incoming, std::vector<float>& data);
    static void  list_tuple_to_vector( PyObject* incoming, std::vector<int>& data);
    static void nparray_to_vector_vector( PyObject* incoming, std::vector< std::vector< float> >& data);
    static void nparray_to_vector( PyObject* incoming, std::vector< float>& data);
    static void nparray_to_vector( PyObject* incoming, std::vector< int >& data);
    static PyObject* vector_vector_to_nparray(const std::vector< std::vector<float> >& vec);
    static PyObject* vector_to_nparray(const std::vector< float >& vec);
    static PyObject* map_to_dict(const std::map<std::string, float>& params);
};


class python_wrapper {

public:
  python_wrapper(const std::string& module = "", const std::string& path="") {
    pmodule_ = NULL;
    pposargs_ = NULL;
    pnamedargs_ = NULL;
    pfunc_ = NULL;
    pvalue_ = NULL;
    if ( module != "") init(module,path);
  }

  ~python_wrapper();
  
  PyObject* return_value()  { return pvalue_; }
  void exec();


public: // positional and named arguments modifiers
    void init_posargs(std::size_t n) {
        if (pposargs_) {
            Py_DECREF(pposargs_);
        }
        pposargs_ = PyTuple_New(n);
        if (pposargs_ == NULL) {
            printf("(WWW): Cannot create positional args \n");
        }
    }

    void set_posarg(std::size_t n, PyObject* o) {
        if (pposargs_ == NULL) {
            printf("(WWW): Cannot set positional argument %d. \n", int(n));
            return;
        } 
        PyTuple_SET_ITEM(pposargs_, n, o);
    }

    void init_namedargs() {
        if (pnamedargs_) {
            Py_DECREF(pnamedargs_);
        }
        pnamedargs_ = PyDict_New();
        if (pnamedargs_ == NULL) {
            printf("(WWW): Cannot create named args \n");
        }
    }

    void set_namedarg(const std::string& k, PyObject* val) {
        if (pnamedargs_ == NULL ) {
            printf("(WWW): Cannot set positional argument %s. \n", k.c_str());
            return;
        }
        if (PyDict_SetItemString(pnamedargs_, k.c_str(), val) < 0) {
            printf("Could not set item %s \n", k.c_str());
        };
        
    }

public: // set function

   void set_func(const std::string& method);

public:
   void init(const std::string& module, const std::string& path);


protected:

  PyObject* pmodule_;
  PyObject* pposargs_;
  PyObject* pnamedargs_;
  PyObject* pfunc_;
  PyObject* pvalue_;
};

#endif
