#import os 
#print(os.getcwd())

import sys
#print( sys.path )

if not sys.argv:
  sys.argv.append("(C++)")

#path = ['C:\\ProgramData\\Miniconda\\pkgs', 'C:\\ProgramData\\Miniconda3\\python37.zip', 'C:\\ProgramData\\Miniconda3\\DLLs', 'C:\\ProgramData\\Miniconda3', 'C:\\ProgramData\\Miniconda3\\lib\\site-packages', 'C:\\ProgramData\\Miniconda3\\lib\\site-packages\\win32', 'C:\\ProgramData\\Miniconda3\\lib\\site-packages\\win32\\lib', 'C:\\ProgramData\\Miniconda3\\lib\\site-packages\\Pythonwin']
#for l in path:
#    sys.path.append(l)

import os
import umap
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
import hdbscan
import numpy as np
import matplotlib.pyplot as pl
import scipy.stats as st

import morse_smale

import math
from sklearn.neighbors import KDTree


os.environ['QT_PLUGIN_PATH']='C:\ProgramData\Miniconda3\Library\plugins'


def compute_pca(x):
    scaler = StandardScaler()
    x = scaler.fit_transform(x)
    pca = PCA(n_components=2)
    y = pca.fit_transform(x)
    return y


def test(a=5,b=3):
    return a+b
    
def compute_tsne(x,perplexity=40,n_iter=300):
    scaler = StandardScaler()
    x = scaler.fit_transform(x)
    tsne = TSNE(n_components=2, perplexity=perplexity, n_iter=n_iter)
    y = tsne.fit_transform(x)
    return y

def compute_umap(x,n_neighbors=15,min_dist=0.1):
    scaler = StandardScaler()
    x = scaler.fit_transform(x)
    y = umap.UMAP(
        n_neighbors=n_neighbors,
        min_dist=min_dist,
        n_components=2,
        random_state=42).fit_transform(x)
    print(y)
    y = y.astype(np.float64)
    return y
    
    
def kmeans_clusters(x, n_clusters = 6):
    kmeans  = KMeans(n_clusters=n_clusters, random_state=0).fit(x)
    labels = kmeans.labels_
    fig = pl.figure()
    ax = fig.gca()
    #ax.scatter(x[:,0],x[:,1],c=labels,cmap='BuPu',s=1.0)
    #pl.savefig('test-kmeans.png',bbox_inches='tight',pad_inches=0)
    return labels

def hdbscan_clusters(x, min_samples = 10, min_cluster_size = 10):
    print(x)
    x  = x.astype(np.float32)
    labels = hdbscan.HDBSCAN(
        min_samples=min_samples,
        min_cluster_size=min_cluster_size).fit_predict(x)
    fig = pl.figure()
    ax = fig.gca()
    ax.scatter(x[:,0],x[:,1],c=labels,cmap='BuPu')
    pl.savefig('test.png',bbox_inches='tight',pad_inches=0)
    print(labels)
    return labels


def compute_kde(data, xlim=(-3,3),ylim=(-3,3), width=128, height=128):
    #f=data[:100,0]
    #print(f)
    #return f
    x = data[:, 0]
    y = data[:, 1]
    xmin, xmax = xlim[0],xlim[1]
    ymin, ymax = ylim[0],ylim[1]

    # Perform the kernel density estimate
    xx, yy = np.mgrid[xmin:xmax:height*1j, ymin:ymax:width*1j]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([x, y])
    kernel = st.gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)
    f_lin = f.reshape( width*height )
    # f = np.array( [1.0, 2.0, 3.0, 4.0, 5.0] )
    print(f_lin)
    return f_lin

def compute_msc( f_lin, xlim=(-3,3),ylim=(-3,3),width=128,height=128,k_neighbors=100,persistence_level=0.0 ):
    xmin, xmax = xlim[0],xlim[1]
    ymin, ymax = ylim[0],ylim[1]

    # Perform the kernel density estimate
    x_space = np.linspace(xlim[0], xlim[1], height)
    y_space = np.linspace(ylim[0], ylim[1], width)
    xs, ys = [np.array(a.flat) for a in np.meshgrid(x_space, y_space)]
        
    
    # Perform the kernel density estimate
    xx, yy = np.mgrid[xmin:xmax:width*1j, ymin:ymax:height*1j]
   
    sample_points = np.stack([xs,ys]) 

    # k_neighbors is the main parameter that may need to be tweaked
    f_lin = f_lin.astype(np.float64)
    msc = morse_smale.nn_partition(sample_points,f_lin,k_neighbors,persistence_level)

    X = []
    for s in msc.min_indices: 
      X.append( [xs[s],ys[s],0.0] )
    for s in msc.max_indices:    
      X.append( [xs[s],ys[s],1.0] )
    X = np.reshape(X, (-1,3))
    print(X)
    return X 
      
  
def kde(data, xlim=(-3,3),ylim=(-3,3),width=100,height=100, fimage='test.png', cmap='BuPu', contourf=True):
    x = data[:, 0]
    y = data[:, 1]
    xmin, xmax = xlim[0],xlim[1]
    ymin, ymax = ylim[0],ylim[1]

    # Perform the kernel density estimate
    xx, yy = np.mgrid[xmin:xmax:height*1j, ymin:ymax:width*1j]
    f = compute_kde(data,xlim,ylim,width,height)

    fig = pl.figure()
    ax = fig.gca()
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    if contourf:
        # Contourf plot
        set = ax.contourf(xx, yy, f, cmap=cmap)
    else:
        ## Or kernel density estimate plot instead of the contourf plot
        ax.imshow(np.rot90(f), cmap=cmap, extent=[xmin, xmax, ymin, ymax])
    # Contour plot
    #set = ax.contour(xx, yy, f, colors='k')
    # Label plot
    #x.clabel(cset, inline=1, fontsize=10)
    #x.set_xlabel('Y1')
    #x.set_ylabel('Y0')
    pl.axis('off')
    #print('fname is', fimage)
    pl.savefig(fimage,bbox_inches='tight',pad_inches=0)
    pl.close()
    return None



def morse_smale_complex_interp(f,msc,xs,ys,xlim, ylim, gamma = 1.5, cmap='Set1', tau = 1.0e-12, geodesic = True):
    
    def _dist2(x,y):
        sum  = 0.0
        for i in range(x.shape[0]):
            sum += (x[i]-y[i])**2
        return sum
        
    def _norm2(v):
        return _dist2(v,np.zeros(v.shape[0]))
    
    def _normalized(v, tau = 1.0e-12):
        res = v
        d2 = _norm2(v)
        if d2 > tau:
            res = 1.0/(d2**0.5)*v
        return res
    
    def _trace(x0,x1,f,nablaf, delta = 0.25, csi = 0.5, tau = 1.0e-12, ascending = True):
        x = x0.astype(float)
        l = 0.0
        pos = []
        sign = 1.0
        if not ascending:
            sign = -1.0
        while _dist2(x,x1) > csi:
            pos.append(x)
            u = max(f.shape[0]-1, min(0, int(x[0])))
            v = max(f.shape[1]-1, min(0, int(x[1])))
            df = np.array( [nablaf[0][u][v],nablaf[1][u][v]])
            if _norm2(df) > tau:
                df = sign * _normalized(df)
            else:
                df = _normalized(x1-x)
            step = delta * np.dot(x1-x,df)
            if (step < 1.0e-3):
                df = _normalized(x1-x)
                step = delta * np.dot(x1-x,df)
            l += step 
            x += step * df
        return l,np.asarray(pos).reshape(-1,2)
            
    c,r=f.shape
    img = np.zeros(shape=(c,r,3))
    colors_max = pl.cm.get_cmap(cmap)
    
    peaks_indices = { i:n for n,i in enumerate(set(msc.max_indices))}
    
    X_m = np.array([ [xs[s],ys[s]] for s in msc.min_indices ]) 
    kdt_m = KDTree(X_m, leaf_size=30, metric='euclidean')
    
    X_M = np.array([ [xs[s],ys[s]] for s in msc.max_indices ]) 
    kdt_M = KDTree(X_M, leaf_size=30, metric='euclidean')
    
#    print (kdt_m.query(X_m[0].reshape(1,-1), k=3, return_distance=False))
    
    valley_indices = { i:n for n,i in enumerate(set(msc.min_indices))}
    
    nablaf = np.gradient(f)
    
    color_min =  np.array([1.0, 1.0, 1.0])
    x0 = np.array( [xlim[0], ylim[0]])
    xrange = np.array([(xlim[1]-xlim[0]),(ylim[1]-ylim[0])])
    xstep = np.array([(xlim[1]-xlim[0])/float(r),(ylim[1]-ylim[0])/float(c)])
    for v in range(c):
        for u in range(r):
            partition = msc.partitions[u+v*r]
            uv = np.array([u,v])
            x = np.array(x0) + np.array([u*xstep[0],v*xstep[1]])
            
            imax = kdt_M.query(x.reshape(1,-1), k=1, return_distance=False)[0]
            icolor = peaks_indices[ msc.max_indices[imax[0]] ]
 
            xmax = X_M[imax][0][0]
            ymax = X_M[imax][0][1]
            
            imin = kdt_m.query(x.reshape(1,-1), k=1, return_distance=False)[0]
 
            xmin = X_m[imin[0]][0]
            ymin = X_m[imin[0]][1]

            umin = int((xmin-x0[0])/xrange[0]*(r-1))            
            umax = int((xmax-x0[0])/xrange[0]*(r-1))
        
            vmin = int((ymin-x0[1])/xrange[1]*(c-1))
            vmax = int((ymax-x0[1])/xrange[1]*(c-1))
        
            uvmin = np.array([umin,vmin])
            uvmax = np.array([umax,vmax])
                        
            color_peak = np.array(colors_max(icolor/(len(peaks_indices)-1)))[:3]
            #color_peak = np.array(colors_max(partition%8/8))[:3]
            
            t = 1.0
            if False:
                if f[vmax,umax]-f[vmin,umin] > tau:
                    t = (f[v,u]-f[vmin,umin])/(f[vmax,umax]-f[vmin,umin])
                    t = min(1.0,max(t**gamma,0.0))
            #print ('norm2=',_norm2(df))
            #print('color_peak=',color_peak.shape)
            if True:
                if not geodesic:
                    p  = np.array([x[0],x[1],f[v,u]])
                    #print(p)
                    pm = np.array([xmin,ymin,f[vmin,umin]])
                    #print(pm)
                    tm = _dist2(p,pm)**0.5
                    pM = np.array([xmax,ymax,f[vmax,umax]])
                    tM = _dist2(p,pM)**0.5
                    t  = min(1.0, (tm/(tm+tM))**gamma )
     #               t  = (f[x]-f[xs[imin],ys[imin]])/(f[xs[imax],ys[imax]]-f[xs[imin],ys[imin]])
                else:
                    l_to_peak, path_to_peak = _trace(uv,uvmax,f,nablaf, ascending = True)
                    l_to_valley, path_to_valley = _trace(uv,uvmin,f,nablaf, ascending = False)
                    #print(l_to_peak)
                    #print(l_to_valley)
                    t_geo = min(1.0,max(l_to_valley/(l_to_peak + l_to_valley),0.0)) 
                    t_rho = min(1.0,max((f[v,u] - f[vmin,umin])/(f[vmax,umax] - f[vmin,umin]),0.0)) 
                    t = (0.5*t_geo+0.5*t_rho)**gamma # min(1.0,max(t_rho,0.0))**gamma
            img[v,u] = t * color_peak + (1.0-t) * color_min
    return img



def kde_msc(data, fimage='morse_smale.png', resolution=100, xlim=(-3,3),ylim=(-3,3), gamma=2.0, cmap='Set2', geodesic=False, k_neighbors=100 ):

    x = data[:, 0]
    y = data[:, 1]
    xmin, xmax = xlim[0],xlim[1]
    ymin, ymax = ylim[0],ylim[1]

    # Perform the kernel density estimate
    x_space = np.linspace(xlim[0], xlim[1], resolution)
    y_space = np.linspace(ylim[0], ylim[1], resolution)
    x_sample, y_sample = [np.array(a.flat) for a in np.meshgrid(x_space, y_space)]
        
    
    # Perform the kernel density estimate
    xx, yy = np.mgrid[xmin:xmax:resolution*1j, ymin:ymax:resolution*1j]
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([x, y])
    kernel = st.gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)
   
    f_lin  = f.reshape(f.shape[0]*f.shape[1])
    sample_points = np.stack([x_sample,y_sample]) 

    # k_neighbors is the main parameter that may need to be tweaked
    msc = morse_smale.nn_partition(sample_points,f_lin,k_neighbors=100,persistence_level=0.0)
    #morse_smale.nn_merged_partition(sample_points,f_lin,k_neighbors=100)
    
    img = morse_smale_complex_interp(f,msc,x_sample,y_sample,xlim=xlim,ylim=ylim,gamma=gamma,cmap=cmap,geodesic=geodesic)
    fig = pl.figure()
    ax = fig.gca()
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    pl.axis('off')
    ax.imshow(np.rot90(img), cmap=cmap, extent=[xmin, xmax, ymin, ymax])
    pl.savefig(fimage,bbox_inches='tight',pad_inches=0)
    pl.close()

    print('msc interp')
    return None
