#ifndef __NORMAL_VOLUME_H__
#define __NORMAL_VOLUME_H__

#include<map>
#include<inttypes.h>
#include<stdio.h>
#include<vector>
#include<string>
#include<limits>
#include<cassert>
#include<array>
#include"vec_t.h"

class normal_volume {
public:
	using data_t = vec3_t;
	normal_volume(void);
	normal_volume(const normal_volume& other);
	normal_volume(uint32_t rx, uint32_t ry, uint32_t rz);
	~normal_volume(void);
	bool empty(void) const;
	void clear(void);
	size_t size(void) const;
	void resize(uint32_t i, uint32_t j, uint32_t k);
	const data_t& operator()(uint32_t i, uint32_t j, uint32_t k) const;
	const data_t& operator()(const uivec_t& p) const;
	data_t& operator()(uint32_t i, uint32_t j, uint32_t k);
	data_t& operator()(const uivec_t& p);
	const data_t& operator[](size_t n) const;
	data_t& operator[](size_t n);
	const uint32_t& dimension(uint32_t n) const;
	const normal_volume& operator=(const normal_volume& other);
	enum class format {
		FLOAT = 0,
		UCHAR = 1,
		USHORT= 2
	};
	bool read(const std::string& name);
	bool read(FILE* stream);
	bool write(const std::string& name, const format& F = format::FLOAT) const;
	bool write(FILE* stream, const format& F = format::FLOAT) const;
	void zero(void);
	void normalize(void);
	normal_volume subsample(void);
	const data_t* data(void) const;
	data_t* data(void);
	inline size_t linear_address(uint32_t i, uint32_t j, uint32_t k) const;
	inline size_t linear_address(const uivec_t& p) const;
	inline uivec_t unpack(size_t laddr) const;
	inline void as_RGB8(std::vector<uint8_t>& result) const;
	inline void as_RGB16(std::vector <uint16_t> & result) const;
	inline void from_RGB8(const std::vector<uint8_t>& data);
	inline void from_RGB16(const std::vector<uint16_t>& data);
	inline void swap(normal_volume& other) noexcept;
protected:
	std::vector<vec3_t>		m_data;
	std::array<uint32_t,3>	m_dims;
};

inline void normal_volume::swap(normal_volume& other) noexcept {
	m_data.swap(other.m_data);
	m_dims.swap(other.m_dims);
}

namespace std {
	template<>
	inline void swap(normal_volume& A, normal_volume& B) noexcept {
		A.swap(B);
	};
};

inline size_t normal_volume::linear_address(uint32_t i, uint32_t j, uint32_t k) const {
	assert("normal_volume::linear_address() -- invalid argument" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	return size_t(i) + size_t(m_dims[0]) * (size_t(j) + size_t(m_dims[1]) * size_t(k));
}
inline size_t normal_volume::linear_address(const uivec_t& p) const {
	return linear_address(p[0], p[1], p[2]);
}

inline uivec_t normal_volume::unpack(size_t laddr) const {
	assert("normal_volume::unpack() -- invalid argument" && laddr < m_data.size());
	uivec_t result;
	result[0] = laddr % size_t(m_dims[0]);
	laddr /= size_t(m_dims[0]);
	result[1] = laddr % size_t(m_dims[1]);
	laddr /= size_t(m_dims[1]);
	result[2] = uint32_t(laddr);
	return result;
}


#endif