#ifndef __BITSTREAM_H__
#define __BITSTREAM_H__

#include<vector>
#include<inttypes.h>
#include<memory>
#include<stdio.h>
#include<string>

template<typename data_t>
class bitstream {
public:
	bitstream(void);															///< default constructor
	bitstream(const uint64_t& size);											///< resize constructor
	bitstream(const bitstream& other);											///< copy constructor
	~bitstream(void);															///< destructor
	bool empty(void) const;														///< true iff empty
	const uint64_t& bitsize(void) const;										///< size of stream in bits
	void clear(void);															///< deallocate memory
	bitstream& operator=(const bitstream& other);								///< assignment operator
	void resize(const uint64_t& bitsize);										///< resize to specified bitsize

	const uint64_t& seek(const uint64_t& pos);									///< sets file pointer to specified position, returns file pointer
	const uint64_t& tell(void) const;											///< returns file pointer
	bool eof(void) const;														///< end of file
	/// THE FOLLOWING IS NOT THREADSAFE!!
	uint32_t read(data_t& output, uint32_t nBits);								///< read from current file pointer, return number of bits read
	uint32_t write(const data_t& input, uint32_t nBits);						///< append at end, return number of bits written
	/// THESE ARE THREADSAFE.
	uint32_t read(data_t& output, const uint64_t& pos, uint32_t nBits);			///< read from position, return number of bits read
	uint32_t write(const data_t& input, const uint64_t& pos, uint32_t nBits);	///< write to specified position, may increase size, return number of bits written

	bool read(const std::string& name);											///< read from file
	bool read(FILE* stream);													///< read from stream
	bool write(const std::string& name) const;									///< write to file
	bool write(FILE* stream) const;												///< write to stream

	template<typename alt_t>
	inline bitstream<alt_t> as(void) const;										///< change underlying data type
	inline data_t*			data(void)			{ return m_data.data(); }		///< direct access to underlying vector
	inline const data_t*	data(void) const	{ return m_data.data(); }		///< direct access to underlying vector
	inline size_t			size(void) const	{ return m_data.size(); }		///< direct access to underlying vector
protected:
	std::vector<data_t>		m_data;
	uint64_t				m_ptr;
	uint64_t				m_size;
	inline size_t			ptr_addr(void) const;
	inline uint32_t			ptr_offs(void) const;
};

template<typename data_t>
bitstream<data_t>::bitstream(void) : m_ptr(0), m_size(0) {
}

template<typename data_t>
bitstream<data_t>::bitstream(const uint64_t& size) : m_ptr(0), m_size(0) {
	resize(size);
}

template<typename data_t>
bitstream<data_t>::bitstream(const bitstream& other) : m_ptr(0), m_size(0) {
	*this = other;
}

template<typename data_t>
bitstream<data_t>::~bitstream(void) {
	clear();
}

template<typename data_t>
bool bitstream<data_t>::empty(void) const {
	return m_data.empty();
}

template<typename data_t>
const uint64_t& bitstream<data_t>::bitsize(void) const {
	return m_size;
}

template<typename data_t>
void bitstream<data_t>::clear(void) {
	m_data.clear();
	m_size = m_ptr = 0;
}

template<typename data_t>
bitstream<data_t>& bitstream<data_t>::operator=(const bitstream& other) {
	m_data = other.m_data;
	m_ptr = other.m_ptr;
	m_size = other.m_size;
	return *this;
}

template<typename data_t>
void bitstream<data_t>::resize(const uint64_t& bitsize) {
	size_t nsize = (bitsize + uint64_t(8 * sizeof(data_t)-1)) / (8 * sizeof(data_t));
	if (nsize != m_data.size()) {
		size_t oldsize = m_data.size();
		m_data.resize(nsize);
		if (m_data.size() > oldsize) memset(m_data.data() + oldsize, 0, sizeof(data_t)*(nsize - oldsize));
	}
	m_size = bitsize;
	m_ptr = std::min(m_ptr, m_size);
}

template<typename data_t>
const uint64_t& bitstream<data_t>::seek(const uint64_t& pos) {
	m_ptr = std::min(pos, m_size);
	return m_ptr;
}

template<typename data_t>
const uint64_t& bitstream<data_t>::tell(void) const {
	return m_ptr;
}

template<typename data_t>
bool bitstream<data_t>::eof(void) const {
	return m_ptr >= m_size;
}

template<typename data_t>
uint32_t bitstream<data_t>::read(data_t& output, uint32_t nBits) {
	output = 0;
	if (eof()) return 0;
	static uint32_t element_size = (sizeof(data_t) << 3);
	size_t addr = ptr_addr();
	uint32_t offs = ptr_offs();
	uint64_t old_ptr = m_ptr;
	uint32_t cap = uint32_t(element_size - offs);
	nBits = std::min(element_size, nBits);
	uint32_t s = 0;
	while (nBits > 0) {
		// advance one element
		if (cap == 0) {
			if (eof()) return uint32_t(m_ptr - old_ptr);
			cap = element_size;
			addr++;
			offs = 0;
		}
		uint32_t r = std::min(cap, nBits);
		data_t m = (r == cap ? 0 : (data_t(1) << (offs + r))) + (data_t(-1) << offs);
		output |= ((m_data[addr] & m) >> offs) << s;
		s += r;
		nBits -= r;
		m_ptr += r;
		cap -= r;
	}
	return uint32_t(m_ptr - old_ptr);
}

template<typename data_t>
uint32_t bitstream<data_t>::write(const data_t& input, uint32_t nBits) {
	if (m_ptr + nBits > m_size) resize(m_ptr + nBits);
	static uint32_t element_size = (sizeof(data_t) << 3);
	size_t addr = ptr_addr();
	uint32_t offs = ptr_offs();
	uint64_t old_ptr = m_ptr;
	uint32_t output = input;
	uint32_t cap = uint32_t(element_size - offs);	// capacity left in this element
	while (nBits > 0) {
		// advance one element
		if (cap == 0) {
			m_data.push_back(data_t(0));
			cap = element_size;
			offs = 0;
			addr++;
		}
		// truncate nBits to capacity, compute mask, and write
		uint32_t w = std::min(cap, nBits);
		data_t m = (w == cap ? 0 : (data_t(1) << (offs + w))) + (data_t(-1) << offs);
		m_data[addr] = (m_data[addr] & (~m)) | ((output << offs)&m);
		output >>= w;
		nBits -= w;
		m_ptr += w;
		cap -= w;
	}
	return uint32_t(m_ptr - old_ptr);
}

template<typename data_t>
uint32_t bitstream<data_t>::read(data_t& output, const uint64_t& pos, uint32_t nBits) {	
	output = 0;
	if (eof()) return 0;
	static uint32_t element_size = (sizeof(data_t) << 3);
	uint64_t ptr = pos;
	size_t addr = size_t(ptr / uint64_t(sizeof(data_t) << 3));
	uint32_t offs = size_t(ptr % uint64_t(sizeof(data_t) << 3));
	uint64_t old_ptr = ptr;
	uint32_t cap = uint32_t(element_size - offs);
	nBits = std::min(element_size, nBits);
	uint32_t s = 0;
	while (nBits > 0) {
		// advance one element
		if (cap == 0) {
			if (eof()) return uint32_t(ptr - old_ptr);
			cap = element_size;
			addr++;
			offs = 0;
		}
		uint32_t r = std::min(cap, nBits);
		data_t m = (r == cap ? 0 : (data_t(1) << (offs + r))) + (data_t(-1) << offs);
		output |= ((m_data[addr] & m) >> offs) << s;
		s += r;
		nBits -= r;
		ptr += r;
		cap -= r;
	}
	return uint32_t(ptr - old_ptr);
}

template<typename data_t>
uint32_t bitstream<data_t>::write(const data_t& input, const uint64_t& pos, uint32_t nBits) {
	uint64_t ptr = pos;
	if (ptr + nBits > m_size) resize(ptr + nBits);
	static uint32_t element_size = (sizeof(data_t) << 3);
	size_t addr = size_t(ptr / uint64_t(sizeof(data_t) << 3));
	uint32_t offs = size_t(ptr % uint64_t(sizeof(data_t) << 3));
	uint64_t old_ptr = ptr;
	uint32_t output = input;
	uint32_t cap = uint32_t(element_size - offs);	// capacity left in this element
	while (nBits > 0) {
		// advance one element
		if (cap == 0) {
			m_data.push_back(data_t(0));
			cap = element_size;
			offs = 0;
			addr++;
		}
		// truncate nBits to capacity, compute mask, and write
		uint32_t w = std::min(cap, nBits);
		data_t m = (w == cap ? 0 : (data_t(1) << (offs + w))) + (data_t(-1) << offs);
		m_data[addr] = (m_data[addr] & (~m)) | ((output << offs) & m);
		output >>= w;
		nBits -= w;
		ptr += w;
		cap -= w;
	}
	return uint32_t(ptr - old_ptr);	
}

template<typename data_t>
bool bitstream<data_t>::read(const std::string& name) {
	if (name.empty())  return false;
	FILE* stream = NULL;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read(stream);
	fclose(stream);
	return result;
}

template<typename data_t>
bool bitstream<data_t>::read(FILE* stream) {
	if (stream == nullptr) return false;
	clear();
	uint64_t bitsize;
	if (fread(&bitsize, sizeof(uint64_t), 1, stream) != 1) return false;
	resize(bitsize);
	size_t bytesize = (m_size + 7) >> size_t(3);
	if (fread(m_data.data(), 1, bytesize, stream) != bytesize) return false;
	return true;
}
template<typename data_t>
bool bitstream<data_t>::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = NULL;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream);
	fclose(stream);
	return result;
}

template<typename data_t>
bool bitstream<data_t>::write(FILE* stream) const {
	if (stream == nullptr) return false;
	if (fwrite(&m_size, sizeof(uint64_t), 1, stream) != 1) return false;
	size_t bytesize = (m_size + 7) >> size_t(3);
	if (fwrite(m_data.data(), 1, bytesize, stream) != bytesize) return false;
	return true;
}

template<typename data_t>
inline size_t bitstream<data_t>::ptr_addr(void) const {
	return size_t(m_ptr / uint64_t(sizeof(data_t) << 3));
}

template<typename data_t>
inline uint32_t bitstream<data_t>::ptr_offs(void) const {
	return uint32_t(m_ptr % uint64_t(sizeof(data_t) << 3));
}

template<typename data_t>
template<typename alt_t>
inline bitstream<alt_t> bitstream<data_t>::as(void) const {
	bitstream<alt_t> result;
	result.resize(m_size);
	memcpy(result.data(), m_data.data(), sizeof(alt_t)*result.size());
	result.seek(m_ptr);
	return result;
}

#endif
