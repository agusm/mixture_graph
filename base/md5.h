#ifndef __MD5_H__
#define __MD5_H__

#define NOMINMAX
#include<Windows.h>
#include<inttypes.h>
#include<string>
#include"bitstream.h"

extern DWORD md5(std::string& hash, const void* mem, uint64_t bytes);

template<class T>
DWORD md5(std::string& hash, const bitstream<T>& data) {
	uint64_t bytes = (data.bitsize() + 7) >> uint64_t(3);
	return md5(hash, data.data(), bytes);
}

#endif
