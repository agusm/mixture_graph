#ifndef __SEGVOLUME_H__
#define __SEGVOLUME_H__

#include"sparse.h"
#include<cassert>
#include<inttypes.h>
#include<stdio.h>
#include<stdlib.h>
#include"volume.h"
#include"memfile.h"

template<typename id_t>
class segvolume {
public:
	segvolume(void);
	segvolume(const segvolume& other);
	segvolume(uint32_t rx, uint32_t ry, uint32_t rz);
	~segvolume(void);
	bool empty(void) const;
	void clear(void);
	size_t size(void) const;
	void resize(uint32_t rx, uint32_t ry, uint32_t rz);

	const sparse::vector<id_t>& operator()(uint32_t i, uint32_t j, uint32_t k) const;
	const sparse::vector<id_t>& operator()(size_t n) const;
	sparse::vector<id_t>& operator()(uint32_t i, uint32_t j, uint32_t k);
	sparse::vector<id_t>& operator()(size_t n);

	void build(const volume& V);
	const uint32_t& dimension(uint32_t n) const;
	const id_t& get_background(void) const;
	void set_background(id_t bg);
	bool is_background(uint32_t i, uint32_t j, uint32_t k);
	bool is_background(size_t n);

	const segvolume& operator=(const segvolume& other);
	static const id_t INVALID;

	double avg_nonzeros(void) const;
	id_t max_id(void) const;

	void trim(void);
	bool read(const std::string& name);
	bool read(FILE* stream);
	bool read(memfile& stream);
	bool write(const std::string& name) const;
	bool write(FILE* stream) const;
	bool write(memfile& stream) const;
protected:
	sparse::vector<id_t>*	m_data;	
	uint32_t				m_dims[3];
	id_t					m_background;
	inline size_t linearAddress(uint32_t i, uint32_t j, uint32_t k) const {
		return size_t(i) + size_t(m_dims[0])*(size_t(j) + size_t(k)*m_dims[1]);
	}
};

template<typename id_t>
const id_t segvolume<id_t>::INVALID = id_t(-1);

template<typename id_t>
segvolume<id_t>::segvolume(void) : m_data(nullptr), m_background(INVALID) {
	for (int i = 0; i < 3; i++) m_dims[i] = 0;
}

template<typename id_t>
segvolume<id_t>::segvolume(const segvolume<id_t>& other) : m_data(nullptr), m_background(INVALID) {
	for (int i = 0; i < 3; i++) m_dims[i] = 0;
	*this = other;
}

template<typename id_t>
segvolume<id_t>::segvolume(uint32_t rx, uint32_t ry, uint32_t rz) : m_data(nullptr), m_background(INVALID) {
	for (int i = 0; i < 3; i++) m_dims[i] = 0;
	resize(rx, ry, rz);
}
template<typename id_t>
segvolume<id_t>::~segvolume(void) {
	clear();
}

template<typename id_t>
bool segvolume<id_t>::empty(void) const {
	return m_data == nullptr;
}

template<typename id_t>
void segvolume<id_t>::clear(void) {
	if (m_data != nullptr) delete[] m_data;
	m_data = nullptr;
	for (int i = 0; i < 3; i++) m_dims[i] = 0;
	m_background = INVALID;
}

template<typename id_t>
size_t segvolume<id_t>::size(void) const {
	return size_t(m_dims[0])*size_t(m_dims[1])*size_t(m_dims[2]);
}

template<typename id_t>
void segvolume<id_t>::resize(uint32_t rx, uint32_t ry, uint32_t rz) {
	size_t ns = size_t(rx)*size_t(ry)*size_t(rz);
	if (ns == 0) {
		clear();
		return;
	}
	if (ns != size()) {
		if (m_data != nullptr) delete[] m_data;
		m_data = new sparse::vector<id_t>[ns];		
	}
	m_dims[0] = rx;
	m_dims[1] = ry;
	m_dims[2] = rz;
}

template<typename id_t>
const sparse::vector<id_t>& segvolume<id_t>::operator()(uint32_t i, uint32_t j, uint32_t k) const {
	assert("segvolume<id_t>::operator() - invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	return m_data[linearAddress(i, j, k)];
}

template<typename id_t>
const sparse::vector<id_t>& segvolume<id_t>::operator()(size_t n) const {
	assert("segvolume<id_t>::operator() - invalid parameter" && n < size());
	return m_data[n];
}

template<typename id_t>
sparse::vector<id_t>& segvolume<id_t>::operator()(uint32_t i, uint32_t j, uint32_t k) {
	assert("segvolume<id_t>::operator() - invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	return m_data[linearAddress(i, j, k)];
}

template<typename id_t>
sparse::vector<id_t>& segvolume<id_t>::operator()(size_t n) {
	assert("segvolume<id_t>::operator() - invalid parameter" && n < size());
	return m_data[n];
}

template<typename id_t>
void segvolume<id_t>::build(const volume& V) {
	resize(V.dimension(0), V.dimension(1), V.dimension(2));
#pragma omp parallel for schedule(dynamic,32)
	for (int64_t n = 0; n < int64_t(V.size()); n++) m_data[n].unit(id_t(V[n]));	
	m_background = V.background();	
}

template<typename id_t>
const uint32_t& segvolume<id_t>::dimension(uint32_t n) const {
	assert("segvolume<id_t>::dimension() - invalid parameter" && n < 3);
	return m_dims[n];
}

template<typename id_t>
const id_t& segvolume<id_t>::get_background(void) const {
	return m_background;
}

template<typename id_t>
void segvolume<id_t>::set_background(id_t bg) {
	for (size_t n = 0; n < size(); n++) {
		if (m_data[n].empty()) m_data[n].set(m_background, 1.0f);
	}
	m_background = bg;
	trim();
}

template<typename id_t>
bool segvolume<id_t>::is_background(uint32_t i, uint32_t j, uint32_t k) {
	assert("segvolume<id_t>::is_background() - invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	const sparse::vector<id_t>& vox(m_data[linearAddress(i, j, k)]);
	if (vox.size()>1) return false;
	if (vox.empty()) return true;
	return vox.get_unit_id() == m_background;
}

template<typename id_t>
bool segvolume<id_t>::is_background(size_t n) {
	assert("segvolume<id_t>::is_background() - invalid parameter" && n < size());
	const sparse::vector<id_t>& vox(m_data[n]);
	if (vox.size()>1) return false;
	if (vox.empty()) return true;
	return vox.get_unit_id() == m_background;
}

template<typename id_t>
const segvolume<id_t>& segvolume<id_t>::operator=(const segvolume& other) {
	resize(other.m_dims[0], other.m_dims[1], other.m_dims[2]);
	for (size_t n = 0; n < size(); n++) {
		m_data[n] = other.m_data[n];		
	}
	m_background = other.m_background;
	return *this;
}

template<typename id_t>
void segvolume<id_t>::trim(void) {
	for (size_t n = 0; n < size(); n++) m_data[n].trim();		
}

template<typename id_t>
bool segvolume<id_t>::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = NULL;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool r = read(stream);
	fclose(stream);
	return r;
}

template<typename id_t>
bool segvolume<id_t>::read(FILE* stream) {
	if (stream == nullptr) return false;
	uint64_t bg;
	if (fread(&bg, sizeof(uint64_t), 1, stream) != 1) return false;
	uint32_t d[3];
	if (fread(d, sizeof(uint32_t), 3, stream) != 3) return false;
	resize(d[0], d[1], d[2]);
	m_background = id_t(bg);
	for (size_t n = 0; n < size(); n++) if (!m_data[n].read(stream)) return false;
	return true;
}

template<typename id_t>
bool segvolume<id_t>::read(memfile& stream) {
	if (!stream.is_open()) return false;
	uint64_t bg;
	if (stream.read(&bg, sizeof(uint64_t), 1) != 1) return false;
	uint32_t d[3];
	if (stream.read(d, sizeof(uint32_t), 3) != 3) return false;
	resize(d[0], d[1], d[2]);
	m_background = id_t(bg);
	for (size_t n = 0; n < size(); n++) if (!m_data[n].read(stream)) return false;
	return true;
}

template<typename id_t>
bool segvolume<id_t>::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = NULL;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool r = write(stream);
	fclose(stream);
	return r;
}

template<typename id_t>
bool segvolume<id_t>::write(FILE* stream) const {
	if (stream == nullptr) return false;
	uint64_t bg = uint64_t(m_background);
	if (fwrite(&bg, sizeof(uint64_t), 1, stream) != 1) return false;
	if (fwrite(m_dims, sizeof(uint32_t), 3, stream) != 3) return false;
	for (size_t n = 0; n < size(); n++) if (!m_data[n].write(stream)) return false;
	return true;
}

template<typename id_t>
bool segvolume<id_t>::write(memfile& stream) const {
	if (!stream.is_open()) return false;
	uint64_t bg = uint64_t(m_background);
	if (stream.write(&bg, sizeof(uint64_t), 1) != 1) return false;
	if (stream.write(m_dims, sizeof(uint32_t), 3) != 3) return false;
	for (size_t n = 0; n < size(); n++) if (!m_data[n].write(stream)) return false;
	return true;
}

template<typename id_t>
double segvolume<id_t>::avg_nonzeros(void) const {
	if (empty()) return 0.0;
	double count = 0.0;
	for (size_t n = 0; n < size(); n++) count += double(m_data[n].size());
	return count / double(size());
}

template<typename id_t>
id_t segvolume<id_t>::max_id(void) const {
	if (empty()) return INVALID;
	id_t r = 0;
	for (size_t n = 0; n < size(); n++) {
		r = std::max(r, m_data[n].maxdim());
	}
	return r;
}

#endif
