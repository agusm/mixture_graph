include(../mixture_graph.pri)

CONFIG+=staticlib  
TEMPLATE= lib

INCLUDEPATH += ../ext
INCLUDEPATH += ../lux

HEADERS= \
bitstream.h \
codebook.h \
compressed_mipvolume.h \
error.h \
glVolume.h \
memfile.h \
md5.h \
mipvolume.h \
normal_volume.h \
quat_t.h \
segvolume.h \
sparse.h \
sparse_pair.h \
sparse_vector.h \
vec_t.h \
volume.h \


SOURCES= \
codebook.cpp \
compressed_mipvolume.cpp \
error.cpp \
normal_volume.cpp \
volume.cpp \
glVolume.cpp \
quat_t.cpp \
vec_t.cpp \
md5.cpp 

TARGET=base
