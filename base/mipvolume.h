#ifndef __MIPVOLUME_H__
#define __MIPVOLUME_H__

#include"volume.h"
#include<cassert>
#include<inttypes.h>
#include<stdio.h>
#include<stdlib.h>
#include"sparse.h"
#include"segvolume.h"
#include"memfile.h"

// Memfiles are currently NOT working for some reason...
#define USE_MEMFILES 

template<typename id_t>
class mipvolume {
public:
	mipvolume(void);
	mipvolume(const volume& V);
	mipvolume(uint32_t rx, uint32_t ry, uint32_t rz);
	mipvolume(const mipvolume& other);
	~mipvolume(void);
	bool empty(void) const;
	void clear(void);
	uint32_t nLevels(void) const;
	size_t size(uint32_t l) const;
	void resize(uint32_t rx, uint32_t ry, uint32_t rz);

	const sparse::vector<id_t>& operator()(uint32_t i, uint32_t j, uint32_t k, uint32_t l) const;
	const sparse::vector<id_t>& operator()(size_t n, uint32_t l) const;
	sparse::vector<id_t>& operator()(uint32_t i, uint32_t j, uint32_t k, uint32_t l);
	sparse::vector<id_t>& operator()(size_t n, uint32_t l);

	void build(const volume& V);
	const uint32_t& dimension(uint32_t n, uint32_t l) const;
	const id_t& get_background(void) const;
	void set_background(id_t bg);
	bool is_background(uint32_t i, uint32_t j, uint32_t k, uint32_t l);	
	bool is_background(size_t n, uint32_t l);

	const mipvolume& operator=(const mipvolume& other);
	bool read(const std::string& name);
	bool read(FILE* stream);
	bool read(memfile& stream);
	bool write(const std::string& name) const;
	bool write(FILE* stream) const;
	bool write(memfile& stream) const;
	static const id_t INVALID;

	double avg_nonzeros(void) const;
	const segvolume<id_t>& level(uint32_t l) const;
	segvolume<id_t>& level(uint32_t l);
	void rebuild(void);

	uint64_t nNodePairs(bool uniformL0 = true, id_t min_size=2) const;
	std::vector<uint32_t> mipvolume<id_t>::node_pair_multiplicity(void) const;

	id_t max_id(void) const;
	double diff(const mipvolume& other) const;
	std::vector<std::pair<id_t, id_t> > get_level_ranges(void) const;

	uint64_t voxel_size(uint32_t i, uint32_t j, uint32_t k, uint32_t l) const;
protected:
	std::vector<segvolume<id_t> > m_data;
	inline size_t linearAddr(uint32_t i, uint32_t j, uint32_t k, uint32_t l) const {
		return size_t(i) + size_t(m_data[l].dimension(0))*(size_t(j) + size_t(m_data[l].dimension(1))*size_t(k));
	}
	id_t m_background;
};

template<typename id_t> const id_t mipvolume<id_t>::INVALID = segvolume<id_t>::INVALID;

template<typename id_t>
mipvolume<id_t>::mipvolume(void) {
}

template<typename id_t>
mipvolume<id_t>::mipvolume(const volume& V) {
	build(V);
}

template<typename id_t>
mipvolume<id_t>::mipvolume(uint32_t rx, uint32_t ry, uint32_t rz) {
	resize(rx, ry, rz);
}

template<typename id_t>
mipvolume<id_t>::mipvolume(const mipvolume& other) {
	*this = other;
}

template<typename id_t>
mipvolume<id_t>::~mipvolume(void) {
	clear();
}

template<typename id_t>
bool mipvolume<id_t>::empty(void) const {
	return m_data.empty();
}

template<typename id_t>
void mipvolume<id_t>::clear(void) {
	m_data.clear();
}

template<typename id_t>
size_t mipvolume<id_t>::size(uint32_t l) const {
	assert("mipvolume<id_t>::size() - invalid parameter" && l < m_data.size());
	return m_data[l].size();
}

template<typename id_t>
uint32_t mipvolume<id_t>::nLevels(void) const {
	return uint32_t(m_data.size());
}

template<typename id_t>
void mipvolume<id_t>::resize(uint32_t rx, uint32_t ry, uint32_t rz) {
	size_t ns = size_t(rx)*size_t(ry)*size_t(rz);
	if (ns == 0) {
		clear();
		return;
	}
	size_t L = 0;
	uint32_t x = rx;
	uint32_t y = ry;
	uint32_t z = rz;
	while (true) {
		L++;
		if (x == 1 && y == 1 && z == 1) break;
		x = (x + 1) >> 1;
		y = (y + 1) >> 1;
		z = (z + 1) >> 1;
	};
	if (m_data.size() != L) m_data.resize(L,segvolume<id_t>());
	L = 0;
	do {
		m_data[L].resize(rx, ry, rz);		
		L++;
		if (rx == 1 && ry == 1 && rz == 1) break;
		rx = (rx + 1) >> 1;
		ry = (ry + 1) >> 1;
		rz = (rz + 1) >> 1;
	} while (true);
}

template<typename id_t>
const sparse::vector<id_t>& mipvolume<id_t>::operator()(uint32_t i, uint32_t j, uint32_t k, uint32_t l) const {
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && l < m_data.size());
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && i < m_dims[3 * l] && j < m_dims[3 * l + 1] && k < m_dims[3 * l + 2]);
	return m_data[l](i, j, k)
}

template<typename id_t>
const sparse::vector<id_t>& mipvolume<id_t>::operator()(size_t n, uint32_t l) const {
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && l < m_data.size());
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && n < m_data[l].size());
	return m_data[l](n);
}

template<typename id_t>
sparse::vector<id_t>& mipvolume<id_t>::operator()(uint32_t i, uint32_t j, uint32_t k, uint32_t l) {
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && l < m_data.size());
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && i < m_dims[3 * l] && j < m_dims[3 * l + 1] && k < m_dims[3 * l + 2]);
	return m_data[l](i, j, k);
}

template<typename id_t>
sparse::vector<id_t>& mipvolume<id_t>::operator()(size_t n, uint32_t l) {
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && l < m_data.size());
	assert("mipvolume<id_t>::operator() - invalid parameter(s)" && n < m_data[l].size());
	return m_data[l](n);
}

template<typename id_t>
const segvolume<id_t>& mipvolume<id_t>::level(uint32_t l) const {
	assert("mipvolume<id_t>::level() - invalid parameter" && l < m_data.size());
	return m_data[l];
}

template<typename id_t>
segvolume<id_t>& mipvolume<id_t>::level(uint32_t l) {
	assert("mipvolume<id_t>::level() - invalid parameter" && l < m_data.size());
	return m_data[l];
}

template<typename id_t>
void mipvolume<id_t>::rebuild(void) {	
	for (uint32_t l = 1; l < uint32_t(m_data.size()); l++) {
		printf("L%i... ", l);		
		const segvolume<id_t>& src(m_data[l - 1]);
		segvolume<id_t>& dst(m_data[l]);						

#pragma omp parallel for schedule(dynamic,1)
		for (int64_t ompk = 0; ompk < int64_t(dst.dimension(2)); ompk++) {			
			uint32_t k = uint32_t(ompk);			
			uint32_t lk = k << 1;
			uint32_t rk = std::min(lk + 2, src.dimension(2));


			for (uint32_t j = 0; j < dst.dimension(1); j++) {
				uint32_t lj = j << 1;
				uint32_t rj = std::min(lj + 2, src.dimension(1));				

				for (uint32_t i = 0; i < dst.dimension(0); i++) {
					uint32_t li = i << 1;
					uint32_t ri = std::min(li + 2, src.dimension(0));

					sparse::vector<id_t>& v(dst(linearAddr(i, j, k, l)));
					v.clear();					
					for (uint32_t z = lk; z < rk; z++) {
						for (uint32_t y = lj; y < rj; y++) {
							for (uint32_t x = li; x < ri; x++) {									
								v += src(linearAddr(x, y, z, l - 1)) * float(voxel_size(x, y, z, l - 1));
							}
						}
					}
					v.trim();
									
					float norm = v.l1_norm();
					if (norm != 0.0) v /= norm;					
				}
			}
		}		
		printf("%i x %i x %i, maxid = %i\n",dst.dimension(0),dst.dimension(1),dst.dimension(2),dst.max_id());		
	}
}

template<typename id_t>
void mipvolume<id_t>::build(const volume& V) {
	resize(V.dimension(0), V.dimension(1), V.dimension(2));
	m_data[0].build(V);	
	rebuild();
}

template<typename id_t>
const uint32_t& mipvolume<id_t>::dimension(uint32_t n, uint32_t l) const {
	assert("mipvolume::dimension() - invalid parameter(s)" && l < m_data.size() && n<3);
	return m_data[l].dimension(n);
}

template<typename id_t>
const id_t& mipvolume<id_t>::get_background(void) const {
	if (m_data.empty()) return INVALID;
	return m_data[0].background();
}

template<typename id_t>
void mipvolume<id_t>::set_background(id_t bg) {
	for (size_t n = 0; n < nLevels(); n++) m_data[n].set_background(bg);
}

template<typename id_t>
bool mipvolume<id_t>::is_background(uint32_t i, uint32_t j, uint32_t k, uint32_t l) {
	assert("mipvolume<id_t>::is_background() - invalid parameter(s)" && l < m_data.size());
	assert("mipvolume<id_t>::is_background() - invalid parameter(s)" && i < m_dims[3 * l] && j < m_dims[3 * l + 1] && k < m_dims[3 * l + 2]);
	return m_data[l].is_background(i, j, k);
}

template<typename id_t>
bool mipvolume<id_t>::is_background(size_t n, uint32_t l) {
	assert("mipvolume<id_t>::is_background() - invalid parameter(s)" && l < m_data.size());
	assert("mipvolume<id_t>::is_background() - invalid parameter(s)" && n< m_data[l].size());
	return m_data[l].is_background(n);
}


template<typename id_t>
const mipvolume<id_t>& mipvolume<id_t>::operator=(const mipvolume& other) {
	m_data = other.m_data;
	m_background = other.m_background;
	return *this;
}


template<typename id_t>
bool mipvolume<id_t>::read(const std::string& name) {
	if (name.empty()) return false;
#ifdef USE_MEMFILES
	memfile stream;
	stream.open_read(name);
	if (!stream.is_open()) return false;
	bool r = read(stream);
	stream.close();
#else
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool r = read(stream);
	fclose(stream);
#endif
	return r;
}
template<typename id_t>
bool mipvolume<id_t>::read(FILE* stream) {
	if (stream == nullptr) return false;
	clear();
	m_data.push_back(segvolume<id_t>());
	if (!m_data.back().read(stream)) return false;
	resize(m_data.back().dimension(0), m_data.back().dimension(1), m_data.back().dimension(2));
	for (size_t l = 1; l < m_data.size(); l++) {
		if (!m_data[l].read(stream)) return false;
	}
	return true;
}

template<typename id_t>
bool mipvolume<id_t>::read(memfile& stream) {
	if (!stream.is_open()) return false;
	clear();
	m_data.push_back(segvolume<id_t>());
	if (!m_data.back().read(stream)) return false;
	resize(m_data.back().dimension(0), m_data.back().dimension(1), m_data.back().dimension(2));	
	for (size_t l = 1; l < m_data.size(); l++) {
		if (!m_data[l].read(stream)) return false;		
	}
	return true;
}


template<typename id_t>
bool mipvolume<id_t>::write(const std::string& name) const {
	if (name.empty()) return false;
#ifdef USE_MEMFILES
	memfile stream;
	stream.open_write(name);
	if (!stream.is_open()) return false;
	bool r = write(stream);
	stream.close();
#else
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool r = write(stream);
	fclose(stream);
#endif
	return r;
}

template<typename id_t>
bool mipvolume<id_t>::write(FILE* stream) const {
	if (stream == nullptr) return false;
	for (size_t l = 0; l < m_data.size(); l++) {
		if (!m_data[l].write(stream)) return false;
	}
	return true;
}

template<typename id_t>
bool mipvolume<id_t>::write(memfile& stream) const {
	if (!stream.is_open()) return false;
	for (size_t l = 0; l < m_data.size(); l++) {
		if (!m_data[l].write(stream)) return false;
	}
	return true;
}


template<typename id_t>
double mipvolume<id_t>::avg_nonzeros(void) const {
	if (empty()) return 0.0;
	double count = 0.0;
	double nvox = 0.0;
	for (size_t l = 0; l < m_data.size(); l++) {
		count += double(m_data[l].avg_nonzeros())*double(m_data[l].size());
		nvox += double(m_data[l].size());
	}
	if (nvox == 0.0) return 0.0;
	return count / nvox;
}

template<typename id_t>
id_t mipvolume<id_t>::max_id(void) const {
	if (empty()) return INVALID;
	id_t r = 0;
	for (size_t n = 0; n < m_data.size(); n++) r = std::max(r, m_data[n].max_id());
	return r;
}


template<typename id_t>
uint64_t mipvolume<id_t>::nNodePairs(bool uniformL0,id_t min_size) const {
	uint64_t r = 0;
	uint32_t l0 = uniformL0 ? 1 : 0;
	CTimer ct;
	for (int32_t l = l0; l < int32_t(nLevels()); l++) {
		const segvolume<id_t>& vol(level(l));
		for (size_t n = 0; n < vol.size(); n++) {
			uint32_t N = vol(n).size();
			if (N < min_size) continue;
			r += (N*(N - 1)) >> 1;
		}
	}
	return r;
}

template<typename id_t>
std::vector<uint32_t> mipvolume<id_t>::node_pair_multiplicity(void) const {
	std::map<std::pair<id_t, id_t>, uint32_t> M;
	for (uint32_t l = 0; l < nLevels(); l++) {
		const segvolume<id_t>& vol(level(l));
		for (size_t n = 0; n < vol.size(); n++) {
			for (sparse::vector<id_t>::const_iterator i = vol(n).cbegin(); i < vol(n).cend(); i++) {
				for (sparse::vector<id_t>::const_iterator j = i + 1; j<vol(n).cend(); j++) {
					M[std::make_pair(i->first, j->first)]++;
				}
			}
		}
	}
	std::vector<uint32_t> r;
	size_t n = 0;
	std::pair<id_t, id_t> best;
	uint32_t bestscore = 0;
	for (std::map<std::pair<id_t, id_t>, uint32_t>::const_iterator it = M.cbegin(); it != M.cend(); it++) {
		printf("%.4i: %i,%i -> %i\n", n++, it->first.first, it->first.second, it->second);
		r.push_back(it->second);
		if (it->second >= bestscore) {
			bestscore = it->second;
			best = it->first;
		}
	}
	std::sort(r.begin(),r.end());
	printf("most frequent node pair (%i,%i) x %i\n", best.first, best.second, bestscore);	
	return r;
}

template<typename id_t>
double mipvolume<id_t>::diff(const mipvolume& other) const {
	double err = 0.0;
	size_t pos = 0;
	uint32_t lev = 0;
	if (nLevels() != other.nLevels()) {
		printf("Level mismatch\n");
		return std::numeric_limits<double>::infinity();
	}
	for (uint32_t l = 0; l < nLevels(); l++) {
		const segvolume<id_t>& vola(level(l));
		const segvolume<id_t>& volb(other.level(l));
		for (int i = 0; i < 3; i++) {
			if (vola.dimension(i) != volb.dimension(i)) {
				printf("Dimension mismatch in level %i\n", l);
				return std::numeric_limits<double>::infinity();
			}
		}
		for (size_t n = 0; n < vola.size(); n++) {
			sparse::vector<id_t> d = vola(n) - volb(n);
			double e = d.l1_norm();
			if (e>err) {
				err = e;
				pos = n;
				lev = l;				
			}			
		}
	}
	printf("maximum error at level %i, position %zi\n", lev, pos);
	
	return err;
}

template<typename id_t>
std::vector<std::pair<id_t, id_t> > mipvolume<id_t>::get_level_ranges(void) const {
	std::vector<std::pair<id_t, id_t> > result;
	result.resize(nLevels(),std::make_pair(std::numeric_limits<id_t>::max(),std::numeric_limits<id_t>::min()));
	for (uint32_t l = 0; l < nLevels(); l++) {
		const segvolume<id_t>& vol(level(l));
		for (size_t n = 0; n < vol.size(); n++) {
			result[l].first = std::min(result[l].first, vol(n).mindim());
			result[l].second = std::max(result[l].second, vol(n).maxdim());
		}
	}
	return result;
}

template<typename id_t>
uint64_t mipvolume<id_t>::voxel_size(uint32_t i, uint32_t j, uint32_t k, uint32_t l) const {
	uint32_t x = i << l;
	uint32_t y = j << l;
	uint32_t z = k << l;
	uint32_t delta = 1 << l;
	uint32_t dimx = std::min(x + delta, m_data[0].dimension(0)) - x;
	uint32_t dimy = std::min(y + delta, m_data[0].dimension(1)) - y;
	uint32_t dimz = std::min(z + delta, m_data[0].dimension(2)) - z;
	return uint64_t(dimx)*uint64_t(dimy)*uint64_t(dimz);
}

#endif