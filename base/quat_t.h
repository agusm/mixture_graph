#ifndef __QUAT_T_H__
#define __QUAT_T_H__

#include"vec_t.h"

class quat_t : public vec_t {
public:
	quat_t(void);
	quat_t(float r, float i, float j, float k);
	quat_t(const quat_t& other);
	~quat_t(void);

	quat_t operator*(const quat_t& other) const;
	quat_t inverse(void) const;
	quat_t conjugate(void) const;

	const float& real(void) const;
	const float& imag(size_t n) const;

	float& real(void);
	float& imag(size_t n);	
};

#endif
