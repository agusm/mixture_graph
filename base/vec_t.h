#ifndef __VEC_T_H__
#define __VEC_T_H__

#include<inttypes.h>

struct vec2_t {
	vec2_t(float _x = 0.0f, float _y = 0.0f) : x(_x), y(_y) {}
	float x, y;
};

class quat_t;

class vec3_t {
public:
	vec3_t(void);
	vec3_t(float x, float y = 0.0f, float z = 0.0f);
	vec3_t(const vec3_t& other);
	~vec3_t(void);
	const vec3_t& operator=(const vec3_t& other);
	const float& operator()(size_t i) const;
	float& operator()(size_t i);
	float dot(const vec3_t& other, size_t d = 3) const;
	float length2(size_t d = 3) const;
	float length(size_t d = 3) const;
	void normalize(size_t d = 3);
	vec3_t normalized(size_t d = 3) const;
	vec3_t operator*(const float& other) const;

	vec3_t operator-(const vec3_t& other) const;
	vec3_t operator+(const vec3_t& other) const;
	vec3_t operator^(const vec3_t& other) const;
	vec3_t& operator/=(const double& other);
	vec3_t& operator+=(const vec3_t& other);
	vec3_t operator-(void) const;
	float operator*(const vec3_t& other) const;
	vec3_t operator/(float other) const;

	operator const float* (void) const;
	operator float* (void);
public:
	float m_data[3];
};

class vec_t {
public:
	vec_t(void);
	vec_t(float x, float y = 0.0f, float z = 0.0f, float w = 0.0f);
	vec_t(const vec_t& other);
	~vec_t(void);
	const vec_t& operator=(const vec_t& other);
	const float& operator()(size_t i) const;
	float& operator()(size_t i);
	float dot(const vec_t& other, size_t d = 4) const;
	float length2(size_t d = 4) const;
	float length(size_t d = 4) const;
	void normalize(size_t d = 4);
	vec_t operator*(const float& other) const;

	vec_t operator-(const vec_t& other) const;
	vec_t operator+(const vec_t& other) const;
	vec_t operator^(const vec_t& other) const;
	vec_t& operator/=(const double& other);

	operator const float*(void) const;
	operator float*(void);
	quat_t get_versor(void) const;
public:
	float m_data[4];
};

class uivec_t {
public:
	uivec_t(void);
	uivec_t(uint32_t x, uint32_t y = 0.0f, uint32_t z = 0.0f, uint32_t w = 0.0f);
	uivec_t(const uivec_t& other);
	~uivec_t(void);
	const uivec_t& operator=(const uivec_t& other);
	const uint32_t& operator()(size_t i) const;
	uint32_t& operator()(size_t i);
	uivec_t operator*(const uint32_t& other) const;

	uivec_t operator-(const uivec_t& other) const;
	uivec_t operator+(const uivec_t& other) const;
	
	operator const uint32_t* (void) const;
	operator uint32_t* (void);
public:
	uint32_t m_data[4];
};


#endif
