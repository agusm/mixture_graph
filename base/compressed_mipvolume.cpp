#include"compressed_mipvolume.h"
#include<cassert>
#include<algorithm>
#include"md5.h"
#include<unordered_set>
#include<omp.h>

const uint64_t compressed_mipvolume::INVALID = uint64_t(-1);

compressed_mipvolume::compressed_mipvolume(void) {
	clear();
}

compressed_mipvolume::compressed_mipvolume(uint32_t dimx, uint32_t dimy, uint32_t dimz) {
	clear();
	resize(dimx, dimy, dimz);
}

compressed_mipvolume::compressed_mipvolume(const compressed_mipvolume& other) {
	clear();
	*this = other;
}

compressed_mipvolume::~compressed_mipvolume(void) {
	clear();
}
compressed_mipvolume& compressed_mipvolume::operator=(const compressed_mipvolume& other) {
	m_data = other.m_data;
	m_addr = other.m_addr;
	m_dimensions = other.m_dimensions;
	return *this;
}

bool compressed_mipvolume::empty(void) const {
	return m_data.empty();
}

void compressed_mipvolume::clear(void) {
	m_data.clear();
	clear_addr();
	m_dimensions.clear();
	m_voxel_addr.clear();
	update_addresses();	
}

uint64_t compressed_mipvolume::bitsize(void) const {
	return m_data.bitsize();
}

uint32_t compressed_mipvolume::get_nLeafs(void) {
	assert("compressed_mipvolume::get_nLeafs() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_leafs + 32);
	uint32_t r = 0;
	read32(r, m_addr.hdr_leafs, 32);
	return r;
}

bool compressed_mipvolume::get_UniformL0(void) {
	assert("compressed_mipvolume::get_UniformL0() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_uniL0 + 1);
	uint32_t r = 0;
	read32(r, m_addr.hdr_uniL0, 1);
	return (r == 1);
}

uint16_t compressed_mipvolume::get_PackingSize(void) {
	assert("compressed_mipvolume::get_PackingSize() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_pack + 1);
	uint32_t r = 0;
	read32(r, m_addr.hdr_pack, 16);
	return r;
}

uint32_t compressed_mipvolume::get_nNodeBits(void) {
	assert("compressed_mipvolume::get_nNodeBits() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_nbits + 6);
	uint32_t r = 0;
	read32(r, m_addr.hdr_nbits, 6);
	return r + 1;
}

uint32_t compressed_mipvolume::get_Dimension(uint32_t n) {
	assert("compressed_mipvolume::get_Dimension() - invalid parameter" && n<3);
	assert("compressed_mipvolume::get_Dimension() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_dims + 96);
	uint32_t r = 0;
	read32(r, m_addr.hdr_dims + 32ui64 * n, 32);
	return r;
}

uint32_t compressed_mipvolume::get_nWeightBits(void) {
	assert("compressed_mipvolume::get_nWeightBits() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_nweights + 4);
	uint32_t r = uint32_t(-1);
	read32(r, m_addr.hdr_nweights, 4);
	return r + 1;
}

uint64_t compressed_mipvolume::get_nLerpNodes(void) {
	uint32_t N = get_nNodeBits();
	assert("compressed_mipvolume::get_nLerpNodes() - insufficient stream" && m_data.bitsize() >= m_addr.hdr_nnodes + N);
	uint64_t r = uint32_t(-1);
	read64(r, m_addr.hdr_nnodes, N);
	return r + 1;
}

uint32_t compressed_mipvolume::get_nVoxelBits(uint32_t l) {
	assert("compressed_mipvolume::get_nVoxelBits() - insufficient stream" && m_data.bitsize() >= m_addr.idx_vbits + 6 * (l + 1));
	uint32_t r = uint32_t(-1);
	read32(r, m_addr.idx_vbits + 6ui64 * l, 6);
	return r + 1;
}

uint64_t compressed_mipvolume::get_NodeOffset(uint32_t l) {
	assert("compressed_mipvolume::get_NodeOffset() - insufficient stream" && m_data.bitsize() >= m_addr.idx_noffs + (l + 1)*get_nNodeBits());
	uint32_t N = get_nNodeBits();
	uint64_t R = 0;
	read64(R, m_addr.idx_noffs+uint64_t(N*l), N);
	return R;
}


uint64_t compressed_mipvolume::get_Voxel(uint32_t i, uint32_t j, uint32_t k, uint32_t l) {
	assert("compressed_mipvolume::get_Voxel() - invalid parameter(s)" && 3 * l + 2 < m_dimensions.size());
	assert("compressed_mipvolume::get_Voxel() - invalid parameter(s)" && i < get_Dimension(0, l) && j < get_Dimension(1, l) && k < get_Dimension(2, l));
	uint32_t v = get_nVoxelBits(l);
	uint64_t addr = m_voxel_addr[l] + v*(i + uint64_t(get_Dimension(0, l))*(j + uint64_t(get_Dimension(1, l))*k));
	uint64_t R = 0;
	read64(R, addr, v);
	return R + get_NodeOffset(l);
}

uint64_t compressed_mipvolume::get_Voxel(uint64_t n, uint32_t l) {
	assert("compressed_mipvolume::get_Voxel() - invalid parameter(s)" && 3 * l + 2 < m_dimensions.size());
	assert("compressed_mipvolume::get_Voxel() - invalid parameter(s)" && n < uint64_t(get_Dimension(0, l))*uint64_t(get_Dimension(1, l))*uint64_t(get_Dimension(2, l)));		
	uint32_t b = get_nVoxelBits(l);	
	uint64_t addr = m_voxel_addr[l] + uint64_t(b)*n;	
	uint64_t R = 0;	
	read64(R, addr, b);	
	get_NodeOffset(l);	
	return R + get_NodeOffset(l);
}

float compressed_mipvolume::get_Code(uint32_t n) {
	assert("compressed_mipvolume::get_Code() - invalid parameter(s)" && m_data.bitsize()>=40 * (n + 1) + m_addr.dat_code);
	uint32_t r = uint32_t(-1);
	read32(r, 40ui64 * n + m_addr.dat_code, 20);
	static float norm = 1.0f / float((1 << 20u) + 1);
	return float(r + 1)*norm;
}

float compressed_mipvolume::get_CodeVar(uint32_t n) {
	assert("compressed_mipvolume::get_Code() - invalid parameter(s)" && m_data.bitsize() > 40 * (n + 1) + m_addr.dat_code);	
	float s = get_maxVariance();
	uint32_t r = 0;
	read32(r, 40ui64 * n + m_addr.dat_code + 20, 20);
	static float norm = 1.0f / float((1 << 20u) - 1);
	return float(r)*s*norm;
}

uint16_t compressed_mipvolume::get_PackingEntry(uint16_t n) {
	assert("compressed_mipvolume::get_PackingEntry() - invalid parameter(s)" && m_data.bitsize() > 16 * (n + 1) + m_addr.dat_pack);
	uint32_t r = 0;
	read32(r, 16ui64 * n + m_addr.dat_pack, 16);
	return uint16_t(r);
}

float compressed_mipvolume::get_maxVariance(void) {
	uint32_t r = 0;
	read32(r, m_addr.hdr_sigma, 32);
	return *reinterpret_cast<const float*>(&r);
}

bool compressed_mipvolume::set_maxVariance(float var) {
	uint32_t r = *reinterpret_cast<const uint32_t*>(&var);
	return write32(r, m_addr.hdr_sigma, 32)==32;
}

bool compressed_mipvolume::set_Node(uint64_t n, uint64_t ch0, uint64_t ch1, uint32_t idx) {
	assert("compressed_mipvolume()::set_Node() - invalid parameter" && m_addr.dat_node != INVALID);
	uint32_t N = get_nNodeBits();
	uint32_t b = get_nWeightBits();
	uint64_t R = ch0 | (ch1 << uint64_t(N)) | (uint64_t(idx) << uint64_t(2ui64 * N));
	uint32_t size = 2 * N + b;
	uint64_t addr = m_addr.dat_node + n*uint64_t(size);	
	return write64(R, addr, size) == size;
}

bool compressed_mipvolume::get_Node(uint64_t n, uint64_t& ch0, uint64_t& ch1, uint32_t& idx) {
	assert("compressed_mipvolume()::get_Node() - invalid parameter" && m_addr.dat_node != INVALID);
	uint32_t N = get_nNodeBits();
	uint32_t b = get_nWeightBits();
	uint64_t R = 0;
	uint32_t size = 2 * N + b;
	uint64_t addr = m_addr.dat_node + n*uint64_t(size);
	if (read64(R, addr, size) != size) return false;	
	ch0 = R&((uint64_t(1) << N) - 1);
	R >>= N;
	ch1 = R&((uint64_t(1) << N) - 1);
	R >>= N;
	idx = uint32_t(R&((uint64_t(1) << b) - 1));
	return true;
}




bool compressed_mipvolume::set_nLeafs(uint32_t val) {
	// NO REQUIREMENTS
	return (m_data.write(val, m_addr.hdr_leafs, 32) == 32);
}

bool compressed_mipvolume::set_UniformL0(bool val) {
	// NO REQUIREMENTS
	uint32_t r = val ? 1 : 0;
	return (m_data.write(r, m_addr.hdr_uniL0, 1) == 1);
}

bool compressed_mipvolume::set_PackingSize(uint16_t val) {
	return (m_data.write(val, m_addr.hdr_pack, 16) == 16);
}

bool compressed_mipvolume::set_nNodeBits(uint8_t val) {
	assert("compressed_mipvolume::set_nNodeBits() - invalid parameter" && val > 0 && val <= 64);
	// NO ADDITIONAL REQUIREMENTS
	if (val == 0 || val > 64) return false;
	bool result = (m_data.write(val - 1, m_addr.hdr_nbits, 6) == 6);
	update_addresses();
	return result;
}

bool compressed_mipvolume::set_Dimension(uint32_t n, uint32_t val) {
	assert("compressed_mipvolume::set_Dimension() - invalid parameter(s)" && n < 3);
	// NO ADDITIONAL REQUIREMENTS
	return (m_data.write(val, m_addr.hdr_dims + n * 32ui64, 32) == 32);
}

bool compressed_mipvolume::set_nWeightBits(uint8_t val) {
	assert("compressed_mipvolume::set_nWeightBits() - invalid parameter(s)" && val>0 && val <= 16);
	// NO ADDITIONAL REQUIREMENTS	
	bool r = (m_data.write(val - 1, m_addr.hdr_nweights, 4) == 4);	
	return r;
}

bool compressed_mipvolume::set_nLerpNodes(uint64_t val) {
	assert("compressed_mipvolume::set_nLerpNodes() - requirements not met: set_nNodeBits" && m_addr.idx_vbits != INVALID);
	uint32_t N = get_nNodeBits();
	assert("compressed_mipvolume::set_nLerpNodes() - invalid parameter" && val != 0 && val <= (uint64_t(1) << uint64_t(N)));
	if (val == 0 || val >= (uint64_t(1) << N)) return false;
	val--;
	if (N <= 32) return (m_data.write(uint32_t(val), m_addr.hdr_nnodes, N) == N);

	if (m_data.write(uint32_t((val)& 0xFFFFFFFF), m_addr.hdr_nnodes, 32) != 32) return false;
	return (m_data.write(uint32_t(val >> uint64_t(32)), m_addr.hdr_nnodes + 32, N - 32) != N - 32);
}

bool compressed_mipvolume::set_nVoxelBits(uint32_t l, uint32_t val) {
	assert("compressed_mipvolume::set_nVoxelBits() - requirements not met: set_nNodeBits, resize" && m_addr.idx_vbits != INVALID);
	assert("compressed_mipvolume::set_nVoxelBits() - invalid parameter(s)" && l<get_nLevels() && val != 0 || val <= 64);
	bool result = (m_data.write(val - 1, m_addr.idx_vbits + 6ui64 * l, 6) == 6);
	update_addresses();
	return result;
}

bool compressed_mipvolume::set_NodeOffset(uint32_t l, uint64_t val) {
	assert("compressed_mipvolume::set_NodeOffset() - requirements not met: set_nNodeBits, resize, set_nVoxelBits" && m_addr.idx_noffs != INVALID);
	assert("compressed_mipvolume::set_NodeOffset() - invalid parameter(s)" && l < get_nLevels());
	uint32_t N = get_nNodeBits();	
	bool result = write64(val, m_addr.idx_noffs + uint64_t(N*l), N) == N;
	return result;
}

bool compressed_mipvolume::set_Voxel(uint32_t i, uint32_t j, uint32_t k, uint32_t l, uint64_t val) {
	assert("compressed_mipvolume::set_Voxel() - invalid parameter(s)" && 3 * l + 2 < m_dimensions.size());
	assert("compressed_mipvolume::set_Voxel() - invalid parameter(s)" && i < get_Dimension(0, l) && j < get_Dimension(1, l) && k < get_Dimension(2, l));
	uint32_t v = get_nVoxelBits(l);
	uint64_t addr = m_voxel_addr[l] + v*(i + uint64_t(get_Dimension(0, l))*(j + uint64_t(get_Dimension(1, l))*k));
	uint64_t noffs = get_NodeOffset(l);
	if (val < noffs) return false;
	return write64(val - noffs, addr, v) == v;
}

bool compressed_mipvolume::set_Voxel(uint64_t n, uint32_t l, uint64_t val) {
	assert("compressed_mipvolume::set_Voxel() - invalid parameter(s)" && 3 * l + 2 < m_dimensions.size());
	assert("compressed_mipvolume::set_Voxel() - invalid parameter(s)" && n < uint64_t(get_Dimension(0, l))*uint64_t(get_Dimension(1, l))*uint64_t(get_Dimension(2, l)));
	uint32_t b = get_nVoxelBits(l);
	uint64_t addr = m_voxel_addr[l] + uint64_t(b)*n;
	uint64_t noffs = get_NodeOffset(l);
	if (val < noffs) return false;
	return write64(val-noffs, addr, b) == b;
}



bool compressed_mipvolume::set_Code(uint32_t n, float val) {
	assert("compressed_mipvolume::set_Code() - invalid parameter(s)" && n<(1u << get_nWeightBits()));
	assert("compressed_mipvolume::set_Code() - invalid parameter(s)" && 0.0f<val && val<1.0f);
	uint32_t r = uint32_t(val*float((1 << 20u) + 1));
	r--;
	return (m_data.write(r, 40ui64 * n + m_addr.dat_code, 20) == 20);
}

bool compressed_mipvolume::set_CodeVar(uint32_t n, float var) {
	assert("compressed_mipvolume::set_CodeRange() - invalid parameter(s)" && n < (1u << get_nWeightBits()));
	assert("compressed_mipvolume::set_CodeRange() - invalid parameter(s)" && var>=0.0f && var<=get_maxVariance());
	uint32_t r = uint32_t((var / get_maxVariance())*float((1 << 20) - 1));
	return m_data.write(r, 40ui64 * n + m_addr.dat_code + 20, 20) == 20;
}

bool compressed_mipvolume::set_PackingEntry(uint16_t n, uint16_t val) {
	assert("compressed_mipvolume::set_PackingEntry() - invalid parameter(s)" && n < get_PackingSize());
	return m_data.write(uint32_t(val), 16ui64 * n + m_addr.dat_pack, 16) == 16;
}

uint32_t compressed_mipvolume::get_nLevels(void) const {
	return uint32_t(m_dimensions.size() / 3);
}

uint32_t compressed_mipvolume::get_Dimension(uint32_t n, uint32_t l) const {
	assert("compressed_mipvolume::get_Dimension() - invalid parameter(s)" && n < 3 && l < get_nLevels());
	return m_dimensions[3ui64 * l + n];
}

void compressed_mipvolume::clear_addr(void) {
	m_addr.hdr_leafs = m_addr.hdr_nbits = m_addr.hdr_dims = m_addr.hdr_nnodes = m_addr.hdr_nweights = INVALID;
	m_addr.idx_vbits = m_addr.idx_noffs = INVALID;
	m_addr.dat_voxel = m_addr.dat_node = m_addr.dat_code = INVALID;
	m_addr.eof = INVALID;
}

void compressed_mipvolume::resize(uint32_t dimx, uint32_t dimy, uint32_t dimz) {
	clear();
	do {
		m_dimensions.push_back(dimx);
		m_dimensions.push_back(dimy);
		m_dimensions.push_back(dimz);
		if (dimx == 1 && dimy == 1 && dimz == 1) break;
		dimx = (dimx + 1) >> 1;
		dimy = (dimy + 1) >> 1;
		dimz = (dimz + 1) >> 1;
	} while (true);
	set_Dimension(0, m_dimensions[0]);
	set_Dimension(1, m_dimensions[1]);
	set_Dimension(2, m_dimensions[2]);
	update_addresses();
}

void compressed_mipvolume::output(FILE* stream) {
	if (stream == nullptr) return;
	fprintf(stream, "HEADER\n");
	if (m_addr.hdr_leafs == INVALID)	fprintf(stream, "    INVALID     : .hdr_leafs    N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_leafs    %i\n", m_addr.hdr_leafs, get_nLeafs());
	if (m_addr.hdr_uniL0 == INVALID)	fprintf(stream, "    INVALID     : .hdr_uniL0    N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_uniL0    %s\n", m_addr.hdr_uniL0, get_UniformL0() ? "true" : "false");
	if (m_addr.hdr_pack == INVALID)		fprintf(stream, "    INVALID     : .hdr_pack     N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_pack     %i\n", m_addr.hdr_pack, get_PackingSize());
	if (m_addr.hdr_nbits == INVALID)	fprintf(stream, "    INVALID     : .hdr_nbits    N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_nbits    %i\n", m_addr.hdr_nbits, get_nNodeBits());
	if (m_addr.hdr_sigma == INVALID)	fprintf(stream, "    INVALID     : .hdr_sigma    N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_sigma    %f\n", m_addr.hdr_sigma, get_maxVariance());
	if (m_addr.hdr_dims == INVALID)		fprintf(stream, "    INVALID     : .hdr_dims     N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_dims     %i %i %i\n", m_addr.hdr_dims, get_Dimension(0), get_Dimension(1), get_Dimension(2));
	if (m_addr.hdr_nweights == INVALID) fprintf(stream, "    INVALID     : .hdr_nweights N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_nweights %i\n", m_addr.hdr_nweights, get_nWeightBits());
	if (m_addr.hdr_nnodes == INVALID)	fprintf(stream, "    INVALID     : .hdr_nnodes   N/A\n");
	else								fprintf(stream, "    %.12I64d: .hdr_nnodes   %I64d\n", m_addr.hdr_nnodes, get_nLerpNodes());
	fprintf(stream, "INDEX\n");
	if (m_addr.idx_vbits == INVALID)	fprintf(stream, "    INVALID     : .idx_vbits    N/A\n");
	else {
		fprintf(stream, "    %.12I64d: .idx_vbits   ", m_addr.idx_vbits);
		for (uint32_t l = 0; l < get_nLevels(); l++) fprintf(stream, " %i", get_nVoxelBits(l));
		fprintf(stream, "\n");
	}
	if (m_addr.idx_noffs == INVALID)		fprintf(stream, "    INVALID     : .idx_noffs    N/A\n");
	else {
		fprintf(stream, "    %.12I64d: .idx_noffs   ", m_addr.idx_noffs);
		for (uint32_t l = 0; l < get_nLevels(); l++) fprintf(stream, " %I64d", get_NodeOffset(l));
		fprintf(stream, "\n");
	}
	fprintf(stream, "DATA\n");
	if (m_addr.dat_voxel == INVALID)		fprintf(stream, "    INVALID     : .dat_voxel    size = N/A\n");
	else {
		uint64_t S = get_VoxelSize();
		if (S == INVALID)	fprintf(stream, "    %.12I64d: .dat_voxel    size = N/A\n", m_addr.dat_voxel);
		else				fprintf(stream, "    %.12I64d: .dat_voxel    size = %I64d\n", m_addr.dat_voxel, get_VoxelSize());
	}
	if (m_addr.dat_node == INVALID)			fprintf(stream, "    INVALID     : .dat_node     size = N/A\n");
	else {
		uint64_t S = get_NodeSize();
		if (S == INVALID)	fprintf(stream, "    %.12I64d: .dat_node     size = N/A\n", m_addr.dat_node);
		else				fprintf(stream, "    %.12I64d: .dat_node     size = %I64d\n", m_addr.dat_node, get_NodeSize());
	}
	if (m_addr.dat_code == INVALID)	{
		fprintf(stream, "    INVALID     : .dat_code     size = N/A\n");
	}
	else {
		uint64_t S = get_CodeSize();
		if (S == INVALID) {
			fprintf(stream, "    %.12I64d: .dat_code     size = N/A\n", m_addr.dat_code);
		}
		else {
			fprintf(stream, "    %.12I64d: .dat_code     size = %I64d\n", m_addr.dat_code, S);
		}
	}
	if (m_addr.dat_pack == INVALID) {
		fprintf(stream, "    INVALID     : .dat_pack     size = N/A\n");
	}
	else {
		uint64_t S = get_PackingSize();
		fprintf(stream, "    %.12I64d: .dat_pack     size = %I64d\n", m_addr.dat_pack, S*16);
	}
	if (m_addr.eof == INVALID) {
		fprintf(stream, "    INVALID     : .eof\n");
	}
	else {
		fprintf(stream, "    %.12I64d: .eof\n", m_addr.eof);
	}

	fprintf(stream, "DERIVED\n");
	if (m_addr.eof != INVALID) {
		fprintf(stream, "    Filesize   : %.3fMB\n", float(double(m_addr.eof) / double(8ui64 << 20ui64)));
	}
	fprintf(stream, "    nLevels    : %i\n", get_nLevels());
	if (get_nLevels()>0) {
		fprintf(stream, "    Dimensions : L00 = %i %i %i\n", get_Dimension(0, 0), get_Dimension(1, 0), get_Dimension(2, 0));
		for (uint32_t l = 1; l < get_nLevels(); l++) {
			fprintf(stream, "                 L%.2i = %i %i %i\n", l, get_Dimension(0, l), get_Dimension(1, l), get_Dimension(2, l));
		}
	}
	if (get_nLevels()>0 && !m_voxel_addr.empty()) {
		fprintf(stream, "    VoxelOffset: L00 = %I64d\n", m_voxel_addr[0]);
		for (uint32_t l = 1; l < get_nLevels(); l++) {
			fprintf(stream, "                 L%.2i = %I64d\n", l, m_voxel_addr[l]);
		}
	}
	std::string hash;
	bool hasMD5 = (md5(hash, m_data)==0);
	fprintf(stream, "    md5        : %s\n", hasMD5 ? hash.c_str() : "error");
	
}

uint64_t compressed_mipvolume::get_LevelSize(uint32_t l) const {
	assert("compressed_mipvolume::get_LevelSize() - invalid parameter" && l < get_nLevels());
	if (l >= get_nLevels()) return 0;
	return uint64_t(m_dimensions[3ui64 * l])*uint64_t(m_dimensions[3ui64 * l + 1])*uint64_t(m_dimensions[3ui64 * l + 2]);
}

uint64_t compressed_mipvolume::get_VoxelSize(void) {
	uint64_t R = 0;
	for (uint32_t n = 0; n < get_nLevels(); n++) {
		if (m_data.bitsize() < m_addr.idx_vbits + 6ui64 * (n + 1)) return INVALID;
		R += get_LevelSize(n)*get_nVoxelBits(n);
	}
	return R;
}

uint64_t compressed_mipvolume::get_NodeSize(void) {
	return (2ui64 * get_nNodeBits() + get_nWeightBits())*get_nLerpNodes();
}

uint64_t compressed_mipvolume::get_CodeSize(void) {
	return 40 * (1ui64 << uint64_t(get_nWeightBits()));
}

bool compressed_mipvolume::update_addresses(void) {
	// static part
	m_addr.hdr_leafs = 0;
	m_addr.hdr_uniL0 = m_addr.hdr_leafs + 32;
	m_addr.hdr_pack = m_addr.hdr_uniL0 + 1;
	m_addr.hdr_nbits = m_addr.hdr_pack + 16;
	m_addr.hdr_dims = m_addr.hdr_nbits + 6;
	m_addr.hdr_sigma = m_addr.hdr_dims + 96;
	m_addr.hdr_nweights = m_addr.hdr_sigma + 32;
	m_addr.hdr_nnodes = m_addr.hdr_nweights + 4;
	if (m_data.bitsize() <= m_addr.hdr_nbits + 6) return false;
	m_addr.idx_vbits = m_addr.hdr_nnodes + get_nNodeBits();
	if (get_nLevels() == 0) return false;
	m_addr.idx_noffs = m_addr.idx_vbits + 6ui64 * get_nLevels();
	
	m_addr.dat_voxel = m_addr.idx_noffs + uint64_t(get_nLevels())*uint64_t(get_nNodeBits());
	align32(m_addr.dat_voxel);

	uint64_t S = get_VoxelSize();	
	if (S == INVALID) return false;
	uint64_t nVoxels = 0;
	m_voxel_addr.clear();
	for (uint32_t l = 0; l < get_nLevels(); l++) {
		if (l>0) m_voxel_addr.push_back(m_voxel_addr[l-1]+ + get_LevelSize(l-1)*uint64_t(get_nVoxelBits(l-1)));
		else m_voxel_addr.push_back(m_addr.dat_voxel);
	}
	m_addr.dat_node = m_addr.dat_voxel + S;
	align32(m_addr.dat_node);

	S = get_NodeSize();
	if (S == INVALID) return false;
	m_addr.dat_code = m_addr.dat_node + S;
	align32(m_addr.dat_code);

	S = get_CodeSize();
	if (S == INVALID) return false;
	m_addr.dat_pack = m_addr.dat_code + S;
	align32(m_addr.dat_pack);

	S = get_PackingSize();
	if (S == INVALID) return false;
	m_addr.eof = m_addr.dat_pack + S*16;

	return true;
}

bool compressed_mipvolume::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool r = read(stream);
	if (stream!=nullptr) fclose(stream);
	return r;
}

bool compressed_mipvolume::read(FILE* stream) {
	if (stream == nullptr) return false;
	clear();
	if (!m_data.read(stream)) return false;

	// build m_dimensions
	uint32_t dimx = get_Dimension(0);
	uint32_t dimy = get_Dimension(1);
	uint32_t dimz = get_Dimension(2);
	//m_dimensions.clear();
	do {
		m_dimensions.push_back(dimx);
		m_dimensions.push_back(dimy);
		m_dimensions.push_back(dimz);
		if (dimx == 1 && dimy == 1 && dimz == 1) break;
		dimx = (dimx + 1) >> 1;
		dimy = (dimy + 1) >> 1;
		dimz = (dimz + 1) >> 1;
	} while (true);

	// update address space and build m_voxel_addr
	return update_addresses();
}

bool compressed_mipvolume::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool r = write(stream);
	if (stream!=nullptr) fclose(stream);
	return r;
}

bool compressed_mipvolume::write(FILE* stream) const {
	if (stream == nullptr) return false;
	return m_data.write(stream);
}

std::vector<std::vector<size_t> > compressed_mipvolume::voxel_ref_lists(void) {
	std::vector<std::vector<size_t> > result;
	for (uint32_t l = 0; l < get_nLevels(); l++) {
		std::unordered_set<size_t> S;
		size_t size = get_LevelSize(l);
		for (size_t n = 0; n < size; n++) S.insert(size_t(get_Voxel(n, l)));
		result.push_back(std::vector<size_t>());
		for (auto it = S.cbegin(); it != S.cend(); it++) result.back().push_back(*it);
		std::sort(result.back().begin(), result.back().end());
	}
	return result;
}

void compressed_mipvolume::invalidate_lookup_table(void) {
	m_lookup_table.clear();
}

void compressed_mipvolume::build_lookup_table(bool force, const std::vector<lux::rangeu32_t>& order) {
	uint32_t nLeafs = get_nLeafs();
	uint32_t nLerps = uint32_t(get_nLerpNodes());
	uint32_t nNodes = nLeafs + nLerps;
	if (m_lookup_table.size() == nNodes && !force) return;
	m_lookup_table.resize(nNodes);
#pragma omp parallel for schedule(dynamic,8)
	for (int n = 0; n < int(nLeafs); n++) {
		m_lookup_table[n].set(n, lux::errord(1.0, 0.0));
		m_lookup_table[n].trim();
	}
	if (order.empty()) {
		// serial build
		for (uint32_t n = nLeafs; n < uint32_t(nNodes); n++) {
			uint64_t ch0, ch1;
			uint32_t idx;
			get_Node(n - nLeafs, ch0, ch1, idx);
			lux::errord weight(get_Code(idx), get_CodeVar(idx));
			m_lookup_table[n] = lerp(m_lookup_table[ch0], m_lookup_table[ch1], weight);
			m_lookup_table[n].trim();
		}
	}
	else {
		// parallel build
		for (size_t n = 1; n < order.size(); n++) {
#pragma omp parallel for schedule(dynamic,16)
			for (int k = int(order[n].vmin); k<int(order[n].vmax); k++) {
				uint64_t ch0, ch1;
				uint32_t idx;
				get_Node(uint64_t(k) - nLeafs, ch0, ch1, idx);
				lux::errord weight(get_Code(idx), get_CodeVar(idx));
				m_lookup_table[k] = lerp(m_lookup_table[ch0], m_lookup_table[ch1], weight);
				m_lookup_table[k].trim();
			}
		}
	}
}

const sparse::vector<uint32_t, lux::errord>& compressed_mipvolume::lookup_node(size_t n) {
	build_lookup_table();
	assert("compressed_mipvolume::lookup_node -- invalid parameter" && n < m_lookup_table.size());
	return m_lookup_table[n];
}

size_t compressed_mipvolume::query_range(const lux::rangeu32_t& X, const lux::rangeu32_t& Y, const lux::rangeu32_t& Z, sparse::vector<uint32_t, lux::errord>& result) {
	build_lookup_table();
	result.clear();
	size_t vol = size_t(X.vmax - X.vmin) * size_t(Y.vmax - Y.vmin) * size_t(Z.vmax - Z.vmin);
	size_t R; // if this is set to 0, COMPILER BUG!!! WILL RETURN 0 despite the next statements having side-effects!!!
	if (vol >= 64) {
		std::vector<sparse::vector<uint32_t, lux::errord> > partial_result(omp_get_max_threads());
		lux::fetch_function_t fetch = [&](lux::coordu32_t& p, void* ptr) {
			uint64_t idx = get_Voxel(p.i >> p.l, p.j >> p.l, p.k >> p.l, p.l);
			lux::errord size(double(uint64_t(1) << uint64_t(3 * p.l)), 0.0);
			partial_result[omp_get_thread_num()] += size * m_lookup_table[idx];
		};
		R = lux::par_footprint3d(X, Y, Z, fetch, nullptr);
		for (size_t n = 0; n < partial_result.size(); n++) result += partial_result[n];
	}
	else {
		lux::fetch_function_t fetch = [&](lux::coordu32_t& p, void* ptr) {
			uint64_t idx = get_Voxel(p.i >> p.l, p.j >> p.l, p.k >> p.l, p.l);
			lux::errord size(double(uint64_t(1) << uint64_t(3 * p.l)), 0.0);
			result += size * m_lookup_table[idx];
		};
		R = lux::footprint3d(X, Y, Z, fetch, nullptr);
	}
	result.trim();
	return R;
}