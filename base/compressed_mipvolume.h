#ifndef __COMPRESSED_MIPVOLUME_H__
#define __COMPRESSED_MIPVOLUME_H__

#include<inttypes.h>
#include"bitstream.h"
#include<vector>
#include<cassert>
#include"mipvolume.h"
#include"error.h"

namespace lux {
	template<class T> struct coord_t;
	template<class T> struct range_t;
};

class compressed_mipvolume {
public:
	compressed_mipvolume(void);														///< default constructor
	compressed_mipvolume(uint32_t dimx, uint32_t dimy, uint32_t dimz);				///< resize constructor
	compressed_mipvolume(const compressed_mipvolume& other);						///< copy constructor
	~compressed_mipvolume(void);													///< destructor
	compressed_mipvolume& operator=(const compressed_mipvolume& other);				///< assignment operator
	bool empty(void) const;															///< true iff volume empty
	void clear(void);																///< deallocate memory
	uint64_t bitsize(void) const;													///< return the number of bits in this stream

	void resize(uint32_t dimx, uint32_t dimy, uint32_t dimz);						///< resize volume to specified dimensions

	size_t query_range(const lux::range_t<uint32_t>& X, const lux::range_t<uint32_t>& Y, const lux::range_t<uint32_t>& Z, sparse::vector<uint32_t, lux::errord>& result);
	void invalidate_lookup_table(void);
	void build_lookup_table(bool force = false, const std::vector<lux::range_t<uint32_t> >& order = std::vector<lux::range_t<uint32_t> >());
	const sparse::vector<uint32_t, lux::errord>& lookup_node(size_t n);

	// direct access
	uint32_t get_nLeafs(void);														///< returns the number of leaf nodes (= number of segment ids in input)
	uint32_t get_nNodeBits(void);													///< returns the number of bits to encode nodes
	uint32_t get_Dimension(uint32_t n);												///< returns the nth dimension of finest level 0
	float    get_maxVariance(void);													///< return maximum variance of weights
	uint32_t get_nWeightBits(void);													///< returns the number of bits to encode each weight
	uint64_t get_nLerpNodes(void);													///< returns the number of linear interpolation nodes
	uint32_t get_nVoxelBits(uint32_t l);											///< returns the number of bits used to encode a voxel at level l
	uint64_t get_NodeOffset(uint32_t l);											///< returns the node offset for level l
	uint64_t get_Voxel(uint32_t i, uint32_t j, uint32_t k, uint32_t l);				///< retrieve the code of voxel ijk at level l
	uint64_t get_Voxel(uint64_t n, uint32_t l);										///< retrieve nth voxel at level l
	bool	 get_Node(uint64_t n, uint64_t& ch0, uint64_t& ch1, uint32_t& idx);		///< retrieve the nth lerpnode
	float	 get_Code(uint32_t n);													///< get the nth interpolation weight
	float	 get_CodeVar(uint32_t n);												///< get the variance of the nth interpolation weight
	bool	 get_UniformL0(void);													///< true iff uniform level0 (i.e., leafs only)
	uint16_t get_PackingSize(void);													///< size of the packing table
	uint16_t get_PackingEntry(uint16_t n);											///< gets the nth packing permutation value

	bool set_nLeafs(uint32_t val);													///< set the number of leaf nodes
	bool set_nNodeBits(uint8_t val);												///< set the number of bits to encode each node [1..64]
	bool set_Dimension(uint32_t n, uint32_t val);									///< set the nth dimension -- does not perform a resize!!
	bool set_maxVariance(float var);												///< set the maximum variance of the weights
	bool set_nWeightBits(uint8_t val);												///< set the number of bits to encode each weight [1..16]
	bool set_nLerpNodes(uint64_t val);												///< set the number of lerp nodes [1..2^nNodeBits]
	bool set_nVoxelBits(uint32_t l, uint32_t val);									///< set the number of bits to encode a voxel in level l
	bool set_NodeOffset(uint32_t l, uint64_t val);									///< set the node offset for level l
	bool set_Voxel(uint32_t i, uint32_t j, uint32_t k, uint32_t l, uint64_t val);	///< set the voxel code for voxel ijk at level l
	bool set_Voxel(uint64_t n, uint32_t l, uint64_t val);							///< set the voxel code for voxel n at level l
	bool set_Code(uint32_t n, float val);											///< set the nth interpolation weight ]0,1[ (20 bits)
	bool set_CodeVar(uint32_t n, float var);										///< set the variance of the nth interpolation weight
	bool set_UniformL0(bool val);													///< set the uniform_L0 flag
	bool set_Node(uint64_t, uint64_t ch0, uint64_t ch1, uint32_t idx);				///< set the nth lerpnode
	bool set_PackingSize(uint16_t s);												///< set the packing table size
	bool set_PackingEntry(uint16_t n, uint16_t val);								///< sets the nth packing permutation value

	// derived
	uint32_t get_nLevels(void) const;												///< get the total number of levels
	uint32_t get_Dimension(uint32_t n, uint32_t l) const;							///< get the nth dimension of level l in voxels
	uint64_t get_LevelSize(uint32_t l) const;										///< get the size of level l in voxels
	uint64_t get_VoxelSize(void);													///< get the size of all voxels in bits
	uint64_t get_NodeSize(void);													///< get the size of all nodes in bits
	uint64_t get_CodeSize(void);													///< get the size of the codebook of weights in bits


	// debug output of header
	void output(FILE* stream = stdout);												///< debug output of header information

	// disk io
	bool read(const std::string& name);												///< read data from a file
	bool read(FILE* stream);														///< read data from a stream
	bool write(const std::string& name) const;										///< write data to a file
	bool write(FILE* stream) const;													///< write data to a stream

	template<typename id_t>
	void reconstruct(mipvolume<id_t>& M, mipvolume<id_t>* error = nullptr);

	std::vector<std::vector<size_t> > voxel_ref_lists(void);

	inline bitstream<uint32_t>& stream(void) { return m_data; }
	inline uint64_t voxel_offset(size_t l) { return m_voxel_addr[l]; }
protected:
	bitstream<uint32_t>					m_data;

	std::vector<sparse::vector<uint32_t, lux::errord> > m_lookup_table;

	// addresses
	struct addr_t {
		uint64_t hdr_leafs;		// (1) number of leafs		32 bit	ADDR: 000000
		uint64_t hdr_uniL0;		// (2) uniform level 0?		 1 bit	ADDR: 000032
		uint64_t hdr_pack;		// (3) size of pack table	16 bit	ADDR: 000033
		uint64_t hdr_nbits;		// (4) bits per node		 6 bit	ADDR: 000049			[1..64]
		uint64_t hdr_dims;		// (5) dimx, dimy, dimz	  3x32 bit	ADDR: 000055
		uint64_t hdr_sigma;		// (6) maximum deviation	32 bit  ADDR: 000151
		uint64_t hdr_nweights;	// (7) quantization bits	 4 bit	ADDR: 000183			[1..16]
		uint64_t hdr_nnodes;	// (8) number of lerp nodes	  VAR	ADDR: 000199			uses bitrate (4)
		
		uint64_t idx_vbits;		// (A) p. level: bpv		 6 bit	ADDR: hdr_nnodes + (4)	[1..64]
		uint64_t idx_noffs;		// (B) p. level: node offset  VAR	ADDR: hdr_vbits + [L]*6	uses bitrate (4)
		uint64_t dat_voxel;		// (C) raw voxel data		  VAR	ADDR: 32-aligned		uses bitrates (A)
		uint64_t dat_node;		// (D) raw node data		  VAR	ADDR: 32-aligned		uses bitrate (4)
		uint64_t dat_code;		// (E) raw codebook		  2x20 bit	ADDR: 32-aligned		uses 2^(7) slots, 20 bit code + 20 bit deviation
		uint64_t dat_pack;		// (F) packing table		16 bit	ADDR: 32-aligned		(3) slots at 16 bit each
		uint64_t eof;
	} m_addr;

	bool update_addresses(void);
	inline uint32_t write64(uint64_t val, uint64_t addr, uint32_t bits);
	inline uint32_t read64(uint64_t& val, uint64_t addr, uint32_t bits);
	inline uint32_t write32(uint32_t val, uint64_t addr, uint32_t bits);
	inline uint32_t read32(uint32_t& val, uint64_t addr, uint32_t bits);
	inline void align32(uint64_t& addr);
	// further convenience
	std::vector<uint32_t> m_dimensions;
	std::vector<uint64_t> m_voxel_addr;

	static const uint64_t INVALID;
	void clear_addr(void);
};

#include"lux_footprint_assembly.h"

inline uint32_t compressed_mipvolume::write64(uint64_t val, uint64_t addr, uint32_t bits) {
	assert("compressed_mipvolume::write64() - invalid parameters" && bits <= 64 && addr != INVALID);
	if (addr == INVALID) return 0;
	if (bits < 32) return m_data.write(uint32_t(val), addr, bits);
	return m_data.write(uint32_t(val), addr, 32) + m_data.write(uint32_t(val >> uint64_t(32)), addr + 32, bits - 32);
}

inline uint32_t compressed_mipvolume::read64(uint64_t& val, uint64_t addr, uint32_t bits) {
	assert("compressed_mipvolume::read64() - invalid parameters" && bits <= 64 && addr != INVALID);
	if (addr == INVALID) return 0;
	uint32_t v=0;
	if (bits < 32) {
		uint32_t r = m_data.read(v, addr, bits);
		val = uint64_t(v);
		return r;
	}
	uint32_t r = m_data.read(v, addr, 32);
	val = uint64_t(v);
	r += m_data.read(v, addr + 32, bits - 32);
	val |= uint64_t(v) << uint64_t(32);
	return r;
}

inline uint32_t compressed_mipvolume::write32(uint32_t val, uint64_t addr, uint32_t bits) {
	assert("compressed_mipvolume::write32() - invalid parameters" && bits <= 32 && addr != INVALID);
	if (addr == INVALID) return 0;
	return m_data.write(val, addr, bits);
}

inline uint32_t compressed_mipvolume::read32(uint32_t& val, uint64_t addr, uint32_t bits) {
	assert("compressed_mipvolume::read32() - invalid parameters" && bits <= 32 && addr != INVALID);
	if (addr == INVALID) return 0;
	return m_data.read(val, addr, bits);
}

template<typename id_t>
void compressed_mipvolume::reconstruct(mipvolume<id_t>& M, mipvolume<id_t>* err) {
	M.clear();
	M.resize(get_Dimension(0), get_Dimension(1), get_Dimension(2));
	uint32_t nLeafs = get_nLeafs();
	size_t nNodes = size_t(get_nLerpNodes());

	if (err == nullptr) { 
		// NO ERROR ESTIMATE
		
		// reconstruct leaf nodes
		std::vector<sparse::vector<id_t, float> > nodes;
		for (size_t n = 0; n < nLeafs; n++) {
			nodes.push_back(sparse::vector<id_t, float>());
			nodes.back().unit(id_t(n));			
		}
		
		// reconstruct interior nodes
		for (size_t n = nLeafs; n < nNodes + nLeafs; n++) {
			uint64_t ch0, ch1;
			uint32_t idx;
			get_Node(n - nLeafs, ch0, ch1, idx);
			float w = get_Code(idx);
			nodes.push_back(lerp(nodes[ch0], nodes[ch1], w));
			nodes.back().trim();
		}		

		// reconstruct volume
		for (uint32_t l = 0; l < get_nLevels(); l++) {
			printf("%i\n", l);
			segvolume<id_t>& vol(M.level(l));
			for (size_t n = 0; n < vol.size(); n++) {
				uint64_t val = get_Voxel(n, l);
				vol(n) = nodes[val];				
			}
		}		
	}
	else { 
		// ERROR ESTIMATE

		err->clear();
		err->resize(get_Dimension(0), get_Dimension(1), get_Dimension(2));
		
		// reconstruct leaf nodes
		std::vector < sparse::vector<id_t, error> > nodes;
		for (size_t n = 0; n < nLeafs; n++) {
			nodes.push_back(sparse::vector<id_t, error>());
			nodes.back().set(id_t(n), error(1.0f, 0.0f));
			nodes.back().trim();
		}
		// reconstruct interior nodes
		for (size_t n = nLeafs; n < nNodes + nLeafs; n++) {
			uint64_t ch0, ch1;
			uint32_t idx;
			get_Node(n - nLeafs, ch0, ch1, idx);
			float w = get_Code(idx);
			float s = get_CodeVar(idx);
			nodes.push_back(lerp(nodes[ch0], nodes[ch1], error(w,s)));
			nodes.back().trim();
		}
		// reconstruct volume
		for (uint32_t l = 0; l < get_nLevels(); l++) {			
			segvolume<id_t>& vol(M.level(l));
			segvolume<id_t>& e(err->level(l));
			for (size_t n = 0; n < vol.size(); n++) {
				uint64_t val = get_Voxel(n, l);
				for (auto it = nodes[val].begin(); nodes[val].end(); it++) {
					vol(n).set(it->first, it->second.mu());
					e(n).set(it->first, it->second.sigma());
				}
				vol(n).trim();
				e(n).trim();
			}
		}
	}
}

inline void compressed_mipvolume::align32(uint64_t& addr) {
	uint64_t al = addr&uint64_t(0x1F);
	if (al == 0) return;
	addr += uint64_t(32) - al;
}

#endif
