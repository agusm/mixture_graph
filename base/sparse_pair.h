#ifndef __SPARSE_PAIR_H__
#define __SPARSE_PAIR_H__

#include<algorithm>

// minimal replacement for std::pair with 1-Byte alignment
namespace sparse {
	template<class A, class B> class pair;	
	template<class A, class B> pair<A, B> make_pair(const A& a, const B& b) { return pair<A, B>(a, b); }
};

template<class A, class B>
class sparse::pair {
public:
	pair(void) : first(A(0)), second(B(0)) {}
	pair(const A& a, const B& b) : first(a), second(b) {}
	pair(const pair& p) = default;	
	pair& operator=(const pair& other) { first = other.first; second = other.second; return *this; }
	void swap(pair& other) {
		std::swap(first, other.first);
		std::swap(second, other.second);
	}
	bool operator==(const pair& other) const {
		return first == other.first && second == other.second;
	}
	bool operator!=(const pair& other) const {
		return !((*this) == other);
	}
	bool operator<(const pair& other) const {
		if (first < other.first) return true;
		if (other.first < first) return false;
		return second < other.second;
	}
	bool operator<=(const pair& other) const {
		if (first < other.first) return true;
		if (other.first<first) return false;
		return !(other.second < second);
	}
	bool operator>(const pair& other) const {
		return !((*this) <= other);
	}
	bool operator>=(const pair& other) const {
		return !((*this) < other);
	}
#ifdef COMPACT
	#pragma	message("sparse::pair -- compact mode")
	#pragma pack (push,1)
		A first;
		B second;
	#pragma pack (pop)
#else
	#pragma message("sparse pair -- performance mode")
		A first;
		B second;
#endif
};

#endif
