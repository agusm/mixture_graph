#include"codebook.h"
#include<cassert>
#include<algorithm>
#include<inttypes.h>

codebook::codebook(void) {
}

codebook::codebook(const codebook& other) {
	*this = other;
}

codebook::~codebook(void) {
	clear();
}

size_t codebook::size(void) const {
	return m_data.size();
}

bool codebook::empty(void) {
	return m_data.empty();
}

void codebook::clear(void) {
	m_data.clear();
	m_diffuse.clear();
	m_specular.clear();
	m_ambient = vec3_t(0.0f, 0.0f, 0.0f);
}

void codebook::resize(size_t n) {
	if (n != m_data.size()) {
		m_diffuse.clear();
		m_specular.clear();
		m_ambient = vec3_t(0.0f, 0.0f, 0.0f);
	}
	m_data.resize(n);
}
typename std::vector<vec3_t>::reference codebook::operator[](size_t n) {
	assert("codebook[] -- invalid argument" && n < m_data.size());
	return m_data[n];
}
typename std::vector<vec3_t>::const_reference codebook::operator[](size_t n) const {
	assert("codebook[] -- invalid argument" && n < m_data.size());
	return m_data[n];
}

enum class model {
	PHONG,
	BLINN_PHONG,
	HALFLIFE
};
void codebook::shade(const light_t& L, const codebook::model& m) {
	vec3_t Ld = L.L_obj_dir.normalized();
	vec3_t Vd = L.V_obj_dir.normalized();
	m_specular.resize(m_data.size());
	m_diffuse.resize(m_data.size());
	m_ambient = L.L_ambient_color;
	switch (m) {
	case model::PHONG:
		for (size_t n = 0; n < m_data.size(); n++) {
			float Idiff = std::max(0.0f, Ld * m_data[n]);
			m_diffuse[n] = L.L_diffuse_color * Idiff;
			if (Idiff > 0.0f) {
				vec3_t R = m_data[n] * 2.0f * (Vd * m_data[n]) - Vd;
				float Ispec = std::max(0.0f, R * Ld);
				if (Ispec > 0.0f) Ispec = float(pow(Ispec, L.shininess));
				m_specular[n] = L.L_specular_color * Ispec;
			}
		}
		break;
	case model::BLINN_PHONG:
	{
		vec3_t H = (Ld + Vd).normalized();	// Half-vector is normal independent
		for (size_t n = 0; n < m_data.size(); n++) {
			float Idiff = std::max(0.0f, Ld * m_data[n]);
			m_diffuse[n] = L.L_diffuse_color * Idiff;
			if (Idiff > 0.0f) {
				float Ispec = H * m_data[n];
				if (Ispec > 0.0f) Ispec = float(pow(Ispec, L.shininess));
				m_specular[n] = L.L_specular_color * Ispec;
			}
		}
		break;
	}
	case model::HALFLIFE:
		for (size_t n = 0; n < m_data.size(); n++) {
			float Idiff = Ld * m_data[n]*0.5f+0.5f;
			m_diffuse[n] = L.L_diffuse_color * Idiff;
			vec3_t R = m_data[n] * 2.0f * (Vd * m_data[n]) - Vd;
			float Ispec = std::max(0.0f, R * Ld);
			if (Ispec > 0.0f) Ispec = float(pow(Ispec, L.shininess));
			m_specular[n] = L.L_specular_color * Ispec;
		}
		break;
	default:
		assert("codebook::shade() -- invalid argument for model" && false);
		m_specular.clear();
		m_diffuse.clear();
		m_ambient = vec3_t(0.0f, 0.0f, 0.0f);
	}
}
vec3_t& codebook::ambient(void) {
	return m_ambient;
}

const vec3_t& codebook::ambient(void) const {
	return m_ambient;
}

typename std::vector<vec3_t>::reference codebook::diffuse(size_t n) {
	assert("codebook::diffuse() -- invalid argument" && n < m_diffuse.size());
	return m_diffuse[n];
}
typename std::vector<vec3_t>::const_reference codebook::diffuse(size_t n) const {
	assert("codebook::diffuse() -- invalid argument" && n < m_diffuse.size());
	return m_diffuse[n];
}

typename std::vector<vec3_t>::reference codebook::specular(size_t n) {
	assert("codebook::specular() -- invalid argument" && n < m_specular.size());
	return m_specular[n];
}

typename std::vector<vec3_t>::const_reference codebook::specular(size_t n) const {
	assert("codebook::specular() -- invalid argument" && n < m_specular.size());
	return m_specular[n];
}

bool codebook::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream);
	if (stream != nullptr) fclose(stream);
	return result;
}

bool codebook::write(FILE* stream) const {
	if (stream == nullptr) return false;
	uint16_t size = uint16_t(m_data.size());
	if (fwrite(&size, sizeof(uint16_t), 1, stream) != 1) return false;
	if (fwrite(m_data.data(), sizeof(vec3_t), m_data.size(), stream) != m_data.size()) return false;
	return true;
}

bool codebook::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read(stream);
	if (stream != nullptr) fclose(stream);
	return result;
}
bool codebook::read(FILE* stream) {
	if (stream == nullptr) return false;
	clear();
	uint16_t size = 0;
	if (fread(&size, sizeof(uint16_t), 1, stream) != 1) return false;
	resize(size);
	if (fread(m_data.data(), sizeof(vec3_t), m_data.size(), stream) != m_data.size()) return false;
	return true;
}

const float* codebook::diffuse_data(void) const {
	if (m_diffuse.empty()) return nullptr;
	return reinterpret_cast<const float*>(m_diffuse.data());
}

const float* codebook::specular_data(void) const {
	if (m_specular.empty()) return nullptr;
	return reinterpret_cast<const float*>(m_specular.data());
}

codebook& codebook::operator=(const codebook& other) {
	m_data = other.m_data;
	m_ambient = other.m_ambient;
	m_diffuse = other.m_diffuse;
	m_specular = other.m_specular;
	return *this;
}

bool codebook::is_shaded(void) const {
	return !m_diffuse.empty();
}
