#ifndef __LUX_ERROR_H__
#define __LUX_ERROR_H__

namespace lux {
	template<class T> class error;
	typedef error<float>  errorf;
	typedef error<double> errord;
};

template<class T>
class lux::error {
public:
	error(void);
	error(T mu, T sigma = 0.0f);
	error(const error& other);
	~error(void);
	const T& mu(void) const;
	const T& sigma(void) const;
	T& mu(void);
	T& sigma(void);
	error& operator=(const error& other);
	bool operator==(const error& other) const;
	bool operator!=(const error& other) const;

	error operator+(const error& other) const;
	error operator*(const error& other) const;
	error operator-(const error& other) const;
	inline const error& operator+= (const error& other) {
		(*this) = (*this) + other;
		return *this;
	}
	inline const error& operator*=(const error& other) {
		(*this) = (*this)*other;
		return *this;
	}
	template<class S>
	inline error<S> as(void) const {
		return error<S>(S(m_mu), S(m_sigma));
	}
protected:
	T m_mu, m_sigma;
};


#include<cmath>

template<class T>
lux::error<T>::error(void) : m_mu(T(0)), m_sigma(T(0)) {
}

template<class T>
lux::error<T>::error(T mu, T sigma) : m_mu(mu), m_sigma(sigma) {
}

template<class T>
lux::error<T>::error(const error& other) {
	*this = other;
}

template<class T>
lux::error<T>::~error(void) {
}

template<class T>
const T& lux::error<T>::mu(void) const {
	return m_mu;
}

template<class T>
const T& lux::error<T>::sigma(void) const {
	return m_sigma;
}

template<class T>
T& lux::error<T>::mu(void) {
	return m_mu;
}

template<class T>
T& lux::error<T>::sigma(void) {
	return m_sigma;
}

template<class T>
lux::error<T>& lux::error<T>::operator=(const error<T>& other) {
	m_mu = other.m_mu;
	m_sigma = other.m_sigma;
	return *this;
}

template<class T>
lux::error<T> lux::error<T>::operator+(const error<T>& other) const {
	return error(m_mu + other.m_mu, sqrt(m_sigma*m_sigma + other.m_sigma*other.m_sigma));
}

template<class T>
lux::error<T> lux::error<T>::operator*(const lux::error<T>& other) const {
	double s1 = m_sigma / m_mu;
	double s2 = other.m_sigma / other.m_mu;
	double m = m_mu*other.m_mu;
	return error(T(m), T(std::abs(m)*sqrt(s1*s1 + s2*s2)));
}

template<class T>
lux::error<T> lux::error<T>::operator-(const error<T>& other) const {
	return error(m_mu - other.m_mu, sqrt(m_sigma*m_sigma + other.m_sigma*other.m_sigma));
}

template<class T>
bool lux::error<T>::operator==(const error<T>& other) const {
	return m_mu == other.m_mu && m_sigma == other.m_sigma;
}

template<class T>
bool lux::error<T>::operator!=(const error<T>& other) const {
	return !((*this) == other);
}

#endif
