#ifndef __VOLUME_H__
#define __VOLUME_H__

#include<map>
#include<inttypes.h>
#include<stdio.h>
#include<vector>
#include<string>
#include<limits>

class volume {
public:
	using id_t = uint16_t;	
	volume(void);
	volume(const volume& other);
	volume(uint32_t rx, uint32_t ry, uint32_t rz);
	~volume(void);
	bool empty(void) const;
	void clear(void);
	size_t size(void) const;
	void resize(uint32_t i, uint32_t j, uint32_t k);
	const id_t& operator()(uint32_t i, uint32_t j, uint32_t k) const;
	id_t& operator()(uint32_t i, uint32_t j, uint32_t k);
	const id_t& operator[](size_t n) const;
	id_t& operator[](size_t n);
	const uint32_t& dimension(uint32_t n) const;
	const volume& operator=(const volume& other);
	bool read(const std::string& name);
	bool read(FILE* stream);
	bool write(const std::string& name) const;
	bool write(FILE* stream) const;
	const id_t& background(void) const;
	id_t& background(void);
	bool is_background(size_t n) const;
	bool is_background(uint32_t i, uint32_t j, uint32_t k) const;
	bool minmax(id_t& vmin, id_t& vmax) const;
	void zero(void);
	typedef std::map<id_t, size_t> histogram_t;
	histogram_t histogram(void) const;
	volume subvolume(uint32_t i, uint32_t j, uint32_t k, uint32_t rx, uint32_t ry, uint32_t rz) const;
	
	const id_t* data(void) const;
	id_t* data(void);

	constexpr static const id_t INVALID = std::numeric_limits<uint16_t>::max();
protected:
	inline size_t linear_address(uint32_t i, uint32_t j, uint32_t k) const;
	std::vector<id_t>		m_data;
	uint32_t				m_dims[3];
	id_t					m_background;
};

#endif
