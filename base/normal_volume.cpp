#include"normal_volume.h"

normal_volume::normal_volume(void) : m_dims({ 0,0,0 }) {
}

normal_volume::normal_volume(const normal_volume& other) : m_dims({ 0,0,0 }) {
	*this = other;
}

normal_volume::normal_volume(uint32_t rx, uint32_t ry, uint32_t rz) : m_dims({ 0,0,0 }) {
	resize(rx, ry, rz);
}

normal_volume::~normal_volume(void) {
	clear();
}

bool normal_volume::empty(void) const {
	return m_data.empty();
}

void normal_volume::clear(void) {
	m_data.clear();
	m_dims = { 0,0,0 };
}

size_t normal_volume::size(void) const {
	return m_data.size();
}

void normal_volume::resize(uint32_t rx, uint32_t ry, uint32_t rz) {
	size_t ns = size_t(rx) * size_t(ry) * size_t(rz);
	if (ns == 0) return clear();
	m_data.resize(ns);
	m_dims = { rx,ry,rz };
}

const normal_volume::data_t& normal_volume::operator()(uint32_t i, uint32_t j, uint32_t k) const {
	return m_data[linear_address(i, j, k)];
}
const normal_volume::data_t& normal_volume::operator()(const uivec_t& p) const {
	return m_data[linear_address(p)];
}

normal_volume::data_t& normal_volume::operator()(uint32_t i, uint32_t j, uint32_t k) {
	return m_data[linear_address(i, j, k)];
}

normal_volume::data_t& normal_volume::operator()(const uivec_t& p) {
	return m_data[linear_address(p)];
}

const normal_volume::data_t& normal_volume::operator[](size_t n) const {
	return m_data[n];
}

normal_volume::data_t& normal_volume::operator[](size_t n) {
	return m_data[n];
}

const uint32_t& normal_volume::dimension(uint32_t n) const {
	assert("normal_volume::dimension() -- invalid argument" && n < 3);
	return m_dims[n];
}

const normal_volume& normal_volume::operator=(const normal_volume& other) {
	m_data = other.m_data;
	m_dims = other.m_dims;
	return *this;
}

enum class format {
	FLOAT,
	UCHAR,
	UINT
};

bool normal_volume::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read(stream);
	fclose(stream);
	return result;
}
bool normal_volume::read(FILE* stream) {
	if (stream == nullptr) return false;
	std::array<uint32_t, 3> ndims;
	if (fread(ndims.data(), sizeof(uint32_t), 3, stream) != 3) return false;
	resize(ndims[0], ndims[1], ndims[2]);
	uint32_t fmt = 0;
	if (fread(&fmt, sizeof(uint32_t), 1, stream) != 1) return false;
	format F = format(fmt);
	switch (F) {
	case format::FLOAT:
	{
		return fread(m_data.data(), sizeof(vec3_t), m_data.size(), stream) == m_data.size();
	}
	case format::UCHAR:
	{
		std::vector<uint8_t> buf;
		buf.resize(size() * 3);
		if (fread(buf.data(), sizeof(uint8_t), buf.size(), stream) != buf.size()) return false;
		from_RGB8(buf);
		return true;
	}
	case format::USHORT:
	{
		std::vector<uint16_t> buf;
		buf.resize(size() * 3);
		if (fread(buf.data(), sizeof(uint16_t), buf.size(), stream) != buf.size()) return false;
		from_RGB16(buf);
		return true;
	}
	default:
		return false;
	}
}

bool normal_volume::write(const std::string& name, const format& F) const {
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream, F);
	if (stream != nullptr) fclose(stream);
	return result;
}

bool normal_volume::write(FILE* stream, const format& F) const {
	if (stream == nullptr) return false;
	if (fwrite(m_dims.data(), sizeof(uint32_t), 3, stream) != 3) return false;
	uint32_t fmt = uint32_t(F);
	if (fwrite(&fmt, sizeof(uint32_t), 1, stream) != 1) return false;
	switch (F) {
	case format::FLOAT:
		return fwrite(m_data.data(), sizeof(vec3_t), m_data.size(), stream) == m_data.size();
	case format::UCHAR:
	{
		std::vector<uint8_t> buf;
		as_RGB8(buf);
		return fwrite(buf.data(), sizeof(uint8_t), buf.size(), stream) == buf.size();
	}
	case format::USHORT:
	{
		std::vector<uint16_t> buf;
		as_RGB16(buf);
		return fwrite(buf.data(), sizeof(uint16_t), buf.size(), stream) == buf.size();
	}
	default:
		return false;
	}
	return true;
}
void normal_volume::zero(void) {
	memset(m_data.data(), 0, sizeof(vec3_t) * m_data.size());
}

void normal_volume::normalize(void) {
#pragma omp parallel for schedule (dynamic,16)
	for (int64_t n=0; n<int64_t(m_data.size()); n++) {
		m_data[n].normalize();
	}
}

normal_volume normal_volume::subsample(void) {
	normal_volume result((m_dims[0] + 1) >> 1, (m_dims[1] + 1) >> 1, (m_dims[2] + 1) >> 1);
#pragma omp parallel for schedule(dynamic,4)
	for (int K = 0; K<int(result.dimension(2)); K++) {
		uint32_t k = uint32_t(K);
		uint32_t kl = std::min(2 + (k >> 1), m_dims[2]);
		for (uint32_t j = 0; j < result.dimension(1); j++) {
			uint32_t jl = std::min(2 + (j >> 1), m_dims[1]);
			for (uint32_t i = 0; i < result.dimension(0); i++) {
				uint32_t il = std::min(2 + (i >> 1), m_dims[0]);
				vec3_t n(0.0f, 0.0f, 0.0f);
				for (uint32_t dk = k >> 1; dk < kl; dk++) {
					for (uint32_t dj = j >> 1; dj < jl; dj++) {
						for (uint32_t di = i >> 1; di < il; di++) {
							n += operator()(di, dj, dk);
						}
					}
				}
				n.normalize();
				result(i, j, k) = n;
			}
		}
	}
	return result;
}

const normal_volume::data_t* normal_volume::data(void) const {
	return m_data.data();
}

normal_volume::data_t* normal_volume::data(void) {
	return m_data.data();
}

inline void normal_volume::as_RGB8(std::vector<uint8_t>& result) const {
	using type = uint8_t;
	constexpr static float scale = 0.5f * float(std::numeric_limits<type>::max());
	result.resize(3 * m_data.size());
#pragma omp parallel for schedule (dynamic,4)
	for (int64_t n = 0; n < int64_t(m_data.size()); n++) {
		vec3_t u(m_data[n]);
		u.normalize();
		for (int64_t c=0; c<3; c++) result[3 * n + c] = type(scale * u[c] + scale);
	}
}

inline void normal_volume::as_RGB16(std::vector <uint16_t>& result) const {
	using type = uint16_t;
	constexpr static float scale = 0.5f * float(std::numeric_limits<type>::max());
	result.resize(3 * m_data.size());
#pragma omp parallel for schedule (dynamic,4)
	for (int64_t n = 0; n < int64_t(m_data.size()); n++) {
		vec3_t u(m_data[n]);
		u.normalize();
		for (int64_t c = 0; c < 3; c++) result[3 * n + c] = type(scale * u[c] + scale);
	}
}

inline void normal_volume::from_RGB8(const std::vector<uint8_t>& data) {
	using type = uint8_t;
	constexpr static float ooScale = 2.0f / float(std::numeric_limits<type>::max());
	m_data.resize(data.size() / 3);
#pragma omp parallel for schedule(dynamic,4)
	for (int64_t n = 0; n < int64_t(size()); n++) {
		for (int64_t c = 0; c < 3; c++) m_data[n][c] = float(data[3 * n + c]) * ooScale - 1.0f;
	}
}

inline void normal_volume::from_RGB16(const std::vector<uint16_t>& data) {
	using type = uint16_t;
	constexpr static float ooScale = 2.0f / float(std::numeric_limits<type>::max());
	m_data.resize(data.size() / 3);
#pragma omp parallel for schedule(dynamic,4)
	for (int64_t n = 0; n < int64_t(size()); n++) {
		for (int64_t c = 0; c < 3; c++) m_data[n][c] = float(data[3 * n + c]) * ooScale - 1.0f;
	}
}