#ifndef __CODEBOOK_H__
#define __CODEBOOK_H__

#include"vec_t.h"
#include<vector>
#include<string>
#include<stdio.h>
#include<stdlib.h>

struct light_t {
	vec3_t	L_obj_dir;			// light direction in object space
	vec3_t  V_obj_dir;			// view direction in object space
	vec3_t  L_ambient_color;	// rgb in [0,1]^3
	vec3_t	L_diffuse_color;	// rgb in [0,1]^3
	vec3_t	L_specular_color;	// rgb in [0,1]^3
	float	shininess;
};

class codebook {
public:
	codebook(void);
	codebook(const codebook& other);
	~codebook(void);
	size_t size(void) const;
	bool empty(void);
	void clear(void);
	void resize(size_t n);
	typename std::vector<vec3_t>::reference operator[](size_t n);
	typename std::vector<vec3_t>::const_reference operator[](size_t n) const;
	enum class model {
		PHONG,
		BLINN_PHONG,
		HALFLIFE
	};
	// shade() will compute two light intensities, diffuse and specular,
	// for each normal in the LUT.
	// lightdir and viewdir to point from object origin to light / camera respectively.
	void shade(const light_t& L, const codebook::model& m = codebook::model::HALFLIFE);
	vec3_t& ambient(void);
	const vec3_t& ambient(void) const;
	typename std::vector<vec3_t>::reference diffuse(size_t n);
	typename std::vector<vec3_t>::const_reference diffuse(size_t n) const;
	typename std::vector<vec3_t>::reference specular(size_t n);
	typename std::vector<vec3_t>::const_reference specular(size_t n) const;
	bool write(const std::string& name) const;
	bool write(FILE* stream) const;
	bool read(const std::string& name);
	bool read(FILE* stream);
	const float* diffuse_data(void) const;
	const float* specular_data(void) const;
	codebook& operator=(const codebook& other);
	bool is_shaded(void) const;
protected:
	std::vector<vec3_t> m_data;
	vec3_t				m_ambient;
	std::vector<vec3_t>	m_diffuse;
	std::vector<vec3_t>	m_specular;
};

#endif
