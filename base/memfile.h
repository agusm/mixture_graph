#ifndef __MEMFILE_H__
#define __MEMFILE_H__

#include<limits>
#include<string>
#include<vector>
#include<inttypes.h>
#include<memory.h>
#include<algorithm>

// sequentially accessed binary file
class memfile {
public:
	inline memfile(size_t buffer = 16<<20);	
	inline ~memfile(void);
	inline bool open_read(const std::string& name);
	inline bool open_write(const std::string& name);
	inline size_t read(void* buffer, size_t element_size, size_t nElements);
	inline size_t write(const void* buffer, size_t element_size, size_t nElements);
	inline void close(void);
	inline bool is_open(void) const;
	inline size_t tell(void) const;
	inline void flush(void);
protected:
	FILE*	m_stream;
	size_t	m_pos;
	size_t  m_avail;
	std::vector<uint8_t> m_buffer;
	size_t	m_buffersize;
	size_t	m_fileptr;
	bool	m_readable;
	bool	m_writeable;
private:
	memfile(const memfile&);
};

inline memfile::memfile(size_t buffer) : m_stream(nullptr), m_buffersize(buffer), m_writeable(false), m_readable(false),m_pos(0),m_fileptr(0),m_avail(0) {
}

inline memfile::~memfile(void) {
	close();
	m_buffer.clear();
}

inline bool memfile::open_read(const std::string& name) {
	if (is_open()) close();
	m_buffer.clear();
	m_buffer.resize(m_buffersize);
	m_pos = 0;
	m_fileptr = 0;
	m_avail = 0;
	m_readable = true;
	m_writeable = false;
	return fopen_s(&m_stream, name.c_str(), "rb") == 0;
}

inline bool memfile::open_write(const std::string& name) {
	if (is_open()) close();
	m_buffer.clear();
	m_buffer.resize(m_buffersize);
	m_pos = 0;
	m_fileptr = 0;
	m_avail = m_buffersize;
	m_readable = false;
	m_writeable = true;
	return fopen_s(&m_stream, name.c_str(), "wb") == 0;
}

inline size_t memfile::read(void* buffer, size_t element_size, size_t nElements) {	
	if (!m_readable) return 0;
	size_t nBytes = element_size * nElements;
	if (nBytes == 0) return  0;
	size_t nServed = 0;
	uint8_t* ptr = reinterpret_cast<uint8_t*>(buffer);
	while (nBytes>0) {
		size_t toserve = std::min(nBytes, m_avail);
		if (m_avail == 0) {
			m_avail = fread(m_buffer.data(), 1, m_buffersize, m_stream);
			if (m_avail==0) return nServed / element_size;
			m_fileptr += m_pos;
			m_pos = 0;
		}
		memcpy(ptr, m_buffer.data() + m_pos, toserve);
		m_pos += toserve;
		nServed += toserve;
		nBytes -= toserve;
		m_avail -= toserve;
		ptr += toserve;
	}
	return nServed / element_size;
}

inline void memfile::flush(void) {
	if (!m_writeable) return;
	fwrite(m_buffer.data(), 1, m_pos, m_stream);
	m_avail = m_buffersize;
	m_fileptr += m_pos;
	m_pos = 0;
}

inline size_t memfile::write(const void* buffer, size_t element_size, size_t nElements) {
	if (!m_writeable) return 0;
	size_t nBytes = element_size * nElements;
	if (nBytes == 0) return  0;
	size_t nServed = 0;
	const uint8_t* ptr = reinterpret_cast<const uint8_t*>(buffer);
	while (nBytes > 0) {
		if (m_avail == 0) {
			flush();
			//if (fwrite(m_buffer.data(), 1, m_buffersize, m_stream) != m_buffersize) return nServed / element_size;
			//m_avail = m_buffersize;
			//m_fileptr += m_pos;
			//m_pos = 0;
		}
		size_t toserve = std::min(nBytes, m_avail);
		memcpy(m_buffer.data() + m_pos, ptr, toserve);
		m_pos += toserve;
		nServed += toserve;
		nBytes -= toserve;
		m_avail -= toserve;
		ptr += toserve;
	}
	return nServed / element_size;
}

inline void memfile::close(void) {
	if (m_stream == nullptr) return;
	flush();
	fclose(m_stream);
	m_buffer.clear();
	m_pos = 0;
	m_avail = 0;
}

inline bool memfile::is_open(void) const {
	return m_stream != nullptr;
}

inline size_t memfile::tell(void) const {
	return m_fileptr + m_pos;
}

#endif

