#include"volume.h"
#include<cassert>
#include<algorithm>
#include<memory>

volume::volume(void) : m_background(INVALID) {
	for (int i = 0; i < 3; i++) m_dims[i] = 0;
}

volume::volume(const volume& other) {
	*this = other;
}

volume::volume(uint32_t rx, uint32_t ry, uint32_t rz) {
	resize(rx, ry, rz);
}

volume::~volume(void) {
	clear();
}

bool volume::empty(void) const {
	return m_data.empty();
}

void volume::clear(void) {
	m_data.clear();
	m_dims[0] = m_dims[1] = m_dims[2] = 0;
	m_background = INVALID;
}

size_t volume::size(void) const {
	return m_data.size();
}

void volume::resize(uint32_t rx, uint32_t ry, uint32_t rz) {
	size_t ns = size_t(rx)*size_t(ry)*size_t(rz);
	if (ns == 0) {
		clear();
		return;
	}
	m_data.resize(ns);
	m_data.shrink_to_fit();
	m_dims[0] = rx;
	m_dims[1] = ry;
	m_dims[2] = rz;
}

const volume::id_t& volume::operator()(uint32_t i, uint32_t j, uint32_t k) const {
	return m_data[linear_address(i, j, k)];	
}

volume::id_t& volume::operator()(uint32_t i, uint32_t j, uint32_t k) {	
	return m_data[linear_address(i, j, k)];
}

const volume::id_t& volume::operator[](size_t n) const {
	assert("volume[] - invalid parameter" && n < m_data.size());
	return m_data[n];
}

volume::id_t& volume::operator[](size_t n) {
	assert("volume[] - invalid parameter" && n < m_data.size());
	return m_data[n];
}

const uint32_t& volume::dimension(uint32_t n) const {
	assert("volume::dimension() - invalid parameter" && n < 3);
	return m_dims[n];
}

const volume& volume::operator=(const volume& other) {
	m_data = other.m_data;
	for (int i = 0; i < 3; i++) m_dims[i] = other.m_dims[i];
	m_background = other.m_background;
	return *this;
}


bool volume::read(const std::string& name) {
	if (name.empty()) return false;
	FILE* stream = NULL;
	if (fopen_s(&stream, name.c_str(), "rb")) return false;
	bool result = read(stream);
	fclose(stream);
	return result;
}

bool volume::read(FILE* stream) {
	if (stream == NULL) return false;
	if (fread(&m_background, sizeof(id_t), 1, stream) != 1) return false;
	if (fread(&m_dims, sizeof(uint32_t), 3, stream) != 3) return false;
	size_t ns = size_t(m_dims[0])*size_t(m_dims[1])*size_t(m_dims[2]);
	if (ns == 0) return true;
	m_data.resize(ns);
	if (fread(m_data.data(), sizeof(id_t), m_data.size(), stream) != m_data.size()) return false;
	return true;
}

bool volume::write(const std::string& name) const {
	if (name.empty()) return false;
	FILE* stream = NULL;
	if (fopen_s(&stream, name.c_str(), "wb")) return false;
	bool result = write(stream);
	fclose(stream);
	return result;
}

bool volume::write(FILE* stream) const {
	if (stream == NULL) return false;
	if (fwrite(&m_background, sizeof(id_t), 1, stream) != 1) return false;
	if (fwrite(m_dims, sizeof(uint32_t), 3, stream) != 3) return false;
	if (fwrite(m_data.data(), sizeof(id_t), m_data.size(), stream) != m_data.size()) return false;
	return true;
}

const volume::id_t& volume::background(void) const {
	return m_background;
}

volume::id_t& volume::background(void) {
	return m_background;
}

bool volume::is_background(size_t n) const {
	assert("volume::is_background() - invalid parameter" && n < m_data.size());
	return m_data[n] == m_background;
}

bool volume::is_background(uint32_t i, uint32_t j, uint32_t k) const {
	assert("volume::is_background() - invalid parameter(s)" && i < m_dims[0] && j < m_dims[1] && k < m_dims[2]);
	return operator()(i, j, k) == m_background;
}

bool volume::minmax(id_t& vmin, id_t& vmax) const {
	vmin = id_t(-1);
	vmax = 0;
	if (empty()) return false;
	for (size_t n = 0; n < m_data.size(); n++) {
		vmin = std::min(vmin, m_data[n]);
		vmax = std::max(vmax, m_data[n]);
	}
	return true;
}

void volume::zero(void) {
	if (m_data.size()>0) memset(m_data.data(), 0, sizeof(id_t)*m_data.size());
}

volume::histogram_t volume::histogram(void) const {
	histogram_t H;
	if (m_data.empty()) return H;
	for (size_t n = 0; n < m_data.size(); n++) H[m_data[n]]++;
	return H;
}

volume volume::subvolume(uint32_t i, uint32_t j, uint32_t k, uint32_t rx, uint32_t ry, uint32_t rz) const {
	if (i >= m_dims[0] || j >= m_dims[1] || k >= m_dims[2]) return volume();
	if (rx + i > m_dims[0]) rx = m_dims[0] - i;
	if (ry + j > m_dims[1]) ry = m_dims[1] - j;
	if (rz + k > m_dims[2]) rz = m_dims[3] - k;
	volume V(rx, ry, rz);
	for (uint32_t z = 0; z < rz; z++) {
		for (uint32_t y = 0; y < ry; y++) {
			for (uint32_t x = 0; x < rx; x++) {
				V(x, y, z) = (*this)(x + i, y + j, z + k);
			}
		}
	}
	return V;
}

inline size_t volume::linear_address(uint32_t i, uint32_t j, uint32_t k) const {
	assert("volume::linear_address() -- invalid argument(s)" && i < m_dims[0] && j < m_dims[1] && m_dims[2] < k);
	return size_t(i) + size_t(m_dims[0]) * (size_t(j) + size_t(m_dims[1]) * size_t(k));
}

const volume::id_t* volume::data(void) const {
	return m_data.data();
}

volume::id_t* volume::data(void) {
	return m_data.data();
}