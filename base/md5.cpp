#include"md5.h"
#include<vector>
#include<algorithm>
#include<wincrypt.h>


DWORD md5(std::string& hash, const void* mem, uint64_t bytes) {
	HCRYPTPROV hProv;
	if (!CryptAcquireContext(&hProv, nullptr, nullptr, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
		return GetLastError();
	}
	HCRYPTHASH hHash;
	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) {
		CryptReleaseContext(hProv, 0);
		return GetLastError();
	}
	const BYTE* pData = reinterpret_cast<const BYTE*>(mem);
	while (bytes > 0) {
		uint32_t tohash = uint32_t(std::min(uint64_t(1) << uint64_t(20), bytes));
		if (!CryptHashData(hHash, pData, tohash, 0)) {
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			return GetLastError();
		}
		pData += tohash;
		bytes -= tohash;
	}

	DWORD len = 256;
	std::vector<uint8_t> H(len, 0);
	if (!CryptGetHashParam(hHash, HP_HASHVAL, H.data(), &len, 0)) {
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		return GetLastError();
	}
	CryptReleaseContext(hProv, 0);
	CryptDestroyHash(hHash);
	hash.clear();
	for (DWORD n = 0; n < len; n++) {
		uint8_t hi = (H[n] >> 4) & 0xF;
		uint8_t lo = (H[n] & 0xF);
		if (hi < 10) hash.push_back(hi + '0');
		else hash.push_back(hi - 10 + 'A');
		if (lo < 10) hash.push_back(lo + '0');
		else hash.push_back(lo - 10 + 'A');
	}
	return 0;
}


