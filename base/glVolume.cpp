#include"glVolume.h"
#include"lux_glerror.h"
#include"timing.h"
#include"flags.h"
#include"lux_glgeometry.h"

constexpr const char vsRayEntry[] = R"(// vertex shader: Ray Entry setup
#version 420

in layout(location=0) vec3 input_position;
out vec3 object_position;
uniform mat4 ModelViewProjection;

void main(void) {
	object_position = input_position;
	gl_Position = ModelViewProjection*vec4(input_position,1.0f);
}
)";

constexpr const char fsRayEntry[] = R"(// fragment shader: Ray Entry setup
#version 420

in vec3 object_position;
uniform vec3 object_camera;
out float out_color;

void main(void) {
	if (gl_FrontFacing) {
		out_color = length(object_position-object_camera);
	} else {
		out_color = 0.0f;
	}
}	
)";

constexpr const char vsRayExit[] = R"(// vertex shader: Ray Exit & Raymarcher
#version 420

in layout(location=0) vec3 input_position;
out vec3 object_position;
uniform mat4 ModelViewProjection;

void main(void) {
	object_position = input_position;
	gl_Position = ModelViewProjection*vec4(input_position,1.0f);
}
)";

constexpr const char fsCubeExit[] = R"(//fragment shader: Ray Exit & Cube renderer
#version 420

in vec3 object_position;
uniform vec3 object_camera;
uniform vec2 lod;
out vec4 output_color;
uniform layout(binding=0, r32f) readonly image2D entry;

float get_lod(float depth) {
	float z = max(abs(depth),1e-4f);
	float L = lod.x*z;
    return max(log(L)/log(2.0)+lod.y,0.0f);
}

void main(void) {
	vec3 dir = object_position-object_camera;
	vec2 t = vec2(imageLoad(entry,ivec2(gl_FragCoord.xy)).r,length(dir));
	dir = normalize(dir);
	// entry poing
	vec3 pos = object_camera + t.x*dir;
	
	float L = get_lod(t.x*dir.z);	
	float mod = 1.0f-pow(fract(L),32.0);

	output_color = mod*vec4(0.5*pos+vec3(0.5,0.5,0.5),1.0);
}
)";

constexpr const char fsCuberilleExit[] = R"(//fragment shader: Ray Exit & Raymarcher
#version 450
#extension GL_NV_gpu_shader5 : require

layout (std430, binding=0) restrict readonly buffer index_t { uint index[]; };
layout (std430, binding=1) restrict readonly buffer voxel_t { uint voxel[]; };
layout (std430, binding=2) restrict readonly buffer node_t { vec4 node[]; };
layout (std430, binding=3) restrict writeonly buffer debug_t { uint debug[]; };

in vec3 object_position;
uniform layout(binding=0, r32f) readonly image2D entry;

// Sampler3D for normal textures
uniform  layout(binding=1) sampler3D normal_texture;

uniform vec3 object_camera;
uniform vec3 light_dir;
uniform vec3 shading_const;

uniform vec4 background;
uniform bool enable_box;
uniform bool enable_shading;
uniform bool using_external_gradients;
uniform bool empty_space_skipping;

uniform int lao_directions;

uniform vec3 box_ll;
uniform vec3 box_ur;
uniform vec4 box_color;
uniform vec2 lod;
out vec4 output_color;

uniform float time;
out vec4 fragment;



// A single iteration of Bob Jenkins' One-At-A-Time hashing algorithm.
uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}



// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }



// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}



// Pseudo-random value in half-open range [0:1].
float random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }


float get_lod(float depth, vec2 limits) {
	float z = max(abs(depth),1e-4f);
	float L = lod.x*z;
    return clamp(log(L)/log(2.0)+lod.y,limits.x,limits.y);
}

// assumed correct
uint get_nLevels(void) { return index[0]; }

// assumed correct
uint get_nVoxelBits(uint l) { return index[1+(l<<3)]; }

// assumed correct
//uint64_t get_node_offset(uint l) { return uint64_t(index[2+(l<<3)])|(uint64_t(index[3+(l<<3)])<<uint64_t(32)); }
uint32_t get_node_offset(uint l) { return index[2+(l<<3)]; }

// assumed correct
uint64_t get_level_offset(uint l) { return uint64_t(index[4+(l<<3)])|(uint64_t(index[5+(l<<3)])<<uint64_t(32)); }

// assumed correct
uvec3 get_LevelDims(uint l) {
	return uvec3(index[6+(l<<3)],index[7+(l<<3)],index[8+(l<<3)]);
}

uint64_t linear_address(uvec4 pos) {
	uvec3 dims = get_LevelDims(pos.w);
	return get_level_offset(pos.w) + uint64_t(get_nVoxelBits(pos.w))*uint64_t(pos.x + dims.x*(pos.y + dims.y*pos.z));
}

uint read(uint64_t pos, uint bits) {
	uint32_t result = 0;
	uint32_t addr = uint32_t(pos >> uint64_t(5));
	uint32_t offs = uint32_t(pos&uint64_t(0x1f));
	uint32_t cap = 32 - offs;
	uint32_t s = 0;
	uint32_t r = min(cap, bits);
	uint32_t m = (r == cap ? 0 : (uint32_t(1) << (offs + r))) + (0xFFFFFFFF << offs);
	result |= ((voxel[addr] & m) >> offs) << s;
	s += r;
	// next element
	if (bits > r) {
		bits -= r;
		uint32_t m = (uint32_t(1) << bits) - 1;
		result |= (voxel[addr+1] & m) << s;
	}
	return result;
}

uint read_voxel_id(uvec4 pos) {
	return read(linear_address(pos),get_nVoxelBits(pos.w))+uint32_t(get_node_offset(pos.w));
}

vec4 read_color(uvec4 pos) {
	return node[read_voxel_id(pos)];
}

uint fetch_id(vec3 pos, vec2 limits) { // x,y,z, lod limits
	float depth = distance(pos,object_camera);
	float l = get_lod(depth,limits);  
    //float l = max(0.0f,lod.y);
	uint uL = uint(l);
	uvec3 voxel_dims = get_LevelDims(uL)-uvec3(1,1,1);
    ivec3 ip = clamp(ivec3(pos*voxel_dims),ivec3(0,0,0),ivec3(voxel_dims));
	uvec4 P = uvec4(uvec3(ip),uL);
	return read_voxel_id(P);
}

vec4 fetch_color(vec3 pos, vec2 limits) { // x,y,z, lod limits
	float depth = distance(pos,object_camera);
	float l = get_lod(depth,limits);  
    //float l = max(0.0f,lod.y);
	uint uL = uint(l);
	uvec3 voxel_dims = get_LevelDims(uL)-uvec3(1,1,1);
    ivec3 ip = clamp(ivec3(pos*voxel_dims),ivec3(0,0,0),ivec3(voxel_dims));
	uvec4 P = uvec4(uvec3(ip),uL);
	return read_color(P);
}

vec4 fetch_linear_color(vec3 pos, vec2 limits) { // x,y,z, lod limits
	float depth = pos.z-object_camera.z;
	float l = get_lod(depth,limits);
	uint uL = uint(l);
	uint uL1 = min(uL+1,index[0]-1);
	float w = l-float(uL);
	uvec3 voxel_dims  = get_LevelDims(uL)-uvec3(1,1,1);
	uvec3 voxel_dims1 = get_LevelDims(uL1)-uvec3(1,1,1);
	uvec4 P0 = uvec4(pos*voxel_dims+vec3(1e-4,1e-4,1e-4),uL);
	uvec4 P1 = uvec4(pos*voxel_dims1+vec3(1e-4,1e-4,1e-4),uL1);
	return mix(read_color(P0),read_color(P1),w);
}


vec2 ray_box(in vec3 dir) {
    vec3 dirty = dir + vec3(0.0000001f,0.0000001f,0.0000001f);
	vec3 t1 = (box_ll-object_camera)/dirty;
	vec3 t2 = (box_ur-object_camera)/dirty;
	vec3 tmin = min(t1,t2);
	vec3 tmax = max(t1,t2);
	float smin = max(max(tmin.x,tmin.y),tmin.z);
	float smax = min(min(tmax.x,tmax.y),tmax.z);
	return vec2(max(0.0f,smin),smax);
}


vec4 shift(in vec4 lao) {
   return vec4(1.0f, lao.x, lao.y, lao.z);
}

float forward_difference4(in vec4 a, float one_over_dh) {
 return one_over_dh * (11.0f*a.x - 18.0f*a.y + 9.0f*a.z -2.0f*a.w);
}

vec3 sample_alpha3(in vec3 pos, in vec3 dir, in float ds, in vec2 limits) {
   return  vec3(1.0f-fetch_color(pos+ds*dir,limits).a, 
                1.0f-fetch_color(pos+2.0*ds*dir,limits).a,
                1.0f-fetch_color(pos+3.0f*ds*dir,limits).a ); 
}

vec2 sample_alpha2(in vec3 pos, in vec3 dir, in float ds, in vec2 limits) {
   return  vec2(1.0f-fetch_color(pos+ds*dir,limits).a, 
                1.0f-fetch_color(pos+2.0*ds*dir,limits).a ); 
}

float forward_difference3(in vec3 a, float one_over_dh) {
 return one_over_dh * (1.0f*a.x - 4.0f*a.y + 3.0f*a.z);
}

vec3 to_rgb(uint v) {
  vec3 c;
  c.r = float(v>>16)/255.0f;
  c.g = float((v-v>>16)>>8)/255.0f;
  c.b = float(v%255)/255.0f;
  return c;
}


void main(void) {	
	vec2 limits = vec2(0.0f,float(index[0]-1));		// necessary

    float Ka = shading_const.r;
    float Kd = shading_const.g;
    float Ks = shading_const.b; 
    
	vec3 dir = object_position-object_camera;
	vec2 t = vec2(imageLoad(entry,ivec2(gl_FragCoord.xy)).r,length(dir));
	dir = normalize(dir);
    vec3 V = dir;
	vec3 L = light_dir.xyz;
    vec3 H = normalize(L+V);

	vec3 E = object_camera + t.x*dir;
  
    bool  enable_lao = (lao_directions>0);
    bool  lao_multidirectional = (lao_directions> 1);

    // Modify shading attributes
    if (enable_shading) {
    } else {
        Kd *= 0.0f;
        Ks *= 0.0f; 
    }

    float one_over_ds = 1.0f/20.0f;
    float one_over_dh = 1.0f/4.0f;

	float len = t.y-t.x;
	uint32_t step = 0;
    
    vec4 lao = vec4(1.0f,1.0f,1.0f,1.0f);
    vec4 alpha = vec4(0.0f,0.0f,0.0f,0.0f);

	vec4 accum = vec4(0.0f,0.0f,0.0f,0.0f);
   
    vec2 sminmax = ray_box(dir);   
    sminmax.x = max( sminmax.x, 0.01f);

    E = object_camera + sminmax.x * dir;
    len = sminmax.y - sminmax.x; 

	const uint32_t max_steps = 2000;
    const uint32_t min_steps = 10;
	float ds = min(len/min_steps,0.0005f);   
    float seed = random(E);
    float sjitter = ds * random(seed) * len;

 //   FIXME --> Ready for picking
 //   float picking_length = 0.01f*len;
 //   vec3 pick_pos = E + picking_length * dir + vec3(0.5f,0.5f,0.5f);
 //   output_color = to_rgb(fetch_id(pick_pos,limits));
 //   return;
   
	for (float s = sjitter; s<len && step<max_steps; s+=ds, step++) {
   
		vec3 pos = 0.5f*mix(E,object_position,s)+vec3(0.5f,0.5f,0.5f);
	    float depth = distance(pos,object_camera);
	    float l = get_lod(depth,limits);  
    	uint uL = uint(l);
	    uvec3 voxel_dims = get_LevelDims(uL)-uvec3(1,1,1);
        ivec3 ip = clamp(ivec3(pos*voxel_dims),ivec3(0,0,0),ivec3(voxel_dims));
	    uvec4 P = uvec4(uvec3(ip),uL);
	    vec4 in_color =  read_color(P);

//		vec4 in_color = fetch_color(pos,limits);
        if (empty_space_skipping) {
          if ( in_color.a < 1.0e-8 ){
		    for( uint lev  =  4;  lev > 1; lev-- ) { 
		  	  uint uL = uint(l) +  lev; 
			  uvec3 voxel_dims = get_LevelDims(uL)-uvec3(1,1,1);
			  ivec3 ip = clamp(ivec3(pos*voxel_dims),ivec3(0,0,0),ivec3(voxel_dims));
              uvec4 P = uvec4(uvec3(ip),uL);
              float aplus = read_color(P).a;
              if (aplus < 1.0e-8 ) {
                 s += ((pow(2.0,lev)-1.0) *ds);
				 //output_color = vec4( (pow(2.0,lev)-1.0)/1024.0, 0.0,0.0,1.0);
				 //return;
                 break;
              }
            }
            continue;
          } 
        }

        float ctheta_d = 0.0f;
        float ctheta_s = 0.0f;
        if( enable_shading ) {
            if ( using_external_gradients ) {
              vec3 N = vec3(1.0f,1.0f,1.0f) - 2.0f*texture(normal_texture,pos).rgb;
		      ctheta_d = 0.5f*( dot(N,L) + 1.0f); // max(0.0f, dot(N,L));
              ctheta_s = max(0.0f, dot(H,N));
             } else {
			  alpha = shift(alpha);
			  alpha.x *= (1.0f-in_color.a);
		      ctheta_d = 0.5f * ( forward_difference4(alpha, one_over_ds) + 1.0f);
              ctheta_s = ctheta_d;    
             }
           // Fixme --> parameterize specular exponent
           ctheta_s *= ctheta_s;
           ctheta_s *= ctheta_s;
       }
       float lao_accum = lao.w;
       if (enable_lao) {
           lao  = shift(lao);
           lao *= (1.0f - in_color.a);
           float one_over_direction = 1.0f / float(lao_directions);

           if (lao_multidirectional) {
              float seed = random(pos);
              float theta = 1.507075f * random(seed);
              float phi  =  3.1415f * random(theta);
              for (uint i = 1; i < lao_directions; ++i) {
                 vec3 S = vec3( sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));
                 theta = 1.507075f * random(phi);
                 phi  =  3.1415f * random(theta);
 				// weight like cos^2 ??
                float w_s = dot(S,V); 
                if( w_s < 0.0f ){
                  S *= -1.0f;  
                } 
                lao_accum += (1.0f - fetch_color(pos + ds*S ,limits).a);
              }
           }
           lao_accum *= one_over_direction;
	    }

		vec3 color_shaded = in_color.rgb*(Ka*lao_accum + Kd*ctheta_d) + Ks*ctheta_s*vec3(1.0,1.0,1.0);  
	    
		// compact form of "Under" operator
		accum += (1.0f-accum.a)*in_color.a*vec4(color_shaded,1.0f);
		if (accum.a>0.99f) break;
	}
	// assuming background to be opaque
	accum += (1.0f-accum.a)*vec4(background.rgb,1.0f);
	output_color = accum;
}
)";

constexpr const char m_compute_shader[] = R"(// Compute Shader: color LUT update 
#version 450 core
#extension GL_NV_gpu_shader5 : require

layout (local_size_x=32) in;
layout (std430, binding=0) restrict readonly buffer node_list_t { uint node[]; };
layout (std430, binding=1) buffer lut_t { vec4 lut[]; };

layout (location=0) uniform uvec2 range;
layout (location=1) uniform uint nLeafs;

void main(void) {
	uint dst = gl_GlobalInvocationID.x+range.x;
	if (dst>=range.y) return;
	uint src = 3*(dst-nLeafs);
	float w = float(node[src+2])/(float(1<<20));
	//lut[dst] = mix(lut[node[src]],lut[node[src+1]],w);
	lut[dst].a = mix(lut[node[src]].a,lut[node[src+1]].a,w);
    if ( lut[dst].a > 0.0f ) {
      lut[dst].rgb = mix(lut[node[src]].rgb*lut[node[src]].a,lut[node[src+1]].rgb*lut[node[src+1]].a,w)/lut[dst].a;
    } else  {
      lut[dst].rgb = vec3(0.0f,0.0f,0.0f);
    }
}
)";

glVolume::glVolume(compressed_mipvolume& V, normal_volume* N) : 
	m_pVolume(nullptr), m_nVolume(nullptr),
	m_hNodeList(0), m_hColorLUT(0), m_LUTsize(0), m_hVoxelData(0), m_hIndexData(0), m_hFBO(0), m_hTex(0), m_hNormalTex(0),
	m_hCompute(nullptr),m_hRayEntry(nullptr),m_hCuberille(nullptr),m_hFancyCuberille(nullptr),m_hDVR(nullptr),
	m_background(1.0f, 1.0f, 1.0f, 1.0f), m_enable_box(false), m_lao_directions(0), m_light_dir(0.0f, 0.0f, -1.0f),  m_enable_shading(false), m_box_ll(-1.0f, -1.0f, -1.0f), m_box_ur(1.0f, 1.0f, 1.0f), m_box_color(1.0f, 0.0f, 0.0f, 1.0f),
	m_lod_scale(1.0f), m_lod_bias(0.0f), m_empty_space_skipping(true)
{
	attach(V,N);
	m_view_dims[0] = m_view_dims[1] = 0;
}

glVolume::glVolume(void) : 
	m_pVolume(nullptr), m_nVolume(nullptr),
	m_hNodeList(0), m_hColorLUT(0), m_LUTsize(0), m_hVoxelData(0), m_hIndexData(0), m_hFBO(0), m_hTex(0), m_hNormalTex(0),
	m_hCompute(nullptr), m_hRayEntry(nullptr), m_hCuberille(nullptr),m_hFancyCuberille(nullptr),m_hDVR(nullptr),
	m_background(1.0f,1.0f,1.0f,1.0f), m_enable_box(false), m_lao_directions(0), m_light_dir(0.0f, 0.0f, -1.0f), m_enable_shading(false), m_box_ll(-1.0f,-1.0f,-1.0f),m_box_ur(1.0f,1.0f,1.0f),m_box_color(1.0f,0.0f,0.0f,1.0f),
	m_lod_scale(1.0f), m_lod_bias(0.0f), m_empty_space_skipping(true)
{
	m_view_dims[0] = m_view_dims[1] = 0;
}

glVolume::~glVolume(void) {
	clear();
}

void glVolume::clear(void) {
	detach();
	gpu_delete_programs();
}

void glVolume::pack(bool packing) {


	m_pack.clear();			// added
	m_unpack.clear();		// added
	m_unpack.resize(m_pVolume->get_PackingSize());	// added
	for (size_t n = 0; n < m_unpack.size(); n++) {
		if (packing)
		{
			m_pack[n] = m_pVolume->get_PackingEntry(n);
			m_unpack[m_pack[n]] = uint16_t(n);
		}
		else {
			m_pack[n] = n;
			m_unpack[n] = n;
		}
	}


}

void glVolume::attach(compressed_mipvolume& V, normal_volume* N) {

	detach();
	m_pVolume = &V;
	if (N != nullptr) {
		m_nVolume = N;
		init_normal_texture();
	}

	pack(true);

	compile_topo_ranges();
	compile_node_list();
	compile_codebook();
	m_leaf_colors.resize(m_pVolume->get_nLeafs());
	gpu_compile_programs();
}

void glVolume::detach(void) {
	m_pVolume = nullptr;
	m_nVolume = nullptr;
	if (m_hNodeList != 0) glDeleteBuffers(1, &m_hNodeList);			m_hNodeList = 0;
	if (m_hColorLUT != 0) glDeleteBuffers(1, &m_hColorLUT);			m_hColorLUT = 0;
	if (m_hVoxelData != 0) glDeleteBuffers(1, &m_hVoxelData);		m_hVoxelData = 0;
	if (m_hIndexData != 0) glDeleteBuffers(1, &m_hIndexData);		m_hIndexData = 0;
	m_topo_order.clear();
	m_node_list.clear();
	m_codebook.clear();
	m_color_lut.clear();
	m_view_dims[0] = m_view_dims[1] = 0;
	m_pack.clear();			// added
	m_unpack.clear();		// added
}


void glVolume::init_normal_texture() {

	std::vector<uint8_t> data;
	m_nVolume->as_RGB8(data);

	glEnable(GL_TEXTURE_3D);
	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &m_hNormalTex);
	glBindTexture(GL_TEXTURE_3D, m_hNormalTex);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	//glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB8, m_nVolume->dimension(0), m_nVolume->dimension(1), m_nVolume->dimension(2), 0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);
	glGenerateMipmap(GL_TEXTURE_3D);

	glBindTexture(GL_TEXTURE_3D, 0);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_3D);

}



bool glVolume::empty(void) const {
	return m_pVolume == nullptr;
}

compressed_mipvolume* glVolume::mipvolume(void) {
	return m_pVolume;
}

const std::vector<lux::rangeu32_t>& glVolume::order(void) const {
	return m_topo_order;
}

std::vector<lux::rangeu32_t>& glVolume::order(void) {
	return m_topo_order;
}

void glVolume::compile_topo_ranges(void) {
	m_topo_order.clear();
	if (m_pVolume == nullptr) return;
	size_t nLeafs = m_pVolume->get_nLeafs();
	size_t nLerps = m_pVolume->get_nLerpNodes();
	if (nLeafs + nLerps == 0) return;
	std::vector<uint32_t> order(nLeafs + nLerps, std::numeric_limits<uint32_t>::max());
	for (size_t n = 0; n < nLeafs; n++) order[n] = 0;
	for (uint32_t n = 0; n < nLerps; n++) {
		uint64_t ch0, ch1;
		uint32_t idx;
		m_pVolume->get_Node(n, ch0, ch1, idx);
		order[n + nLeafs] = std::max(order[ch0], order[ch1]) + 1;
	}
	m_topo_order.push_back(lux::rangeu32_t(0u, uint32_t(nLeafs)));
	uint32_t pos = uint32_t(nLeafs);
	uint32_t p0 = uint32_t(nLeafs);
	uint32_t level = 1;
	while (pos < order.size()) {
		if (order[pos] > level) {
			m_topo_order.push_back(lux::rangeu32_t(p0, pos));
			p0 = pos;
			level++;
		}
		else pos++;
	}
	m_topo_order.push_back(lux::rangeu32_t(p0, uint32_t(order.size())));
}

const std::vector<uint32_t>& glVolume::node_list(void) const {
	return m_node_list;
}

std::vector<uint32_t>& glVolume::node_list(void) {
	return m_node_list;
}

void glVolume::compile_node_list(void) {
	m_node_list.clear();
	if (m_pVolume == nullptr) return;
	size_t nLerps = m_pVolume->get_nLerpNodes();
	m_node_list.resize(3 * nLerps);
#pragma omp parallel for schedule(dynamic,32)
	for (int n = 0; n<int(nLerps); n++) {
		uint64_t ch0, ch1;
		uint32_t idx;
		m_pVolume->get_Node(n, ch0, ch1, idx);
		m_node_list[3 * n] = uint32_t(ch0);
		m_node_list[3 * n + 1] = uint32_t(ch1);
		m_node_list[3 * n + 2] = uint32_t(m_pVolume->get_Code(idx)*float(1 << 20));
	}
}

const std::vector<float>& glVolume::codebook(void) const {
	return m_codebook;
}

std::vector<float>& glVolume::codebook(void) {
	return m_codebook;
}

void glVolume::compile_codebook(void) {
	m_codebook.clear();
	if (m_pVolume == nullptr) return;
	size_t nCodes = (m_pVolume->get_CodeSize() >> 1) / 20;
	m_codebook.resize(2 * nCodes);
#pragma omp parallel for schedule(dynamic,4)
	for (int n = 0; n<int(nCodes); n++) {
		m_codebook[2 * n] = m_pVolume->get_Code(n);
		m_codebook[2 * n + 1] = m_pVolume->get_CodeVar(n);
	}
}

bool glVolume::cpu_update_color_lut(void) {
	m_color_lut.clear();
	if (m_pVolume == nullptr) return false;
	size_t nLeafs = m_pVolume->get_nLeafs();
	size_t nLerps = m_pVolume->get_nLerpNodes();
	m_color_lut.resize(nLeafs + nLerps);
#ifdef BENCHMARK
	const size_t nRuns = 32;
	CTimer ct;
	for (size_t bench = 0; bench < nRuns; bench++) {
#endif
#pragma omp parallel for schedule(dynamic,2)
		for (int n = 0; n<int(nLeafs); n++) m_color_lut[n] = m_leaf_colors[n];
		if (m_topo_order.empty()) {
			for (int n = 0; n < int(nLerps); n++) {
				uint32_t ch0, ch1;
				float weight;
				if (n + nLeafs == 416025) {
					printf("\n");
				}
				read_node(n, ch0, ch1, weight);
				m_color_lut[n] = lerp(m_color_lut[ch0], m_color_lut[ch1], weight);
			}
		}
		else {
			for (size_t l = 1; l < m_topo_order.size(); l++) {
#pragma omp parallel for schedule(dynamic,4)
				for (int k = int(m_topo_order[l].vmin); k<int(m_topo_order[l].vmax); k++) {
					uint32_t ch0, ch1;
					float w;
					if (k == 416025) {
						printf("\n");
					}
					read_node(k - int(nLeafs), ch0, ch1, w);
					m_color_lut[k] = lerp(m_color_lut[ch0], m_color_lut[ch1], w);
				}
			}
		}
#ifdef BENCHMARK
	}
	double dT = ct.Query()*1000.0 / double(nRuns);
	printf("cpu_update_color_lut took %.6fms\n", dT);
	printf("cpu_update_color_lut %zi leafs, %zi nodes\n", nLeafs, nLeafs + nLerps);
#endif
	return true;
}

const std::vector<lux::color>& glVolume::cpu_color_lut(void) const {
	return m_color_lut;
}

std::vector<lux::color>& glVolume::cpu_color_lut(void) {
	return m_color_lut;
}

bool glVolume::upload_gpu(void) {
	if (m_pVolume == nullptr) return false;

	if (m_node_list.empty()) return false;
	if (m_hNodeList != 0) glDeleteBuffers(1, &m_hNodeList);
	glCreateBuffers(1, &m_hNodeList);
	glNamedBufferData(m_hNodeList, m_node_list.size() * sizeof(uint32_t), m_node_list.data(), GL_STATIC_READ);

	if (m_hColorLUT != 0) glDeleteBuffers(1, &m_hColorLUT);
	glCreateBuffers(1, &m_hColorLUT);
	m_LUTsize = GLsizei(4 * sizeof(float)*(m_pVolume->get_nLeafs() + m_pVolume->get_nLerpNodes()));
	glNamedBufferData(m_hColorLUT, m_LUTsize, nullptr, GL_STREAM_COPY);
	
	gpu_compile_programs();

	// UPLOAD VOXEL DATA
	if (m_hVoxelData != 0) glDeleteBuffers(1, &m_hVoxelData);
	size_t size = (m_pVolume->get_VoxelSize() + 31) >> size_t(5); // size in uint32_t
	size_t offs = (m_pVolume->voxel_offset(0) + 31) >> size_t(5); // offset in uint32_t, should be an integer number
	glCreateBuffers(1, &m_hVoxelData);
	glGetError();
	glNamedBufferData(m_hVoxelData, size * sizeof(uint32_t), m_pVolume->stream().data() + offs, GL_STATIC_DRAW);
	GLuint err = glGetError();
	if (err != GL_NO_ERROR) printf("glVolume::upload_gpu -- error uploading voxel data: %s\n", lux::errstr(err).c_str());
	else printf("glVolume::upload_gpu success: %.2fMB, offset %zi\n", double(size * sizeof(uint32_t) / double(1 << 20)),offs);
	
	// UPLOAD INDEX DATA
	if (m_hIndexData != 0) glDeleteBuffers(1, &m_hIndexData);
	std::vector<uint32_t> index;
	printf("glVolume::upload_gpu: building index data\n");
	index.push_back(m_pVolume->get_nLevels());	
	printf("  000   .nlevels             %i\n", index[0]);
	uint32_t dim[3] = { m_pVolume->get_Dimension(0),m_pVolume->get_Dimension(1),m_pVolume->get_Dimension(2) };
	for (size_t l = 0; l < index[0]; l++) {
		index.push_back(m_pVolume->get_nVoxelBits(uint32_t(l)));
		printf("  %.3zi+1 .level%zi.voxelbits    %i [bits]\n", index.size() - 1, l, index.back());
		uint64_t node_offs = m_pVolume->get_NodeOffset(uint32_t(l));
		index.push_back(uint32_t(node_offs)); // lo
		index.push_back(uint32_t(node_offs >> uint64_t(32))); // hi
		printf("  %.3zi+2 .level%zi.node_offset  %I64d [bits]\n", index.size() - 2, l, node_offs);
		uint64_t level_offs = m_pVolume->voxel_offset(l) - m_pVolume->voxel_offset(0);
		index.push_back(uint32_t(level_offs)); // lo
		index.push_back(uint32_t(level_offs >> uint64_t(32))); // hi
		printf("  %.3zi+2 .level%zi.level_offset %I64d [bits]\n", index.size() - 2, l, level_offs);
		for (int i = 0; i < 3; i++) {
			index.push_back(dim[i]);
			dim[i] = (dim[i] + 1) >> 1;
		}
		printf("  %.3zi+3 .level%zi.dims         %i %i %i [voxels]\n", index.size() - 3, l, index[index.size() - 3], index[index.size() - 2], index[index.size() - 1]);
	}
	printf("  %.3zi+0 .eof\n", index.size());

	glGetError();
	glCreateBuffers(1, &m_hIndexData);
	glNamedBufferData(m_hIndexData, index.size() * sizeof(uint32_t), index.data(), GL_STATIC_DRAW);
	
	if (m_hDebugBuffer != 0) glDeleteBuffers(1, &m_hDebugBuffer);
	glCreateBuffers(1, &m_hDebugBuffer);
	glNamedBufferData(m_hDebugBuffer, 16384 * sizeof(uint32_t), nullptr, GL_STREAM_COPY);
	err = glGetError();
	if (err != GL_NO_ERROR) {
		printf("Error creating buffers '%s'\n", lux::errstr(err).c_str());
		exit(0);
	}

	return true;
}

bool glVolume::gpu_update_color_lut(void) {
	static bool init = false;

	if (m_pVolume == nullptr) return false;
	if (m_topo_order.empty()) return false;
	if (m_hCompute == nullptr) {
		if (!gpu_compile_programs()) return false;
		init = false;
	}
	
	static GLuint u_nRange = 0;
	static GLuint u_nLeafs = 1;
	
	GLuint hProgram = *m_hCompute;
	if (!init) {
		u_nLeafs = glGetUniformLocation(hProgram, "nLeafs");
		u_nRange = glGetUniformLocation(hProgram, "range");
		init = true;
	}

	// Bind buffer bases, trigger compute shader
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_hNodeList);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_hColorLUT);
	glUseProgram(hProgram);
	glProgramUniform1ui(hProgram, u_nLeafs, uint32_t(m_leaf_colors.size()));
#ifdef BENCHMARK
	GLuint timer;
	glGenQueries(1, &timer);
	glBeginQuery(GL_TIME_ELAPSED, timer);
#endif
		glNamedBufferSubData(m_hColorLUT, 0, m_leaf_colors.size() * sizeof(float) * 4, m_leaf_colors.data());
		for (size_t l = 1; l < m_topo_order.size(); l++) {
			uint32_t size = m_topo_order[l].vmax - m_topo_order[l].vmin;
			glProgramUniform2ui(hProgram, u_nRange, m_topo_order[l].vmin, m_topo_order[l].vmax);
			glDispatchCompute((size + 31) >> 5, 1, 1);
			glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		}
#ifdef BENCHMARK
	glEndQuery(GL_TIME_ELAPSED);
	glUseProgram(0);
#endif

#ifdef VALIDATION
	// VALIDATION
	cpu_update_color_lut();
	std::vector<lux::color> clr(m_pVolume->get_nLeafs() + m_pVolume->get_nLerpNodes());
	glGetNamedBufferSubData(m_hColorLUT, 0, clr.size() * sizeof(lux::color), clr.data());
	auto diff = [](const lux::color& a, const lux::color& b) {
		float d[4] = { a(0) - b(0),a(1) - b(1),a(2) - b(2),a(3) - b(3) };
		return d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + d[3] * d[3];
	};
	std::vector<float> D(m_color_lut.size());
	double Dsum = 0.0;
	for (size_t n = 0; n < clr.size(); n++) {
		D[n]= diff(clr[n], m_color_lut[n]);
		Dsum += D[n];
	}
	printf("gpu_update_color_lut verification %g, ref 0.0\n", Dsum);
#endif
#ifdef BENCHMARK
	GLuint64 dt;
	glGetQueryObjectui64v(timer, GL_QUERY_RESULT, &dt);
	double DT = double(dt) / 1e6; // results in nanoseconds, 10^6 ms each
	printf("gpu_update_color_lut %.6fms\n", DT);
	printf("gpu_update_color leafs %zi, nodes %i\n", nLeafs(), m_topo_order.back().vmax);
#endif
	return true;
}

void glVolume::reshape(uint32_t width, uint32_t height) {
	if (width != m_view_dims[0] || height != m_view_dims[1]) {
		// reshape framebuffer
		if (m_hFBO==0) {
			glGenFramebuffers(1, &m_hFBO);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_hFBO);
		}
		else {
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_hFBO);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 0, 0);
		}
		if (m_hTex != 0) glDeleteTextures(1, &m_hTex);
		glGetError();
		glGenTextures(1, &m_hTex);
		glBindTexture(GL_TEXTURE_2D, m_hTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, nullptr);
		glBindTexture(GL_TEXTURE_2D, 0);
		GLuint err = glGetError();
		if (err != GL_NO_ERROR) {
			printf("glVolume::reshape -- error '%s' while resizing FBO to %i x %i\n", lux::errstr(err).c_str(), width, height);
			exit(0);
		}
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_hTex, 0);
		GLenum res = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (res != GL_FRAMEBUFFER_COMPLETE) {
			printf("glVolume::reshape -- error '%s' while resizing FBO to %i x %i\n", lux::errstr(res).c_str(), width, height);
			exit(0);
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	m_view_dims[0] = width;
	m_view_dims[1] = height;
}




bool glVolume::gpu_delete_programs(void) {
	if (m_hCube)			delete m_hCube;				m_hCube = nullptr;
	if (m_hCompute)			delete m_hCompute;			m_hCompute = nullptr;
	if (m_hRayEntry)		delete m_hRayEntry;			m_hRayEntry = nullptr;
	if (m_hCuberille)		delete m_hCuberille;		m_hCuberille = nullptr;
	if (m_hFancyCuberille)	delete m_hFancyCuberille;	m_hFancyCuberille = nullptr;
	if (m_hDVR)				delete m_hDVR;				m_hDVR = nullptr;
	return true;
}

bool glVolume::gpu_compile_programs(bool force) {
	if (force) gpu_delete_programs();

	if (m_hCompute==nullptr) {
		lux::shader compute(GL_COMPUTE_SHADER, m_compute_shader, lux::SOURCE_TEXT);
		if (compute == 0) goto FAIL;
		m_hCompute = new lux::program("compute shader lut update");
		m_hCompute->attach(compute);
		if (!m_hCompute->link()) goto FAIL;
	}

	if (m_hRayEntry==nullptr) {
		lux::shader vert(GL_VERTEX_SHADER, vsRayEntry, lux::SOURCE_TEXT);
		if (vert == 0) goto FAIL;
		lux::shader frag(GL_FRAGMENT_SHADER, fsRayEntry, lux::SOURCE_TEXT);
		if (frag == 0) goto FAIL;
		m_hRayEntry = new lux::program("RayEntry");
		m_hRayEntry->attach(vert);
		m_hRayEntry->attach(frag);
		if (!m_hRayEntry->link()) goto FAIL;
	}

	{ // block shares vert 
		lux::shader vert(GL_VERTEX_SHADER, vsRayExit, lux::SOURCE_TEXT);
		if (vert == 0) goto FAIL;

		if (!m_hCube) {
			lux::shader frag(GL_FRAGMENT_SHADER, fsCubeExit, lux::SOURCE_TEXT);
			if (frag == 0) goto FAIL;
			m_hCube = new lux::program("RayCube");
			m_hCube->attach(vert);
			m_hCube->attach(frag);
			if (!m_hCube->link()) goto FAIL;
		}

		if (!m_hCuberille) {
			lux::shader frag(GL_FRAGMENT_SHADER, fsCuberilleExit, lux::SOURCE_TEXT);
			if (frag == 0) goto FAIL;
			m_hCuberille = new lux::program("Cuberille");
			m_hCuberille->attach(vert);
			m_hCuberille->attach(frag);
			if (!m_hCuberille->link()) goto FAIL;
		}

		if (!m_hFancyCuberille) {

		}

		if (!m_hDVR) {

		}
	}
	return true;

FAIL:
	gpu_delete_programs();
	exit(0);
	return false;
}

bool glVolume::draw(const shader_t& s, const lux::mat4f& projection, const lux::mat4f& modelview) {
	switch (s) {
	case SHADER_COMPUTE:	// WORKING
		return gpu_update_color_lut();
	case SHADER_CUBE:		// WORKING
		return draw_cube(projection, modelview);
	case SHADER_CUBERILLE:
		return draw_cuberille(projection,modelview);
	case SHADER_FANCY_CUBERILLE:
		return draw_base(projection, modelview,m_hFancyCuberille);
	case SHADER_DVR:
		return draw_base(projection, modelview,m_hDVR);
	default: 
		return false;
	}
	return true;
}

bool glVolume::draw_cube(const lux::mat4f& projection, const lux::mat4f& modelview) {
	static bool init = false;

	if (m_pVolume == nullptr) return false;
	if (m_topo_order.empty()) return false;
	if (m_hRayEntry == nullptr || m_hCube == nullptr) {
		if (!gpu_compile_programs()) return false;
		init = false;
	}

	static GLuint u_ModelViewProjection_entry = -1;
	static GLuint u_object_camera_entry = -1;
	
	static GLuint u_ModelViewProjection_exit = -1;
	static GLuint u_object_camera_exit = -1;
	static GLuint u_background = -1;
	static GLuint u_lod = -1;


	static GLuint INVALID = GLuint(-1);

	GLuint hEntry = *m_hRayEntry;
	GLuint hExit = *m_hCube;
	if (!init) {
		u_ModelViewProjection_entry = glGetUniformLocation(hEntry, "ModelViewProjection");
		u_object_camera_entry = glGetUniformLocation(hEntry, "object_camera");
		u_ModelViewProjection_exit = glGetUniformLocation(hExit, "ModelViewProjection");
		u_object_camera_exit = glGetUniformLocation(hExit, "object_camera");
		u_background = glGetUniformLocation(hExit, "background");
		u_lod = glGetUniformLocation(hExit, "lod");
		init = true;
	}
	
	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);

	float lod[2] = { m_lod_scale,m_lod_bias };
	lux::mat4f MVP = projection*modelview;
	lux::vec4f campos = modelview.inverse().col(3);
	if (u_ModelViewProjection_entry != INVALID) glProgramUniformMatrix4fv(hEntry, u_ModelViewProjection_entry, 1, false, MVP);
	if (u_object_camera_entry != INVALID) glProgramUniform3fv(hEntry, u_object_camera_entry, 1, campos);
	glBindFramebuffer(GL_FRAMEBUFFER, m_hFBO);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glCullFace(GL_BACK);
	lux::static_cube::draw(hEntry);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glBindImageTexture(0, m_hTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);

	//glBindImageTexture(1, m_hNormalTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8UI);


	if (u_ModelViewProjection_exit != INVALID) glProgramUniformMatrix4fv(hExit, u_ModelViewProjection_exit, 1, false, MVP);
	if (u_object_camera_exit != INVALID) glProgramUniform3fv(hExit, u_object_camera_exit, 1, campos);
	if (u_background != INVALID) glProgramUniform4fv(hExit, u_background, 1, m_background);
	if (u_lod != INVALID) glProgramUniform2fv(hExit, u_lod, 1, lod);
	glCullFace(GL_FRONT);
	lux::static_cube::draw(hExit);
	return true;
}

#include<unordered_set>
#include<deque>

bool glVolume::draw_cuberille(const lux::mat4f& projection, const lux::mat4f& modelview) {
	static bool init = false;

	if (m_pVolume == nullptr) return false;
	if (m_topo_order.empty()) return false;
	if (m_hRayEntry == nullptr || m_hCuberille == nullptr) {
		if (!gpu_compile_programs()) return false;
		init = false;
	}
	
	static GLuint u_ModelViewProjection_entry = -1;
	static GLuint u_object_camera_entry = -1;

	static GLuint u_ModelViewProjection_exit = -1;
	static GLuint u_object_camera_exit = -1;
	static GLuint u_background = -1;
	static GLuint u_lao_directions = -1;
	static GLuint u_light_dir = -1;
	static GLuint u_shading_const = -1;

	static GLuint u_box_ll = -1;
	static GLuint u_box_ur = -1;

	static GLuint u_normal_texture = -1;


	static GLuint u_enable_shading = -1;
	static GLuint u_using_external_gradients = -1;
	static GLuint u_empty_space_skipping = -1;


	static GLuint u_enable_box = -1;
	static GLuint u_lod = -1;

	static GLuint INVALID = GLuint(-1);

	GLuint hEntry = *m_hRayEntry;
	GLuint hExit = *m_hCuberille;
	if (!init) {
		u_ModelViewProjection_entry = glGetUniformLocation(hEntry, "ModelViewProjection");
		u_object_camera_entry = glGetUniformLocation(hEntry, "object_camera");
		u_ModelViewProjection_exit = glGetUniformLocation(hExit, "ModelViewProjection");
		u_object_camera_exit = glGetUniformLocation(hExit, "object_camera");
		u_background = glGetUniformLocation(hExit, "background");
		u_lod = glGetUniformLocation(hExit, "lod");
		u_lao_directions = glGetUniformLocation(hExit, "lao_directions");
		u_shading_const = glGetUniformLocation(hExit, "shading_const");
		u_light_dir = glGetUniformLocation(hExit, "light_dir");
		u_box_ll = glGetUniformLocation(hExit, "box_ll");
		u_box_ur = glGetUniformLocation(hExit, "box_ur");


		u_enable_shading = glGetUniformLocation(hExit, "enable_shading");
		u_normal_texture = glGetUniformLocation(hExit, "normal_texture");
		u_using_external_gradients = glGetUniformLocation(hExit, "using_external_gradients");
		u_empty_space_skipping = glGetUniformLocation(hExit, "empty_space_skipping");

		u_enable_box = glGetUniformLocation(hExit, "enable_box");
		init = true;
	}

	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_hIndexData);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_hVoxelData);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, m_hColorLUT);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, m_hDebugBuffer);

	lux::mat4f MVP = projection*modelview;
	lux::vec4f campos = modelview.inverse().col(3);

	if (u_ModelViewProjection_entry != INVALID) glProgramUniformMatrix4fv(hEntry, u_ModelViewProjection_entry, 1, false, MVP);
	if (u_object_camera_entry != INVALID) glProgramUniform3fv(hEntry, u_object_camera_entry, 1, campos);
	glBindFramebuffer(GL_FRAMEBUFFER, m_hFBO);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glCullFace(GL_BACK);
	lux::static_cube::draw(hEntry);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glBindImageTexture(0, m_hTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);

	if (u_normal_texture != INVALID) {
		glEnable(GL_TEXTURE_3D);
		glActiveTexture(GL_TEXTURE1);
		glUniform1i(u_normal_texture, 1);
		//glBindTextureUnit(1, m_hNormalTex);
		glBindTexture(GL_TEXTURE_3D, m_hNormalTex);
		//glProgramUniform1i(hExit, u_normal_texture, 1);
	}

	if (u_ModelViewProjection_exit != INVALID) glProgramUniformMatrix4fv(hExit, u_ModelViewProjection_exit, 1, false, MVP);
	if (u_object_camera_exit != INVALID) glProgramUniform3fv(hExit, u_object_camera_exit, 1, campos);
	if (u_background != INVALID) glProgramUniform4fv(hExit, u_background, 1, m_background);
	if (u_enable_box != INVALID) glProgramUniform1i(hExit, u_enable_box, m_enable_box);
	if (u_lao_directions != INVALID) glProgramUniform1i(hExit, u_lao_directions, m_lao_directions);
	if (u_light_dir != INVALID) glProgramUniform3fv(hExit, u_light_dir, 1, m_light_dir);
	if (u_using_external_gradients != INVALID) glProgramUniform1i(hExit, u_using_external_gradients, (m_nVolume != nullptr));
	if (u_empty_space_skipping != INVALID) glProgramUniform1i(hExit, u_empty_space_skipping, m_empty_space_skipping && (m_nVolume != nullptr));

	if (u_box_ll != INVALID) glProgramUniform3fv(hExit, u_box_ll, 1, m_box_ll);
	if (u_box_ur != INVALID) glProgramUniform3fv(hExit, u_box_ur, 1, m_box_ur);

	if (u_shading_const != INVALID) glProgramUniform3fv(hExit, u_shading_const , 1, m_shading_const);

	if (u_enable_shading != INVALID) glProgramUniform1i(hExit, u_enable_shading, m_enable_shading);


	float lod[2] = { m_lod_scale,m_lod_bias };
	if (u_lod != INVALID) glProgramUniform2fv(hExit, u_lod, 1, lod);

	glCullFace(GL_FRONT);
	lux::static_cube::draw(hExit);
	/*
	std::vector<uint32_t> buf(16384);
	glGetNamedBufferSubData(m_hDebugBuffer, 0, buf.size() * sizeof(uint32_t), buf.data());
	cpu_update_color_lut();
	lux::color mpf;
	uint32_t id = m_pVolume->get_Voxel(0, 0, 0, 8);
	glGetNamedBufferSubData(m_hColorLUT, id*4*sizeof(float), sizeof(lux::color), &mpf);

	printf("000: %i [0x%.8X] :: %i\n", buf[0], buf[0], m_pVolume->get_Voxel(0, 0, 0, 8));
	lux::color clr = cpu_color_lut()[id];
	printf("001: %i [0x%.8X] :: %i :: %i\n", buf[1], buf[1], uint32_t(clr(0)*255),uint32_t(mpf(0)*255.0f));
	printf("002: %i [0x%.8X] :: %i :: %i\n", buf[2], buf[2], uint32_t(clr(1) * 255),uint32_t(mpf(1)*255.0f));
	printf("002: %i [0x%.8X] :: %i :: %i\n", buf[3], buf[3], uint32_t(clr(2) * 255), uint32_t(mpf(2)*255.0f));
	printf("003: %i [0x%.8X] :: %i :: %i\n", buf[4], buf[4], uint32_t(clr(3) * 255), uint32_t(mpf(3)*255.0f));
	printf("\n");

	std::unordered_set<uint64_t> visited;
	std::deque<uint64_t> todo;
	todo.push_back(id);
	while (!todo.empty()) {
		uint32_t id = todo.front();
		todo.pop_front();
		if (id < m_pVolume->get_nLeafs()) {
			printf("id = %i, leaf : %f %f %f %f\n", id, cpu_color_lut()[id](0), cpu_color_lut()[id](1), cpu_color_lut()[id](2), cpu_color_lut()[id](3));
		}
		else {
			uint64_t ch0, ch1;
			uint32_t idx;
			m_pVolume->get_Node(id - m_pVolume->get_nLeafs(), ch0, ch1, idx);
			printf("id = %i, ch0=%i, ch1=%i, weight=%f: %f %f %f\n", id, ch0, ch1, m_pVolume->get_Code(idx), cpu_color_lut()[id](0), cpu_color_lut()[id](1), cpu_color_lut()[id](2), cpu_color_lut()[id](3));
			if (visited.find(ch0) == visited.end()) {
				visited.insert(ch0);
				todo.push_back(ch0);
			}
			if (visited.find(ch1) == visited.end()) {
				visited.insert(ch1);
				todo.push_back(ch1);
			}
		}
	}
	*/
	if (u_normal_texture != INVALID) {
		glBindTexture(GL_TEXTURE_3D, 0);
		glDisable(GL_TEXTURE_3D);
		glActiveTexture(GL_TEXTURE0);
	}
	return true;
}

bool glVolume::draw_base(const lux::mat4f& projection, const lux::mat4f& modelview, lux::program* EXIT) {
	static bool init = false;

	if (m_pVolume == nullptr) return false;
	if (m_topo_order.empty()) return false;
	if (m_hRayEntry == nullptr || EXIT == nullptr) {
		if (!gpu_compile_programs()) return false;
		init = false;
	}
	if (EXIT == nullptr) return false;

	static GLuint u_ModelViewProjection_entry = 0;
	static GLuint u_object_camera_entry = 0;

	static GLuint INVALID = GLuint(-1);

	GLuint hEntry = *m_hRayEntry;
	GLuint hExit = *EXIT;
	if (!init) {
		u_ModelViewProjection_entry = glGetUniformLocation(hEntry, "ModelViewProjection");
		u_object_camera_entry = glGetUniformLocation(hEntry, "object_camera");
		init = true;
	}
	GLuint u_ModelViewProjection_exit = glGetUniformLocation(hExit, "ModelViewProjection");
	GLuint u_object_camera_exit = glGetUniformLocation(hExit, "object_camera");
	GLuint u_box_ll = glGetUniformLocation(hExit, "box_ll");
	GLuint u_box_ur = glGetUniformLocation(hExit, "box_ur");
	GLuint u_enable_box = glGetUniformLocation(hExit, "enable_box");

	GLuint u_background = glGetUniformLocation(hExit, "background");
	GLuint u_boxcolor = glGetUniformLocation(hExit, "boxcolor");


	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);

	lux::mat4f MVP = projection*modelview;
	lux::vec4f campos = modelview.inverse().col(3);
	if (u_ModelViewProjection_entry != INVALID) glProgramUniformMatrix4fv(hEntry, u_ModelViewProjection_entry, 1, false, MVP);
	if (u_object_camera_entry != INVALID) glProgramUniform3fv(hEntry, u_object_camera_entry, 1, campos);
	glBindFramebuffer(GL_FRAMEBUFFER, m_hFBO);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glCullFace(GL_BACK);
	lux::static_cube::draw(hEntry);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glBindImageTexture(0, m_hTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32F);
	//glBindImageTexture(0, m_hNormalTex, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8UI);


	//if (renderstate.enable_box) {
	//	lux::vec4f box_ll(renderstate.box[0].vmin, renderstate.box[1].vmin, renderstate.box[2].vmin, 0);
	//	lux::vec4f box_ur(renderstate.box[0].vmax, renderstate.box[1].vmax, renderstate.box[2].vmax, 0);
	//	renderstate.program[SHADER_RAY_EXIT]->set_uniform("box_ll", box_ll);
	//	renderstate.program[SHADER_RAY_EXIT]->set_uniform("box_ur", box_ur);
	//}

	if (u_enable_box != INVALID) glProgramUniform1i(hExit, u_enable_box, false);
	if (u_ModelViewProjection_exit != INVALID) glProgramUniformMatrix4fv(hExit, u_ModelViewProjection_exit, 1, false, MVP);
	if (u_object_camera_exit != INVALID) glProgramUniform3fv(hExit, u_object_camera_exit, 1, campos);
	if (u_background != INVALID) glProgramUniform4fv(hExit, u_background, 1, m_background);

	//renderstate.program[SHADER_RAY_EXIT]->set_uniform("enable_box", renderstate.enable_box);
	glCullFace(GL_FRONT);
	lux::static_cube::draw(hExit);
	return true;
}

void glVolume::requantize(uint32_t nBits) {
	uint32_t cur_bits = 9;
	uint32_t size = 512;
	while (cur_bits > nBits) {
		for (size_t n = 0; n < size; n += 2) {
			float nCode = 0.5f*(m_pVolume->get_Code(n) + m_pVolume->get_Code(n + 1));
			float nVar = m_pVolume->get_CodeVar(n) + m_pVolume->get_CodeVar(n + 1);
			m_pVolume->set_Code(n, nCode);
			m_pVolume->set_Code(n + 1, nCode);
			m_pVolume->set_CodeVar(n, nVar);
			m_pVolume->set_CodeVar(n + 1, nVar);
			size >>= 1;
			cur_bits--;
		}
	}
}