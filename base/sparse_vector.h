#ifndef __SPARSE_VECTOR_H__
#define __SPARSE_VECTOR_H__

#include"sparse_pair.h"

#include<inttypes.h>
#include<string>
#include<stdio.h>
#include<vector>
#include<algorithm>
#include<cassert>
#include"memfile.h"

#define NEW_DISK_FORMAT

namespace sparse {
	template<typename id_t,typename data_t=float> class vector;			
};


#ifdef PROFILE_COVERAGE
	#include<map>	
	#include"timing.h"
	namespace sparse {
		std::map<std::string,CTimer,std::less<std::string>,std::allocator<std::pair<std::string,CTimer> > > profile_vector;		
		void output_profile_vector(FILE* stream = stdout, bool by_time = true) {
			if (stream == nullptr) return;

			std::vector<std::pair<std::string, CTimer> > v(profile_vector.cbegin(), profile_vector.cend());
			if (by_time) std::sort(v.begin(), v.end(), 
				[](const std::pair<std::string, CTimer>& a, const std::pair<std::string, CTimer>& b) {
					return std::make_pair(b.second.Query(), b.first) < std::make_pair(a.second.Query(), a.first);
			});
			else std::sort(v.begin(), v.end(), 
				[](const std::pair<std::string, CTimer>& a, const std::pair < std::string, CTimer>& b) {
				return std::make_pair(a.first, a.second.Query()) < std::make_pair(b.first, b.second.Query());
			});
			for (size_t n = 0; n < v.size(); n++) {
				fprintf(stream, "%s: %.3fs\n", v[n].first.c_str(), v[n].second.Query());
			}

		}
	};
#define prof(x) x;
#else
	#define prof(x)
#endif



// uses a vector that is kept sorted. Needs to be resorted when using sparse::vector::iterator

template<typename id_t, typename data_t>
class sparse::vector {
public:	
	vector(void);											///< default constructor,		O(1)				prof:  5.37
	vector(const vector& other);							///< copy constructor,			O(N)				prof: 18.54
	~vector(void);											///< destructor,				O(1)				prof: 14.03
	bool empty(void) const;									///< true iff empty,			O(1)
	id_t size(void) const;									///< number of non-zeros,		O(1)
	void clear(void);										///< deallocate memory,			O(1)				prof:  5.98
	vector& operator=(const vector& other);					///< assignment operator,		O(N)				prof: 17.42
	const pair<id_t, data_t>& operator[](id_t n) const;		///< RO access to non-zeros		O(1)
	pair<id_t, data_t>& operator[](id_t n);					///< RW access to non-zeros		O(1)				prof:  6.52
	data_t operator()(id_t n) const;						///< element read access,		O(log N)
	void set(id_t n, data_t val);							///< element write access,		O(log N)..O(N)		prof:  8.81
	void add(id_t n, data_t val);							///< element update,			O(log N)..O(N)
	void trim(void);										///< trim allocated memory		O(N)				prof: 74.80
	void resort(void);										///< re-sorts vector			O(N log N)
	vector operator+(const vector& other) const;			///< addition					O(N+M)				prof: 65.26
	vector operator-(const vector& other) const;			///< subtraction				O(N+M)
	data_t operator*(const vector& other) const;			///< dot product				O(N+M)
	vector operator-(void) const;							///< subtraction				O(N+M)
	vector operator*(data_t val) const;						///< scaling					O(N)				prof: 13.04
	vector operator/(data_t val) const;						///< scaling					O(N)				prof:  3.60
	data_t length2(void) const;								///< squared L2 length			O(N)
	data_t length(void) const;								///< L2 length					O(N)
	void normalize(void);									///< L2 normalization			O(N)
	data_t l1_norm(void) const;								///< L1 norm					O(N)				prof:  0.18

	vector& operator+=(const vector& other);				///< addition					O(N+M)				prof: 83.36
	vector& operator-=(const vector& other);				///< subtraction				O(N+M)
	vector& operator*=(data_t val);							///< scaling					O(N)
	vector& operator/=(data_t val);							///< scaling					O(N)
	bool operator==(const vector& other) const;				///< equality					O(min(N,M))
	bool operator!=(const vector& other) const;				///< inequality					O(min(N,M))
	bool operator<(const vector& other) const;				///< comparison					O(min(N,M))
	bool operator>=(const vector& other) const;				///< comparison					O(min(N,M))
	bool operator>(const vector& other) const;				///< comparison					O(min(N,M))
	bool operator<=(const vector& other) const;				///< comparison					O(min(N,M))

	const typename pair<id_t, data_t>& front(void) const;
	const typename pair<id_t, data_t>& back(void) const;	

	inline bool is_compressed(void) const;
	inline bool is_unit(void) const;
	inline void unit(id_t val);
	inline void try_expand(void);
	inline id_t get_unit_id(void) const;
	inline void try_compress(void);
#ifdef COMPRESSED_UNITS
	static const size_t UNIT_MASK;
#endif

	typedef typename pair<id_t, data_t>* iterator;
	iterator begin(void);
	iterator end(void);
	typedef typename const pair<id_t, data_t>* const_iterator;
	const_iterator cbegin(void) const;
	const_iterator cend(void) const;

	inline void factorize(const_iterator i, const_iterator j, id_t k);
	inline void factorize(std::pair<const_iterator, const_iterator>& p, id_t k);

	id_t maxdim(void) const;								///< id of max non-zero			O(1)
	id_t mindim(void) const;								///< id of min non-zero			O(1)

	void output(FILE* stream = stdout) const;				///< debug output				O(N)
	
	bool write(std::string& file) const;
	bool write(FILE* stream) const;
	bool write(memfile& stream) const;
	bool read(std::string& file);
	bool read(FILE* stream);
	bool read(memfile& stream);
	
	const_iterator find(id_t n) const;
	bool x_find_pair(std::pair<const_iterator, const_iterator>& result, const std::pair<id_t, id_t>& p) const;
	const_iterator lower_bound(id_t n) const;
	
	iterator find(id_t n);											///<	prof:  0.54
	bool x_find_pair(std::pair<iterator,iterator>& result, const std::pair<id_t, id_t>& p);
	iterator lower_bound(id_t n);

	friend vector<id_t,data_t> lerp(const vector<id_t,data_t>& a, const vector<id_t,data_t>& b, const data_t& w) {
		if (w == data_t(0)) return a;
		if (w == data_t(1)) return b;
		vector<id_t,data_t> r;
		data_t ow = data_t(1) - w;
#ifdef COMPRESSED_UNITS
		if (a.is_compressed()) {
			id_t p1 = a.get_unit_id();
			if (b.is_compressed()) {
				// a and b are compressed unit vectors
				id_t p2 = b.get_unit_id();
				if (p1 < p2) {
					r.push_back(p1, ow);
					r.push_back(p2, w);
					return r;
				}
				if (p2 < p1) {
					r.push_back(p2, w);
					r.push_back(p1, ow);
					return r;
				}
				r.unit(p1);
				return r;
			}
			// only a is a compressed unit vector
			r = b*w;
			r.add(p1, ow);			
			return r;
		}
		if (b.is_compressed()) {
			// only b is a compressed unit vector
			id_t p2 = b.get_unit_id();
			r = a*ow;
			r.add(p2, w);			
			return r;
		}
#endif
		id_t p1 = 0, p2 = 0;
		while (p1 < a.m_size && p2 < b.m_size) {
			if (a.m_data[p1].first < b.m_data[p2].first) {
				if (a.m_data[p1].second != data_t(0)) r.push_back(a.m_data[p1].first, ow*a.m_data[p1].second);
				p1++;
			}
			else {
				if (b.m_data[p2].first < a.m_data[p1].first) {
					if (b.m_data[p2].second != data_t(0)) r.push_back(b.m_data[p2].first, w*b.m_data[p2].second);
					p2++;
				}
				else {
					data_t val = ow*a.m_data[p1].second + w*b.m_data[p2++].second;
					if (val != data_t(0)) r.push_back(a.m_data[p1].first, val);
					p1++;
				}
			}
		}
		// remainder
		for (; p1 < a.m_size; p1++) if (a.m_data[p1].second != data_t(0)) r.push_back(a.m_data[p1].first, ow*a.m_data[p1].second);
		for (; p2 < b.m_size; p2++) if (b.m_data[p2].second != data_t(0)) r.push_back(b.m_data[p2].first, w*b.m_data[p2].second);
		r.try_compress();
		return r;
	}
	friend vector max(const vector& a, const vector& b) {
		sparse::vector<id_t> r;
#ifdef COMPRESSED_UNITS
		if (a.is_compressed()) {
			id_t p1 = a.get_unit_id();
			if (b.is_compressed()) {
				id_t p2 = b.get_unit_id();
				if (p1 < p2) {
					r.push_back(p1, data_t(1));
					r.push_back(p2, data_t(1));
					return r;
				}
				if (p2 < p1) {
					r.push_back(p2, data_t(1));
					r.push_back(p1, data_t(1));
					return r;
				}
				r.unit(p1);
				return r;
			}
			// only a is a compressed unit vector
			r = b;
			r.set(p1, std::max(r(p1), data_t(1)));
			return r;			
		}
		if (b.is_compressed()) {
			// only b is a compressed unit vector
			id_t p2 = b.get_unit_id();
			r = a;
			r.set(p2, std::max(r(p2), data_t(1)));
			return r;
		}
#endif
		id_t p1 = 0, p2 = 0;
		while (p1 < a.m_size && p2 < b.m_size) {
			if (a.m_data[p1].first < b.m_data[p2].first) {
				if (a.m_data[p1].second > data_t(0)) r.push_back(a.m_data[p1]);
				p1++;
			}
			else {
				if (b.m_data[p1].first < a.m_data[p1].first) {
					if (b.m_data[p2].second > data_t(0)) r.push_back(b.m_data[p2]);
				}
				else {
					data_t val = std::max(a.m_data[p1].second, b.m_data[p2++].second);
					if (val != data_t(0)) r.push_back(a.m_data[p1].first, val);
					p1++;
				}
			}
		}
		for (; p1 < a.m_size; p1++) if (a.m_data[p1].second > data_t(0)) r.push_back(a.m_data[p1]);
		for (; p2 < b.m_size; p2++) if (b.m_data[p2].second > data_t(0)) r.push_back(b.m_data[p2]);
		r.try_compress();
		return r;
	}
	friend vector min(const vector& a, const vector& b) {
		sparse::vector<id_t> r;
#ifdef COMPRESSED_UNITS
		if (a.is_compressed()) {
			id_t p1 = a.get_unit_id();
			if (b.is_compressed()) {
				id_t p2 = b.get_unit_id();
				if (p1 == p2) r.unit(p1);
				return r;
			}
			// only a is a compressed vector
			r = b;
			r.set(p1, std::min(r(p1), data_t(1)));
			return r;
		}
		if (b.is_compressed()) {
			// only b is a compressed vector
			id_t p2 = b.get_unit_id();
			r = a;
			r.set(p2, std::min(r(p2), data_t(1)));
			return r;
		}
#endif
		id_t p1 = 0, p2 = 0;
		while (p1 < a.m_size && p2 < b.m_size) {
			if (a.m_data[p1].first < b.m_data[p2].first) {
				if (a.m_data[p1].second < data_t(0)) r.push_back(a.m_data[p1]);
				p1++;
			}
			else {
				if (b.m_data[p1].first < a.m_data[p1].first) {
					if (b.m_data[p2].second < data_t(0)) r.push_back(b.m_data[p2]);
				}
				else {
					data_t val = std::min(a.m_data[p1].second, b.m_data[p2++].second);
					if (val != data_t(0)) r.push_back(a.m_data[p1].first, val);
					p1++;
				}
			}
		}
		for (; p1 < a.m_size; p1++) if (a.m_data[p1].second < data_t(0)) r.push_back(a.m_data[p1]);
		for (; p2 < b.m_size; p2++) if (b.m_data[p2].second < data_t(0)) r.push_back(b.m_data[p2]);
		return r;
	}
	friend vector operator*(const data_t& lhs, const vector& rhs) {
		return rhs*lhs;
	}
protected:
	pair<id_t, data_t>*	m_data;
#ifdef COMPACT
	#pragma message("sparse::vector -- compact mode")
	#pragma pack (push,1)
		id_t				m_size;
		uint8_t				m_capacity;
	#pragma pack (pop)
#else
	#pragma message("sparse::vector -- performance mode")
		id_t				m_size;
		uint8_t				m_capacity;
#endif

	// auxiliary functions
	void push_back(const pair<id_t, data_t>& p);					///<	prof: 38.08
	void push_back(id_t n, data_t val);								///<	prof:  4.75
	void reserve(id_t new_size);									///<	prof: 25.12
	void erase(const pair<id_t, data_t>* ptr);
	void emplace(const pair<id_t, data_t>* ptr, id_t n, data_t val);
	void shrink_to_fit(void);										///<	prof:  5.46
};

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>::vector(void) : m_data(nullptr), m_size(0), m_capacity(0) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>::vector(const vector<id_t, data_t>& other) : m_data(nullptr), m_size(0), m_capacity(0) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	*this = other;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>::~vector(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	clear();
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::empty(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	return m_size == 0;
}

template<typename id_t, typename data_t>
id_t sparse::vector<id_t, data_t>::size(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	return m_size;
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::clear(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (m_data == nullptr) return;
#ifdef COMPRESSED_UNITS
	if (!is_compressed()) {
		if (m_data) {
			free(m_data);
		}
	}
#else
	if (m_data) free(m_data);
#endif
	m_data = nullptr;	
	m_size = 0;
	m_capacity = 0;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>& sparse::vector<id_t, data_t>::operator=(const vector<id_t, data_t>& other) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (other.is_compressed()) {
		if (!is_compressed()) clear();
		m_data = other.m_data;
		m_size = 1;
		m_capacity = 0;
	}
	else {
		reserve(other.m_size);
		if (other.m_size > 0)	{
			memcpy(m_data, other.m_data, other.m_size*sizeof(pair<id_t, data_t>));
			m_size = other.m_size;
			m_capacity = 0;
		}
	}
#else
	reserve(other.m_size);
	if (other.m_size>0)	{
		memcpy(m_data, other.m_data, other.m_size*sizeof(pair<id_t, data_t>));
		m_size = other.m_size;
		m_capacity = 0;
	}	
#endif
	return *this;
}

template<typename id_t, typename data_t>
const sparse::pair<id_t, data_t>& sparse::vector<id_t, data_t>::operator[](id_t n) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector[] - invalid parameter(s)" && n < m_size && m_data != nullptr);
	try_expand();
	return m_data[n];
}

template<typename id_t, typename data_t>
sparse::pair<id_t, data_t>& sparse::vector<id_t, data_t>::operator[](id_t n) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector[] - invalid parameter(s)" && n < m_size && m_data != nullptr);
	try_expand();
	return m_data[n];
}

template<typename id_t, typename data_t>
data_t sparse::vector<id_t, data_t>::operator()(id_t n) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return get_unit_id() == n ? data_t(1) : data_t(0);
#endif
	const_iterator ptr = find(n);
	if (ptr != cend()) return ptr->second;
	return data_t(0);	
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::set(id_t n, data_t val) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (empty() && val == data_t(1)) {
		unit(n);
		return;
	}
	try_expand();
	iterator ptr = lower_bound(n);
	if (ptr==end()) { // insert new element at the end
		if (val != data_t(0)) push_back(n, val);
		return;
	}
	if (ptr->first == n) { // element found
		if (val == data_t(0)) erase(ptr);
		else ptr->second = val;
		return;
	}
	// element not found, insert before iterator
	if (val != data_t(0)) emplace(ptr, n, val);
	try_compress();	
}

template<typename id_t, typename data_t>
inline void sparse::vector<id_t, data_t>::factorize(const_iterator i, const_iterator j, id_t k) {
	// we know this vector can't be empty since i,j are valid iterators
	// we know that k will be appended at the end
	// we know that this vector is not a compressed unit vector
	data_t w = i->second + j->second;	
	// quick erase i,j	
	id_t n1 = id_t(i - m_data);
	id_t n2 = id_t(j - m_data);
	for (id_t l = n1; l < n2 - 1; l++) m_data[l] = m_data[l + 1];
	for (id_t l = n2-1; l < m_size - 2; l++) m_data[l] = m_data[l + 2];	
	// quick push		
	m_data[m_size-2].first = k;
	m_data[m_size-2].second = w;
	// quick trim
	m_size--;
	void* mem = realloc(m_data, m_size*sizeof(pair<id_t, data_t>));	
	m_data = reinterpret_cast<pair<id_t, data_t>*>(mem);
	m_capacity = 0;		
}

template<typename id_t, typename data_t>
inline void sparse::vector<id_t, data_t>::factorize(std::pair<const_iterator,const_iterator>& p, id_t k) {
	// we know this vector can't be empty since i,j are valid iterators
	// we know that k will be appended at the end
	// we know that this vector is not a compressed unit vector
	data_t w = p.first->second + p.second->second;
	// quick erase i,j	
	id_t n1 = id_t(p.first - m_data);
	id_t n2 = id_t(p.second - m_data);
	for (id_t l = n1; l < n2 - 1; l++) m_data[l] = m_data[l + 1];
	for (id_t l = n2 - 1; l < m_size - 2; l++) m_data[l] = m_data[l + 2];
	// quick push		
	m_data[m_size - 2].first = k;
	m_data[m_size - 2].second = w;
	// quick trim
	m_size--;
	void* mem = realloc(m_data, m_size * sizeof(pair<id_t, data_t>));
	m_data = reinterpret_cast<pair<id_t, data_t>*>(mem);
	m_capacity = 0;
}



template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::add(id_t n, data_t val) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (val == data_t(0)) return;
	if (empty() && val == data_t(1)) {
		unit(n);
		return;
	}
	try_expand();
	iterator ptr = lower_bound(n);
	if (ptr == end()) {// insert new element at the end
		if (val != data_t(0)) push_back(n, val);
		try_compress();
		return;
	}
	if (ptr->first == n) { // element found
		val += ptr->second;
		if (val == data_t(0)) erase(ptr);
		else ptr->second = val;
		try_compress();
		return;
	}
	// element not found, insert before iterator
	emplace(ptr, n, val);
	try_compress();
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::trim(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (empty() || is_unit()) return;
	sparse::vector<id_t, data_t> v(*this);
	clear();
	for (id_t n = 0; n < v.m_size; n++) {
		if (v[n].second != data_t(0))  push_back(v[n]);
	}
	shrink_to_fit();
	try_compress();
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::resort(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	std::sort(m_data, m_data + m_size);
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t> sparse::vector<id_t, data_t>::operator+(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));	
	if (empty()) return other;
	if (other.empty()) return *this;
	vector<id_t, data_t> r;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		r = other;
		r.add(get_unit_id(), data_t(1));		
		return r;
	}
	if (other.is_compressed()) {
		r = *this;
		r.add(other.get_unit_id(), data_t(1));		
		return r;
	}
#endif	
	id_t p1 = 0, p2 = 0;
	while (p1 < m_size && p2 < other.m_size) {
		if (m_data[p1].first < other.m_data[p2].first) {
			if (m_data[p1].second != data_t(0)) r.push_back(m_data[p1]);
			p1++;
		}
		else {
			if (other.m_data[p2].first < m_data[p1].first) {
				if (other.m_data[p2].second != data_t(0)) r.push_back(other.m_data[p2]);
				p2++;
			}
			else {
				data_t val = m_data[p1].second + other.m_data[p2++].second;
				if (val != data_t(0)) r.push_back(m_data[p1].first, val);
				p1++;
			}
		}
	}
	// remainder
	for (; p1 < m_size; p1++) if (m_data[p1].second != data_t(0)) r.push_back(m_data[p1]);
	for (; p2 < other.m_size; p2++) if (other.m_data[p2].second != data_t(0)) r.push_back(other.m_data[p2]);
	r.try_compress();
	return r;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t> sparse::vector<id_t, data_t>::operator-(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	vector<id_t> r;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		r = other;
		r.add(get_unit_id(), data_t(-1));		
		return r;
	}
	if (other.is_compressed()) {
		r = *this;
		r.add(other.get_unit_id(), data_t(-1));		
		return r;
	}
#endif
	id_t p1 = 0, p2 = 0;
	while (p1 < m_size && p2 < other.m_size) {
		if (m_data[p1].first < other.m_data[p2].first) {
			if (m_data[p1].second != data_t(0)) r.push_back(m_data[p1]);
			p1++;
		}
		else {
			if (other.m_data[p2].first < m_data[p1].first) {
				if (other.m_data[p2].second != data_t(0)) r.push_back(other.m_data[p2].first, -other.m_data[p2].second);
				p2++;
			}
			else {
				data_t val = m_data[p1].second - other.m_data[p2++].second;
				if (val != data_t(0)) r.push_back(m_data[p1].first, val);
				p1++;
			}
		}
	}
	// remainder
	for (; p1 < m_size; p1++) if (m_data[p1].second != data_t(0)) r.push_back(m_data[p1]);
	for (; p2 < other.m_size; p2++) if (other.m_data[p2].second != data_t(0)) r.push_back(other.m_data[p2].first, -other.m_data[p2].second);
	r.try_compress();
	return r;
}


template<typename id_t, typename data_t>
data_t sparse::vector<id_t, data_t>::operator*(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		if (other.is_compressed()) {
			return get_unit_id() == other.get_unit_id() ? data_t(1) : data_t(0);
		}
		return other(get_unit_id());		
	}
	if (other.is_compressed()) {
		return (*this)(other.get_unit_id());		
	}
#endif
	double r = 0.0;
	id_t p1 = 0, p2 = 0;
	while (p1 < m_size && p2 < other.m_size) {
		if (m_data[p1].first < other.m_data[p2].first) {
			p1++;
		}
		else {
			if (other.m_data[p2].first < m_data[p1].first) {
				p2++;
			}
			else {
				r += m_data[p1++].second*m_data[p2++].second;
			}
		}
	}
	return data_t(r);
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t> sparse::vector<id_t, data_t>::operator-(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	vector<id_t> r(*this);
	r.try_expand();	
	for (id_t n = 0; n < r.m_size; n++) r.m_data[n].second = -r.m_data[n].second;
	r.try_compress();
	return r;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t> sparse::vector<id_t, data_t>::operator*(data_t val) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (val == data_t(0)) return vector<id_t,data_t>();
	if (val == data_t(1)) return *this;
	vector r(*this);
	r.try_expand();	
	for (id_t n = 0; n < r.m_size; n++) r.m_data[n].second *= val;
	r.try_compress();
	return r;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t> sparse::vector<id_t, data_t>::operator/(data_t val) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector/(scalar) - division by zero" && val != data_t(0));
	if (val == data_t(1)) return *this;
	data_t ooV = data_t(1.0 / double(val));
	vector r(*this);
	r.try_expand();
	for (id_t n = 0; n < r.m_size; n++) r.m_data[n].second *= ooV;
	r.try_compress();
	return r;
}

template<typename id_t, typename data_t>
data_t sparse::vector<id_t, data_t>::length2(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return data_t(1);
#endif
	double r = 0.0;
	for (id_t n = 0; n < m_size; n++) r += m_data[n].second*m_data[n].second;
	return data_t(r);
}

template<typename id_t, typename data_t>
data_t sparse::vector<id_t, data_t>::length(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return data_t(1);
#endif
	double r = 0.0;
	for (id_t n = 0; n < m_size; n++) r += m_data[n].second*m_data[n].second;
	return data_t(sqrt(r));
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::normalize(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return;
#endif
	double r = 0.0;
	for (id_t n = 0; n < m_size; n++) r += m_data[n].second*m_data[n].second;
	if (r == 0.0) return;
	data_t fr = data_t(1.0 / sqrt(r));
	for (id_t n = 0; n < m_size; n++) m_data[n] *= fr;
}

template<typename id_t, typename data_t>
data_t sparse::vector<id_t, data_t>::l1_norm(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return data_t(1);
#endif
	double r = 0.0;
	for (id_t n = 0; n < m_size; n++) r += std::abs(m_data[n].second);
	return data_t(r);
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>& sparse::vector<id_t, data_t>::operator+=(const vector<id_t, data_t>& other) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	// lazy solution
	(*this) = (*this) + other;
	return *this;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>& sparse::vector<id_t, data_t>::operator-=(const vector<id_t, data_t>& other) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	// lazy solution
	(*this) = (*this) - other;
	return *this;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>& sparse::vector<id_t, data_t>::operator*=(data_t val) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (val == data_t(1)) return *this;
	if (val == data_t(0)) {
		clear();
	}
	else {
		try_expand();
		for (id_t n = 0; n < m_size; n++) m_data[n] *= val;
		try_compress();
	}
	return *this;
}

template<typename id_t, typename data_t>
sparse::vector<id_t, data_t>& sparse::vector<id_t, data_t>::operator/=(data_t val) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector/=(scalar) - division by zero" && val != data_t(0));
	if (val == data_t(1)) return *this;
	val = data_t(1) / val;
	try_expand();
	for (id_t n = 0; n < m_size; n++) m_data[n].second *= val;	
	try_compress();
	return *this;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::operator==(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (m_size != other.m_size) return false;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		if (other.is_compressed()) return get_unit_id() == other.get_unit_id();
		return other(get_unit_id()) == data_t(1);
	}
	if (other.is_compressed()) return (*this)(other.get_unit_id()) == data_t(1);
#endif
	for (id_t n = 0; n < m_size; n++) if (m_data[n] != other.m_data[n]) return false;
	return true;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::operator!=(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	return !((*this) == other);
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::operator<(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (empty() && !other.empty()) return true;
	if (!empty() && other.empty()) return false;

#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		id_t p1 = get_unit_id();
		if (other.is_compressed()) {
			return other.get_unit_id() < p1;
		}
		// only this is a compressed vector
		if (p1 < other.m_data[0].first) return false;
		if (other.m_data[0].first < p1) return true;
		return data_t(1) < other.m_data[p2].second;
	}
	if (other.is_compressed()) {
		// only other is a compressed vector
		id_t p2 = other.get_unit_id();
		if (m_data[0].first < p2) return false;
		if (p2 < m_data[0].first) return true;
		return m_data[p1].second < data_t(1);
	}
#endif
	id_t p1 = 0, p2 = 0;
	while (p1 < m_size && p2 < other.m_size) {
		if (m_data[p1].first < other.m_data[p2].first) return false;
		if (other.m_data[p2].first < m_data[p1].first) return true;
		if (m_data[p1].second < other.m_data[p2].second) return true;
		if (other.m_data[p2].second < m_data[p1].second) return false;
		p1++;
		p2++;
	}
	return (p1 == m_size);
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t,data_t>::operator>=(const vector<id_t,data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	return !((*this)<other);
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::operator>(const vector<id_t, data_t>& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	return (other < (*this));
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::operator<=(const vector& other) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	return !((*this)>other);
}

template<typename id_t, typename data_t>
id_t sparse::vector<id_t, data_t>::maxdim(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (m_size == 0) return 0;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return get_unit_id();
#endif
	return m_data[m_size - 1].first;
}

template<typename id_t, typename data_t>
id_t sparse::vector<id_t, data_t>::mindim(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (m_size == 0) return 0;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return get_unit_id();
#endif
	return m_data[0].first;
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::output(FILE* stream) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (stream == NULL) return;
	if (empty()) fprintf(stream, "<empty>");
	fprintf(stream, "<");
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		fprintf(stream, " (%i|1.0) >", get_unit_id());
		return;
	}
#endif
	for (size_t n = 0; n < m_size; n++) {
		fprintf(stream, " (%i|%g)", m_data[n].first, m_data[n].second);
	}
	fprintf(stream, " >");
}


template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::const_iterator sparse::vector<id_t, data_t>::lower_bound(id_t n) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::lower_bound() const -- invalid for compressed unit vectors" && !is_compressed());
	switch (m_size) {
	case 0: return cend();
	case 1: return (n>m_data[0].first ? cend() : &m_data[0]);
	default:
	{
			   if (n>m_data[m_size - 1].first) return cend();
			   if (n<m_data[0].first) return cbegin();
			   id_t first = 0;
			   id_t last = m_size - 1;
			   while (last - first>1) {
				   id_t mid = (first + last + 1) >> 1;
				   if (n > m_data[mid].first) first = mid;
				   else last = mid;
			   }
			   if (m_data[first].first == n) return &m_data[first];
			   return &m_data[last];
	}
	}
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::iterator sparse::vector<id_t, data_t>::lower_bound(id_t n) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	try_expand();
	switch (m_size) {
	case 0: return end();
	case 1: return (n>m_data[0].first ? end() : &m_data[0]);
	default:
		{
			   if (n > m_data[m_size - 1].first) return end();
			   if (n<m_data[0].first) return begin();
			   id_t first = 0;
			   id_t last = m_size - 1;
			   while (last - first>1) {
				   id_t mid = (first + last + 1) >> 1;
				   if (n > m_data[mid].first) first = mid;
				   else last = mid;
			   }
			   if (m_data[first].first == n) return &m_data[first];
			   return &m_data[last];
		}
	}
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::const_iterator sparse::vector<id_t, data_t>::find(id_t n) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::find() const -- invalid for compressed unit vectors" && !is_compressed());
	const_iterator it = lower_bound(n);
	if (it->first != n) return cend();
	return it;
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::iterator sparse::vector<id_t, data_t>::find(id_t n) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	iterator it = lower_bound(n);
	if (it->first != n) return end();
	return it;
}


template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::x_find_pair(std::pair<const_iterator,const_iterator>& result, const std::pair<id_t, id_t>& p) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::find_pair() const -- invalid for compressed unit vectors" && !is_compressed());
	// rule out obvious misses
	if (m_size < 2) return false;
	if (p.second > m_data[m_size - 1].first) return false;
	if (p.first < m_data[0].first) return false;

	// Find p.first	
	id_t first = 0;
	id_t last = m_size - 1;
	while (last - first > 1) {
		id_t mid = (first + last + 1) >> 1;
		if (p.first > m_data[mid].first) first = mid;
		else last = mid;
	}
	if (m_data[first].first == p.first) {
		if (first >= m_size - 1) return false;
		result.first = &m_data[first];
		first++;
	}
	else {
		if (m_data[last].first == p.first) {			
			if (last >= m_size - 2) return false;
			result.first = &m_data[last];
			first = last + 1;
		}
		else {
			return false;
		}
	}		
	// Find p.second	
	last = m_size - 1;	
	while (last - first > 1) {
		id_t mid = (first + last + 1) >> 1;
		if (p.second > m_data[mid].first) first = mid;
		else last = mid;
	}
	if (m_data[first].first == p.second) {		
		result.second = &m_data[first];
		return true;
	}	
	else {
		if (m_data[last].first == p.second) {			
			result.second = &m_data[last];
			return true;
		}
	}	
	return false;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::x_find_pair(std::pair<iterator,iterator>& result, const std::pair<id_t, id_t>& p) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));	
	// rule out obvious misses
	if (m_size < 2) return false;
	if (p.second > m_data[m_size - 1].first) return false;
	if (p.first < m_data[0].first) return false;

	// Find p.first	
	id_t first = 0;
	id_t last = m_size - 1;
	while (last - first > 1) {
		id_t mid = (first + last + 1) >> 1;
		if (p.first > m_data[mid].first) first = mid;
		else last = mid;
	}
	if (m_data[first].first == p.first) {
		if (first >= m_size - 1) return false;
		result.first = &m_data[first];
		first++;
	}
	else {
		if (m_data[last].first == p.first) {
			if (last >= m_size - 2) return false;
			result.first = &m_data[last];
			first = last + 1;
		}
		else {
			return false;
		}
	}
	// Find p.second	
	last = m_size - 1;
	while (last - first > 1) {
		id_t mid = (first + last + 1) >> 1;
		if (p.second > m_data[mid].first) first = mid;
		else last = mid;
	}
	if (m_data[first].first == p.second) {
		result.second = &m_data[first];
		return true;
	}
	else {
		if (m_data[last].first == p.second) {
			result.second = &m_data[last];
			return true;
		}
	}
	return false;
}


template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::push_back(const pair<id_t, data_t>& p) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	try_expand();
	if (m_capacity == 0) {
		assert("sparse::vector::push_back() - out of memory" && m_size < id_t(-1));
		id_t add_cap = std::min(id_t(-1) - m_size, id_t(4));
		reserve(m_size + add_cap);
	}
	m_data[m_size] = p;	
	m_size++;
	m_capacity--;
	try_compress();
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::push_back(id_t n, data_t val) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	try_expand();
	if (m_capacity == 0) {
		assert("sparse::vector::push_back() - out of memory" && m_size < id_t(-1));
		id_t add_cap = std::min<id_t>(id_t(-1) - m_size, id_t(4));
		reserve(m_size + add_cap);
	}
	m_data[m_size].first = n;
	m_data[m_size].second = val;
	m_size++;
	m_capacity--;
	try_compress();
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::reserve(id_t new_size) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (new_size == 0) {
		clear();
		return;
	}
	try_expand();	
	void* mem = realloc(m_data, new_size*sizeof(pair<id_t,data_t>));
	assert("sparse::vector::reserve() - out of memory" && mem != nullptr);
	m_data = reinterpret_cast<pair<id_t, data_t>*>(mem);	
	m_size = std::min(m_size, new_size);
	m_capacity = new_size - m_size;
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::erase(const pair<id_t, data_t>* ptr) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));	
	try_expand();
	id_t n = id_t(ptr - m_data);
	assert("sparse::vector::erase() - invalid parameter" && id_t(n) < m_size);
	// pull all elements forward
	//memmove(m_data + n, m_data + n + 1, (m_size - 1 - n)*sizeof(pair<id_t, data_t>));
	for (id_t k = n; k < m_size - 1; k++) m_data[k] = m_data[k + 1];
	m_size--;
	m_capacity++;
	if (m_capacity>8) reserve(m_size + 4);
	try_compress();
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::emplace(const pair<id_t, data_t>* ptr, id_t n, data_t val) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	try_expand();
	id_t k = id_t(ptr - m_data);
	assert("sparse::vector::emplace() - invalid parameter" && id_t(k) < m_size);
	if (m_capacity == 0) {
		assert("sparse::vector::emplace() - out of memory" && m_size < id_t(-1));
		id_t add_cap = std::min<id_t>(id_t(-1) - m_size, id_t(4));
		reserve(m_size + add_cap);
	}
	// push all elements backward
	//memmove(m_data + n + 1, m_data + n, (m_size - n)*sizeof(pair<id_t, data_t>));
	for (id_t m = m_size; m > k; m--) m_data[m] = m_data[m - 1];
	m_data[k].first = n;
	m_data[k].second = val;
	m_size++;
	m_capacity--;
	try_compress();
}

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::shrink_to_fit(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (m_capacity > 0) reserve(m_size);
	try_compress();
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::iterator sparse::vector<id_t, data_t>::begin(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	try_expand();
	return m_data;
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::iterator sparse::vector<id_t, data_t>::end(void) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	try_expand();
	return m_data + m_size;
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::const_iterator sparse::vector<id_t, data_t>::cbegin(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::cbegin() const -- invalid for compressed unit vectors" && !is_compressed());
	return m_data;
}

template<typename id_t, typename data_t>
typename sparse::vector<id_t, data_t>::const_iterator sparse::vector<id_t, data_t>::cend(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::cend() const -- invalid for compressed unit vectors" && !is_compressed());
	return m_data + m_size;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::write(std::string& file) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (name.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, file.c_str(), "wb")) return false;
	bool r = write(stream);
	fclose(stream);
	return r;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::write(FILE* stream) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (stream == nullptr) return false;
	if (fwrite(&m_size, sizeof(id_t), 1, stream) != 1) return false;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {		
#ifdef NEW_DISK_FORMAT
		id_t p = get_unit_id();				
		if (fwrite(&p, sizeof(id_t), 1, stream) != 1) return false;
#else
		pair<id_t, data_t> p(get_unit_id(), data_t(1));
		if (fwrite(&p, sizeof(pair<id_t, data_t>), 1, stream) != 1) return false;
#endif
		return true;
	}
#endif		
	if (fwrite(m_data, sizeof(pair<id_t, data_t>), m_size, stream) != m_size) return false;
	return true;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::write(memfile& stream) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (!stream.is_open()) return false;
	if (stream.write(&m_size, sizeof(id_t), 1) != 1) return false;
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
#ifdef NEW_DISK_FORMAT
		id_t p = get_unit_id();
		if (stream.write(&p, sizeof(id_t), 1) != 1) return false;
#else
		pair<id_t, data_t> p(get_unit_id(), data_t(1));
		if (stream.write(&p, sizeof(pair<id_t, data_t>), 1) != 1) return false;
#endif
		return true;
	}
#endif		
	if (stream.write(m_data, sizeof(pair<id_t, data_t>), m_size) != m_size) return false;
	return true;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::read(std::string& file) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (file.empty()) return false;
	FILE* stream = nullptr;
	if (fopen_s(&stream, file.c_str(), "rb")) return false;
	bool r = read(stream);
	fclose(stream);
	return r;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::read(FILE* stream) {	
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (stream == nullptr) return false;
	clear();
	id_t n;
	if (fread(&n, sizeof(id_t), 1, stream) != 1) return false;		
#ifdef COMPRESSED_UNITS
#ifdef NEW_DISK_FORMAT
	if (n == 1) {
		id_t p;
		if (fread(&p, sizeof(id_t), n, stream) != n) return false;
		unit(p);
		return true;
	}
#endif
#endif
	reserve(n);
	if (fread(m_data, sizeof(pair<id_t, data_t>), n, stream) != n) return false;
	m_size = n;
	try_compress();
	return true;
}

template<typename id_t, typename data_t>
bool sparse::vector<id_t, data_t>::read(memfile& stream) {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	if (!stream.is_open()) return false;
	clear();
	id_t n;	
	if (stream.read(&n, sizeof(id_t), 1) != 1) return false;
#ifdef COMPRESSED_UNITS
#ifdef NEW_DISK_FORMAT
	if (n == 1) {		
		id_t p;
		if (stream.read(&p, sizeof(id_t), n) != n) return false;
		unit(p);
		return true;
	}
#endif
#endif
	reserve(n);	
	if (stream.read(m_data, sizeof(pair<id_t, data_t>), n) != n) return false;	
	m_size = n;	
	try_compress();
	return true;
}

template<typename id_t, typename data_t>
const typename sparse::pair<id_t, data_t>& sparse::vector<id_t, data_t>::front(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::front() - vector empty" && m_size > 0);
#ifdef COMPRESSED_UNITS
	assert("sparse::vector::front() const -- cannot be used on compressed unit vectors." && !is_compressed());
#endif
	return m_data[0];
}

template<typename id_t, typename data_t>
const typename sparse::pair<id_t, data_t>& sparse::vector<id_t, data_t>::back(void) const {
	prof(scoped_timer __ST(profile_vector[__FUNCSIG__]));
	assert("sparse::vector::back() - vector empty" && m_size > 0);
#ifdef COMPRESSED_UNITS
	assert("sparse::vector::back() const -- cannot be used on compressed unit vectors." && !is_compressed());
#endif
	return m_data[m_size - 1];
}


// COMPRESSED UNITS

template<typename id_t, typename data_t>
inline bool sparse::vector<id_t, data_t>::is_compressed(void) const {
#ifdef COMPRESSED_UNITS
	return (reinterpret_cast<const size_t>(m_data)&UNIT_MASK) != size_t(0);
#endif
	return false;
}

template<typename id_t, typename data_t>
inline bool sparse::vector<id_t, data_t>::is_unit(void) const{
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return true;
#endif
	if (m_size != 1) return false;
	return m_data[0].second == data_t(1);
}

template<typename id_t, typename data_t>
inline void sparse::vector<id_t, data_t>::unit(id_t val) {
#ifdef COMPRESSED_UNITS
	clear();
	m_data = reinterpret_cast<pair<id_t,data_t>*>(UNIT_MASK | size_t(val));
	m_size = 1;
	m_capacity = 0;
#else
	clear();
	m_data = reinterpret_cast<pair<id_t,data_t>*>(malloc(sizeof(pair<id_t,data_t>)));
	m_data[0].first = val;
	m_data[0].second = 1.0f;
	m_size = 1;
	m_capacity = 0;	
#endif
}

template<typename id_t, typename data_t>
inline void sparse::vector<id_t, data_t>::try_expand(void) {
#ifdef COMPRESSED_UNITS
	if (is_compressed()) {
		id_t val = get_unit_id();
		m_data = reinterpret_cast<pair<id_t, data_t>*>(malloc(sizeof(pair<id_t, data_t>)));
		m_data[0].first = val;
		m_data[0].second = data_t(1);		
	}
#endif
}


template<typename id_t, typename data_t>
inline id_t sparse::vector<id_t, data_t>::get_unit_id(void) const {
#ifdef COMPRESSED_UNITS
	if (is_compressed()) return id_t(reinterpret_cast<size_t>(m_data) & (~UNIT_MASK));
#endif
	return m_data[0].first;
}

#ifdef COMPRESSED_UNITS
	template<typename id_t, typename data_t>
	#ifdef _WIN64
		const size_t sparse::vector<id_t, data_t>::UNIT_MASK = size_t(1) << size_t(63);
	#else
		const size_t sparse::vector<id_t,data_t>::UNIT_MASK = size_t(1)<<size_t(31);
	#endif
#endif

template<typename id_t, typename data_t>
void sparse::vector<id_t, data_t>::try_compress(void) {
#ifdef COMPRESSED_UNITS
	if (!is_compressed()) {
		if (is_unit()) unit(m_data[0].first);
	}
#endif
}

#endif
