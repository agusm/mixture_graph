#ifndef __GL_VOLUME_H__
#define __GL_VOLUME_H__

#include"lux_shader.h"
#include"compressed_mipvolume.h"
#include"lux_color.h"
#include<functional>
#include"lux_math.h"
#include"lux_footprint_assembly.h"
#include"normal_volume.h"
#include <unordered_map>

class glVolume {
public:
	glVolume(compressed_mipvolume& V, normal_volume* N = nullptr);
	glVolume(void);
	~glVolume(void);
	void clear(void);
	bool empty(void) const;
	void attach(compressed_mipvolume& V, normal_volume* N = nullptr);
	void detach(void);
	compressed_mipvolume* mipvolume(void);

	bool upload_gpu(void);

	// topological order ranges of nodes
	const std::vector<lux::rangeu32_t>& order(void) const;
	std::vector<lux::rangeu32_t>& order(void);

	// node list
	const std::vector<uint32_t>& node_list(void) const;
	std::vector<uint32_t>& node_list(void);

	// codebook
	const std::vector<float>& codebook(void) const;
	std::vector<float>& codebook(void);

	// color lookup table CPU / GPU
	inline size_t nLeafs(void) const { return m_leaf_colors.size(); }

	inline void set_leaf_color(size_t n,const lux::color& clr) {
		if (n >= m_unpack.size()) {
			printf("Error setting color at leaf %zi \n", n);
			exit(1);
		}
		auto k = m_unpack[n];			// added
		
		m_leaf_colors[k] = clr;	// added
	}

	inline const lux::color& get_leaf_color(size_t n) const {
		// FIXME
		//if (n==0) return m_leaf_colors[0];
		if (n >= m_unpack.size()) {
				printf("Error setting color at leaf %zi \n", n);
				exit(1);
		}
		auto k = m_unpack[n];		// added
		return  m_leaf_colors[k];			// added
	}

#if 0
	inline lux::color& leaf_color(size_t n) {
		//auto k = m_pack[n];			// added
		return m_leaf_colors[n];	// added
	}
	inline const lux::color& leaf_color(size_t n) const {
		//auto k = m_unpack[uint16_t(n)];		// added
		return m_leaf_colors[n];			// added
	}
#endif

	// color lookup table CPU
	bool cpu_update_color_lut(void);
	bool gpu_update_color_lut(void);

	// compile programs
	bool gpu_delete_programs(void);
	bool gpu_compile_programs(bool force = false);

	const std::vector<lux::color>& cpu_color_lut(void) const;
	std::vector<lux::color>& cpu_color_lut(void);

	enum shader_t {
		SHADER_COMPUTE = 0,
		SHADER_CUBE,
		SHADER_CUBERILLE,
		SHADER_FANCY_CUBERILLE,
		SHADER_DVR,
		NUM_SHADERS
	};


	void reshape(uint32_t width, uint32_t height);
	bool draw(const shader_t& s,const lux::mat4f& projection=lux::mat4f(), const lux::mat4f& modelview=lux::mat4f());
	
	// draw modifiers & attributes
	inline lux::color& background_color(void) { return m_background; }
	inline const lux::color& background_color(void) const { return m_background; }
	inline bool& enable_box(void) { return m_enable_box; }
	inline const bool& enable_box(void) const { return m_enable_box; }
	inline std::size_t& lao_directions(void) { return m_lao_directions; }
	inline const std::size_t& lao_directions(void)  const { return m_lao_directions; }


	inline bool& enable_shading(void) { return m_enable_shading; }
	inline const bool& enable_shading(void) const { return m_enable_shading; }
	inline lux::vec3f& light_dir(void) { return m_light_dir; }
	inline const lux::vec3f& light_dir(void) const { return m_light_dir; }
	inline lux::vec3f& shading_const(void) { return m_shading_const; }
	inline const lux::vec3f& shading_const(void) const { return m_shading_const; }

	inline bool& empty_space_skipping(void) { return m_empty_space_skipping; }
	inline const bool& empty_space_skipping(void) const { return m_empty_space_skipping; }

	void pack(bool packing);


	inline lux::vec3f& box_ll(void) { return m_box_ll; }
	inline const lux::vec3f& box_ll(void) const { return m_box_ll; }
	inline lux::vec3f& box_ur(void) { return m_box_ur; }
	inline const lux::vec3f& box_ur(void) const { return m_box_ur; }
	inline lux::color& box_color(void) { return m_box_color; }
	inline const lux::color& box_color(void) const { return m_box_color; }
	inline float& lod_scale(void) { return m_lod_scale; }
	inline const float& lod_scale(void) const { return m_lod_scale; }
	inline float& lod_bias(void) { return m_lod_bias; }
	inline const float& lod_bias(void) const { return m_lod_bias; }
	bool compute_lod_scale(float fovy) {
		if (m_pVolume == nullptr) return false;
		double s = 2.0 / double(m_view_dims[1])*tan(fovy*M_PI / 360.0);
		double voxelside = ::sqrt(12.0) / ::sqrt(
			double(m_pVolume->get_Dimension(0)*m_pVolume->get_Dimension(0))
			+ double(m_pVolume->get_Dimension(1)*m_pVolume->get_Dimension(1))
			+ double(m_pVolume->get_Dimension(2)*m_pVolume->get_Dimension(2)));
		m_lod_scale = voxelside / s;
		return true;
	}

	void requantize(uint32_t nBits);

protected:
	bool draw_cube(const lux::mat4f& projection, const lux::mat4f& modelview);
	bool draw_cuberille(const lux::mat4f& projection, const lux::mat4f& modelview);
	bool draw_base(const lux::mat4f& projection, const lux::mat4f& modelview, lux::program* hExit);
	compressed_mipvolume*			m_pVolume;


	normal_volume* m_nVolume;


	void init_normal_texture();

	// ranges that can be evaluated in parallel -- each range comprises all nodes of exactly one topological level
	std::vector<lux::rangeu32_t>	m_topo_order;
	void compile_topo_ranges(void);

	// node list for GPU processing
	std::vector<uint32_t>			m_node_list;
	void compile_node_list(void);
	inline void read_node(int n, uint32_t& ch0, uint32_t& ch1, float& idx) {
		ch0 = m_node_list[3 * n];
		ch1 = m_node_list[3 * n + 1];
		idx = float(m_node_list[3 * n + 2])/float(1<<20);
	}

	// codebook
	std::vector<float>				m_codebook;
	void compile_codebook(void);

	// CPU-based color lut
	std::vector<lux::color>			m_color_lut;

	// GPU part
	GLuint							m_hNodeList;
	GLuint							m_hColorLUT;
	GLuint							m_hVoxelData;
	GLuint							m_hIndexData;
	GLuint							m_hDebugBuffer;
	GLuint							m_hFBO;
	GLuint							m_hTex;
	// Texture
	GLuint							m_hNormalTex;


	lux::program*					m_hCompute;
	lux::program*					m_hCube;
	lux::program*					m_hRayEntry;
	lux::program*					m_hCuberille;
	lux::program*					m_hFancyCuberille;
	lux::program*					m_hDVR;

	lux::color						m_background;
	bool							m_enable_box;
	lux::vec3f                      m_light_dir;
	lux::vec3f                      m_shading_const;
	std::size_t                     m_lao_directions;
	bool                            m_enable_shading;
	bool							m_empty_space_skipping;
	
	lux::vec3f						m_box_ll;
	lux::vec3f						m_box_ur;
	lux::color						m_box_color;

	GLsizei							m_LUTsize;
	std::vector<lux::color>			m_leaf_colors;
	uint32_t						m_view_dims[2];
	float							m_lod_scale;
	float							m_lod_bias;

	//static std::function<bool(glVolume* pInstance)> init_shader[NUM_SHADERS];
	//static std::function<bool(glVolume* pInstance)> done_shader[NUM_SHADERS];
	std::unordered_map<uint32_t, uint16_t> m_pack;	// added
	std::vector<uint16_t> m_unpack;	// added

private:
	glVolume(const glVolume& other);
};

#endif
