#include"quat_t.h"
#include<cassert>

quat_t::quat_t(void) : vec_t(1.0f,0.0f,0.0f,0.0f) {}

quat_t::quat_t(float r, float i, float j, float k) : vec_t(r,i,j,k) {}

quat_t::quat_t(const quat_t& other) {
	*this = other;
}

quat_t::~quat_t(void) {}

quat_t quat_t::operator*(const quat_t& other) const {
	quat_t result;
	float* R(result.m_data);
	R[0] = m_data[0] * other.m_data[0] - m_data[1] * other.m_data[1] - m_data[2] * other.m_data[2] - m_data[3] * other.m_data[3];
	R[1] = m_data[0] * other.m_data[1] + m_data[1] * other.m_data[0] + m_data[2] * other.m_data[3] - m_data[3] * other.m_data[2];
	R[2] = m_data[0] * other.m_data[2] - m_data[1] * other.m_data[3] + m_data[2] * other.m_data[0] + m_data[3] * other.m_data[1];
	R[3] = m_data[0] * other.m_data[3] + m_data[1] * other.m_data[2] - m_data[2] * other.m_data[1] + m_data[3] * other.m_data[0];
	return result;
}

quat_t quat_t::inverse(void) const {
	float norm = length2();
	norm = norm > 0.0f ? 1.0f / norm : 0.0f;
	return quat_t(m_data[0] * norm, m_data[1] * norm, -m_data[2] * norm, -m_data[3] * norm);
}

quat_t quat_t::conjugate(void) const {
	return quat_t(m_data[0], -m_data[1], -m_data[2], -m_data[3]);
}

const float& quat_t::real(void) const {
	return m_data[0];
}

const float& quat_t::imag(size_t n) const {
	assert("quat::imag - invalid parameter" && n < 3);
	return m_data[n + 1];
}

float& quat_t::real(void) {
	return m_data[0];
}

float& quat_t::imag(size_t n) {
	assert("quat::imag - invalid parameter" && n < 3);
	return m_data[n + 1];
}