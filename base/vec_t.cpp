#include<memory.h>
#include<cassert>
#include<cmath>
#include"vec_t.h"
#include"quat_t.h"

vec3_t::vec3_t(void) {
	memset(m_data, 0, 3 * sizeof(float));
}

vec3_t::vec3_t(float x, float y, float z) {
	m_data[0] = x;
	m_data[1] = y;
	m_data[2] = z;
}

vec3_t::vec3_t(const vec3_t& other) {
	*this = other;
}

vec3_t::~vec3_t(void) {
}

vec3_t vec3_t::operator/(float other) const {
	float ooS = 1.0f / other;
	return vec3_t(m_data[0]*ooS, m_data[1]*ooS, m_data[2] * ooS);
}

vec3_t vec3_t::operator-(void) const {
	return vec3_t(-m_data[0], -m_data[1], -m_data[2]);
}

float vec3_t::operator*(const vec3_t& other) const {
	return m_data[0] * other.m_data[0] + m_data[1] * other.m_data[1] + m_data[2] * other.m_data[2];
}

const vec3_t& vec3_t::operator=(const vec3_t& other) {
	memcpy(m_data, other.m_data, 3 * sizeof(float));
	return *this;
}

const float& vec3_t::operator()(size_t i) const {
	assert("vec3_t() - invalid parameter" && i < 3);
	return m_data[i];
}

float& vec3_t::operator()(size_t i) {
	assert("vec3_t() - invalid parameter" && i < 3);
	return m_data[i];
}

float vec3_t::dot(const vec3_t& other, size_t d) const {
	assert("vec_t::dot - invalid parameter" && d < 3);
	float r = 0.0;
	for (size_t n = 0; n < d; n++) r += m_data[n] * other.m_data[n];
	return r;
}
vec3_t& vec3_t::operator+=(const vec3_t& other) {
	for (int n = 0; n < 3; n++) m_data[n] += other.m_data[n];
	return *this;
}

float vec3_t::length2(size_t d) const {
	assert("vec3_t::length2 - invalid parameter" && d <= 3);
	float r = 0.0;
	for (size_t n = 0; n < d; n++) r += m_data[n] * m_data[n];
	return r;
}

float vec3_t::length(size_t d) const {
	assert("vec3_t::length - invalid parameter" && d <= 3);
	return sqrt(length2(d));
}

void vec3_t::normalize(size_t d) {
	assert("vec3_t::normalize - invalid parameter" && d <= 3);
	float nrm = length2(d);
	if (nrm <= 0.0f) return;
	nrm = 1.0f / sqrt(nrm);
	for (size_t n = 0; n < d; n++) m_data[n] *= nrm;
}

vec3_t vec3_t::normalized(size_t d) const {
	vec3_t result(*this);
	result.normalize(d);
	return result;
}

vec3_t vec3_t::operator-(const vec3_t& other) const {
	return vec3_t(m_data[0] - other.m_data[0], m_data[1] - other.m_data[1], m_data[2] - other.m_data[2]);
}

vec3_t vec3_t::operator+(const vec3_t& other) const {
	return vec3_t(m_data[0] + other.m_data[0], m_data[1] + other.m_data[1], m_data[2] + other.m_data[2]);
}

vec3_t vec3_t::operator^(const vec3_t& other) const {
	return vec3_t(
		m_data[1] * other.m_data[2] - m_data[2] * other.m_data[1],
		m_data[2] * other.m_data[0] - m_data[0] * other.m_data[2],
		m_data[0] * other.m_data[1] - m_data[1] * other.m_data[0]);
}

vec3_t::operator const float* (void) const {
	return m_data;
}

vec3_t::operator float* (void) {
	return m_data;
}

vec3_t& vec3_t::operator/=(const double& other) {
	assert("vec3_t::operator/=() - division by zero" && other != 0.0);
	for (size_t n = 0; n < 3; n++) m_data[n] = float(m_data[n] / other);
	return *this;
}

vec3_t vec3_t::operator*(const float& other) const {
	return vec3_t(m_data[0] * other, m_data[1] * other, m_data[2] * other);
}


vec_t::vec_t(void) {
	memset(m_data, 0, 4 * sizeof(float));
}

vec_t::vec_t(float x, float y, float z, float w) {
	m_data[0] = x;
	m_data[1] = y;
	m_data[2] = z;
	m_data[3] = w;
}

vec_t::vec_t(const vec_t& other) {
	*this = other;
}

vec_t::~vec_t(void) {
}

const vec_t& vec_t::operator=(const vec_t& other) {
	memcpy(m_data, other.m_data, 4 * sizeof(float));
	return *this;
}

const float& vec_t::operator()(size_t i) const {
	assert("vec_t() - invalid parameter" && i < 4);
	return m_data[i];
}

float& vec_t::operator()(size_t i) {
	assert("vec_t() - invalid parameter" && i < 4);
	return m_data[i];
}

float vec_t::dot(const vec_t& other, size_t d) const {
	assert("vec_t::dot - invalid parameter" && d < 4);
	float r = 0.0;
	for (size_t n = 0; n < d; n++) r += m_data[n] * other.m_data[n];
	return r;
}

float vec_t::length2(size_t d) const {
	assert("vec_t::length2 - invalid parameter" && d <= 4);
	float r = 0.0;
	for (size_t n = 0; n < d; n++) r += m_data[n] * m_data[n];
	return r;
}

float vec_t::length(size_t d) const {
	assert("vec_t::length - invalid parameter" && d <= 4);
	return sqrt(length2(d));
}

void vec_t::normalize(size_t d) {
	assert("vec_t::normalize - invalid parameter" && d <= 4);
	float nrm = length2(d);
	if (nrm <= 0.0f) return;
	nrm = 1.0f / sqrt(nrm);
	for (size_t n = 0; n < d; n++) m_data[n] *= nrm;
}

vec_t vec_t::operator-(const vec_t& other) const {
	return vec_t(m_data[0] - other.m_data[0], m_data[1] - other.m_data[1], m_data[2] - other.m_data[2], m_data[3] - other.m_data[3]);
}

vec_t vec_t::operator+(const vec_t& other) const {
	return vec_t(m_data[0] + other.m_data[0], m_data[1] + other.m_data[1], m_data[2] + other.m_data[2], m_data[3] + other.m_data[3]);
}

vec_t vec_t::operator^(const vec_t& other) const {
	return vec_t(
		m_data[1] * other.m_data[2] - m_data[2] * other.m_data[1],
		m_data[2] * other.m_data[0] - m_data[0] * other.m_data[2],
		m_data[0] * other.m_data[1] - m_data[1] * other.m_data[0],
		0.0f);
}

vec_t::operator const float* (void) const {
	return m_data;
}

vec_t::operator float* (void) {
	return m_data;
}

quat_t vec_t::get_versor(void) const {
	return quat_t(0.0f, m_data[0], m_data[1], m_data[2]);
}

vec_t& vec_t::operator/=(const double& other) {
	assert("vec_t::operator/=() - division by zero" && other != 0.0);
	for (size_t n = 0; n < 4; n++) m_data[n] = float(m_data[n] / other);
	return *this;
}

vec_t vec_t::operator*(const float& other) const {
	return vec_t(m_data[0] * other, m_data[1] * other, m_data[2] * other, m_data[3] * other);
}

uivec_t::uivec_t(void) {
	m_data[0] = m_data[1] = m_data[2] = m_data[3] = 0;
}

uivec_t::uivec_t(uint32_t x, uint32_t y, uint32_t z, uint32_t w) {
	m_data[0] = x;
	m_data[1] = y;
	m_data[2] = z;
	m_data[3] = w;
}

uivec_t::uivec_t(const uivec_t& other) {
	*this = other;
}

uivec_t::~uivec_t(void) {
}

const uivec_t& uivec_t::operator=(const uivec_t& other) {
	for (int n = 0; n < 4; n++) m_data[n] = other.m_data[n];
	return *this;
}

const uint32_t& uivec_t::operator()(size_t i) const {
	assert("uivec_t() - invalid argument" && i < 4);
	return m_data[i];
}

uint32_t& uivec_t::operator()(size_t i) {
	assert("uivec_t() - invalid argument" && i < 4);
	return m_data[i];
}

uivec_t uivec_t::operator*(const uint32_t& other) const {
	return uivec_t(m_data[0] * other, m_data[1] * other, m_data[2] * other, m_data[3] * other);
}

uivec_t uivec_t::operator-(const uivec_t& other) const {
	return uivec_t(m_data[0] - other.m_data[0], m_data[1] - other.m_data[1], m_data[2] - other.m_data[2], m_data[3] - other.m_data[3]);
}

uivec_t uivec_t::operator+(const uivec_t& other) const {
	return uivec_t(m_data[0] + other.m_data[0], m_data[1] + other.m_data[1], m_data[2] + other.m_data[2], m_data[3] + other.m_data[3]);
}

uivec_t::operator const uint32_t* (void) const {
	return m_data;
}

uivec_t::operator uint32_t* (void) {
	return m_data;
}
